সম্প্রতি প্রকাশ করা গেইমটির সর্বশেষ ট্রেইলার দেখে বোঝা যায়, এই সংস্করণেও ধারা বজায় রয়েছে- বলা হয়েছে ব্যবসা-বাণিজ্যবিষয়ক মার্কিন সাইট বিজনেস ইনসাইডার-এর প্রতিবেদনে। 
গেইম নির্মাতা প্রতিষ্ঠান রকস্টার-এর গেইমটি আর্থার মরগান নামের এক লোক ও ফন ডার লিন্ডে নামের একটি চক্রের উপর ভিত্তি করে বানানো হয়েছে। এই চক্র “বেঁচে থাকার জন্য আমেরিকার রুক্ষ ও বিশাল এলাকায় ডাকাতি, চুরি ও লড়াই করে।”
রেড রেড রিডেম্পশন-এর প্রথম সংস্করণের ভক্তরা এই চক্রকে চিনতে পারার কথা। প্রথম গেইমটিতে এই দলের নেতা ডাচ আর তার সহকর্মীরা গেইমের মূল চরিত্র ছিল। সেই সঙ্গে অন্যান্য আরও চরিত্র তো থাকছেই।
২০১৮ সালের বসন্তে এই গেইমটি এক্সবক্স ওয়ান আর প্লেস্টেশন ৪ কনসোলের জন্য আনার কথা রয়েছে।
আরও খবর-
২০১৭-তেই রেড ডেড রিডেম্পশন ২
