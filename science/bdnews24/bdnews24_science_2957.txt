শুক্রবার টেসলা প্রধান ইলন মাস্ক এক টুইট বার্তায় বিষয়টি নিশ্চিত করেন। আগের বছর অক্টোবরে সৌর শক্তির এই প্যানেল উন্মোচন করে বৈদ্যুতিক গাড়ি নির্মাতা মার্কিন প্রতিষ্ঠানটি।
সোলারসিটি’র সঙ্গে চুক্তিতে গাড়ির ছাদে সোলার প্যানেল ব্যবহার করতে যাচ্ছে টেসলা, জানিয়েছে রয়টার্স।
নতুন এই টাইলস প্রচলিত সোলার প্যানেলকে বদলে দেবে বলে দাবী করা হচ্ছে। সোলার টাইলস ছাড়াও বাড়ির জন্য ‘পাওয়ারঅল’ নামে নতুন ব্যাটারিও উন্মোচন করেছে টেসলা। এই ব্যাটারি প্রচলিত ব্যাটারির থেকে আরও বেশি দীর্ঘস্থায়ী হবে বলেও জানানো হয়।
এই পণ্যগুলোর মাধ্যমে ‘জীবাশ্ম জ্বালানিবিহীন জীবনযাপনের’ লক্ষ্যে আরেক ধাপ এগিয়ে গেলেন মাস্ক।
পণ্যগুলো উন্মোচনকালে মাস্ক বলেন, “এটি অনেকটা সংহত ভবিষ্যত। একটি বৈদ্যুতিক গাড়ি, একটি পাওয়ারঅল এবং সোলার ছাদ। এখানে মূল বিষয়টি হচ্ছে এটি সুন্দর, সাধ্যের মধ্যে এবং সংহত হতে হবে।”
