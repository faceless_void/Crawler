ব্রিটিশ দৈনিক ইনডিপেনডেন্ট জানিয়েছে, ইংল‌্যান্ডের ন্যাশনাল হেলথ সার্ভিসের (এনএইচএস) পৃষ্ঠপোষকতায় নতুন বছরে ম্যানচেস্টার রয়্যাল আই হসপিটাল ও লন্ডনের মুরফিল্ডস আই হসপিটালে পাঁচজন করে মোট দশজনকে এই চিকিৎসা দেওয়া হবে।
অস্ত্রোপচারের মাধ্যমে এই দশজনের রেটিনায় বসানো হবে একটি বৈদ্যুতিক চিপ; তাদের পরতে দেওয়া হবে ক্যামেরা বসানো একটি চশমা। চশমায় লাগানো ক্যামেরা সামনের দৃশ‌্য থেকে আলো ধারণ করে ব্লুটুথের মাধ‌্যমে পাঠাবে রেটিনায় বসানো চিপে। এরপর সেই চিপ বা ইমপ্ল‌্যান্ট মস্তিষ্কে সংকেত পাঠাবে, অন্ধ ব্যক্তি পাবেন দেখার অনুভূতি।
বায়োনিক আই ইমপ্ল্যান্টের পর তা কতোটা সহায়ক হচ্ছে তা বোঝার জন‌্য তাদের এক বছর পর্যবেক্ষণ করা হবে।  
বায়োনিক চোখ প্রতিস্থাপন নিয়ে বেশ কিছুদিন ধরে গবেষণা চললেও যুক্তরাজ‌্যের বিজ্ঞানীরা সম্প্রতি ২০ বছর ধরে অন্ধ এক ব্যক্তিকে দৃষ্টিশক্তি ফিরিয়ে দেওয়ার চেষ্টায় সাফল‌্য পান।
এর আগে ‘আরগাস টু’ নামের একটি বায়োনিক আই ইমপ্ল্যান্টের পরীক্ষায় যুক্ত অধ্যাপক পাওলা স্ট‌্যাঞ্জা বলেন, তাদের গবেষণা পথ ধরে এনএইচএস যে সহায়তার হাত বাড়িয়ে দিয়েছে, এতে তিনি খুশি।  
যুক্তরাজ‌্যে একেবারে শুরুর দিকে যারা বায়োনিক আই পেয়েছেন, তাদের মধ‌্যে একজন কিথ হেইম্যান। ম্যানচেস্টার রয়্যাল আই হসপিটালে ২০০৯ সালে পরীক্ষামূলকভাবে তার প্রতিস্থাপন হয়।
৬৮ বছর বয়সী হেইম্যানের যখন রেটিনাইটিস পিগমেনটোসা ধরা পড়ে তখন তার বয়স বিশের কোঠায়। দৃষ্টশক্তি হারানোর কারণে ১৯৮১ সালে তাকে কসাইয়ের কাজ ছেড়ে অবসরে যেতে হয়।   
