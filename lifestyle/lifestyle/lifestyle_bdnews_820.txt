এক গবেষণায় দেখা গেছে সালাদে সয়াবিন তেল মেশালে পুষ্টিগুণ বাড়ে।
গবেষণার ফলাফলে বলা হয়, সালাদে সয়াবিন তেল যোগ করে এতে চর্বির মাত্রা বাড়ালে তা শরীরে সাতটি ভিন্ন ‘মাইক্রোনিউট্রিয়েন্ট’য়ের শোষণে সাহায্য করে। এই ‘মাইক্রোনিউট্রিয়েন্ট’গুলো ক্যান্সারের ঝুঁকি কমানো, দৃষ্টিশক্তি বাড়ানো ইত্যাদি স্বাস্থ্যগত উপকার করে।
“এই পুষ্টি উপাদানগুলোতে চার ধরনের ‘ক্যারোটিনয়েড থাকে- ‘আলফা’ ও ‘বেটা ক্যারোটিনয়েড’, ‘লুটেইন’ এবং ‘লাইকোপেন’। আরও থাকে দুই ধরনের ভিটামিন-ই এবং ভিটামিন-কে।” গবেষণার ভিত্তিতে এই তথ্য দিয়েছে যুক্তরাষ্ট্রের আইওয়া স্টেট ইউনিভার্সিটির গবেষকরা।
সালাদে তেল শরীরে ভিটামিন-এ শোষণের ক্ষমতা বাড়ায়। এই ক্ষমতা তৈরি হয় অন্ত্রে, ‘আলফা’ ও ‘বেটা ক্যারোটিনয়েড’ থেকে।
আইওয়া স্টেট ইউনিভার্সিটির সহকারি অধ্যাপক ওয়েনডি হোয়াইট বলেন, “সালাদে সাধারণত যতটুকু ড্রেসিং ব্যবহার করা হয় তার দ্বিগুণ পরিমাণ তেল ব্যবহার করলে শরীরে পুষ্টি শোষিত হবে দ্বিগুন।”
গবেষণায় আরও দেখা যায়, সবজিতে যত বেশি তেল দেওয়া হয়, শরীরে ওই সবজির পুষ্টিগুন শোষিত হয় তত বেশি।
সালাদে তেল ব্যবহার না করলে শরীরে পুষ্টি উপাদান শোষিত না হওয়ার সম্ভাবনা রয়েছে।
গবেষণার ফলাফল থেকে পরামর্শ দেওয়া হয় যে, যারা ওজন কমাতে চাচ্ছেন তারা নিশ্চিন্তে সালাদে ড্রেসিং হিসেবে সয়াবিন তেল মেশাতে পারেন।
 ‘আমেরিকান জার্নাল অফ ক্লিনিকাল নিউট্রিশন’য়ে প্রকাশিত এই গবেষণার জন্য কলেজ পড়ুয়া নারীদের বিভিন্ন মাত্রায় সয়াবিন তেল মেশানো সালাদ খাওয়ানো হয়।
ফলাফলে দেখা যায়, সর্বোচ্চ পরিমাণ পুষ্টি উপাদান শোষিত হয়েছে ৪২ গ্রাম বা দুই টেবিল-চামচ তেল মেশানো সালাদ খেয়ে। পরীক্ষায় এক বাটি সালাদে সর্বোচ্চ এই পরিমাণ তেলই মেশানো হয়েছিল।
তবে কিছু অংশগ্রহণকারীর ক্ষেত্রে ফলাফল ভিন্ন ছিল।
আরও পড়ুন
