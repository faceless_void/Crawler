এই চুক্তির মাধ্যমে প্রতিষ্ঠানে উবারকে দেওয়া হবে একটি অংশের মালিকানা আর শেষ হবে দুই প্রতিষ্ঠানের মধ্যে চলতে থাকা প্রতিযোগিতা।
এই চুক্তি আনুষ্ঠানিকভাবে প্রকাশের আগে নাম প্রকাশ করতে অনিচ্ছুক এমন এক সূত্রের বরাতে রয়টার্স জানায়, এই চুক্তির ‘ওজন’ সাড়ে তিন হাজার কোটি ডলারের বেশি। এ ক্ষেত্রে দিদি-এর মূল্য দুই হাজার আটশ' কোটি ডলার আর উবার চায়নার মূল্য সাতশ' কোটি দেখানো হয়েছে। দিদি তাদের অফিসিয়াল মাইক্রোব্লগে এই চুক্তির খবর নিশ্চিত করলেও, আর্থিক কোনো তথ্য উপস্থাপন করেনি।
এই চুক্তির ফলে চীনা প্রতিষ্ঠানটির ৫.৮৯ শতাংশের মালিক হবে মার্কিন প্রতিষ্ঠানটি। তবে, এক্ষেত্রে সুদের হারে কিছুটা অসামঞ্জস্যতা রয়েছে। উবারের জন্য এই হার হবে ১৭.৭ শতাংশ আর উবার চায়না-এর শেয়ারধারীদের জন্য এই হার ২.৩ শতাংশ।
এক বিবৃতিতে দিদি'র পক্ষ থেকে বলা হয়, "উবারের সঙ্গে সহযোগিতা পুরো মোবাইল ট্রাভেল খাতকে একটি ভালো অবস্থান ও উচ্চমাত্রার একটি উন্নয়নের সময় এনে দেবে।" 
কাহিনীর মোড় নেওয়া এখানেই শেষ নয়, উবার প্রধান ট্রাভিস কালানিক যোগ দেবেন দিদি'র পরিচালনা পর্ষদে আর একই সময়ে দিদি ছুসিং প্রধান চেং ওই উবার-এর পরিচালনা পর্ষদে যোগ দেবেন।
এক বিবৃতে কালানিক বলেন, "চীনের শহরগুলোতে, চালকদের আর যাত্রীদের টেকসই সেবা শুধু লাভের ভিত্তিতেই সম্ভব। এই একীভূতকরণ একটি বড় অভিযানে আমাদের দল আর দিদি'র পথ খুলে দিল।"
উবার চীনে ৬০টিরও বেশি শহরে সেবা দিয়ে যাচ্ছিল, আর প্রতি সপ্তাহে তারা চার কোটিরও বেশি যাত্রা সম্পন্ন করে বলে জানান উবার প্রধান।
চীন উবারের জন্য একটি 'চ্যালেঞ্জিং' বাজার হয়ে দাড়িয়েছিল। এই আগুনে ঘি ঢেলে দেয় এক সময়কার প্রতিদ্বন্দ্বী দিদি'র তহবিলে মার্কিন প্রযুক্তি জায়ান্ট অ্যাপলের শতকোটি ডলার বিনিয়োগ। তবে, মার্কিন যুক্তরাষ্ট্র, কানাডাসহ বিভিন্ন দেশের শতাধিক শহরে উবার লাভজনক অবস্থায় রয়েছে।
এই চুক্তির অংশ হিসেবে দিদি উবারে শতকোটি ডলার বিনিয়োগও করবে। 'উবার' বলতে উবারের যে ব্যবসা চীনের বাইরে বিশ্বব্যাপী পরিচালিত হয় তাকে বোঝানো হয়েছে।
