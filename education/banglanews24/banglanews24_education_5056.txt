রাবি: রাজশাহী বিশ্ববিদ্যালয়ের (রাবি) গণযোগাযোগ ও সাংবাদিকতা বিভাগে দুই বছর মেয়াদি সান্ধ্য মাস্টার্স কোর্সের নবম ব্যাচের ভর্তির আবেদন প্রক্রিয়া শুরু হচ্ছে বুধবার (২২ নভেম্বর)। আবেদন করা যাবে আগামী ২৬ ডিসেম্বর পর্যন্ত।
মঙ্গলবার (২১ নভেম্বর) বিকেলে কোর্সের নবম ব্যাচের কো-অর্ডিনেটর ও বিভাগের সহযোগী অধ্যাপক ড. মাহাবুবুর রহমান স্বাক্ষরিত বিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
বিজ্ঞপ্তিতে বলা হয়, দেশ-বিদেশের যেকোনো বিশ্ববিদ্যালয় থেকে ন্যূনতম সম্মান অথবা পাস ডিগ্রিপ্রাপ্তরা এ কোর্সে ভর্তির জন্য আবেদন করতে পারবেন। বিভাগের অফিস থেকে সকাল ৯টা থেকে বিকেল ৫টা পর্যন্ত ৫০০ টাকার বিনিময়ে আবেদনপত্র সংগ্রহ এবং জমা দেওয়া যাবে।
বিজ্ঞপ্তিতে আরও বলা হয়, ব্যাচটির ক্লাস শুরু হবে আগামী ২৭ জানুয়ারি। দুই বছর মেয়াদি এ কোর্সে আসন সংখ্যা ৫০টি।
ভর্তি সংক্রান্ত বিস্তারিত তথ্য বিভাগে অথবা মোবাইল ফোনে (০৭২১ ৭৬১১১৩, ০১৭১৬ ৫৫২৩৯৬, ০১৭১৬ ৭৮৯৭৮৩) যোগাযোগ করে জানা যাবে।
