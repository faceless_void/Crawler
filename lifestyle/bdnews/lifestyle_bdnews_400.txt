বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজের সমাজবিজ্ঞানের সহকারী অধ্যাপক আতিয়া পারভিন বলেন, “মা হওয়া বিষয়টা আনন্দের হলেও অনেক মা তার সন্তানের সুস্থতা ও পরিচর্যা নিয়ে প্রায়ই দুশ্চিন্তাগ্রস্ত থাকেন যা মোটেও উচিত নয়। সন্তানের ভালো মন্দ চিন্তা করা আর তাকে নিয়ে দুশ্চিন্তা করা মোটেও এক না।”
সন্তান জন্মের পর মায়েদের মনে নানা রকমের ভয় কাজ করে। অন্যের কোলে দিতে, অন্যের হাতে খাবার খাওয়াতে, শিশুকে কেউ বাইরে বেড়াতে নিয়ে যেতে চাইলে ইত্যাদি নানান ক্ষেত্রেই মায়ের মনে ভয় বা শঙ্কা কাজ করাটা অস্বাভাবিক নয়। শিশুর প্রতি সচেতন থেকে দুশ্চিন্তা কাটিয়ে উঠতে হবে। 
আশা আহমেদ, নতুন মা হয়েছেন। তিনি বলেন, “সন্তানকে অন্য কেউ কোলে নিলে ঠিক মতো নিচ্ছে কিনা বা শিশু কোনোভাবে ব্যথা পাচ্ছে কিনা এসব নিয়ে একটু আতঙ্কিত থাকি। মা-বাবা ছাড়া অন্য কারও কোলে দিতে শান্তি লাগে না। আর বাবুকে নিয়ে কেউ একা কোথাও যেতে চাইলে কোনোভাবেই রাজি হইনা।”
“তাছাড়া সবসময় বাসার মূল দরজা আটকিয়ে রাখি। ওকে কেউ কিছু খাওয়াতে চাইলেও একটু সাবধান থাকি। আমার কাছে সন্তানের নিরাপত্তা সবার আগে।”
তিনি আরও বলেন, “মনের ভেতর সবসময়ই ভয় ভয় লাগে। অকারণ দুশ্চিন্তা হয় আমি নিজেই বুঝি এত দুশ্চিন্তা করার কিছু নেই তারপরও কেনো জানি নিজের মনকে বোঝাতে পারিনা।”
শিশুর সুরক্ষা নিয়ে চিন্তিত থাকা স্বাভাবিক। তবে তা যেন কখনই বাড়াবাড়ির দিকে না যায় সেদিকে খেয়াল রাখার পরামর্শ দেন আতিয়া পারভিন।
* অযথা সন্তানকে নিয়ে সারাক্ষণ চিন্তা না করা ঠিক না। এতে দুশ্চিন্তাই বাড়ে।
* শিশুকে আত্মীয়স্বজন সবার কোলেই যেতে দিতে হবে। তবে অবশ্যই সে ঠিক মতো কোলে নিচ্ছে কিনা সেদিকে খেয়াল রাখতে হবে।
* অপরিচিত কারও কাছে শিশুকে না দেওয়া।
* শিশুর যত্নের পাশাপাশি মাকে নিজের প্রতিও যত্নশীল হতে হবে। মনে রাখতে হবে মা সুস্থ না থাকলে শিশুও সুস্থ থাকবে না, তাই নিজের প্রতি যত্নশীল হতে হবে।
* নয় মাস গর্ভধারণের ফলে সন্তানের প্রতি মায়ের রক্ষণশীল মনোভাব কাজ করতে পারে। তবে কোনোভাবেই এই মনোভাবকে প্রশ্রয় দেওয়া ঠিক নয়। বুঝতে হবে শিশুটি যেমন আপনার সন্তান তেমনি পরিবারেরও আপন। শিশুকে নিয়ে তার পরিবারের অন্যান্য সদস্যদেরও ভালোবাসার অনুভূতি কাজ করে।
* শিশুর বেড়ে ওঠার প্রতিটি মুহূর্ত ফ্রেমে আটকে রাখা সম্ভব নয়। তাই তার বড় হওয়া নিজের চোখেই উপভোগ করুন।
* শিশুর যত্ন নেওয়ার ক্ষেত্রে অভিজ্ঞদের পরামর্শ নিতে পারেন। তবে কোনো পদক্ষেপ নেওয়ার আগে অবশ্যই চিকিৎসকের পরামর্শ নিতে হবে। 
