মাছে-ভাতে বাঙালি বলে কথা। তাই মাছ না হলে কি চলে। সপ্তাহে অনন্ত তিন দিনই মাছ খেয়ে থাকেন অনেকে। ঘরেই হয়তো অনেক গৃহিণী পরিবারের প্রিয়জনদের জন্য মাছ দিয়ে তৈরি করুন মাছ ভাজি, কোপ্তা ও কারি। কিন্তু মাছের বড়া হয়তো অনেকেই তৈরি করেন না, আবার খেতেও অভ্যস্ত নয়।
আপনি জানেন কি মাছ দিয়ে তৈরি করা যায় সুস্বাদু মাছের বড়া। গরম ভাত, পোলাও কিংবা খিচুড়ির সঙ্গে খেতে বেশ সুস্বাদু।
আসুন জেনে নিই কীভাবে তৈরি করবেন মাছের বড়া-
উপকরণ কাঁচকি মাছ ১ কাপ, মসুর ডাল বাটা আধা কাপ, পেঁয়াজ কুচি আধা কাপ, কাঁচামরিচ কুচি ৫টি, মৌরি একচিমটি, রসুন বাটা আধা চা চামচ, আদা বাটা আধা চা চামচ, লবণ স্বাদমতো, ধনেপাতা কুচি ২ টেবিল চামচ, হলুদ আধা চা চামচ ও তেল ভাজার জন্য পরিমাণমতো।
প্রস্তুত প্রণালি মাছ ধুয়ে পানি ঝরিয়ে নিন। তেল ছাড়া সব উপকরণ মাছের সঙ্গে ভালো করে মিশিয়ে নিন। এবার গোল আকারে গড়ে ডুবো তেলে ভেজে গরম গরম পরিবেশন করুন।
