জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের (জাবি) সঙ্গে দ্য ইনস্টিটিউট অব চার্টার্ড একাউন্টস অব বাংলাদেশ (আইসিএবি)-এর সমঝোতা চুক্তি স্বাক্ষর হয়েছে।
রোববার (২৬ নভেম্বর) বিশ্ববিদ্যালয়ের কাউন্সিল কক্ষে এ সমঝোতা চুক্তি হয়।
এতে বিশ্ববিদ্যালয়ের পক্ষে কোষাধ্যক্ষ অধ্যাপক শেখ মো. মনজুরুল হক ও আইসিএবি-এর পক্ষে প্রতিষ্ঠানটির সভাপতি আবিদ হোসাইন খান সই করেন।
এই চুক্তির আওতায় জাবির একাউন্টিং অ্যান্ড ইনফরমেশন সিস্টেমস বিভাগের শিক্ষার্থীরা আইসিএবি-এর কোর্স করার ক্ষেত্রে বিশেষ সুবিধা পাবেন।
এ সময় বিশ্ববিদ্যালয় উপাচার্য অধ্যাপক ফারজানা ইসলাম, বিজনেস স্টাডিজ অনুষদের ডিন নীলাঞ্জন কুমার সাহা, একাউন্টিং অ্যান্ড ইনফরমেশন সিস্টেমস বিভাগের শিক্ষক-শিক্ষার্থীরা উপস্থিত ছিলেন।
