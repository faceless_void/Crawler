package comments;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ProthomAloComments {
    private HashSet<String> links;
    private HashSet<String> linksForParsing;
    private List<List<String>> articles;
    private static final int MAX_DEPTH = 100;
    int count = 0;
    int linkCountTest = 0;
    String filename = "/home/void/IdeaProjects/Crawler/comments/prothomalo/prothomalo_comments_";

    public ProthomAloComments() {
        links = new HashSet<>();
        articles = new ArrayList<>();
        linksForParsing = new HashSet<>();

    }

    //Find all URLs that start with "http://www.mkyong.com/page/" and add them to the HashSet
    public void getPageLinks(String URL,int depth) {
        if (!links.contains(URL) && (depth < MAX_DEPTH ) ) {
            linkCountTest++;
            try {
                Document document = Jsoup.connect(URL)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .timeout(10*1000).get();
               // System.out.println("this is doucument "+document);
                //System.out.println(document.text());
                Elements otherLinks = document.select("a[href^=\"/technology\"]");
                System.out.println("calling with this "+ URL);
                links.add(URL);

                for (Element page : otherLinks) {
                    {

                        linksForParsing.add(URLDecoder.decode( page.attr("abs:href"), StandardCharsets.UTF_8.name() )) ;
                        //System.out.println(URL);
                        if (links.contains(URLDecoder.decode( page.attr("abs:href"), StandardCharsets.UTF_8.name() )))
                            continue;
                        getPageLinks(page.attr("abs:href"),++depth);
                    }

                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
            }
            finally {
                //System.out.println("exception ");
//                getArticles();
//                writeToFile();
//                System.out.println("getArticle called");

            }
        }
    }

    //Connect to each link saved in the article and find all the articles in the page
    public void getArticles() throws IOException {
        AtomicInteger totalComment = new AtomicInteger();
        System.out.println("links count "+linksForParsing.size());
        FileWriter fileWriter = new FileWriter(filename+Integer.toString(count++)+".txt");


        linksForParsing.forEach(x -> {
            int contentId = 0;
            Pattern pattern = Pattern.compile("/(\\d{5,20})/");
            Matcher matcher = pattern.matcher(x);
            if(matcher.find()){
                contentId = Integer.parseInt(matcher.group(1));
            }
            System.out.println("content id "+contentId);



                String doc = "";

                String commentAPIURL = "http://www.prothomalo.com/api/comments/get_comments_json/?content_id="+contentId;
                System.out.println(commentAPIURL);

                if(contentId != 0)
                    try {

                    String jsonCheck = Jsoup.connect(commentAPIURL).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").ignoreContentType(true).header("Accept", "application/json").timeout(10*1000).referrer("http://www.google.com").execute().body();
                    if (jsonCheck.length() > 10){
                        JSONObject jsonObject = new JSONObject(jsonCheck);
                        Iterator<String> iterator = jsonObject.keys();
                        while (iterator.hasNext()){
                            JSONObject singleComment = jsonObject.getJSONObject(iterator.next());
                            //System.out.println(singleComment);
                            String comment = (String) singleComment.get("comment");
                            String englishCheck = "will be satellite connection with it";
                            Pattern alphaNumericPattern = Pattern.compile("[a-zA-Z]+");
                            Matcher matcher1 = alphaNumericPattern.matcher(comment);
                            //System.out.println(englishCheck.matches("[A-Za-z0-9]+"));
                            //System.out.println(englishCheck.contains("[A-Za-z0-9]+"));

                            if(comment.length() < 600 && !matcher1.find()){
                                comment = comment.replaceAll("\\p{IsPunctuation}"," ").trim();
                                comment = comment.replaceAll("\\p{Punct}"," ").trim();
                                fileWriter.write(comment);
                                fileWriter.write("\n");
                                totalComment.getAndIncrement();
                            }
                        }
                        }
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
                finally {
//                    writeToFile();
                }

        });
        System.out.println("total comments "+totalComment);
        fileWriter.close();
    }

    private void writeNow(String text) {
        FileWriter writer ;
        {

            {
                try {
                    writer= new FileWriter(filename+Integer.toString(count++)+".txt");

                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer.write(text);
                    writer.close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
//            writer[0].close();
        }
    }

    public void writeToFile() {

        final FileWriter[] writer = new FileWriter[1];
        {

            articles.forEach(a -> {
                try {
                    writer[0] = new FileWriter(filename+Integer.toString(count++));
                    String temp = a.get(0) ;
                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer[0].write(temp);
                    writer[0].close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            });
//            writer[0].close();
        }
    }

    public static void main(String[] args) throws IOException {
        ProthomAloComments prothomAloComments = new ProthomAloComments();
        prothomAloComments.getPageLinks("http://www.prothomalo.com/",0);
        prothomAloComments.getArticles();
        //bwc.writeToFile();
        System.out.println("done.");
    }
}