জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: বিসিএস’র প্রস্তুতি নিতে জাহাঙ্গীরনগর বিশ্ববিদ্যালয় ক্যারিয়ার ক্লাবের উদ্যোগে বসবে ‘বিসিএস আড্ডা’।
শুক্রবার (১০ নভেম্বর) ক্যারিয়ার ক্লাবের সভাপতি সাইদুল কবির বাংলানিউজকে এ তথ্য নিশ্চিত করেন।
তিনি জানান, বর্তমান চাকরির বাজরে বিসিএসকে সবাই প্রথম চয়েজে রাখে। আর দেড় মাস পরে ৩৮তম বিসিএস’র প্রিলিমিনারি। কম সময়ে বিসিএস পরীক্ষায় কিভাবে ভালো করা যায়। তাছাড়া কিভাবে বিসিএস পরীক্ষার প্রস্তুতি নেবে তা অনেকেই জানে না। তাই বিসিএস পরীক্ষার্থী ও বিশ্ববিদ্যালয়ের সাবেকদের মধ্যে যারা বিসিএস ক্যাডার হয়েছেন তাদের নিয়ে একটি আড্ডার আয়োজন করতে যাচ্ছি।
শনিবার (১১ নভেম্বর) বিশ্ববিদ্যালয় জহির রায়হান মিলনায়তনে দুপুর আড়াইটায় এ আড্ডা বসবে। আড্ডা সবার জন্য উন্মুক্ত থাকবে। যে কেউ চাইলেই অংশ নিতে পারবে।
এখানে বাংলাদেশ কর্ম কমিশনের সাবেক সদস্য ও বিশ্ববিদ্যালয়ের সাবেক উপাচার্য অধ্যাপক শরীফ এনামুল কবির, খাদ্য মন্ত্রণালয়ের সচিব কায়কোবাদ হোসাইনসহ অন্যান্য বিসিএস ক্যাডাররা তাদের অভিজ্ঞতা শেয়ার করবেন বলেও জানান সাইদুল কবির।
