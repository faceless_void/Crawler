রাজধানীর প্রতিষ্ঠিত একটি কলেজে উচ্চ মাধ্যমিক শ্রেণিতে পড়াশোনা করতে বার্ষিক টিউশন ফি প্রকৃতপক্ষে প্রায় লাখ টাকারও বেশি।
ঢাকা: রাজধানীর প্রতিষ্ঠিত একটি কলেজে উচ্চ মাধ্যমিক শ্রেণিতে পড়াশোনা করতে বার্ষিক টিউশন ফি প্রকৃতপক্ষে প্রায় লাখ টাকারও বেশি। তবে এসএসসিতে কোনো শিক্ষার্থীর যদি জিপিএ-৫ থাকে তাহলে তার টিউশন ফি পড়বে বার্ষিক ৪৮ হাজার টাকা! আর বাকি অর্থ মিলবে বৃত্তিবাবদ।
আবার কোনো শিক্ষার্থী যদি এসএসসিতে জিপিএ ৪.০০ থেকে ৪.৯৯ পায় তাহলে তার বৃত্তির পরিমাণ দাঁড়াবে বার্ষিক ১ লাখ ২০ হাজার টাকা অর্থাৎ তার বার্ষিক টিউশন ফি পড়বে ৬০ হাজার টাকা!
একইভাবে কারও যদি জিপিএ ৩.০০ থেকে ৩.৯৯ হয় তাহলে তার টিউশন ফি দাঁড়াবে বছরে ৭২ হাজার টাকা অর্থাৎ সে বৃত্তিবাবদ পাবে ১ লাখ ৮ হাজার টাকা।
বৃত্তিপ্রাপ্ত প্রতিটি শিক্ষার্থীই তাদের টিউশন ফি ১২টি কিস্তিতে পরিশোধ করতে পারবে।
যারা এবার উচ্চ মাধ্যমিকে ভর্তি হতে যাচ্ছে তাদের জন্য বিশেষ এই বৃত্তি দিচ্ছে ঢাকার বারিধারা কুড়িল বিশ্বরোডস্থ ‘কুইন মেরী কলেজ’।
ব্যবসায়িক চিন্তা থেকে নয়, বরং একবিংশ শতাব্দীর চ্যালেঞ্জ মোকাবেলায় সক্ষম একটি শিক্ষিত জাতি গড়ার লক্ষ্যেই ২০১০ সালে কলেজটি প্রতিষ্ঠিত হয়েছে বলে প্রতিষ্ঠানটির দাবি। সেগা জামাল ফাউন্ডেশন প্রতিষ্ঠিত এই প্রতিষ্ঠানে উচ্চ মাধ্যমিকে ভর্তি ও বৃত্তির বিষয়ে আরো জানতে- ফোন: ০১৭৫০০০০৯৪৬-৭, ওয়েবসাইট: www.queenmarycollege.edu.bd
