যশোর: আওয়ামী লীগ শিক্ষা বান্ধব সরকার, বর্তমান সরকার শিক্ষকদের মান উন্নয়নসহ শিক্ষা ক্ষেত্রে বৈপ্লবিক পরিবর্তন ঘটিয়েছে। বছরের প্রথম দিন শিক্ষার্থীদের হাতে বই পৌঁছে দেওয়া ছাড়াও শিক্ষকদের বেতন তিনগুণ বাড়ানো হয়েছে।
শুক্রবার (২৬ মে) বিকেলে যশোর সার্কিট হাউসে বাঘারপাড়া উপজেলা প্রাথমিক শিক্ষক সমিতির কমিটি গঠন উপলক্ষে আয়োজিত সভায় প্রধান অতিথির বক্তব্যে যশোর-৪ আসনের সংসদ সদস্য রনজিত কুমার রায় এসব কথা বলেন।
তিনি বলেন, স্বাধীনতা পরবর্তী সময়ে যুদ্ধবিধ্বস্ত বাংলাদেশে প্রাথমিক শিক্ষাকে জাতীয়করণ করেছিলেনে জাতির জনক বঙ্গবন্ধু শেখ মুজিবুর রহমান। আর ঠিক তার ৪০ বছর পরে তার কন্যা প্রধানমন্ত্রী শেখ হাসিনা ২৬ হাজার ১৯৩টি বেসরকারি প্রাথমিক বিদ্যালয়কে জাতীয়করণ করে নজির স্থাপন করেছেন।
এছাড়াও প্রতিটি উপজেলায় একাধিক স্কুল-কলেজ জাতীয়করণ করা হচ্ছে উল্লেখ করে জানান, আগামী নির্বাচনে ক্ষমতায় এলে সব মাধ্যমিক বিদ্যালয় জাতীয়করণ করবে আওয়ামী লীগ।
বক্তব্য শেষে সবার সর্বসম্মতিক্রমে সুভাষ সমাদ্দারকে সভাপতি এবং আনিছুজ্জামানকে সাধারণ সম্পাদক করে উপজেলা প্রাথমিক শিক্ষক সমিতির কমিটি গঠন করা হয়। পরে নবনির্বাচিত নেতারা এমপি রনজিত রায়কে ফুলদিয়ে শুভেচ্ছা জানান।
এ সময় তিনি সমিতির উন্নয়ন অবিলম্বে ১০ লাখ টাকা অনুদান দেওয়ার ঘোষণা দেন। পাশাপাশি শিক্ষক সমাজকে আওয়ামী লীগের পাশে থেকে শেখ হাসিনার হাতকে শক্তিশালী করার আহ্বান জানান।
