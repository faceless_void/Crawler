রেসিপি দিয়েছেন অসিত কর্মকার সুজন।
উপকরণ: বড় টমেটো ৪,৫টি। দারুচিনি ১ টুকরা। রসুনের কোয়া ৪টি। তেজপাতা ১টি। গোলমরিচ ৫,৬টি। মিস্টি কুমড়া ১ টুকরা। মরিচগুঁড়া আধা চা-চামচ। চিনি ৩ টেবিল-চামচ। কর্নফ্লাওয়ার ১ চা-চামচ। ভিনিগার ১ টেবিল-চামচ অথবা একটু বেশি। লবণ অল্প পরিমাণ মতো (না দিলেও চলবে)।
পদ্ধতি: দারুচিনি, তেজপাতা, রসুন, গোলমরিচ একটি পরিষ্কার পাতলা কাপড় দিয়ে পুটলি করে রাখুন। এবার টমেটো ছোট ছোট করে কেটে ব্লেন্ডারে ব্লেন্ড করে ছেঁকে নিন।
মিস্টিকুমড়া, মরিচগুঁড়া আর চিনি ব্লেন্ডারে ব্লেন্ড করে নিন। মিহি পেস্ট হতে হবে।
একটি নন-স্টিক পাত্রে টমেটো পেস্ট আর পুটলিটা দিয়ে জ্বাল করুণ যতক্ষণ না টমেটোর কাঁচা গন্ধটা না যাচ্ছে। হয়ে গেলে চালনি দিয়ে চেলে নিন।
চেলে রাখা সসের সঙ্গে ভিনিগার, কর্নফ্লাওয়ার গুলিয়ে মেশান। আরেকটি ননস্টিক পাত্রে আবার জ্বাল দিন। ঘন হয়ে গেলে নামিয়ে ঠাণ্ডা করে সংরক্ষণ করুন।
