জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের (জাবি) দর্শন বিভাগের আয়োজনে দুই দিনব্যাপী ‘বাংলার দর্শন: স্বরূপ ও শেকড়ের খোঁজে’ শীর্ষক কর্মশালা শুরু হয়েছে।
বুধবার (২৫ অক্টোবর) সকালে বিভাগের সেমিনার কক্ষে এ কর্মশালার উদ্বোধন করেন বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ফারজানা ইসলাম।
উদ্বোধনকালে তিনি বলেন, দর্শন সব জ্ঞানের উৎস। আর সেই দর্শন খুঁজে বের করে আনা আমাদের একাডেমিক দায়িত্ব। এ কর্মশালা থেকে দর্শনের মূল উৎস বেরিয়ে আসবে বলে তিনি আশা প্রকাশ করেন। একই সঙ্গে এ ধরণের কর্মশালার আয়োজন করার জন্য কর্তৃপক্ষকে ধন্যবাদ জানান।
দর্শন বিভাগের সভাপতি অধ্যাপক মুহাস্মদ তারেক চৌধুরীর সভাপতিত্বে সেমিনারে স্বাগত বক্তব্য রাখেন কর্মশালার সমন্বয়কারী অধ্যাপক মোহাম্মদ কামরুল আহসান।
এ সময় আরও উপস্থিত ছিলেন- বিশ্ববিদ্যালয় উপ-উপাচার্য মো. অধ্যাপক আবুল হোসেন, কোষাধ্যক্ষ অধ্যাপক শেখ মনজুরুল হক, কলা ও মানবিকী অনুষদের ডিন অধ্যাপক মো. মোজাম্মেল হক, প্রাইম এশিয়া বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক হাননান চৌধুরী প্রমুখ।
