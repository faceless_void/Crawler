নতুন এপিআইয়ের মাধ্যমে প্রতিষ্ঠানগুলো পেশাদার সম্প্রচার এবং ভিডিও এডিটিং আগের চেয়ে সহজে করতে পারেবন বলে জানিয়েছে প্রযুক্তিবিষয়ক সাইট টেকক্রাঞ্চ।
বর্তমানে ‘পেরিস্কোপ প্রোডিউসার’-এর মাধ্যমে টুইটারে লাইভ ভিডিও প্রচার করা যায়। এবার নতুন এপিআইয়ের মাধ্যমে আরও বেশি এডিটিং ফিচার পাওয়া যাবে বলে জানানো হয়েছে।
আগের সপ্তাহেই নতুন এ এপিআইয়ের কথা জানানো হয়। এবার টেকক্রাঞ্চ নিশ্চিত হয়েছে যে ২২ মার্চ সকালে এটি চালু করা হবে। নতুন এপিআই-এ অংশীদারগুলো হলো টেলিস্ট্রিম, ওয়্যারকাস্ট এবং লাইভস্ট্রিম সুইচার। এ ব্যাপারে কোনো মন্তব্য করতে রাজী হয়নি টুইটার।
এক সূত্রের বরাত দিয়ে টুইটার জানায় নতুন এপিআই ফেইসবুক লাইভ এপিআইয়ের মতো কাজ করবে। আগের বছর এপ্রিলে এটি চালু করে ফেইসবুক।
টুইটারের নতুন এপিআয়ের মাধ্যমে বড় ভিডিও ক্যামেরা, কম্পিউটারের ভিডিও এডিটিং সফটওয়্যার, স্যাটেলাইট ভ্যান এবং অন্যান্য মাধ্যম থেকে সরাসরি ভিডিও সম্প্রচার করা যাবে বলে জানানো হয়।
নতুন এপিআই চালু হলে পেরিস্কোপ ফিচার রাখা হবে কিনা তা নিয়ে প্রশ্ন থেকেই যাচ্ছে। আগের বছর ডিসেম্বরেই পেরিস্কোপ-এ সরাসরি ৩৬০ ডিগ্রি ভিডিও প্রচারের সুবিধা যোগ করে টুইটার।
