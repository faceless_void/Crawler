এই খাবারে সাধারণত পান্ডালিফ নামে একটি উপাদান দেওয়া হয়। তাই এর স্বাদ অন্যরকম৷ পান্ডালিফের বদলে লেমন গ্রাস ব্যবহার করতে পারেন।
রেসিপি দিয়েছেন আমেনা আলি মেঘলা।
উপকরণ
বিরিয়ানির মুরগি রান্নার জন্য: মুরগির মাংস ১২ টুকরা চামড়াসহ অথবা ২টি মুরগি বড় বড় করে কাটা। পেঁয়াজবাটা ৪ টেবিল-চামচ। আদা-রসুনবাটা ৩ টেবিল-চামচ। টক দই আধা কাপ। টমেটো ১টি (বড়)। কারি পাউডার ১ টেবিল-চামচ। জায়ফল-বাটা ১ চা-চামচ। জিরাবাটা ১ চা-চামচ। জয়িত্রিবাটা আধা চা-চামচ। পেঁয়াজ বেরেস্তা দেড় কাপ। আলু বোখারা ৬,৭টি। গোলাপ জল ১ টেবিল-চামচ। স্বাদ মতো লবণ। তেল আধা কাপ। কাচাঁমরিচ ১ মুঠো।
বিরিয়ানির জন্য: বাসমতি চাল ১ কেজি। পেঁয়াজকুচি আধা কাপ। লেমন গ্রাস একটি লাঠি। আদাবাটা ১ টেবিল-চামচ। গরম মসলা, এলাচি, দারুচিনি ইচ্ছা মতো। টোমেটো সস ১ টেবিল-চামচ। নারিকেল দুধ আধা কাপ। পানি ৯ কাপ (পানি সঙ্গে নারিকেল দুধ মিশিয়ে পানি গরম করে রাখতে হবে)। কেওড়া জল ১ টেবিল-চামচ। কাজু বাদাম এবং কিশমিশ পরিমাণ মতো। কিছু পেঁয়াজ বেরেস্তা। ঘি ১/৪ কাপ।
পদ্ধতি: প্রথমে বাসমতি চাল ধুয়ে পানিতে ভিজিয়ে রাখুন৷ মুরগি রান্না হতে হতে চাল নরম হয়ে যাবে৷
এবার মুরগির টুকরাগুলো ধুয়ে, কিচেন টিস্যু দিয়ে ভালো করে মুছে নিন৷
১ চা-চামচ লবণ এবং ১ চা-চামচ হলুদগুঁড়া মুরগির সঙ্গে মাখিয়ে রাখুন৷ কড়াইতে তেল গরম করে পেঁয়াজ বেরেস্তা করে তুলে রাখুন। একই তেলে মুরগিগুলো হালকা করে ভেজে রাখুন৷
এখন এই তেলে পেঁয়াজ, আদা ও রসুন বাটা দিয়ে ভালো করে ভেজে টক দই, টমেটো কারি পাউডার, জয়িত্রি, জায়ফল, শাহজিরা-বাটা, একটু পানি এবং অল্প লবণ দিয়ে ভালো করে মসলা কষিয়ে নিন।
তারপর ভাজা মুরগিগুলো দিয়ে আবার কষান।


অন্য একটি হাঁড়িতে চাল রান্নার জন্য ঘি, বাদাম, কিশমিশ ভেজে তুলে রাখুন।
এবার ঘিয়ের সঙ্গে এলাচ, দারুচিনি, লেমন গ্রাস এবং পেঁয়াজ দিয়ে বাদামি করে ভেজে, আদাবাটা, টমেটো সস দিয়ে কষিয়ে নিন।
তারপর পানি ঝরানো চাল দিয়ে ৭ থেকে ৮ মিনিট ভাজতে থাকুন৷ চাল ভাজার সময় হাত ব্যথা হয়ে আসবে তাই একটু ধৈর্য্য নিয়ে ভাজতে হবে৷ ভাজার এক পর্যায় চালের রং পরিবর্তন হলে, নারিকেল দুধ মেশানো গরম পানি ও স্বাদ মতো লবণ দিয়ে নেড়ে দিন৷
তিন থেকে চার বার ফুটে উঠে যখন চাল আধা রান্না হবে তখন আলাদা একটি হাঁড়িতে কিছু চাল উঠিয়ে রাখুন।
এবার রান্না করা মুরগির মাংস সব চালের হাঁড়িতে ঢেলে এবং তুলে রাখা চাল উপরে দিয়ে দিন৷
এ্র উপর দিয়ে কেওড়ার জল, বাদাম, কিশমিশ এবং পেঁয়াজ-বেরেস্তা ছিটিয়ে ভালো ভাবে ঢেকে দমে রাখুন ২০ মিনিট।
