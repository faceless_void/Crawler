সম্প্রতি বাংলাদেশে সংঘটিত সাম্প্রদায়িক হামলা ও মায়ানমারে রোহিঙ্গা জনগোষ্ঠী নিয়ে মীর লোকমানের একক মূকাভিনয় ‘ওভারকাম’ প্রদর্শিত হয়েছে।
ঢাকা বিশ্ববিদ্যালয়: সম্প্রতি বাংলাদেশে সংঘটিত সাম্প্রদায়িক হামলা ও মায়ানমারে রোহিঙ্গা জনগোষ্ঠী নিয়ে মীর লোকমানের একক মূকাভিনয় ‘ওভারকাম’ প্রদর্শিত হয়েছে।
 
সোমবার (৫ ডিসেম্বর) সন্ধ্যায় ঢাকা বিশ্ববিদ্যালয়ের ছাত্র-শিক্ষক কেন্দ্র মিলনায়তনে এ প্রদর্শনীর উদ্বোধন করেন ব্রিটিশ কাউন্সিলের বাংলাদেশ শাখার ভারপ্রাপ্ত পরিচালক জিম স্কার্থ।
‘বিজয়ের আলোয় সমুদ্ভাসিত হোক নির্বাক শব্দমালা’ প্রতিপাদ্য নিয়ে এই প্রদর্শনীর আয়োজন করা হয়।
ঢাকা বিশ্ববিদ্যালয়ের মাইম অ্যাকশান এ অনুষ্ঠানের আয়োজন করে। এতে প্রধান অতিথি ছিলেন বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ড. আ আ ম স আরেফিন সিদ্দিক।
তিনি বলেন, তরুণ প্রজন্মকে আগামী দিনের নেতৃত্ব দেওয়ার জন্য যোগ্য নাগরিক হতে হবে। এজন্য একাডেমিক শিক্ষার পাশাপাশি সাংস্কৃতিক চেতনা ধারণ করে বড় হতে হবে। দেশের জন্য জীবন দেওয়ার জন্য প্রস্তুত থাকতে হবে। দেশের জন্য মৃত্যু বরণ করাটাই আভিজাত্যের মৃত্যু।
ঢাকা বিশ্ববিদ্যালয়ের বিশ্বধর্ম ও সংস্কৃতি বিভাগের চেয়ারম্যান ড. ফাদার তপন ডি রোজারিওর সভাপতিত্বে এতে আরও বক্তব্য রাখেন নরসিংদী ৩ আসনের সংসদ সদস্য সিরাজুল ইসলাম মোল্লা, ঢাকা বিশ্ববিদ্যালয় অ্যালমনাই সভাপতি একে আজাদ, গার্ডিয়ান লাইফ ইন্স্যুরেন্সের ব্যবস্থাপনা পরিচালক এম মনিরুল আলম, নরসিংদীর শিবপুর উপজেলা চেয়ারম্যান আরিফুল ইসলাম মৃধা প্রমুখ।
