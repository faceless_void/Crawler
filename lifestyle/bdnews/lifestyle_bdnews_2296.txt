রাস্তার নোংরা এড়াতে অনেকেই উঁচু হিল পরতে পছন্দ করেন, তবে এতে পায়ের গোড়ালিতে ব্যথাসহ আরও নানান সমস্যা হতে পারে। তাই নিয়মিত যত্ন নেওয়া জরুরি।
দিল্লি স্কিন সেন্টারের কসমেটিকস ডার্মাটলজিস্ট এবং পরিচালক মেঘনা গুপ্তা জানান, রাস্তার ধুলাবালি ও ময়লা পানি এবং পা ঘামার কারণে নানা ধরনের সমস্যা হতে পারে পায়ের ত্বকে।
তাই পায়ের যত্ন নিয়ে মেঘনা গুপ্তা কিছু পরামর্শ দেন।
- পায়ের ফাঙ্গাসের সমস্যা গোড়ার থেকেই খেয়াল রাখা উচিত। শুরুতেই ফাঙ্গাসের সমস্যা সনাক্ত করা গেলে দ্রুত সারিয়ে তোলা সম্ভব।
- বাইরে থেকে ঘরে ফিরে প্রথমে জীবাণু নাশক কোনো সাবান দিয়ে পা ধুয়ে, পায়ে ময়েশ্চারাইজার লাগিয়ে নিতে হবে। নতুবা ত্বক শুষ্ক হয়ে খসখসে হয়ে যেতে পারে। এক্ষেত্রে এএইচএ বা ল্যাকটিক অ্যাসিড সমৃদ্ধ ময়েশ্চারাইজার ব্যবহার করা উচিত।
- পায়ের নখ যতটা সম্ভব ছোট করে কেটে রাখতে হবে। এতে নখের ভেতর ময়লা ঢুকে সংক্রমণের সমস্যা থাকবে না।
- পায়ের গোড়ালি ফাঁটাও পানি শূন্যতার লক্ষণ। তাই পায়ের গোড়ালি ফাঁটা শুরু হলে অবশ্যই পানি পানের পরিমাণ বাড়াতে হবে। পাশাপাশি প্রচুর ফলমূল ও শাকসবজি খেতে হবে। এতে শরীরের পানির চাহিদা পূরণ হবে। তাছাড়া দৈনিক ক্যাফেইন ও অ্যালকোহল গ্রহণের মাত্রাও কমাতে হবে।
- মুখের ত্বক পরিষ্কার রাখতে এবং মৃতকোষ দূর করতে যেমন স্ক্রাবিং বেশ জরুরি তেমনি পায়ের ত্বকের ক্ষেত্রেও স্ক্রাবিং গুরুত্বপূর্ণ। রুক্ষ এবং প্রাণহীন পা দেখতে বেশ বেমানান। তাই সপ্তাহে অন্তত একবার পা স্ক্রাব করা উচিত। পা স্ক্রাব বা ঘষার জন্য বিশেষ ধরনের পাথর ও ব্রাশ পাওয়া যায় এগুলোও বেশ কার্যকর।
- নিয়ম করে পার্লারে গিয়ে পেডিকিওর করানোর সময় যদি না হয় তাহলে একদিন সময় বের করে ঘরেই পায়ের যত্ন নেওয়া যায়। এরজন্য তৈরি করে নিতে পারেন বিশেষ স্ক্রাব।
