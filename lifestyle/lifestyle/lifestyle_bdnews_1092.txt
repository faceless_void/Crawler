রেসিপি দিয়েছন আতিয়া হোসেন তনী।
উপকরণ
২ টেবিল-চামচ ময়দা। ২ টেবিল-চামচ কোকো পাউডার। ২,৩ টেবিল-চামচ চিনি। ২ টেবিল-চামচ তরল দুধ।
১ টি ডিম। ১/৪ চামচ বেকিং পাউডার। ১ টেবিল-চামচ তেল। কয়েক ফোঁটা ভ্যানিলা এসেন্স। ২ টেবিল-চামচ চকলেট চিপস। আর ১টি ওভেন প্রুফ মগ।
পদ্ধতি
একটি মগে সব উপকরণ নিয়ে ভালো মতো মেশান। বড় মগ হলে একটিতেই হয়ে যাবে কেক।
মাইক্রোওয়েভ ওভেনে এক মিনিট ৩০ সেকেন্ডের জন্য দিতে হবে। কেক বেইক হয়ে দ্বিগুণ হবে। প্রয়োজন হলে আরও ৩০  সেকেন্ড দেবেন।


টিপস
মাইক্রোওয়েভ ওভেনের ধরন অনুযায়ী সময় কম বেশি হতে পারে। এজন্য খেয়াল রাখবেন। যখন কেক বেইক হয়ে দ্বিগুন আকার হবে তখন ওভেন বন্ধ করে দিন।
সমন্বয়ে: ইশরাত মৌরি।
পাইনঅ্যাপল আপ সাইড-ডাউন কেক
অরেঞ্জ কেক
