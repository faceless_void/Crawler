স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটের প্রতিবেদনে জানানো হয়, বেশি রাতে খেলে শরীরের স্বাভাবিক হজম প্রক্রিয়ায় বাধা, ঘুমের সমস্যাসহ নানারকম অসুবিধা হতে পারে।
অসময়ে ক্যালরি গ্রহণের কারণে হরমোনের ভারসাম্যও নষ্ট হয়। ফলে মেজাজ হতে পারে খিটখিটে।
এছাড়াও যা হতে পারে তা হল:
বাড়তি ক্যালরি: রাতের খাবার খেতে যত দেরি করবেন, ততই বেশি ক্যালরি গ্রহণ করা হবে। কারণ রাতের খাবারের পর শারীরিক পরিশ্রমের কোনো সম্ভাবনা থাকে না বললেই চলে। ফলে শরীর বাড়তি ক্যালরি খরচ করার সুযোগ পায় না।
অপর্যাপ্ত ঘুম: রাতে দেরি করে ঘুমালে শরীর ও মন পর্যাপ্ত বিশ্রাম পায় না। তাই ভালো ঘুম পেতে রাতের খাবার জলদি খেতে হবে।
বিপাক ব্যবস্থা: রাতের খাবার খেতে দেরি করা এবং অপর্যাপ্ত ঘুম শরীরের বিপাকীয় কার্যকলাপ ধীর করে দেয়। ফলে ওজন বাড়ে।
বুক জ্বালাপোড়া ও অ্যাসিডিটি: প্রতিবার খাওয়ার পর ওই খাবারকে প্রক্রিয়াজাত করতে শরীরের চাই কিছুটা সময়। তবে খাওয়ার পরপরই শুয়ে পড়লে পাকস্থলীতে থাকা অ্যাসিড উপরে উঠে আসতে পারে। ফলে বুক জ্বালাপোড়া ও অ্যাসিডিটি হতে পারে।
