বিবিসির এক প্রতিবেদনে শুক্রবার জানানো হয়, উবারের লাইসেন্স আর নবায়ন করা হবে না, এবং বিষয়টি তাদের জানিয়ে দিয়েছে লন্ডন ট্রান্সপোর্ট অথরিটি।
এই নিয়ন্ত্রক সংস্থা বলছে, উবারের কর্পোরেট দায়িত্বশীলতাসহ বিভিন্ন ক্ষেত্রে বড় ধরনের ঘাটতি রয়েছে, যা নাগরিকদের জন্য নিরাপত্তা ঝুঁকি তৈরি করতে পারে।
রয়টার্সের খবরে বলা হয়, লন্ডনে উবারের লাইসেন্সের মেয়াদ চলতি মাসের শেষেই তামাদি হয়ে যাবে। তবে উবার কর্তৃপক্ষ ট্রান্সপোর্ট অথরিটির এই সিদ্ধান্তের বিরুদ্ধে আপিল করতে পারবে এবং তার নিষ্পত্তির আগ পর্যন্ত লন্ডনে উবারের ট্যাক্সি সেবায় বাধা নেই।
লন্ডনের এমন সিদ্ধান্তে ইতোমধ্যেই এর বিরোধিতা করতে জোরালো অবস্থান নিয়েছে উবার।
প্রতিষ্ঠানটি জানায়, “এই সিদ্ধান্ত আপনাকে শহরে যাতায়াতের ক্ষেত্রে সুবিধাজনক পথ থেকে বঞ্চিত করবে এবং লাইসেন্সধারী ৪০ হাজার চালক যারা আমাদের অ্যাপের ওপর নির্ভর করে তাদেরকেও বঞ্চিত করবে।”
কর্মীদেরকে ইমেইলে উবার প্রধান দারা খোসরোশাহি বলেন, “তিনি লন্ডনের অবস্থা নিয়ে হতাশ এবং উবারকে নিয়ে লন্ডন যা বলেছে তা সত্য বলে তিনি বিশ্বাস করেন না।”
খোসরোশাহি আরও বলেন, “কর্মীদের সব সমালোচনাকে অন্যায্য না বলে নিজস্ব প্রতিফলনের দিকে নজর দেওয়া উচিত।”
“আমরা কীভাবে এখানে পৌঁছেছি তা যাচাই করার বিষয়। মানুষ আমাদের নিয়ে কী চিন্তা করে তা আসলেই গুরুত্বপূর্ণ। বিশেষ করে আমদের মতো বৈশ্বিক ব্যবসায় যেখানে বিশ্বের এক প্রান্তে কোনো পদক্ষেপ নেওয়া হলে অন্য প্রান্তে তার মারাত্মক প্রভাব পড়তে পারে।”
লাইসেন্স নবায়ন না করার সিদ্ধান্তের বিপক্ষে অবিলম্বে আদালতে যাওয়ার ঘোষণা দিয়েছে উবার কর্তৃপক্ষ। তবে সেখানে তাদের আবেদন না টিকলে তা হবে ৪০ হাজার চালকের জন্য একটি বড় ধাক্কা।
অবশ্য লন্ডনের মেয়র সাদিক খান  ট্রান্সপোর্ট অথরিটির সিদ্ধান্তকে স্বাগত জানিয়ে বলেছেন, লন্ডনবাসীর জন্য ঝুঁকি তৈরি হলে উবারকে ফের লাইসেন্স দেওয়া হবে ভুল সিদ্ধান্ত।  
এই সেবার জন্য উবারের নিজস্ব কোনো ট্যাক্সি নেই। ব্যক্তিগত গাড়ি আছে এমন যে কেউ অ্যাপ ডাউনলোড করে নিবন্ধনের মাধ্যমে উবারের চালক হয়ে যেতে পারেন। অ্যাপ ব্যবহার করেই সেবা নেন যাত্রীরাও।
যুক্তরাষ্ট্রের স্যান ফ্রান্সিসকোভিত্তিক অনলাইন ট্র্যান্সপোর্টেশন নেটওয়ার্ক কোম্পানি উবারের দাবি, বিশ্বের ৭৪টি দেশের ৪৫০টি শহরের মানুষ তাদের অ্যাপ ব্যবহার করে প্রতিদিন গড়ে ৫০ লাখের বেশি বার ট্যাক্সিতে চড়ছে।
বিশ্বের বিভিন্ন বড় শহরে ব্যাপকভাবে জনপ্রিয় হলেও বিভিন্ন দেশে আইনি জটিলতা ও সমালোচনার মুখে পড়তে হয়েছে উবারকে। লন্ডনেও উবারের কাজের ধরন ও শর্ত নিয়ে বিভিন্ন  শ্রমিক ইউনিয়ন, ব্ল্যাক ক্যাবের চালক ও আইনপ্রণেতাদের আপত্তি রয়েছে।
