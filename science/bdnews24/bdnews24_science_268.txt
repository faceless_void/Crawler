ওইদিন এ নিয়ে করা তদন্তের ফলাফল ঘোষণা করবে দক্ষিণ কোরীয় ইলেকট্রনিক জায়ান্টটি।
স্যামসাং জানিয়েছে, এর কর্মকর্তা ও স্বাধীন বিশেষজ্ঞরা দক্ষিণ কোরিয়ার রাজধানী সিউলে এক সংবাদ সম্মেলনে এই তদন্তের ফলাফল প্রকাশ করবে ও ভবিষ্যতে এই ধরনের ঘটনা এড়াতে প্রতিষ্ঠানটি কী কী পদক্ষেপ নিচ্ছে তা জানাবে।
শুক্রবার এক বিবৃতিতে স্যামসাং জানায়, ওইদিন প্রতিষ্ঠানটির মোবাইল ব্যবসায়ের প্রধান কোহ ডং-জিন তদন্ত ফলাফল ঘোষণার অনুষ্ঠানে উপস্থিত থাকবেন। সেপ্টেম্বরে তিনিই নোট ৭ ফেরতের আহ্বান জানিয়েছিলেন।
চলতি সপ্তাহের শুরুতে এই ঘটনার সঙ্গে সংশ্লিষ্ট এক ব্যাক্তি রয়টার্সকে জানায় স্যামসাংয়ের তদন্তে দেখা গেছে ব্যাটারিই নোট ৭ আগুনের প্রধান কারণ।
বিশ্বের শীর্ষ স্মার্টফোন নির্মাতা প্রতিষ্ঠানটিকে গ্রাহকদের পুনরায় নিশ্চিত করতে হবে যে তাদের ডিভাইসগুলো এখন নিরাপদ, বলা হয়েছে রয়টার্স-এর প্রতিবেদনে।
বিনিয়ীগকারীরা বলছেন, স্যামসাং-কে অবশ্যই গ্যালাক্সি নোট ৮ স্মার্টফোন বের করার আগে আগুনের ঘটনার একটি বিশ্বাসযোগ্য ব্যাখ্যা দিতে হবে। চলতি বছরের প্রথমার্ধে গ্যালাক্সি নোট ৮ আনা হবে বলে আশা করা হচ্ছে।
