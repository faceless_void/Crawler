সম্প্রতি এক গবেষণায় দেখা গেছে, প্রেমিক-প্রেমিকা যারা দূরে থাকেন তাদের ক্ষেত্রে একে অপরের সঙ্গে যোগাযোগ করতে দারুণ সহযোগী ফেইসবুক। তবে যারা একই শহরে রয়েছেন, তাদের ক্ষেত্রে এটি ততোটা উপযোগী নয়।
স্যান ডিয়েগো, ক্যালিফোর্নিয়ার ইন্টারঅ্যাকটিভ মিডিয়া ইন্সটিটিউট, ব্রেন্ডা কে. উইডারহোল্ড বলেন, “যারা নিজের সঙ্গীর কাছ থেকে দূরে থাকেন তারা অন্যদের তুলনায় বেশি সোশাল নেটওয়ার্কিং সাইটগুলো ব্যবহার করে থাকেন।”
তিনি আরও বলেন, “এখন দূরে থাকা মানুষের সঙ্গে সম্পর্ক থাকা স্বাভাবিক আর সেটা সফলও হচ্ছে। তাই প্রযুক্তি সম্পর্ক গড়া বা ভাঙায় কী ভূমিকা রাখছে তা জানা মূল্যবান হয়ে পড়েছে।”
গবেষণায় দেখা গেছে, ফেইসবুকের মতো সোশাল নেটওয়ার্কিং সাইটগুলো ভৌগলিকভাবে দূরে থাকা দুজনের সম্পর্ক বজায় রাখতে গুরুত্বপূর্ণ ভূমিকা পালন করে।
এই গবেষণার বিষয়ে কথা বলেন সহকারী লেখক ভিইউ ইউনিভার্সিটি অ্যামাটারডামের চেরি জো বিলেডো, দ্য নেদারল্যান্ডসের পিটার কেরখোফ এবং ইউনিভার্সিটি অফ দ্য ফিলিপিন্স’য়ের ক্যাটরিন ফিনকেনাওয়ার। তারা দূরে থাকা এবং কাছে থাকা জুটির সম্পর্কে সোশাল নেটওয়ার্কিং সাইটগুলো ব্যবহারের তীব্রতা ও ব্যবহারের ধরন সম্পর্কে ব্যাখ্যা করেন।
তারা গবেষণাপত্রে উল্লেখ করেন, “সঙ্গীর সম্পর্ক এবং অনুগত থাকার মানদণ্ড হিসেবে ব্যবহৃত হতে পারে এই পদ্ধতি। যা সম্ভাব্য নেতীবাচক বা ক্ষতিকর প্রভাবের সঙ্গে জড়িত।”
সাইবারসাইকোলজি, বিহেইভিয়র অ্যান্ড সোশাল নেটওয়ার্কিং জার্নালে গবেষণাটি প্রকাশিত হয়।
ছবি: রয়টার্স।
অনলাইনে সম্পর্ক
একসঙ্গে ঘুমাতে গেলে সম্পর্ক সুখের হয়
কর্মক্ষেত্রে পেতে পারেন ভালোবাসা
