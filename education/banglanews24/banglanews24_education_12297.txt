ফুলবাড়িয়া ডিগ্রি কলেজ সরকারিকরণের দাবিতে চলমান আন্দোলনে উদ্ভিদবিদ্যা বিভাগের সহকারী অধ্যাপক আবুল কালাম আজাদ হত্যার বিচারের দাবিতে বাংলাদেশ কৃষি বিশ্ববিদ্যালয়ে (বাকৃবি) মানববন্ধন হয়েছে।
বাকৃবি (ময়মনসিংহ): ফুলবাড়িয়া ডিগ্রি কলেজ সরকারিকরণের দাবিতে চলমান আন্দোলনে উদ্ভিদবিদ্যা বিভাগের সহকারী অধ্যাপক আবুল কালাম আজাদ হত্যার বিচারের দাবিতে বাংলাদেশ কৃষি বিশ্ববিদ্যালয়ে (বাকৃবি) মানববন্ধন হয়েছে।
মঙ্গলবার (২৯ নভেম্বর) দুপুর ১টার দিকে বিশ্ববিদ্যালয়ের শিল্পাচার্য জয়নুল আবেদিন মিলনায়তন মুক্তমঞ্চের সামনে এ  মানববন্ধন করে বাকৃবি শাখা সমাজতান্ত্রিক ছাত্র ফ্রন্ট।
ছাত্র ফ্রন্টের ভারপ্রাপ্ত সভাপতি জুনায়েদ হাসানের সভাপতিত্বে মানববন্ধনে উপস্থিত ছিলেন-গ্রামীণসমাজ বিজ্ঞান বিভাগের সহযোগী অধ্যাপক কাজী শেখ ফরিদ, আনন্দমোহন কলেজ ছাত্র ফ্রন্ট শাখার সহ সভাপতি আরিফুল হাসান ও ছাত্র ফ্রন্টের দপ্তর সম্পাদক মাগফুরা জেরিন।
ছাত্র ফ্রন্টের সাধারণ সম্পাদক গৌতম কর মানববন্ধনের সঞ্চালনা করেন।
এছাড়া বিশ্ব সাহিত্য কেন্দ্র, চারণ সাংস্কৃতিক কেন্দ্র ও ছাত্র ফ্রন্টের নেতাকর্মীরা মানববন্ধনে উপস্থিত ছিলেন।
এ সময় বক্তারা হত্যাকাণ্ডে জড়িতদের বিচার ও কলেজটি সরকারিকরণের দাবি জানান।
