স্বাস্থ্যবিষয়ক ওয়েবসাইটে প্রকাশিত প্রতিবেদন থেকে খাবারের বিষক্রিয়া থেকে হওয়া পেটের পীড়া কমানোর ঘরোয়া উপায় সম্পর্কে জানা যায়।
জিরা: খাবারের বিষক্রিয়ার ফলে তৈরি অস্বস্তি এবং প্রদাহ থেকে আরাম দিতে সাহায্য করে। চাইলে সারাদিন জিরা মুখে রেখে চিবাতে পারেন অথবা জিরা দিয়ে পানীয় তৈরি করে পান করতে পারেন।
এক কাপ পানিতে এক টেবিল-চামচ জিরা নিয়ে ফুটান। চাইলে এতে এক টেবিল-চামচ ধনিয়ার রস এবং সামান্য লবণ মেশাতে পারেন। সমস্যা দূর করতে দিনে দুইবার পান করুন।
মধু: এর ব্যাকটেরিরা ও ফাঙ্গাস বিরোধী উপাদানের জন্য সুপরিচিত। মধু হজম সমস্যার পাশাপাশি খাদ্যের অন্যান্য বিষক্রিয়া থেকে তৈরি সমস্যা দূর করতে কাজে লাগে।
প্রতিদিন এক টেবিল-চামচ প্রাকৃতিক মধু খান। এটি শরীরের অতিরিক্ত অ্যাসিড নিয়ন্ত্রণ করে। যা পেটের সমস্যা দূর করতে সহায়তা করে।
কলা: ডায়রিয়া ও বমির কারণে শরীর থেকে পটাশিয়াম কমে যায়। শরীরে পটাশিয়ামের মাত্রা ঠিক রাখতে পাকাকলা খান। এটি শরীরে শক্তি যোগাতেও সহায়তা করে।
চাইলে কলার শেইক তৈরি করে খেতে পারেন। দুইটি কলা ও এক কাপ ফুটানো ঠাণ্ডাদুধ ব্লেন্ড করে কলার শেইক তৈরি করুন। এতে এক চিমটি দারুচিনি মিশিয়ে দিনে তিনবার পান করুন।
দই এবং মেথি বীজ: দইয়ের মাইক্রোবিয়াল এবং ব্যাকটেরিয়া রোধী উপাদান রয়েছে যা ব্যাকটেরিয়ার কারণে হওয়া খাদ্যের বিষক্রিয়ার বিরুদ্ধে কাজ করে। মেথির বীজ পেটের অস্বস্তি কমাতে সাহায্য করে।
এক টেবিল-চামচ মেথির বীজ নরম না হওয়া পর্যন্ত ভিজিয়ে রাখুন। এরপর এক টেবিল-চামচ দইয়ের সঙ্গে মিশিয়ে খান। সারাদিনে নির্দিষ্ট সময় পর পর খেলে উপকার মিলবে।
কমলার রস: তাজা কমলার রসে খনিজ উপাদান, ভিটামিন এবং পুষ্টিকর পদার্থে ভরপুর যা শরীরের রক্তচাপের মাত্রা নিয়ন্ত্রণে রাখে।
