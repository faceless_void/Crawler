অর্থবিষয়ক দৈনিক ফিনান্সিয়াল টাইমস এক প্রতিবেদনে জানায়, আয়ারল্যান্ডে অ্যাপলের কর ব্যবস্থাপনা নিয়ে তিন বছরের তদন্ত শেষে সামনের সপ্তাহে এ নিয়ে ইউরোপিয়ান কমিশন রায় দেবে বলে আশা করা হচ্ছে।
রায়ের পর অ্যাপলকে কত ডলার শোধ করা লাগতে পারে? এ প্রশ্নের জবাব এখনও স্পষ্টভাবে কোথাও খুঁজে পাওয়া না গেলেও, আনুমানিক অংক দেওয়া হচ্ছে। বিশ্লেষণা প্রতিষ্ঠান জেপি মরগান এর হিসাব অনুযায়ী এই অংকটা এক হাজার নয়শ' কোটি ডলার হতে পারে।
এই মামলা-কে গুরুত্বের সঙ্গে বিবেচনা করছে ওবামা প্রশাসন। অ্যাপল আর আয়ারল্যান্ড-এর বিরুদ্ধে কোনো রায় এলে, ফলাফল কেমন হবে তা নিয়ে মার্কিন সরকারের পক্ষ থেকে ইউরোপিয়ান কমিশন-কে সতর্ক করা হয়েছে।
বুধবার সাদা কাগজে প্রকাশিত এক বিবৃতিতে ইউএস ট্রেজারি'র পক্ষ থেকে বলা হয়, "কমিশন বর্তমান কার্যক্রম অব্যাহত রাখলে সম্ভাব্য জবাব কী হতে পারে তা নিয়ে বিবেচনা করছে তারা।"
