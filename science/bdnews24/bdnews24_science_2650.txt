সোমবার মার্কিন সংবাদমাধ্যম সিএনবিসি-এর সঙ্গে সাক্ষাৎকারে কুক বলেন, “যুক্তরাষ্ট্রে অ্যাপল ইতোমধ্যেই ২০ লাখ চাকুরি তৈরি করেছে। ভবিষ্যতে বাড়তি হাজারো কর্মী নিয়োগ দেওয়া হবে।”
কুক আরও বলেন, তারা আরও বেশি চাকুরি বাড়ানোর পথ খুঁজছেন। তহবিলের একশ’ কোটি মার্কিন ডলারকে ‘প্রাথমিক’ অনুদান হিসেবে উল্লেখ করেন তিনি। আর ইতোমধ্যেই একটি প্রতিষ্ঠানে বিনিয়োগের জন্য তাদের সঙ্গে কথা বলেছে অ্যাপল, বলা হয়েছে সিএনএন-এর প্রতিবেদনে।
আপাতত বিনিয়োগের ব্যাপারে বিস্তারিত ব্যাখ্যা দেওয়া হয়নি প্রতিষ্ঠানের পক্ষ থেকে। তবে, কুক বলেন চলতি মাসের শেষ দিকে এ তহবিলের ব্যাপারে বিস্তারিত ঘোষণা দেওয়া হতে পারে।
বিনিয়োগের ব্যাপারে কুক বলেন, “এই প্রোগ্রামে অ্যাপলের বিনিয়োগের পরিকল্পনা রয়েছে যাতে পরবর্তী প্রজন্মের অ্যাপ ডেভেলপারদের প্রশিক্ষণ দেওয়া যায়।”
“আপনারা দেখছেন কীভাবে কর্মীর ভিত্তি বাড়ানো যায়, সে লক্ষ্যে আমরা অনেক গভীরে চিন্তা করছি। কীভাবে আমরা আমাদের ডেভেলপারদের ভিত্তি বাড়াতে পারি? এবং কীভাবে উৎপাদন বাড়াতে পারি?”, বলেন কুক।
