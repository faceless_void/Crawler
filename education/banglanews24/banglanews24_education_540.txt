ঢাকা: জাতীয় বিশ্ববিদ্যালয়ের ২০১৭-২০১৮ শিক্ষাবর্ষের স্নাতক (সম্মান) প্রথমবর্ষের ভর্তি কার্যক্রমের তারিখ ঘোষণা করেছে ভর্তি কমিটি।
সোমবার (১২ জুন) সকালে বিশ্ববিদ্যালয়ের ভর্তি কমিটির সাধারণ সভায় অধিভুক্ত কলেজে ভর্তি পরীক্ষা কার্যক্রম শুরু, আসন সংখ্যা, ভর্তিচ্ছুক শিক্ষার্থীদের ক্লাস শুরু ইত্যাদি বিষয়ে সিদ্ধান্ত হয়েছে।
সিদ্ধান্ত অনুযায়ী, ভর্তি কার্যক্রমে অনলাইনে আবেদন ২৪ আগস্ট থেকে শুরু হবে এবং নতুন শিক্ষাবর্ষে ক্লাস শুরু হবে ১৫ অক্টোবর।
সভায় সভাপতিত্ব করেন বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ড. হারুন অর রশিদ।
উপ-উপাচার্য ড. মো. মশিউর রহমান, কোষাধ্যক্ষ নোমান উর রশীদ, সব ডিন, রেজিস্ট্রার, পরীক্ষা নিয়ন্ত্রক সভায় উপস্থিত ছিলেন।
