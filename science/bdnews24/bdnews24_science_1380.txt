শিকাগো’র ও’হেয়ার এয়ারপোর্ট থেকে শহরের কেন্দ্র পর্যন্ত একটি উচ্চ-গতির পরিবহন ব্যবস্থা আনার উপায় যাচাই করছেন সেখানকার মেয়র রাম ইমানুয়েল। এক্ষেত্রে তিনি মাটির নিচে সুড়ঙ্গের কথাও মাথায় রেখেছেন বলে বিবিসি’র প্রতিবেদনে উল্লেখ করা হয়েছে।
মাস্ক এক টুইটে বলেছেন, শিকাগোতে একটি ‘উচ্চ-গতির লুপ’ নির্মাণে বোরিং কোম্পানি একটি প্রস্তাবনা দাখিল করবে। এই পরিকল্পনায় মাটির নিচের সুড়ঙ্গ দিয়ে ‘বৈদ্যুতিক পড’ ব্যবহার করার বিষয়টি বিবেচনা করা হচ্ছে।
শিকাগো’র জন্য নিজের পরিকল্পনা বর্ণনা করতে গিয়ে মার্কিন এই ধনকুবের ও প্রকৌশলী বলেন, “বৈদ্যুতিক পড নিশ্চিত ব্যবহার করা হবে। রেল হয়তো থাকবে, হয়তো না।” 
ইতোমধ্যে ক্যালিফোর্নিয়ায় মাস্কের অপর প্রতিষ্ঠান স্পেসএক্স-এর জায়গায় পরীক্ষামূলক সুড়ঙ্গ বানানোর কাজ করছে বোরিং কোম্পানি।
শিকাগো’র কর্তৃপক্ষ আশা করছে এরকম একটি পরিবহন ব্যবস্থা করদাতাদের অর্থ ছাড়াই বানানো যাবে। এক্ষেত্রে নির্মাণকারী প্রতিষ্ঠান এই সেবা পরিচালনার মাধ্যমেই অনেক অর্থ আয় করতে পারবে বলেও আশা প্রকাশ করা হয়েছে।
আরও খবর-
হতাশ গ্রাহকের জবাব দিলেন মাস্ক
বোরিং কোম্পানি: পরের ধাপ দেখালেন মাস্ক
