ঢাকা বিশ্ববিদ্যালয়: ঢাকা বিশ্ববিদ্যালয়ের ৯৩তম প্রতিষ্ঠাবার্ষিকী উপলক্ষে কলা ভবন ও মল চত্বর এলাকায় দু’দিনব্যাপী পরিচ্ছন্নতা অভিযান শুরু হয়েছে।
রোববার ঢাকা বিশ্ববিদ্যালয় রোভার স্কাউট গ্রুপ এ অভিযান শুরু করে।
বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ড. আ আ ম স আরেফিন সিদ্দিক প্রধান অতিথি হিসাবে উপস্থিত থেকে এ কর্মসূচির আনুষ্ঠানিক উদ্বোধন করেন।
উদ্বোধনী অনুষ্ঠানে বক্তব্য রাখেন- কলা অনুষদের ডিন অধ্যাপক  ড. সদরুল আমিন, ঢাকা বিশ্ববিদ্যালয় রোভার স্কাউট গ্রুপের সম্পাদক অধ্যাপক মো. রমজুল হক প্রমুখ।
উপাচার্য অধ্যাপক ড. আ আ ম স আরেফিন সিদ্দিক তার উদ্বোধনী বক্তব্যে বলেন, সকল ধর্মে পরিস্কার-পরিচ্ছন্নতার কথা বলা হয়েছে। আমরা যদি আমাদের মন-মানসিকতা পরিষ্কার পরিচ্ছন্ন রাখি, তাহলেই পরিবেশ পরিচ্ছন্ন হবে, সমাজ পরিচ্ছন্ন হবে এবং দেশ পরিচ্ছন্ন হবে। আমাদের প্রত্যেকের শপথ হোক আমরা পরিবেশ অপরিচ্ছন্ন করবো না।
এই পরিষ্কার-পরিচ্ছন্ন অভিযানে ঢাকা বিশ্ববিদ্যালয় রোভার স্কাউট গ্রুপের  প্রায় ২০০ সদস্য স্বতঃস্ফূর্ত ভাবে অংশ গ্রহণ করছে। এছাড়া বিশ্ববিদ্যালয়ের ৪র্থ শ্রেণির পরিচ্ছন্ন কর্মীরাও এই অভিযানে অংশ নেন। 
