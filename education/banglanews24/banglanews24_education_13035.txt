ঝালকাঠি: ঝালকাঠির কাঠালিয়া পাইলট উচ্চ মাধ্যমিক বিদ্যালয়ে ছাত্রী ভর্তিতে নিষেধাজ্ঞা জারি করা হয়েছে।
সোমবার (০৯ অক্টোবর) ওই নিষেধাজ্ঞা জারি করে বরিশাল মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষাবোর্ড।
বোর্ডের কলেজ পরিদর্শক ড. মো. লিয়াকত হোসেন স্বাক্ষরিত নোটিশে বলা হয়েছে, ওই বিদ্যালয়ে ছাত্রী ভর্তি না করতে সুপ্রিম কোর্টের হাইকোর্ট বিভাগে রিট করা হয়। রিটের সিদ্ধান্ত মতে শিক্ষা মন্ত্রণালয়ের নির্দেশে ছাত্রী ভর্তি করা যাবে না।
নিষেধাজ্ঞা উপেক্ষা করে ছাত্রী ভর্তি করা হলে বিদ্যালয়ের প্রধান শিক্ষকের বিরুদ্ধে ‍সুপ্রিম কোর্টের আদেশ অমান্যের দায়ে বিধিসম্মত ব্যবস্থা নেওয়া হবে বলেও জানিয়েছে বোর্ড।  
