এমন কয়েকটি আত্মহত্যার খবর শোনা গেছে যেখানে ব্লু হোয়েল-এর প্রভাব রয়েছে বলে দাবি করা হয়েছে। ঢাকার এক স্কুলছাত্রীর বাবা তার মেয়ের আত্মহত্যার জন্যও এই ইন্টারনেট গেইমকে দায়ী করেছেন।
গেইমটি বাবা মায়ের দুশ্চিন্তার কারণ হতে পারে বলেও ধারণা করা হচ্ছে। এ কারণে সন্তানদেরকে নজরে রাখার পরামর্শও দিয়েছেন অনেকে।
‘ব্লু হোয়েল’ বা ‘ব্লু হোয়েল চ্যালেঞ্জ’ একটি অনলাইন গেইম, যা অংশগ্রহণকারীকে মৃত্যুর পথে নিয়ে যায়। নীল তিমিরা মারা যাওয়ার আগে জল ছেড়ে ডাঙায় ওঠে যেন আত্মহত্যার জন্যই- সেই ধারণা থেকে এই গেইমের নাম হয়েছে ‘ব্লু হোয়েল’।
এই গেইমে খেলোয়াড়দের বিভিন্ন কাজ সম্পন্ন করতে দেওয়া হয়, পুরো কাজের সিরিজ সম্পন্ন করার জন্য সময় থাকে ৫০ দিন। প্রতিটি কাজ সম্পন্ন হওয়ার পর একটি করে ছবি পাঠাতে হয় গেইমারকে। একদম সব শেষে চূড়ান্ত কাজ হিসেবে খেলোয়াড়কে আত্মহত্যা করতে বলা হয়।
২০১৩ সালে রাশিয়ায় ‘এফ৫৭’ নামে যাত্রা শুরু করে গেইমটি। এই গেইম খেলার কারণ প্রথম আত্মহত্যার অভিযোগ আসে ২০১৫ সালে। বিশ্ববিদ্যালয় থেকে বহিষ্কৃত ফিলিপ বুদেইকিন নামের এক সাবেক মনোবিদ্যা শিক্ষার্থী এই গেইম বানিয়েছেন বলে দাবি করেন। কিন্তু কেন এই গেইম বানালেন তিনি? তার দাবি, এর উদ্দেশ্য হচ্ছে সমাজে যাদের কোনো মূল্য নেই বলে তিনি বিবেচনা করেন তাদেরকে আত্মহত্যার দিকে প্ররোচিত করার মাধ্যমে সমাজকে ‘পরিষ্কার’ করা।
গেইমটি খেলে যারা আত্মহত্যা করছেন তাদেরকে মূল্যহীন ভাবছেন গেইমটির ডেভেলপাররা।
২০১৬ সালে রাশিয়ায় এক সাংবাদিক এই গেইম নিয়ে প্রতিবেদন করলে এটি কিশোরদের মাঝে ছড়িয়ে পড়ে।
গেইমটি প্রথমে মোবাইল অ্যাপ হিসেবে থাকলেও এই সুযোগ কাজে লাগিয়ে পরবর্তীতে সামাজিক যোগাযোগ মাধ্যমগুলোতেও কিছু ব্যক্তি গেইমটি চালাতে থাকেন। তারা সামাজিক মাধ্যম থেকেই ব্যবহারকারীদের নানা চ্যালেঞ্জ দিতে শুরু করেন।
বুদেইকিন-কে পরে রাশিয়ায় আটক করা হয় আর তার গেইমের জন্য ‘অন্তত ১৬ জন কিশোরী আত্মহত্যা’ করেছেন বলে অভিযোগ আনা হয়।
রাশিয়ায় যাত্রা শুরু করলেও পরে তা অন্য দেশেও ছড়িয়ে পড়ে। ভারতে এই গেইমটি খেলে কয়েকজনের আত্মহত্যার খবর আসার পর দেশটির সরকার ‘ব্লু হোয়েল’র মতো বিপজ্জনক অনলাইন গেইমের লিঙ্ক বন্ধ করার নির্দেশ দেয়।
সম্প্রতি বাংলাদেশেও সামাজিক মাধ্যমগুলোতে গেইমটি নিয়ে আলোচনা শুরু হয়। এবার ঢাকার স্কুল ছাত্রী এই গেইমের কারণেই আত্মহত্যা করেছেন বলে দাবি করা হয়েছে।
আত্মহত্যার জন্য গেইমটিকে দায়ী করা হলেও এখন পর্যন্ত এটি যাচাই করে দেখা হয়নি। এমনকি এর কারণে আত্মহত্যাকারীর যে সংখ্যা বলা হয়েছে এবং গেইমের প্রতিটি স্তরে যে কাজগুলো করতে বলা হয় সেটিও যাচাই করা হয়নি।
গেইমটি নিয়ে যখনই কোনো প্রতিবেদন প্রকাশ করা হয়েছে তাতে আগের একটি নির্দিষ্ট প্রতিবেদনের উল্লেখ করা হয় যার তথ্যগুলোও যাচাইকৃত নয়। অনেক প্রতিবেদকই ছদ্মনামে গেইমটির প্রতিটি স্তরের তথ্য নেওয়ার চেষ্টা করেছেন। কিন্তু তারা সামাজিক যোগাযোগ মাধ্যমগুলোতে গেইমের ‘অ্যাডমিনিস্ট্রেটরকে’ খুঁজে পাননি। তাই ধারণা করা হচ্ছে এর মাধ্যমে ভুল ধারণা তৈরি হচ্ছে সমাজে-- খবর ভারতীয় সংবাদমাধ্যম হিন্দু’র। ভাইরাল হওয়া এই আতংক-কে ‘কাল্পনিক’ হিসেবেই আখ্যা দিয়েছে সংবাদমাধ্যমটি। 
আর গণমাধ্যম ও সামাজিক যোগাযোগের মাধ্যমগুলোতে এই খবর শেয়ার করায় আরও বেশি ভাইরাল হচ্ছে গেইমটি।
আত্মহত্যার পেছনে ব্লু হোয়েল আসলেও দায়ী কিনা তা যাচাই করা না হলেও এ নিয়ে বাবা মা-কে সতর্ক থাকতে বলা হয়েছে। সন্তানদেরকে অনলাইনে নিরাপদ রাখতে  বাবা মায়ের জন্য কিছু পরামর্শের কথা বলা হয়েছে হিন্দুস্তান টাইমস-এর প্রতিবেদনে-
● সন্তানরা যে অনলাইন সাইটগুলো ব্রাউজ করছে বয়সের ভিত্তিতে তা তাদের জন্য ঠিক কিনা তা যাচাই করে দেখা।
● সন্তানরা যাতে এমন স্থানে কম্পিউটার ব্যবহার করে যেখানে পরিবারের লোকেরা চলাফেরা করেন তা নিশ্চিত করা।
● বাবা মায়ের উচিত সন্তানের সঙ্গে আলোচনা করা। একসঙ্গে অনলাইন ব্রাউজ করা এবং তাদেরকে মজাদার অনলাইন কনটেন্ট দেখানো এবং অনৈতিক কাজগুলো সম্পর্কে ধারণা দেওয়া।
● বাচ্চারা যে ডিভাইসগুলো থেকে ইন্টারনেট ব্যবহার করে সেগুলোর পর্দায় নজর রাখা ও এতে বাবা মায়ের নিয়ন্ত্রণে রাখা।
● নিজেদের অনলাইন কার্যক্রমে মনযোগী হয়ে সন্তানের কাছে নিজেদেরকে অনুকরণীয় আদর্শ হিসেবে তুলে ধরা।
● ইন্টারনেটের সাম্প্রতিক ঘটানাগুলোর বিষয়ে খবর রাখা।
● সন্তানকে কাছ থেকে পর্যবেক্ষণ করা। তার মধ্য অস্বাভাবিক কোনো পরিবর্তন আসছে কিনা, পড়াশোনায় বা প্রতিদিনের স্বভাবিক কাজকর্মে র মতো বিষয়গুলো লক্ষ্য করা।
