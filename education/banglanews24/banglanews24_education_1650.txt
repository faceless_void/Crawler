বাকৃবি (ময়মনসিংহ): বাংলাদেশ কৃষি বিশ্ববিদ্যালয়ে (বাকৃবি) সিড সায়েন্স অ্যান্ড টেকনোলজি বিভাগের মাস্টার্স প্রথম পর্বের শিক্ষার্থীদের ওরিয়েন্টেশন অনুষ্ঠিত হয়েছে। 
 
বুধবার সকাল ১০টায় সীড সায়েন্স অ্যান্ড টেকেনোলজি বিভাগের কনফারেন্স রুমে মাস্টার্স প্রথম পর্বের শিক্ষার্থীদের ওরিয়েন্টেশন অনুষ্ঠিত হয়।
 
সহকারী অধ্যাপক আতিক-উস-সাইদের সঞ্চালনায় ওরিয়েন্টেশন অনুষ্ঠানে করেন, প্রধান অতিথি হিসেবে উপস্থিত ছিলেন সীড সায়েন্স অ্যান্ড টেকেনোলজি বিভাগের বিভাগীয় প্রধান অধ্যাপক ড. একেএম জাকির হোসেন।
 
বিশেষ অতিথি হিসেবে উপস্থিত ছিলেন অধ্যাপক ড.আবুল হোসেন, অধ্যাপক ড. ছোলায়মান আলী ফকির প্রমুখ। অনুষ্ঠানের সঞ্চালনা করেন সহকারী অধ্যাপক আতিক-উস-সাইদ।
 
