চলতি সপ্তাহে প্রতিষ্ঠানটি জানায়, তারা ওয়াশিংটন ডিসি’র হাওয়ার্ড ইউনিভার্সিটি’র সঙ্গে চুক্তি করতে যাচ্ছে। এই চুক্তির মাধ্যমে তিন মাসের একটি গ্রীষ্মকালীন কর্মসূচী চালু করা হবে। এই কার্যক্রম কম্পিউটার বিজ্ঞানের মেধাবী শিক্ষার্থীদের জন্য উন্মুক্ত থাকবে।
গুগল জানায়, আবাসিক এই কার্যক্রমের ২৫ থেকে ৩০ জন শিক্ষার্থীকে বাছাই করা হবে। এই শিক্ষার্থীদের জ্যেষ্ঠ গুগল প্রকৌশলীরা শেখাবেন। এক কার্যক্রমের নাম দেওয়া হয়েছে ‘হাওয়ার্ড ওয়েস্ট’। এ ক্ষেত্রে শিক্ষার্থীদের আলাদা কোনো অর্থ পরিশোধ করতে হবে না। এই কার্যক্রম ঐতিহাসিকভাবে ‘কৃষ্ণাঙ্গদের’ শিক্ষা প্রতিষ্ঠান হিসেবে পরিচিত অন্যান্য প্রতিষ্ঠানের সঙ্গেও এমন চুক্তির মাধ্যমে এই কার্যক্রমের পরিসর বৃদ্ধির পরিকল্পনা করছে।
এক বিবৃতিতে হাওয়ার্ড ইউনিভার্সিটি প্রেসিডেন্ট ওয়েইন ফ্রেডেরিক বলেন, “হাওয়ার্ড ওয়েস্ট-এ কাজ করতে প্রস্তুত এমন কয়েকশ’ কৃষ্ণাঙ্গ কম্পিউটার বিজ্ঞান গ্র্যাজুয়েট তৈরি করবে।” তিনি আরও বলেন, “আমাদের স্বপ্ন হচ্ছে এই কার্যক্রম বড় ফলাফল নিয়ে আসবে।”
এদিকে গুগলের কর্মক্ষেত্রে বৈচিত্র্য আরও অনেক দূর যাওয়া বাকি, বলা হয়েছে ব্রিটিশ দৈনিক ইন্ডিপেনডেন্ট-এর প্রতিবেদনে।
প্রতিষ্ঠানটির বার্ষিক বৈচিত্র্য প্রতিবেদন অনুযায়ী, ২০১৫ সালের শেষ পর্যন্ত প্রতিষ্ঠানটির মোট কর্মীসংখ্যার মাত্র দুই শতাংশ কৃষ্ণাঙ্গ ছিল।
