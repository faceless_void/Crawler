এই গরমে সুন্দর ত্বকের জন্য হারবাল পণ্যের প্রসাধনী খুবই উপকারী। আর তা যদি ঘরেই তৈরি করা যায় তাহলে তো কথাই নেই।
গ্ল্যামএনগ্লোরি ডটকম অবলম্বনে গ্রীষ্মের উপযোগী হারবাল প্রসাধনী সামগ্রী তৈরির কিছু উপায় দেওয়া হল।
গরমে সবচেয়ে ভালো হারবাল প্রসাধনী হল চন্দন ও মুলতানি মাটি। এই দুটির মিশ্রণে তৈরি প্যাক সপ্তাহে এক বা দুইবার মুখে ব্যবহার করলে উপকার পাওয়া যায়। এটা ত্বক থেকে বাড়তি তেল শুষে নেয়।
চন্দন ত্বক করে নরম, মসৃণ ও নমনীয়। এই প্যাকে গোলাপ পানি ব্যবহার করলে আরও ভালো উপকার পাওয়া যায়।
গোলাপ পানির প্রাকৃতিক উপাদান রোদপোড়া লালচেভাব দূর করতে সাহায্য করে।   
তৈলাক্ত ত্বকে খুব সহজেই ময়লা আটকে যায়। আর তা লোমকূপে ঢুকে নানান রকম ঝামেলা তৈরি করে। এর থেকে ব্রণ, ফুস্কুড়ি হয়।
গোলাপ পানি সপ্তাহে এক বা দুইবার ব্যবহার করলে তৈলাক্ত ত্বকে লেগে থাকা ময়লা খুব ভালোমতো পরিষ্কার হয়।
মসুরের ডাল দিয়ে ঘরেই তৈরি করা যায় স্ক্রাব। সপ্তাহে এক বা দুইবার এই স্ক্রাব ব্যবহার করল ফুস্কুড়ি বা ব্রণ হওয়া থেকে পরিত্রাণ পাওয়া যায়। এটা ত্বক থেকে বাড়তি তেলও শুষে নেয়। আর প্রাকৃতিক এক্সফলিয়েটর হিসেবে কাজ করে।
ত্বক পরিষ্কারের জন্য ল্যাভেন্ডার টোনার ব্যবহার করা যেতে পারে। এই টোনার দিয়ে ফেইশল করলে ত্বকে জ্বালাপোড়া কমে। সেই সঙ্গে পিএইচ সমন্বয়ের মাধ্যমে ত্বক আদ্র রাখে।


যে কোনো প্রকার রোগ থেকে রক্ষা পেতে ত্বক সবসময় পরিষ্কার রাখুন। ঘাম ত্বকের উপর শুকাতে দিবেন না। ঘাম মুছতে সবসময় টিস্যু বা রুমাল সঙ্গে রাখুন।
ট্যালকাম পাউডারও ঘাম শুষে নিতে সাহায্য করে। বরফে মোটা কাপড় জড়িয়ে ত্বকের ক্ষতিগ্রস্ত এলাকায় হালকা করে মালিশ করলে উপকার পাওয়া যায়।
সূর্যের কড়া আলো ও বাতাসের জলীয়বাষ্পের কারণে ত্বকের বিভিন্ন জায়গায় ‘হিট র‌্যাশ’ বা লাল লাল ফুস্কুড়ি হয়। গরমের সময় এই ধরনের সংক্রমণ থেকে রক্ষা পেতে আক্রান্ত জায়গায় গোলাপপানি ব্যবহার করুন।
দিনে বেশ কয়েকবার গোলাপ পানি তুলায় ভিজিয়ে আক্রান্ত জায়গায় লাগালে ভালো উপকার পাওয়া যায়।
