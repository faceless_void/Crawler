মাধ্যমিক ও উচ্চ শিক্ষা অধিদপ্তরের আওতায় দরিদ্র শিক্ষার্থীদের মধ্যে মোবাইল ব্যাংকিংয়ের মাধ্যমে উপবৃত্তি বিতরণের জন্য চুক্তি সাক্ষরিত হয়েছে।
ঢাকা: মাধ্যমিক ও উচ্চ শিক্ষা অধিদপ্তরের আওতায় দরিদ্র শিক্ষার্থীদের মধ্যে মোবাইল ব্যাংকিংয়ের মাধ্যমে উপবৃত্তি বিতরণের জন্য চুক্তি সাক্ষরিত হয়েছে।
রোববার (১৬ অক্টোবর) দুপুরে সচিবালয়ে শিক্ষামন্ত্রণালয়ের সম্মেলন কক্ষে চুক্তি সাক্ষর করেন মাউসি মহা পরিচালক ড. এস এম ওয়াহিদুজ্জামান ও অগ্রণী ব্যাংকের ডেপুটি ম্যানেজিং ডিরেক্টর ইসমাইল হোসেন।
সেকেন্ডারি এডুকেশন সেক্টর ইনভেস্টমেন্ট প্রোগ্রাম  (সেসিপ) এর অধীনে ২০১৪ সালের জুলাই থেকে ২০১৭ সালের ডিসেম্বর পর্যন্ত উপবৃত্তি কার্যক্রম চলছে। এ প্রকল্পে মোট ব্যয় হচ্ছে ৩৮২৬ কোটি ৯২ লাখ টাকা। এরমধ্যে সরকার দিচ্ছে ১৬৯৯ কোটি ৯২ লাখ টাকা। আর এডিবি থেকে প্রকল্প সহায়তা আসছে ২১২৭ কোটি টাকা।
প্রকল্প সংশ্লিষ্টরা বলছেন, এতোদিন ম্যানুয়াল পদ্ধতিতে শিক্ষার্থীদের মধ্যে উপবৃত্তির টাকা বিতরণ হলেও এখন থেকে তা মোবাইল ব্যাংকিংয়ের মাধ্যমে হবে। বিকাশের মাধ্যমে টাকা পৌছাবে অগ্রণী ব্যাংক। উপবৃত্তির ২৬৫ কোটি টাকা বিতরণ করবে তারা।
১৭ জেলার ৫৪ উপজেলায় এ বৃত্তি বিতরণ হবে প্রতি শিক্ষা বর্ষে ২ বার।
বাংলাদেশ সময়: ১২৪৭ ঘণ্টা, অক্টোবর ১৬, ২০১৬ আরএম/বিএস
