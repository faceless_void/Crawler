রেসিপি দিয়েওছেন সুমনা সুমি।
উপকরণ: গাজর ১ কেজি (মিহি কুচি করা)। দুধ ১ কাপ। চিনি আধা কাপ। কনডেন্সড মিল্ক আধা কাপ। বাদামকুচি ১/৪ কাপ (কাঠ ও কাজু বাদাম মেশানো মিক্স)। গুঁড়াদুধ ১/৪ কাপ। এলাচগুঁড়া আধা চা-চামচ। মাওয়া বা ছানা আধা কাপ। ঘি ১/৪ কাপ। খাবার রং, যতটুকু লাগে।
পদ্ধতি: প্যানে ২ টেবিল-চামচ ঘি গরম করে একে একে গাজর, চিনি, এলাচগুঁড়া ও খাবার রং দিয়ে পাঁচ মিনিট ভাজুন। দুধ দিয়ে নেড়ে ঢেকে অল্প আঁচে ২০ মিনিট রান্না করুন।
গাজরের পানি শুকিয়ে গেলে কনডেন্সড মিল্ক দিয়ে নাড়তে থাকুন। বাদামকুচি, বাকি ঘি ও ছানা দিয়ে মিশিয়ে নিন। গুড়াদুধ দিন। এখন মিশ্রণটি একদম ঘন মাখামাখা হবে।
