রেসিপি দিয়েছেন আনার সোহেল।
উপকারণ
চালের মণ্ডের জন্য: আতপ চালের গুঁড়া ১ কাপ। ময়দা ২ টেবিল-চামচ। ডিম ২টি (১টি চালের মণ্ডে গুলে দিতে হবে, আরেকটি সবজিতে দিতে হবে)। হালকা কুসুম গরম পানি পরিমাণ মতো। পেঁয়াজ ও আদার রস। লবণ সামান্য।
সবজির জন্য: গাজরকুচি আধা কাপ। সিদ্ধ সবুজ মটর আধা কাপ। লাল কাঁচামরিচের কুচি ২/৩টি। পেঁয়াজকুচি ২ টেবিল-চামচ। সামান্য গোলমরিচের গুঁড়া। হালকা কুসুম গরম পানি পরিমাণমতো। তেল ৪ টেবিল-চামচ (২ টেবিল-চামচ সবজিতে লাগবে আর ২ টেবিল-চামচ পিঠা ভাজার সময় প্যানে ব্রাশ করতে হবে)। লবণ পরিমাণ মতো।
পদ্ধতি
একটি বড় বাটিতে মণ্ড তৈরির সব উপকরণ দিয়ে পাতলা মণ্ড তৈরি করে নিন। বেশি পাতলা হবে না। পাটিসাপটা পিঠার মণ্ড যেভাবে করা হয় সেরকম হবে।
এখন প্যানে ২ টেবিল-চামচ তেল গরম করে সবুজ-মটর দিন। একটু নেড়ে গাজর, পেঁয়াজ দিয়ে লবণ ও গোলমরিচের গুঁড়াসহ দুই মিনিট রান্না করুন।
নামানোর আগে ধনেপাতার কুচি দিয়ে নামিয়ে নিন। একটু ঠান্ডা হলে ১টি ডিম ভেঙে মেশান। ডিম দিলে সবজিগুলো জমাট বেঁধে থাকবে। না হলে খাওয়ার সময় সবজিগুলো পড়ে যাবে।
আবার প্যান গরম করে চুলার আঁচ মাঝারি রাখুন। প্যানে চালের মণ্ড ঢেলে রুটির মতো আকার দিয়ে একপাশে রান্না করা সবজি লম্বা করে দিয়ে ভাজ করে ঢেকে দিন। দুই মিনিট রাখুন।
