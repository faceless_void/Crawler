import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class ExtractorAgroBangla {
    private HashSet<String> links;
    private HashSet<String> linksForParsing;
    private List<List<String>> articles;
    private static final int MAX_DEPTH = 10;
    int count = 0;
    String filename = "prothom_alo_lifestyle_";

    public ExtractorAgroBangla() {
        links = new HashSet<>();
        articles = new ArrayList<>();
        linksForParsing = new HashSet<>();

    }

    public void getPageLinks(String URL,int depth) {
        if (!links.contains(URL) && (depth < MAX_DEPTH)) {
            try {
                Document document = Jsoup.connect(URL)
                        .header("Host", "brickseek.com")
                        .header("Connection", "keep-alive")
//              .header("Content-Length", ""+c.request().requestBody().length())
                        .header("Cache-Control", "max-age=0")
                        .header("Origin", "https://brickseek.com/")
                        .header("Upgrade-Insecure-Requests", "1")
                        .header("User-Agent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.48 Safari/537.36")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                        .referrer("http://brickseek.com/walmart-inventory-checker/")
                        .header("Accept-Encoding", "gzip, deflate, br")
                        .header("Accept-Language", "en-US,en;q=0.8")
                        .cookie("auth", "token")
                        .userAgent("Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2")
                        .timeout(30*1000).get();
                System.out.println("connected");
                Elements otherLinks = document.select("a[href^=\"/\"]");
                System.out.println(otherLinks.size());
                for (Element page : otherLinks) {
                    {

                        links.add(URL);

                        System.out.println("connected");
                        //Remove the comment from the line below if you want to see it running on your editor
                        //System.out.println(URL);
                        System.out.println("calling with this "+ page.attr("abs:href"));
                        //if(!page.attr("abs:href").contains("/category/"))
                            linksForParsing.add(page.attr("abs:href"));
//                         if (links.contains(page.attr("abs:href")))
//                             continue;
                        getPageLinks(page.attr("abs:href"),++depth);
                    }

                }
            } catch (IOException e) {
                getArticles();
                System.err.println(e.getMessage());
            }
            finally {
                //System.out.println("exception ");
//                getArticles();
//                writeToFile();
//                System.out.println("getArticle called");

            }
        }
    }

    //Connect to each link saved in the article and find all the articles in the page
    public void getArticles() {
        System.out.println("-----------------links count "+linksForParsing.size()+"-------------");

        linksForParsing.forEach(x -> {

            {
                String doc = "";

                Document document;
                try {
                    document = Jsoup.connect(x).ignoreHttpErrors(true).timeout(20*1000).get();
                    Elements articleBody = document.select("div.col_in>p");

                    System.out.println("calledArticle from "+ x );

                    if(articleBody.size() >3)
                        articleBody.remove(articleBody.size()-1);

                    System.out.println("total articles in this page " + articleBody.size());
                    for (Element article : articleBody) {
                        if(article.hasText())
                        {
                            doc += article.text() ;
                        }
                    }

                    System.out.println("article size "+doc.length());
                    if(!(doc.length() <100))
                    writeNow(doc);

                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
                finally {
//                    writeToFile();
                }
            }
        });
    }

    private void writeNow(String text) {
        FileWriter writer ;
        {

            {
                try {
                    writer= new FileWriter(filename+Integer.toString(count++));

                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer.write(text);
                    writer.close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
//            writer[0].close();
        }
    }

    public void writeToFile() {

        final FileWriter[] writer = new FileWriter[1];
        {

            articles.forEach(a -> {
                try {
                    writer[0] = new FileWriter(filename+Integer.toString(count++));
                    String temp = a.get(0) ;
                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer[0].write(temp);
                    writer[0].close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            });
//            writer[0].close();
        }
    }

    public static void main(String[] args) throws Exception {
        ExtractorAgroBangla bwc = new ExtractorAgroBangla();

        bwc.getPageLinks("http://www.prothomalo.com/life-style",0);
//        bwc.getPageLinks("http://www.agronewsbd.com",0);
        bwc.getArticles();
        //bwc.writeToFile();
        System.out.println("done.");
    }
}
