ড্যানিশ ডেটা প্রটেকশন এজেন্সি’র কাছে মার্কিন প্রযুক্তি জায়ান্ট অ্যালফাবেট অধীনস্থ গুগলের বিরুদ্ধে অভিযোগ করেছে ডেনমার্ক-এর একটি ভোক্তা অধিকার পর্যবেক্ষণ সংস্থা।
গুগল সার্ভারে ব্যক্তিগত ডেটা জমা করা নিয়ে মার্কিন ওয়েব জায়ান্টটি দেশটির প্রাইভেসি আইন লঙ্ঘন করছে, মঙ্গলবার এক বিবৃতিতে পর্যবেক্ষণ সংস্থাটি এমন দাবি করে।
ব্যবহারকারীদের স্মার্টফোন আর মেইল অ্যাকাউন্টগুলো থেকে নেওয়া স্থান ও সার্চবিষয়ক ডেটা কোথায় কীভাবে সংরক্ষণ করা হয় তা নিয়ে গুগল আর ফেইসবুকের মতো ওয়েব প্রতিষ্ঠানগুলোর উপর নজরদাড়ি বাড়ছে বলে জানিয়েছে রয়টার্স।
পর্যবেক্ষণ সংস্থাটি তাদের অভিযোগে বলে, “ভোক্তা কাউন্সিল ট্যায়েঙ্ক ডেটা প্রটেকশন এজেন্সি-কে গুগলের ডেটা সংগ্রহ ভোক্তাদের মৌলিক প্রাইভেসি অধিকার মেনে চলে কিনা তা যাচাইয়ের আহ্বান জানায়।” এতে আরও বলা হয়, “আমরা একটি বিষয়ে সতর্ক যে, এখন গুগলের কাছে  ব্যবহারকারীদের গুগল অ্যাকাউন্ট থেকে ৯-১০ বছরের ডেটা আছে।”
২০১৬ সালের জুলাইয়ে গুগল ইতালির প্রাইভেসি নিয়ন্ত্রকদের অনুরোধে তাদের ডেটা সংরক্ষণ চর্চায় পরিবর্তন আনে। একটি নির্দিষ্ট সময়ে গুগল কীভাবে ব্যবহারকারীদের ডেটা ব্যবহার করছে ও তা নিয়ে নিশ্চয়তা দিচ্ছে সে সময় তা প্রতিষ্ঠানটিকে স্পষ্ট করতে হয়েছিল।
ডেনমার্কে এই অভিযোগ নিয়ে অনুরোধ করা হলেও গুগলের পক্ষ থেকে তাৎক্ষণিকভাবে কোনো সাড়া দেওয়া হয়নি বলে জানিয়েছে রয়টার্স।
