রেসিপি দিয়েছেন আনার সোহেল।
উপকরণ: আলু (মাঝারি আকার) ২টি। পেঁয়াজ ১টি। কাঁচামরিচ ২টি কুচি করা। ধনেপাতা ১ টেবিল-চামচ। লালমরিচ-গুঁড়া আধা চা-চামচ। ভাজা জিরাগুঁড়া আধা চা-চামচ। বেসন ৩ টেবিল-চামচ। চালের গুঁড়া ১ চা-চামচ। লবণ স্বাদ মতো। তেল ভাজার জন্য। পানি খুব সামান্য।
পদ্ধতি: প্রথমে আলুর খোসা ছাড়িয়ে কুচি কুচি করে আলু ভাজির মতো কেটে, চা-চামচ লবণ দিয়ে মাখিয়ে এক ঘণ্টা রেখে দিন।
বাকি উপকরণ কেটে তৈরি করে রাখুন।
এখন পানি দিয়ে আলু ধুয়ে ভালো করে চেপে চেপে পানি ঝরিয়ে আলু ও অন্যান্য উপকরণ একসঙ্গে মিশিয়ে নিন।
কড়াইতে তেল গরম করে অল্প অল্প আলুর মিশ্রণ নিয়ে পাকোড়ার আকার বানিয়ে তেলে ছাড়ুন। মাঝারি আঁচে বাদামি করে ভেজে তুলুন।
