রেসিপি দিয়েছেন ফারাহ তানজীন সুবর্ণা।
উপকরণ
কাঁচাটমেটো ৬টি (একটু মোটা কুচি করে কাটা)। মুরগির মাংস কুচি ১ কাপ (সিদ্ধ কিংবা আগের রান্না করা)। পেঁয়াজ ১টি (মিহিন করে কুচানো)। রসুন ২ কোয়া (কুচানো)। হলুদগুঁড়া সামান্য। জিরাগুঁড়া ১/৪ চা-চামচ। লবণ স্বাদমতো। চিনি আধা চা-চামচ। কাঁচামরিচ ৩-৪টি। ধনেপাতা ২ টেবিল-চামচ (কুচানো)। তেল দেড় টেবিল-চামচ।
পদ্ধতি
প্যানে তেল দিয়ে কুচি করা মুরগি, অল্প হলুদ আর লবণ দিয়ে ভাজতে হবে। তারপর পেঁয়াজ, রসুনকুচি মিশিয়ে আরও একটু ভেজে হালকা রং ধরলে অল্প হলুদ আর জিরাগুঁড়া দিয়ে নেড়ে নিয়ে কুচানো টমেটো আর লবণ দিন।
ফালি করে কাটা কাঁচামরিচ দিয়ে ভাজুন। ভাজা হয়ে এলে চিনি আর ধনেপাতার কুচি দিয়ে মিশিয়ে নামিয়ে ফেলুন।
একইভাবে মুরগির বদলে কাঁটা ছাড়ানো মাছ দিয়েও করা যায়।
বেগুন টমেটো ভর্তা
