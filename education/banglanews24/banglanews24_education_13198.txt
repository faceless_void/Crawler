জাহাঙ্গীরনগর বিশ্ববিদ্যায়ের (জাবি) ৫টি অনুষদের ডিন নির্বাচনে আওয়ামী লীগপন্থি শিক্ষকদের প্যানেল সংখ্যাগরিষ্ঠতা লাভ করলেও ভরাডুবি হয়েছে বিএনপিপন্থি প্যানেলের।
জাবি: জাহাঙ্গীরনগর বিশ্ববিদ্যায়ের (জাবি) ৫টি অনুষদের ডিন নির্বাচনে আওয়ামী লীগপন্থি শিক্ষকদের প্যানেল সংখ্যাগরিষ্ঠতা লাভ করলেও ভরাডুবি হয়েছে বিএনপিপন্থি প্যানেলের।
মঙ্গলবার (১০ মে) বিকেলে ডিন নির্বাচনের রিটার্নিং কর্মকর্তা ও বিশ্ববিদ্যালয়ের রেজিস্ট্রার আবু বকর সিদ্দিক এ ফলাফল ঘোষণা করেন।
ফলাফলে গাণিতিক ও পদার্থ বিষয়ক অনুষদে আওয়ামীপন্থি প্যানেল থেকে পরিসংখ্যান বিভাগের অধ্যাপক অজিত কুমার মজুমদার ৫৭ ভোট পেয়ে জয় লাভ করেছেন। তার অপর দুই প্রতিদ্বন্দ্বী পদার্থ বিজ্ঞান বিভাগের অধ্যাপক এএ মামুন ৫৫ ভোট, রসায়ন বিভাগের অধ্যাপক ইলিয়াস মোল্লা মাত্র ৪ ভোট পেয়ে পরাজিত হয়েছেন।
সমাজ বিজ্ঞান অনুষদে আওয়ামী প্যানেল থেকে অর্থনীতি বিভাগের অধ্যাপক আমির হোসেন ৫৮ ভোট পেয়ে বিজয়ী হয়েছেন। তার অপর দুই প্রতিদ্বন্দ্বী ভূগোল ও পরিবেশ বিভাগের অধ্যাপক মুহম্মদ নজরুল ইসলাম ১৭ ভোট এবং সরকার ও রাজনীতি বিভাগের অধ্যাপক নইম সুলতান ২২ ভোট পেয়েছেন।
কলা ও মানবিক অনুষদে স্বতন্ত্রপ্রার্থী হিসেবে প্রত্নতত্ত্ব বিভাগের অধ্যাপক মোজাম্মেল হক ৬০ ভোট পেয়ে বিজয়ী হয়েছেন। তার নিকটতম প্রতিদ্বন্দ্বী হিসেবে বাংলা বিভাগের অধ্যাপক খালেদ হোসাইন ৪৫ ভোট, দর্শন বিভাগের অধ্যাপক মোহাম্মদ কামরুল আহসান ১৬ ভোট পেয়েছেন।
এদিকে জীব বিজ্ঞান অনুষদে আওয়ামী প্যানেল থেকে প্রাণিবিদ্যা বিভাগের অধ্যাপক আবদুল জব্বার হাওলাদার ৪৫ ভোট পেয়ে জয় লাভ করেছেন।
এছাড়া আওয়ামী প্যানেল থেকে বিজনেস স্টাডিজ অনুষদে ফিন্যান্স অ্যান্ড ব্যাংকিং বিভাগের সহযোগী অধ্যাপক নীলাঞ্জন কুমার সাহা বিনা প্রতিদ্বন্দ্বিতায় নির্বাচিত হয়েছেন।
নির্বাচনে ৪৫৯ জন ভোটারের মধ্যে ৪৪০ জন তাদের ভোটাধিকার প্রয়োগ করেন।
