সেটা রাতে নয় বরং ঊষালগ্নে। ভোর ৫টা ৪৮ মিনিটে, যখন সাধারণত আপনি হাঁটাহাঁটি কিংবা যোগব্যায়াম শুরু করার পরিকল্পনা করেন।
ইতালির গবেষকদের মতে, ভোরবেলায় নারী এবং পুরুষ উভয়েরই টেস্টোস্টেরনের মাত্রা থাকে তুঙ্গে- যা যৌনমিলনের পূর্বশর্ত।
ডেইলি মেইলের এক প্রতিবেদনে সেক্স থেরাপিস্ট জেরাল্ডিন মায়ারস বলেন, “এসময় উভয়ের কর্মশক্তির মাত্রাও থাকে সর্বোচ্চ। মানসিকভাবে, এসময় জীবনের চাহিদাগুলো নিয়ে দুশ্চিন্তা কম থাকে বলে এটি মিলনের যথাযথ সময়।
ভোর ৫টা ৪৮মিনিটই সঙ্গমের সবচেয়ে উত্তম সময়, এই উপসংহারে এসেছেন গবেষকরা। তারা আরও জানা, এসময় ‘অর্গাজম’ হওয়ার সম্ভাবনাও বেশি থাকে।
সম্প্রতি ব্রিটিশ মেডিকল জার্নালে প্রকাশিত এক গবেষণায় জানা গেছে, সূর্যের আলো মস্তিষ্কের হরমোন নিয়ন্ত্রণকারী অংশ ‘হাইপোথ্যালামাস’-কে উদ্দীপ্ত করে টেস্টোস্টেরনের মাত্রা বাড়ায়।
গবেষকরা বলেন, “আমাদের দেহঘড়ি ‘সার্কাডয়ান রাইমস’ নামক জৈবিক প্রক্রিয়া পরিচালনা করে যা আমাদের মানসিকতা এবং কর্মশক্তির মাত্রাকে নিয়ন্ত্রণ করে।”
এমনকি একজন পুরুষ ঘুম থেকে জেগে ওঠার আগ থেকেই তার টেস্টোস্টেরনের মাত্রা তুঙ্গে থাকে। দিনের অন্যান্য সময়ের তুলনায় শতকরা ২৫ থেকে ৫০ ভাগ বেশি।
লন্ডনের সেইন্ট বার্থোলোমিওস হাসপাতালের নিউরোএন্ডোক্রিনোলজির অধ্যাপক অ্যাশলে গ্রোসম্যান বলেন, “এই বর্ধিত টেস্টোস্টেরন মাত্রার কারণে বেশিরভাগ পুরুষেরই সপ্তাহে দুই থেকে তিনবার যৌনাঙ্গ উত্থিত অবস্থায় ঘুম ভাঙতে পারে।”
দিন গড়ানোর সঙ্গে ধীরভাবে পুরুষের শরীরে টেস্টোস্টেরন তৈরি হতে থাকবে। কারণ মাংসপেশী গঠন এবং শুক্রাণু তৈরিতেও এই হরমোন প্রয়োজন হয়।
ছবি: রয়টার্স।
গোপন সমস্যা গোপন নয়
টেস্টোস্টেরন বাড়ায় যেসব খাবার
