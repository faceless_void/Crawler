ওয়েব অ্যাড্রেস পরিবর্তনের এই সিদ্ধান্তকে টেসলার প্রধান নির্বাহী ইলন মাস্কের সর্বশেষ এবং এখনও অপ্রকাশিত সম্ভাব্য 'মহাপরিকল্পনার' অংশ হিসেবে দেখছেন অনেকে, জানিয়েছে রয়টার্স।
এর আগে আরেক প্রতিষ্ঠান অ্যাপল যখন কেবল কম্পিউটার থেকে আওতা বাড়িয়ে মিউজিক, মোবাইলসহ বিভিন্ন দিকে ব্যবসা বাড়ায় তখন প্রতিষ্ঠানের নাম ‘অ্যাপল কম্পিউটার ইনকর্পোরেটেড’ থেকে বদলিয়ে ‘অ্যাপল ইনকর্পোরেটেড’ করা হয়।
সোমবার টেসলার ডোমেইন নাম এবং ঠিকানা আপডেট করা হয়েছে। ডোমেইন নেইম নিয়ন্ত্রক প্রতিষ্ঠান আইসিএনএন-এর মতে, ওয়েব ব্যবহারকারীদেরকে এখন স্বয়ংক্রিয়ভাবে নতুন ওয়েব ঠিকানা Tesla.com এ পাঠিয়ে দেওয়া হচ্ছে।
'ওয়েব অ্যাড্রেস পরিবর্তন কি প্রতিষ্ঠানের নাম পরিবর্তনের ইঙ্গিত কিনা' - এমন প্রশ্নের সরাসরি কোনো উত্তর দেয়নি প্রতিষ্ঠানটি।
বৈদ্যুতিক গাড়ি ছাড়াও টেসলা ঘরে ব্যবহার এবং ব্যবসায়ের জন্য 'স্টেশনারি স্টোরেজ' ব্যাটারি তৈরি করে থাকে। মাস্ক সম্প্রতি টেসলা আর সৌর প্যানেল সরবরাহকারী প্রতিষ্ঠান সোলারসিটি’র মধ্যে সংযুক্তির প্রস্তাব করেছেন। তিনি উভয় প্রতিষ্ঠানেরই বৃহত্তম শেয়ারধারী ও প্রধান।
