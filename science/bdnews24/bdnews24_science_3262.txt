নতুন এই ফিচারে গ্রাহককে বেশি বেশি ছবি তুলতে অনুপ্রেরণা দিচ্ছে ফেইসবুক। শুধু তাই না ছবিতে আরও বেশি ইফেক্ট ও ফিল্টার যোগ করতে উদ্বুদ্ধ করছে প্রতিষ্ঠানটি, জানিয়েছে বিবিসি।
নতুন আপেডেটের মাধ্যমে গ্রাহকরা শুধু একবার সোয়াইপ করেই ফেইসবুকের ক্যামেরা চালু করতে পারবেন। আর বন্ধুর সঙ্গে ব্যক্তিগতভাবে ছবি শেয়ার করতে পারবেন।
একই ধরনের স্ন্যাপচ্যাট ফিচারে গ্রাহক গ্যালারিতে ছবি যোগ করতে পারেন। কিন্তু তা ২৪ ঘণ্টা পর নিজে থেকেই মুছে যায়।
স্ন্যাপচ্যাটের ফিচার নকল করাটা নতুন কিছু নয়। সাম্প্রতিক সময়ে ফেইসবুকের ছবি শেয়ারিং অ্যাপ ইনস্টাগ্রাম, মেসেঞ্জার এবং হোয়াটসঅ্যাপেও একই ফিচার চালু করা হয়েছে।
সম্প্রতি স্ন্যাপচ্যাট-এর স্টোরিজ ফিচারও নকল করেছে ফেইসবুক। এরপর স্ন্যাপচ্যাট-এর ‘সিক্রেট চ্যাট’ ফিচার নকল করেছে ভাইবার
এবার ক্যামেরা ফিচার নকল করার বিষয়টি অস্বীকার করেছে ফেইসবুক। প্রতিষ্ঠানটির দাবী তারা ব্যবহারকারীর কাছ থেকেই এই ধারণা পেয়েছে।
ফেইসবুকের পণ্য ব্যবস্থাপক কনার হেইস বলেন, “আমাদের লক্ষ্য হল গ্রাহককে ফেইসবুকে আরও বেশি কিছু করতে দেওয়া এবং এটাই ছিল মূল অনুপ্রেরণা।”
বর্তমানে ১৮৬ কোটি গ্রাহক রয়েছে ফেইসবুকের।
