মার্কিন সংবাদ মাধ্যম সিএনবিসি-এর সঙ্গে এক সাক্ষাতকারে মাস্ক বলেন, তদন্তকারীরা এটি খুঁজে বের করেছেন কেন ফ্যালকন ৯ রকেটটি ১ সেপ্টেম্বর অগ্নিকাণ্ডে ভস্মীভূত হয়। পূর্বনির্ধারিত রুটিন প্রি-ফ্লাইট পরীক্ষার জন্য জ্বালানী সরবারহ করার সময় এই দুর্ঘটনা ঘটে।
ওই দুর্ঘটনায় ইসরায়েলের একটি ২০ কোটি মার্কিন ডলারের যোগাযোগ স্যাটেলাইট বিধ্বস্ত হয়, যেটি ছিল ১৪ মাসের মধ্যে ফ্যালকন ৯ বহরের দ্বিতীয় দুর্ঘটনা।
“আমি মনে করি আমরা সমস্যার তলানি পর্যন্ত যেতে পেরেছি”- বলেন মাস্ক। রকেটবিদ্যা ইতিহাসে এর আগে কেউ এমন ঘটনার সম্মুখীন হয়নি বলেও মন্তব্য করেন তিনি, জানিয়েছে রয়টার্স।
পরবর্তীতে প্রতিষ্ঠানটি কী মিশন চালু করবে সে বিষয়ে কিছু জানাননি মাস্ক। স্পেসএক্স ফ্লোরিডার কেনেডি স্পেস সেন্টারের নতুন একটি লঞ্চপ্যাড নাকি ক্যালিফোর্নিয়ার ভ্যান্ডেনবার্গ বিমান বাহিনী ঘাঁটির পশ্চিম উপকূল থেকে উড্ডয়ন সম্পন্ন করবে সেটিও নিশ্চিত নয়।
