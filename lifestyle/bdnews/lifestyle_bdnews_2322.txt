সম্প্রতি এর উত্তর জানার চেষ্টা চালিয়েছেন যুক্তরাষ্ট্রের একদল গবেষক।
পরিচয়ের কিছুদিনের মধ্যেই যেসব পুরুষ ও নারী প্রেমের সম্পর্কে জড়িয়ে পড়েন তারা বন্ধু থেকে প্রেমে পড়া যুগলদের তুলনায় শারীরিক সৌন্দর্যের দিক থেকে বেশি সদৃশ হয়ে থাকেন, বলছে তাদের নতুন গবেষণা।
উদাহরণ স্বরূপ, একে-অপরের সঙ্গে কয়েক মাস ধরে পরিচয় থাকলে আকর্ষণীয় নন এমন একজন নারীর সঙ্গে একজন আকর্ষণীয় পুরুষের প্রেমের সম্পর্ক গড়ে ওঠার সম্ভাবনা বেশি থাকে।
সাইকোলজিকাল সাইন্স নামক জার্নালে প্রকাশিত এই গবেষণায় বলা হয়, প্রথম পরিচয়ের এক মাসের মধ্যেই প্রেমের সম্পর্কে জড়িয়ে পড়া যুগলদের মধ্যে শারীরিক আকর্ষণের সঙ্গে পারষ্পরি সম্পর্ক রয়েছে।
গবেষণার সহ-লেখক, নর্থওয়েস্টার্ন ইউনিভার্সিটির মনোবিজ্ঞান বিভাগের অধ্যাপক এলি ফিনকেল বলেন, “এই গবেষণায় দেখা যায়, কারও সঙ্গে সম্পর্কে জড়ানোর আগে সেই ব্যক্তিকে আগে চিনতাম কি না তার উপর ভিত্তি করে বিয়ের বিষয়ে আমরা ভিন্ন ভিন্ন সিদ্ধান্ত নিয়ে থাকি।”
তিনি আরও বলেন, “যদি আমরা পরিচয়ের কিছুদিনের মধ্যে প্রেমের সম্পর্কে জড়িয়ে যাই, তবে সিদ্ধান্ত নেওয়ার ক্ষেত্রে শারীরিক আকর্ষণ একটি গুরুত্বপূর্ণ বিষয় হয়ে দাঁড়ায়। এবং আমরা শারীরিকভাবে নিজের মতো আকর্ষণীয় একজন মানুষের সঙ্গেই সম্পর্কে জড়াই।”
 “অপরদিকে যদি প্রেমের সম্পর্কে জড়ানোর পূর্বে একে-অপরের সঙ্গে পরিচয় থাকে কিংবা তাদের মধ্যে বন্ধুত্বের সম্পর্ক থাকে, তবে সেক্ষেত্রে শারীরিক সৌন্দর্যের গুরুত্ব কমে যায়। এবং সঙ্গীর সৌন্দর্যের দিকে থেকে সামঞ্জস্যপূর্ণ হওয়ার মাত্রা কমে আসে।” বলেন ফিনকেল।  
গবেষকরা ৬৭ জন ডেইটিং করছেন এবং ১০০ বিবাহিত দম্পতি, এরকম ১৬৭টি যুগলদের তথ্য নিয়ে এই গবেষণা করেন। যারা অঞ্চল ভিত্তিক বা দ্রাঘিমারেখা অনুসারে প্রেমের সম্পর্কে গবেষণার কাজে অংশ নিয়েছিলেন।
যুগলদের মধ্যে সম্পর্কের সবচেয়ে কম সময়সীমা ছিল তিন মাস। আর সর্বোচ্চ ৫৩ বছর। এবং গড়ে সম্পর্কের স্থায়ীত্ব ছিল আট বছর আট মাস।
ফলাফলে দেখা যায়, সম্পর্কে জড়ানোর আগে রোমান্টিক যুগলরা একে-অপরের সঙ্গে যত বেশি সময় ধরে পরিচিত ছিলেন, তাদের মধ্যে শারীরিক সৌন্দর্যের উপর ভিত্তি করে সম্পর্ক গড়ে ওঠার সম্ভাবনা ততটাই কম দেখা গেছে।
তবে ডেটিংয়ের বহু আগে থেকে যাদের মধ্যে জানাশোনা ছিল তাদের ক্ষেত্রে এই সম্ভাবনা আরও কম। সম্পর্ক বন্ধুত্ব থেকে প্রেমে গড়িয়েছে এমন যুগলদের পর্যবেক্ষণ করেও একই ‘প্যাটার্ন’ লক্ষ করেছেন গবেষকরা। অপরিচিতদের মধ্যে প্রেমের সম্পর্কের তুলনায় বন্ধু ও বান্ধবীর মধ্যে প্রেমের সম্পর্কে সৌন্দর্যের গুরুত্ব কম।
ছবি: রয়টার্স।
একসঙ্গে ঘুমাতে গেলে সম্পর্ক সুখের হয়
ছেলে-মেয়েতে বন্ধুত্ব
