বাংলাদেশ কৃষি বিশ্ববিদ্যালয়ে (বাকৃবি) পূর্ব শত্রুতার জের ধরে ছাত্রলীগের বিলুপ্ত কমিটির নেতা মনোয়ারুল ইসলাম নয়নকে ছুরিকাঘাত করেছেন শাকিল নামে স্থানীয় এক যুবক।
বাকৃবি: বাংলাদেশ কৃষি বিশ্ববিদ্যালয়ে (বাকৃবি) পূর্ব শত্রুতার জের ধরে ছাত্রলীগের বিলুপ্ত কমিটির নেতা মনোয়ারুল ইসলাম নয়নকে ছুরিকাঘাত করেছেন শাকিল নামে স্থানীয় এক যুবক।
রোববার (১৪ আগস্ট) বিশ্ববিদ্যালয়ের বোটানিকেল গার্ডেনের সামনে দুপুর আড়াইটার দিকে এ ঘটনা ঘটে।
ছুরির আঘাতে নয়নের বাম হাতে গুরুতর ক্ষত হয়। তাকে বিশ্ববিদ্যালয়ের হেলথ কেয়ার সেন্টারে প্রাথমিক চিকিৎসা দিয়ে ময়মনসিংহ মেডিকেল কলেজ হাসপাতালে পাঠানো হয়।
আহত নয়ন পশুপালন অনুষদের শেষবর্ষের শিক্ষার্থী। তিনি ছাত্রলীগের বাবু-সাইফুল কমিটির প্রচার ও প্রকাশনা সম্পাদক ছিলেন।
পরে সাদ হত্যাকাণ্ডে জড়িত থাকার অভিযোগে তাকে কমিটি থেকে অব্যাহতি দেওয়া হয়। এরপর ছাত্রলীগের কমিটি বিলুপ্ত হওয়ার পর থেকে তিনি ক্যাম্পাসের বাইরে মেসে থাকেন।
হামলাকারী শাকিল মিয়া (২৩) ক্যাম্পাস সংলগ্ন করিম ভবন এলাকার মো. সুলতানের ছেলে।
বাকৃবির প্রক্টর অধ্যাপক ড. এ কে এম জাকির হোসেন বাংলানিউজকে বলেন, হামলাকারীকে আটক করতে পুলিশকে বলা হয়েছে।
