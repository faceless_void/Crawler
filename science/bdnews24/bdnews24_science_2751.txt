এরই মধ্যে এই দম্পতির একমাত্র কন্যা ম্যাক্স-এর বয়স এক বছর পেরিয়েছে।
নিজের ফেইসবুক পোস্টে বিশ্বের সবচেয়ে বড় সামাজিক মাধ্যমটির প্রধান বলেন, “ম্যাক্স-কে নিয়ে আমাদের কঠিন অভিজ্ঞতার পর, আমরা নিশ্চিত ছিলাম না কি আশা করা উচিত বা আমরা আরেকটি সন্তান নিতে প্রস্তুত কিনা।”
“যখন আমি আর প্রিসিলা প্রথম জানতে পারলাম, প্রিসিলা আবারও গর্ভবতী হয়েছে, আমাদের প্রথম আশা ছিল শিশুটি স্বাস্থ্যবান হবে। আমার পরবর্তী আশা হচ্ছে শিশুটি মেয়ে হবে। একটি বোন (ম্যাক্স-এর) থাকার চেয়ে বড় উপহার আমি ভাবতে পারি না আর আমি অনেক খুশি যে ম্যাক্স আর আমাদের নতুন সন্তান একে অপরের বোন হতে যাচ্ছে।”
“তাদের (প্রিসিলা ও তার বোনদের) নিজেদের মধ্যে অনেক মজার বিষয় আছে… যেগুলো শুধু তারা বোনরাই বুঝতেই পারে”, যোগ করেন তিনি।
