উপকরণ : নুডলস, জেলি, সাগুদানা, দুধ, আইসক্রিম, চিনি, বরফ, বাদাম ও কাঠবাদাম।
পদ্ধতি : প্রথমে নুডলস তৈরি করে নিতে হবে। তারপর পরিমাণমতো দুধ, আইসক্রিম, চিনি, বরফ, বাদাম ও কাঠবাদাম নিয়ে ব্লেন্ডারে মিশ্রণ করতে হবে। পরে সবগুলোর মিশ্রণ ও নুডলস দিয়ে আপনার পছন্দের রংয়ের জেলি দিয়ে পরিবেশন করুন।
উপকরণ : ভেজা চিঁড়া   আধ কাপ। মিষ্টি দই আধা কাপ। আখের গুড় ২ টেবিল-চামচ। লেবুর রস ২ টেবিল-চামচ। পানি ২ কাপ।
পদ্ধতি : লেবুর রস ছাড়া সব উপকরণ একসঙ্গে ব্লেন্ডারে ব্লেন্ড করে নিন। এরপর লেবুর রস মিশিয়ে বরফ দিয়ে পরিবেশন করুন।

উপকরণ : দই এক কাপ। চিনি ২ টেবিল-চামচ। ঠান্ডা পানি ১ কাপ। বরফ কুচি ও গোলাপজল।
