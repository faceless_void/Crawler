পবিত্র ঈদ উল আজহা উপলক্ষে জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) ছুটি শুরু হচ্ছে বুধবার (৭ সেপ্টেম্বর)। টানা ১৩ দিন ছুটি শেষে আগামী ২০ সেপ্টেম্বর (মঙ্গলবার) ফের খুলবে জবি।
জগন্নাথ বিশ্ববিদ্যালয়: পবিত্র ঈদ উল আজহা উপলক্ষে জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) ছুটি শুরু হচ্ছে বুধবার (৭ সেপ্টেম্বর)। টানা ১৩ দিন ছুটি শেষে আগামী ২০ সেপ্টেম্বর (মঙ্গলবার) ফের খুলবে জবি।
বিশ্ববিদ্যালয়ের রেজিস্ট্রার প্রকৌশলী ওয়াহিদুজ্জামান সোমবার (৫ সেপ্টেম্বর) বাংলানিউজকে এ তথ্য জানান।
তিনি বলেন, ঈদ উল আজহা উপলক্ষে আগামী ৭ সেপ্টেম্বর থেকে ১৯ সেপ্টেম্বর (সোমবার) পর্যন্ত বিশ্ববিদ্যালয়ের সকল ক্লাস-পরীক্ষা ও প্রশাসনিক কার্যক্রম বন্ধ থাকবে। ২০ সেপ্টেম্বর মঙ্গলবার থেকে পুনরায় বিশ্ববিদ্যালয়ের কার্যক্রম শুরু হবে।
তবে বিশ্ববিদ্যালয়ে ছুটি চললেও ২০১৬-২০১৭ শিক্ষাবর্ষের ভর্তি প্রক্রিয়া যথানিয়মে চলবে বলে জানান রেজিস্ট্রার।
এদিকে, হলের দাবিতে শিক্ষার্থীদের একমাসেরও বেশি সময় টানা আন্দোলনের ফলে অনেক বিভাগেই ক্লাস-পরীক্ষা বন্ধ রয়েছে। ফলে এবার প্রশাসনিক ছুটির আগেই অনেকে শিক্ষার্থী বাড়ির পথে ছুটেছেন। 
