বিশ্ব সাহিত্য কেন্দ্রের মাধ্যমে মাধ্যমিক শিক্ষা প্রতিষ্ঠানের শিক্ষার্থীদের মধ্যে পাঠাভ্যাস উন্নয়ন কর্মসূচি বাস্তবায়ন করছে মাধ্যমিক ও উচ্চ শিক্ষা অধিদপ্তর। বাস্তবায়িত সংশোধিত দ্বিতীয় প্রকল্পের ব্যয় ধরা হয়েছে প্রায় সাড়ে ৩ হাজার কোটি।
ঢাকা: বিশ্ব সাহিত্য কেন্দ্রের মাধ্যমে মাধ্যমিক শিক্ষা প্রতিষ্ঠানের শিক্ষার্থীদের মধ্যে পাঠাভ্যাস উন্নয়ন কর্মসূচি বাস্তবায়ন করছে মাধ্যমিক ও উচ্চ শিক্ষা অধিদপ্তর। বাস্তবায়িত সংশোধিত দ্বিতীয় প্রকল্পের ব্যয় ধরা হয়েছে প্রায় সাড়ে ৩ হাজার কোটি।   সেকেন্ডারি এডুকেশন কোয়ালিটি অ্যান্ড অ্যাকসেস এনহান্সমেন্ট প্রজেক্টের চারটি কম্পোনেন্টের আওতাধীন ১৩টি সাব কম্পোনেন্টে অতিরিক্ত শ্রেণি শিক্ষক নিয়োগ, শিক্ষা সচেতনতা ও সামাজিক অংশগ্রহণ, নিয়োগ পাওয়া অতিরিক্ত শ্রেণি শিক্ষকরা যথাযথভাবে দায়িত্ব পালন করছেন কি না এবং কাঙ্ক্ষিত ফলাফল পাওয়া যাচ্ছে কি না তা মনিটরিং করা হবে।   আরও রয়েছে শিক্ষা সচেতনতা ও সামাজিক অংশগ্রহণের আওতায় উপজেলা পর্যায়ে উদ্বুদ্ধকরণ সভা, প্রতিষ্ঠান পর্যায়ে শিক্ষক, অভিবাবক, ইউনিয়ন বা ক্লাস্টার পর্যায়ে শিক্ষক, পরিচালনা, ব্যবস্থাপনা কমিটির সদস্যদের মধ্যে আলোচনা ও মতবিনিময় করা।
