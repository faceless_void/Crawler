রাজশাহী প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ের (রুয়েট) ২০১৬-১৭ অর্থবছরের জন্য ৫১ কোটি ২০ লাখ টাকার বাজেট পাস হয়েছে।
রাজশাহী: রাজশাহী প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ের (রুয়েট) ২০১৬-১৭ অর্থবছরের জন্য ৫১ কোটি ২০ লাখ টাকার বাজেট পাস হয়েছে।
বুধবার (২৯ জুন) বিশ্ববিদ্যালয়ের ৭৮তম সিন্ডিকেট সভায় ভিসি অধ্যাপক মোহা. রফিকুল আলম বেগের সভাপতিত্বে এ বাজেট অনুমোদন হয়।
বিকেলে রুয়েটের জনসংযোগ কর্মকর্তা জিএম মুর্তজা বিষয়টি নিশ্চিত করেন।
বাজেটের ৪৮ কোটি ২০ লাখ টাকা বিশ্ববিদ্যালয় মঞ্জুরি কমিশনের (ইউজিসি) দেওয়া সরকারি বরাদ্দ এবং বাকি তিন কোটি টাকা নিজস্ব উৎস থেকে আয় ধরা হয়েছে।
পুরো বাজেটে আলাদাভাবে শিক্ষা খাতে কোনো বরাদ্দ রাখা হয়নি।
শিক্ষক, কর্মকর্তা ও কর্মচারীদের বেতন ও ভাতা খাতে ২৯ কোটি ২২ লাখ, পেনশন খাতে ৬ কোটি ৫০ লাখ, মুলধন মঞ্জুরি খাতে ৩ কোটি ৬০ লাখ এবং অন্যান্য খাতে ১১ কোটি ৭০ লাখ টাকা বরাদ্দ রাখা হয়েছে।
এদিকে সিন্ডিকেট সভায় বিগত অর্থবছরের ৪৫ কোটি ১৫ লাখ টাকার সংশোধিত বাজেটও অনুমোদন করা হয়।
সংশোধিত বাজেটের ৩৮ কোটি ১৫ লাখ টাকা বিশ্ববিদ্যালয় মঞ্জুরি কমিশনের মাধ্যমে সরকারি বাজেট এবং ৭ কোটি টাকা নিজস্ব আয় থেকে অর্জিত দেখানো হয়েছে।
