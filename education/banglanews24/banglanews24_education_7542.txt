ঢাকা: বেসরকারি তিন বিশ্ববিদ্যালয়ের কার্যক্রম সম্পূর্ণ অবৈধ হওয়ায় এসব বিশ্ববিদ্যালয় থেকে পিএইচডি নেওয়া থেকে বিরত থাকতে বলেছে বিশ্ববিদ্যালয় মঞ্জুরি কমিশন (ইউজিসি)।
বিশ্ববিদ্যালয়গুলো হলো- ইন্টারন্যাশনাল কালচার ইউনিভার্সিটি, স্টাডি ওয়ার্ল্ড ইউনিভার্সিটি ও সেডস ইউনিভার্সিটি।
সোমবার (০৫ জুন) সরকারি তথ্য বিবরণীতে জানানো হয়, এ তিন বিশ্ববিদ্যালয় অর্থের বিনিময়ে ভুয়া পিএইচডি ডিগ্রি দিচ্ছে।
বিশ্ববিদ্যালয় মঞ্জুরি কমিশন নিবন্ধন ব্যতীত এসব বিশ্ববিদ্যালয়ের কার্যক্রম সম্পূর্ণ অবৈধ। সম্প্রতি বিশ্ববিদ্যালয় মঞ্জুরি কমিশন এসব বিশ্ববিদ্যালয়ের বিরুদ্ধে আইনানুগ ব্যবস্থা নিতে সুপারিশ করেছে।
এসব অবৈধ প্রতিষ্ঠান থেকে টাকার বিনিময়ে ভুয়া ডিগ্রি সনদ ক্রয় করে অনেকেই সরকারি ও বেসরকারি বিভিন্ন প্রতিষ্ঠানে চাকরি করে প্রতারণার আশ্রয় নিচ্ছে বলেও মঞ্জুরি কমিশন শিক্ষা মন্ত্রণালয়ে প্রতিবেদন দিয়েছে।
কার্যক্রম অবৈধ হওয়ায় এসব বিশ্ববিদ্যালয়ের পিএইচডি নেওয়া থেকে সংশ্লিষ্ট সবাইকে সাবধান থাকতে বিশ্ববিদ্যালয় মঞ্জুরি কমিশন ও শিক্ষা মন্ত্রণালয় পরামর্শ দিয়েছে।
