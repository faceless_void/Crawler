‘সাইক্লিং পাথওয়েইস টু মার্স’ নামের চলচ্চিত্রটির দৈর্ঘ্য ১০ মিনিট। এতে অলড্রিনের হলোগ্রাফ ব্যবহারকারীকে মঙ্গল গ্রহে ভ্রমণের অনুভূতি অনুবাদ করবে, জানিয়েছে বিবিসি।
অলড্রিনের পরিকল্পনায় চাঁদ ও পৃথিবীর ব্যবহারও রয়েছে। এখান থেকে লাল গ্রহটিতে যাত্রা করবে মানুষ। সাধারণত এই গ্রহে যেতে বা ফিরে আসতে ছয় মাস সময় লাগে।
বিবিসিকে দেওয়া এক সাক্ষাৎকারে অলড্রিন বলেন, তিনি আশা করছেন চলচ্চিত্রটি সরকারকে মঙ্গলে যেতে একটি নির্দিষ্ট পরিকল্পনায় এগুতে সহায়তা করবে।
“আপনার সবকিছু করার সামর্থ্য নেই। কারণ এটি বাজেটের বাইরে চলে যাচ্ছে আর আমরা কোথাও যেতে পারছি না,” বলেন অলড্রিন।
মঙ্গলে মানুষ নিতে কাজ করছে মার্কিন ধনকুবের ইলন মাস্কের মহাকাশ গবেষণা প্রতিষ্ঠান স্পেসএক্স। তার এই বিনিয়োগকে স্বাগত জানিয়েছেন অলড্রিন। কিন্তু তিনি বলেন মঙ্গল গ্রহে বসতি স্থাপনে অবশ্যই সরকারের দিক নির্দেশনা দেওয়া উচিত।
