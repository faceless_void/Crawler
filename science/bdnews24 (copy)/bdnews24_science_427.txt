এক ফেইসবুক পোস্টে দেশটির ল্যান্ড ট্রান্সপোর্ট অথরিটি বলে, "ট্রাকের সঙ্গে সংঘর্ষের আগে পরীক্ষামূলক গাড়িটি লেইন পরিবর্তন করছিল।" 
এই প্রযুক্তির নির্মাতা ও পরীক্ষা পরিচালন প্রতিষ্ঠান নুতোনমি জানায়, সে সময় গাড়িটিতে দুইজন প্রকৌশলী ছিলেন আর এটি ধীরগতিতে চলছিল।
বিশ্বব্যাপী বিভিন্ন দেশ স্বচালিত গাড়ি প্রযুক্তি উন্নয়নে উৎসাহ দিচ্ছে। সীমিত জায়গা আর কর্মীসংখ্যা থাকায় সিঙ্গাপুরও আশা করছে স্বচালিত গাড়ি তাদের দেশের অধিবাসীদের এক গাড়ি শেয়ার করা আর গণপরিবহন ব্যবহারে প্রেরণা যোগাবে, বলা হয়েছে রয়টার্স-এর প্রতিবেদনে।
সিঙ্গাপুরের পশ্চিমাঞ্চলে চারটি গ্রুপ স্বচালিত গাড়ি নিয়ে পরীক্ষা চালাচ্ছে। চলতি বছর সেপ্টেম্বরে অংশগ্রহণকারীদের দেওয়া রুট দ্বিগুণ হয়ে ১২ কিমি দীর্ঘ হয়েছে। ওই মাসেই রাইড-হেইলিং সেবাদাতা প্রতিষ্ঠা গ্র্যাব কিছু ব্যবহারকারীকে অ্যাপের মাধ্যমে স্বচালিত গাড়ি ভাড়া করার সুযোগ দিতে নুতোনমি'র সঙ্গে চুক্তিবদ্ধ হয়েছে।  
সম্প্রতি যুক্তরাজ্যের প্রথমবারের মতো সাধারণ মানুষদের মধ্যে একটি চালকবিহীন গাড়ি পরীক্ষা চালানো হয়েছে। বাকিংহামশায়ারের মিল্টন কেনিস-এলাকায় ওই পরীক্ষা চালানো হয়।
দুই আসনবিশিষ্ট এই বৈদ্যুতিক গাড়ি মিল্টন কেনিস-এ রেলওয়ে স্টেশনের ফুটপাতে এক কিলোমিটার পথ পাড়ি দিয়েছে।
