দৃঢ় চাপে এই কাঁচের ভাঙ্গা টুকরোগুলো কয়েক সেকেন্ডের মধ্যে জোড়া লেগে যেতে পারে। আর কয়েক ঘণ্টার মধ্যেই এটি আগের শক্ত অবস্থায় ফিরে আসে।
এই গবেষণা এখনও প্রাথমিক পর্যায়ে আছে। জাপানি দৈনিকটির ভাষ্য হচ্ছে- “একটি আশার দিকে এটি মাত্রই একটি ধাপ।” 
শিল্পখাতের বিস্তৃত পরিসরে এই ‘স্ব-মেরামতী’ কাঁচ একটি আকর্ষণীয় বিষয় হবে বলে উল্লেখ করা হয়েছে বিজনেস ইনসাইডার-এর প্রতিবেদনে। স্মার্টফোন খাতে এর ব্যবহারে নিজে থেকেই ঠিক হয়ে যাবে ডিসপ্লে।
এর আগেও স্মার্টফোন খাতে স্বমেরামতকারী বস্তু দেখা গেছে। এলজি জি ফ্লেক্স আর জি ফ্লেক্স ২-এর পেছনে প্লাস্টিকে চাবি বা এর মতো কোনো কিছু থেকে হালকা দাগ পড়লে তা নিজে থেকেই ঠিক কর ফেলত প্লাস্টিকটি। 
