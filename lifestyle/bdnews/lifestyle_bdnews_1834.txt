ডায়বেটিস এবং ত্বকের উপর এর প্রভাব নিয়ে স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটের প্রতিবেদনে জানানো হয়, আমাদের দেহের সবচেয়ে বড় অঙ্গ হল ত্বক। যা সমগ্র দেহকে আবরণ দেয়। বিশাল এই অঙ্গের ভিন্ন ভিন্ন জায়গায় ভিন্ন ভিন্ন রূপে সংক্রামণ দেখা দিতে পারে।
কেনো এই ত্বকের সমস্যা
ভারতীয় চর্মরোগ বিশেষজ্ঞ ড. অপ্রতীম গোয়েল জানান, ডায়াবেটিসে আক্রান্ত রোগীরা চর্ম রোগে ভোগেন। গরমকালে এই রোগের প্রকোপ বেশ বেড়ে যায়।
গরম এবং আর্দ্রতার কারণে সবারই কম বেশি চর্মরোগ হতে পারে। তবে যারা বহুমূত্ররোগগ্রস্ত তাদের ডায়াবেটিসও একটা কারণ। অপরদিকে ডায়াবেটিসের কারণে ক্ষতও সহজে শুকায় না। তাই সমস্যা জটিল আকার ধারণ করে। এছাড়াও যাদের দীর্ঘদিন ধরে ডায়াবেটিস আছে তাদের ত্বক অন্যদের থেকে শুষ্ক হয়ে যায় এবং চুলকানির সমস্যা দেখা যায়।
ডায়াবেটিস বিশেষজ্ঞ ড. প্রদীপ গাজী বিষয়টি ব্যাখ্যা করেন এভাবে, “রক্তে থাকা উচ্চ মাত্রার শর্করা ছত্রককে ত্বকে বসবাস করার মতো উপযুক্ত পরিবেশ দেয়। ফলে ত্বকে ছত্রাক ও অন্য রোগ জীবাণুরা বসবাসের সুযোগ পায়।”   
ড. গাজী আরও জানান, রক্তের শর্করার পরিমাণ যদি নিয়ন্ত্রণের বাইরে চলে যায় তবে ত্বকের সিবেইসাস ও ঘাম গ্রন্থিতে প্রভাব ফেলে। ফলে হাত ও পা চুলকায়। যা পরে ছত্রাকের বসবাসের স্থান তৈরিতে সহায়তা করে।
চিকিৎসা
বিশেষজ্ঞরা বলেন, ডায়াবেটিসে আক্রান্ত রোগীদের নিয়মিত ত্বক পরীক্ষা করে দেখতে হবে হবে। প্রতিদিন দেখতে হবে ত্বকে কোনো ঘামাচি, ফোড়া, ফুসকুড়ি ধরনের কিছু হয়েছে কি-না।
বিশেষ করে ত্বকের যেসব জায়গায় ইনসুলিন দেওয়া হয় সে সব জায়গায়।
চর্মরোগ থেকে বাঁচতে হলে রক্তের শর্করার মাত্রাও নিয়ন্ত্রণ করতে হবে। বিশেষজ্ঞরা বলেন, এটাই সর্বোত্তম পন্থা। তাছাড়া ডায়াবেটিকদের স্বাস্থ্যকর খাবার খাওয়া, নিয়মিত ব্যায়াম করার মতো সাধারণ নিয়মগুলো মেনে চলতে হবে।
এতেও যদি কাজ না হয় তবে বিশেষজ্ঞ চিকিৎসকের পরামর্শে ওষুধ প্রয়োগ করতে হবে।
