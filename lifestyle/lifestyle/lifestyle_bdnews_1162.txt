এই বিষয়ে রাজধানী’র রেনেসাঁ হাসপাতালের মেডিকেল অফিসার আহমেদ আল সাজিদ বলেন, “সত্যি বলতে এসব চিকিৎসার কোনো ভিত্তি নেই। আমাদের দেশে যৌনশিক্ষার যথেষ্ট অভাব রয়েছে।”
তিনি আরও বলেন, “এসব ওষুধে যে কাজ হয় না তা নয়। তবে অবশ্যই পার্শ্বপ্রতিক্রিয়া থাকে। আর একবার ওষুধ নির্ভর হয়ে গেলে স্বাভাবিক প্রক্রিয়ায় ফিরে আসা সম্ভব হয় না।”
তাই কোনো বিজ্ঞাপনে না ভুলে আগে নিশ্চিত হওয়া উচিত আসলেই কোনো সমস্যা আছে কিনা।
অভিজ্ঞতার ঝুলি থেকে এই ডাক্তার বলেন, “চর্ম ও যৌন বিষয়ে পোস্ট গ্র্যাজুয়েশন ট্রেনিং চলার সময় এই ধরনের বহু রোগির সঙ্গে আমার সাক্ষাত হয়েছে। সবারই প্রায় একই সমস্যা। পেনিস ছোট, আগা মোটা গোড়া চিকন, দ্রুত বীর্যপাত ইত্যাদি। আর এসব সমস্যা নিয়ে যারা আসত তাদের বেশিরভাগেরই বয়স ২০ থেকে ২৮ বছরের মধ্যে। আসলে যারা নিয়মিত যৌনজীবনে অভ্যস্ত বা অনেকদিন বিবাহিত জীবন পার করছেন, তারপর কোনো সমস্যা হলে তখ্নই আমরা গুরুত্ব দেই। এছাড়া বাকি যারা আসেন তাদের জন্য আমারা থেরাপির ব্যবস্থা করি।”
যৌনবিষয় নিয়ে ভুল ধারণা, প্রতিকার, চিকিৎসা নিয়ে বিস্তারিত জানান এই চিকিৎসক।
ভুল ধারণা কাটান
আমাদের দেশের বেশিরভাগ পুরুষই মনে করেন তাদের পুরুষাঙ্গ ছোট। তার যৌনকাজে স্থায়িত্ব কম। এটা ‘বাংলাদেশি সিনড্রম’ নামেও পরিচিত। আর এই ধারণার জন্মানোর জন্য দায়ী হচ্ছে এক্সরেটেড মুভিগুলো।
এই উপমহাদেশের মানুষের দৈহিক গঠন হিসেবে বেশিরভাগ পুরুষের যৌনাঙ্গের গড় আকার লম্বায় হয় চার থেকে ছয় ইঞ্চি। আর নারীর যৌনাঙ্গের অনুভুতিপ্রবণ এলাকার গভীরতা হচ্ছে চার ইঞ্চি, বাকি অংশে তেমন অনুভূতি কাজ করে না। তাই ছোট পুরুষাঙ্গ থাকলেও যে কোনো পুরুষ তার নারী সঙ্গীকে আনন্দ দিতে সক্ষম হবেন।
দ্বিতীয় যে বিষয় নিয়ে বেশিরভাগ পুরুষ দুশ্চিন্তায় ভোগেন তা হচ্ছে স্থায়িত্ব। আসলে বিষয় হচ্ছে যে কোনো বিষয়ে পারদর্শী হতে সময় লাগে। তাই যারা বিবাহিত বা নিয়মিত যৌনজীবনে অভ্যস্ত তাদের যদি হঠাৎ দ্রুত বীর্যপাত শুরু হয় তাহলে চিকিৎসার প্রয়োজন হতে পারে।
মাস্টারবেশন বা হস্তমৈথুনে কোনো ক্ষতি হয় না। তবে অতিরিক্ত করলে বা চাপ প্রয়োগ করে করলে পুরুষাঙ্গের ভেতরে ক্ষতি হতে পারে। যাকে ডাক্তরি ভাষায় বলা হয় ‘পেনাইল ফ্র্যাকচার’। পুরুষাঙ্গের ভেতরে অনেক নালী আছে। দৃঢ় হওয়ার সময় এসব নালী প্রসারিত হয়ে রক্ত চলাচল দ্রুত হয়। এতে পুরুষাঙ্গ দৃঢ় হতে থাকে। এই অবস্থায় বেকায়দায় চাপ পড়ে রক্তনালীগুলো ক্ষতিগ্রস্ত হলে প্রচণ্ড ব্যথা হতে পারে। অতিরিক্ত চাপ প্রয়োগ করে হস্তমৈথুন করলেও এটা হতে পারে। তখন পুরুষাঙ্গ দৃঢ় হওয়ার ক্ষমতা হারাতে পারে।
এ কারণে ক্রিম বা লোশন ব্যবহার করলে চাপ পড়ার সম্ভাবনা থাকে না। তবে হস্তমৈথুন কম করাই ভালো।
চিকিৎসা
মানসিক চাপ, অনিদ্রা, স্ত্রীর সঙ্গে সম্পর্ক ভালো না যাওয়া, পর্ন ছবি দেখায় আসক্তি, ‍ধূমপান ইত্যাদি কারণে যৌন কাজে সমস্যা হতে পারে।
ডা. সাজিদ বলেন, “বিয়ে ছাড়া সঙ্গীর সঙ্গে কোথাও দৈহিক সম্পর্ক করতে গেলে মানসিক চাপ অনুভূত হতেই পারে। কেউ এসে পড়লো কিনা, তাড়াতাড়ি করতে হবে— এই ধরনের চিন্তা থেকে দ্রুত বীর্যপাত হয়ে যায়। এই একই কারণে রেড অ্যালার্ট এরিয়াতে ‍গিয়েও অনেক পুরুষ একই সমস্যায় ভোগন। আমরা এরকম অনেক রোগী পাই।”
আবার বিয়ের প্রথম অবস্থায় মানসিক চাপ থাকে স্ত্রীকে সুখ দিতে পারবেন কিনা। দুশ্চিন্তা থেকে অনেকসময় দৃঢ় হওয়াতে সমস্যা হয়। আবার হলেও দ্রুত বীর্যপাত হয়ে যেতে পারে।
তাই ডা. সাজিদ বলেন, “প্রথমেই জানা দরকার যারা সমস্যায় ভুগছেন তাদের এই ধরনের কোনো মানসিক সমস্যা চলছে কিনা। আসলে পুরো বিষয়টা সঙ্গীর সঙ্গে যত দ্রুত সুন্দরভাবে নিষ্পত্তি করে নিতে পারবেন, দুজনের মধ্যে যত ভালো সমঝোতা থাকবে, তাদের যৌনজীবন ততই আনন্দের হবে।”
তিনি আরও বলেন, “এজন্য যারা নিয়মিত যৌনজীবনে অভ্যস্ত নয় তাদের ক্ষেত্রে আমরা তেমন কোনো চিকিৎসা দেই না। খুব বেশি হলে ভিটামিন ক্যাপসুল ট্যাবলেট দিয়ে থাকি। তবে যাদের নিয়মিত যৌনজীবন আছে তাদেরকে আমরা সেক্স কাউন্সেলিং করি। আসলে নিয়মিত সুন্দর জীবন কাটানো, পুষ্টিকর খাওয়া দাওয়া, সঠিক নিয়মে রাতে ঘুম, দুঃশ্চিন্তা মুক্ত থাকা, সঙ্গীর সঙ্গে মধুর সম্পর্ক— সব মিলিয়েই যৌনজীবন সুন্দর হয়। এটা কোনো নির্দিষ্ট একটা বিষয়ের উপর নির্ভর করে না।”
প্রতিকার
অতিরিক্ত ধূমপান অবশ্যই যৌনস্বাস্থ্যের ক্ষতি করে। কারণ এটা পরীক্ষিত যে, ধূমপানের ফলে রক্তনালী সরু হয়ে যায়। ফলে রক্তসঞ্চালনে বাধা পড়লে পুরুষাঙ্গ দৃঢ় হতে সমস্যা হবেই।
সুস্থ জীবনযাপন সুন্দর যৌনজীবন দিতে পারে। নানান ধরনের খাবার যেমন গাজর, আঙুর, ডিম, মধু, পেঁয়াজ এসব ‘সেক্স ড্রাইভ’ বাড়ায়। ভিটামিন ই এবং সি যৌনাকাঙ্ক্ষা বাড়াতে সাহায্য করে।
ডা. সাজিদ বলেন, “সমস্যা মনে করলে ডাক্তারের পরামর্শে ভিটামিন ই ক্যাপসুল খেলে উপকার পাওয়া যেতে পারে।”
আসলেই সমস্যা আছে কিনা বোঝার উপায়
ডা. সাজিদ বলেন, “মর্নিং গ্লোরি’ বোঝেন? ঘুমের ভেতর ভোর রাতের দিকে পুরুষাঙ্গ আপনাআপনি দৃঢ় হওয়াকে বলা হয় ‘মর্নিং গ্লোরি’। অনেকসময় এর কারণে সকালে ঘুমের পর প্রস্রাব করতেও বেশ চাপ দিতে হয়। আর এটা যদি হয় তো সাধারণভাবে বুঝতে হবে আপনার সব ঠিক আছে।”
তিনি আরও বলেন, “তারপরও যদি মনে সন্দেহ থাকে তবে ডাক্তারের পরামর্শে শরীরে টেস্টোস্টেরন ও প্রোল্যাকটিন হরমোনের মাত্রা পরীক্ষা করে দেখা যেতে পারে। স্বাভাবিকের চাইতে এই মাত্রা কম বা বেশি থাকলে চিকিৎসকের পরামর্শ অনুযায়ী চলতে হবে।”
স্থানভেদে এই পরীক্ষা দুটি করাতে খরচ হতে পারে এক থেকে দুই হাজার টাকা।
তবে থাইরয়েড সমস্যা বা ডায়াবেটিস থাকলে অন্য কথা। এক্ষেত্রে ডায়াবেটিস নিয়ন্ত্রণ আর থাইরয়ডের চিকিৎসা নিতে হবে। কারণ এই দুটি সমস্যার কারণে যৌনজীবনে প্রভাব ফেলে।
স্থায়িত্ব বাড়ানোর ব্যায়াম
যৌনকাজে স্থায়িত্ব বাড়ানোর জন্য ডা. সাজিদ সহজ একটি ব্যায়ামের কথা উল্লেখ করে। সেটা হচ্ছে, প্রস্রাব বেগ আটকানোর জন্য নিম্নাঙ্গে যেভাবে চাপ প্রয়োগ করে রাখা হয় সেরকম চাপ প্রয়োগ করে এক থেকে দশ পর্যন্ত গুনতে হবে। তারপর ছেড়ে দিয়ে আবার চাপ প্রয়োগ করে দশ পর্যন্ত গুনতে হবে। এটা যে কোনো জায়গায়, যে কোনো সময়, বসে, শুয়ে, দাঁড়িয়ে, কোথাও যেতে যেতে- অর্থাৎ সময় পেলেই এই ব্যায়াম চালিয়ে গেলে স্থায়িত্ব বাড়ানো সম্ভব।
এছাড়া সহবাসের স্থায়িত্ব বাড়ানোর জন্য সঙ্গীর ভূমিকাও গুরুত্বপূর্ণ।
ডা. সাজিদ বলেন, “প্রাকৃতিকভাবেই মেয়েদের যৌনাকাঙ্ক্ষা বেশি। তাদের উঠতেও দেরি হয়। তাই স্ত্রী বা প্রেমিকা যেই হোক তাকে পুরাপুরি তৈরি করে নি্য়েই সহবাসে যাওয়া উচিত। আরেকটা বিষয় হচ্ছে এটা এমনই একটি খেলা যেখানে উভয়কেই হারতে হয়। আর এই খেলায় হারের মাঝেই আছে জয়ের আনন্দ।”
আপনি স্টান্ট ম্যান নন
“এক্সরেইটেড ছবিগুলোতে মডেল হিসেবে বেছে বেছে তাদেরকেই অভিনয় করানো হয় যাদের পুরুষাঙ্গ মোটা ও লম্বা। আর অনেকক্ষণ ধরে যে বিষয়টা দেখানো হয় তা যে কোনো অ্যাকশন মুভির মতোই অনেকবার শুটিং করে ধারণ করা হয়।” বললেন ডা. সাজিদ।
তাই এই চিকিৎসকের পরামর্শ হচ্ছে “দিস পারফরমেন্স ডান বাই প্রফেশনাল, ডু নট ট্রাই দিস ইন হোম। মানে ওই সব ছবি দেখে মাথা খারাপ করার দরকার নেই। বরং আপনার যা আছে তাই নিয়েই আত্মবিশ্বাসী হোন। তাতেই অনেক সমস্যা সমাধান হয়ে যাবে।”
কনডম ব্যবহারের ভুলগুলো
সঠিক অন্তর্বাস
পুরুষের যৌন প্রবৃত্তি বাড়ায় মসলাদার খাবার
যৌন তাড়না কমানোর খাবার
