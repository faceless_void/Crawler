ঢাকার কেরানীগঞ্জের একমাত্র ইংরেজি মাধ্যম স্কুল ‘লন্ডন স্কুল অব ইংলিশ’ ২০১৭ ইং শিক্ষাবর্ষে প্রতি শ্রেণির শিক্ষার্থীদের জন্য এক লাখ টাকা করে মোট ১০ লাখ টাকার শিক্ষাবৃত্তির ঘোষণা দিয়েছে।
ঢাকা (কেরানীগঞ্জ): ঢাকার কেরানীগঞ্জের একমাত্র ইংরেজি মাধ্যম স্কুল ‘লন্ডন স্কুল অব ইংলিশ’ ২০১৭ ইং শিক্ষাবর্ষে প্রতি শ্রেণির শিক্ষার্থীদের জন্য এক লাখ টাকা করে মোট ১০ লাখ টাকার শিক্ষাবৃত্তির ঘোষণা দিয়েছে।
তবে এ বৃত্তির জন্য শিক্ষার্থীদের স্কুল কর্তৃক আয়োজিত বৃত্তি পরীক্ষায় অংশ নিতে হবে।
স্কুলটির প্রতিষ্ঠাতা চেয়ারম্যান মো. ইয়ানুস মিয়া বাংলানিউজকে জানান, যেকোনো শিক্ষার্থী এ বৃত্তি পরীক্ষায় অংশ নিতে পারবে। এজন্য তাদের দুই কপি পাসপোর্ট সাইজ ছবি ও জন্ম নিবন্ধন সার্টিফিকেটসহ নিবন্ধন ফরম পূরণ করতে হবে।
তিনি জানান, অনলাইনে নিবন্ধন ফরম পূরণ করা যাবে http://www.lse-bd.com ওয়েবসাইট ও lse.dhaka@gmail.com মেইলের মাধ্যমে।
ফরম পূরণের শেষ তারিখ আগামী ৫ নভেম্বর। বৃত্তি পরীক্ষা অনুষ্ঠিত হবে ১১ সকাল ১০টা থেকে ১১টা পর্যন্ত। এ বিষয়ে বিস্তারিত জানা যাবে ০১৬৭৭৭৬৬২২২ নম্বরে।
