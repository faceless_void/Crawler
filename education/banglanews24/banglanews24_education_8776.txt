ময়মনসিংহের ত্রিশালে প্রতিষ্ঠিত জাতীয় কবি কাজী নজরুল ইসলাম বিশ্ববিদ্যালয়ে বৃক্ষরোপণ কর্মসূচি শুরু হয়েছে। 
ময়মনসিংহ : ময়মনসিংহের ত্রিশালে প্রতিষ্ঠিত জাতীয় কবি কাজী নজরুল ইসলাম বিশ্ববিদ্যালয়ে বৃক্ষরোপণ কর্মসূচি শুরু হয়েছে। 
মঙ্গলবার (২৬ এপ্রিল) সকালে বিশ্ববিদ্যালয়ের ‘এনভায়রনমেন্টাল সায়েন্স অ্যান্ড ইঞ্জিনিয়ারিং বিভাগ’ এর উদ্যোগে আমের চারা রোপণ করে এ কর্মসূচির উদ্বোধন করেন উপাচার্য অধ্যাপক ড. মোহীত উল আলম।  
এ সময় বিশ্ববিদ্যালয়ের ট্রেজারার অধ্যাপক এ এম এম শামসুর রহমান, প্রক্টর ড. মো. মাহবুব হোসেন, রেজিস্ট্রার (অতিরিক্ত দায়িত্ব) মো. ফজলুল কাদের চৌধুরী, ইংরেজি ভাষা ও সাহিত্য বিভাগের বিভাগীয় প্রধান অধ্যাপক ড. মোহাম্মদ ইমদাদুল হুদাসহ বিভিন্ন বিভাগের শিক্ষক, কর্মকর্তা ও শিক্ষার্থীরা উপস্থিত ছিলেন। 
এ কর্মসূচির প্রথম দিনেই বিভিন্ন প্রজাতির শতাধিক বৃক্ষের চারা রোপণ করা হয়। চলতি মৌসুমে বিভিন্ন প্রজাতির ফলজ, বনজ এবং ঔষুধি মিলিয়ে মোট এক হাজার গাছের চারা রোপণ করা হবে। বৃক্ষরোপণ কর্মসূচির সার্কিক তত্ত্বাবধানে রয়েছেন এনভায়রনমেন্টাল সায়েন্স অ্যান্ড ইঞ্জিনিয়ারিং বিভাগের বিভাগীয় প্রধান ড. আশরাফ আলী সিদ্দিকী। 
বাংলাদেশ সময়: ১৯০১ ঘণ্টা, এপ্রিল ২৬, ২০১৬ 
এমএএএম/পিসি
