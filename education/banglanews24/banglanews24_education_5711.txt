শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয় শিক্ষার্থী এবং ছাত্রলীগের সহ-সম্পাদক বদরুল আলমকে আজীবন বহিষ্কার করেছে বিশ্ববিদ্যালয় প্রশাসন।
শাবিপ্রবি: শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয় শিক্ষার্থী এবং ছাত্রলীগের সহ-সম্পাদক বদরুল আলমকে আজীবন বহিষ্কার করেছে বিশ্ববিদ্যালয় প্রশাসন।
সিলেট সরকারি মহিলা কলেজের শিক্ষার্থী খাদিজা আক্তার নার্গিসকে কুপিয়ে হত্যাচেষ্টার অভিযোগে বিশ্ববিদ্যালয় থেকে তাকে বহিষ্কার করা হয়।
রোববার (২৩ অক্টোবর) বিকেলে অনুষ্ঠিত সিন্ডিকেটের ২০১তম সভায় এ সিদ্ধান্ত নেওয়া হয়েছে বলে বাংলানিউজকে জানান রেজিস্ট্রার মো. ইশফাকুল হোসেন।
তিনি বলে, গত ০৪ অক্টোবর (মঙ্গলবার) বদরুলকে সাময়িক বহিষ্কার পরবর্তী বিশ্ববিদ্যালয়ের ভারপ্রাপ্ত প্রক্টর অধ্যাপক রাশেদ তালুকদারকে প্রধান করে গঠিত তদন্ত কমিটির রিপোর্টের ভিত্তিতে এ সিদ্ধান্ত নেয় সিন্ডিকেট।
খাদিজাকে উদ্ধারে শিক্ষার্থী ইমরানসহ যারা সহযোগিতা করেছেন সিন্ডিকেট তাদের ধন্যবাদ জানিয়েছে বলেও জানান ইশফাকুল হোসেন।
অন্যদিকে, বিশ্ববিদ্যালয়ের শৃঙ্খলা ভঙ্গ করে শাবিপ্রবি’র গোলাবি রেস্টুরেন্টে হামলা ও ভাংচুরের দায়ে কেমিকৌশল বিভাগের চতুর্থ বর্ষের শিক্ষার্থী ও শাখা ছাত্রলীগের সাবেক সহ-সম্পাদক মোশাররফ হোসেন রাজুকে এক সেমিস্টার বহিষ্কার করেছে সিন্ডিকেট।
সংগঠনের শৃঙ্খলা ভঙ্গের দায়ে রাজুকে ১৭ অক্টোবর (সোমবার) ছাত্রলীগ থেকে অব্যাহতি দেয় কেন্দ্রীয় সংসদ। রাজু গোলাবিতে ভাংচুরে নেতৃত্ব দেয় বলে অভিযোগ ওঠে।
