তিন বেলার খাবারে মধ্যে সকালের নাস্তাকে বিশেষজ্ঞরা বেশি গুরুত্বপূর্ণ হিসেবে দাবি করেন। কারণ, এসময় যা খাবেন, শরীর তা কাজে লাগানোর চেষ্টা করবে পুরোপুরি। সারারাত ঘুমানোর পর শরীরের কোষগুলোকে প্রয়োজনীয় জ্বালানি যোগায় এই সকালের নাস্তা, তাই এসময় খাওয়া চাই পুষ্টিগুণে ভরপুর খাবার ও পানীয়।
পুষ্টিবিজ্ঞানে কমলা লেবু ও দুধের উপকার ও অপকারের কথা দুটোই বলা আছে। কারও কারও দুধ এমনিতে সহ্য হয় না। আবার খালি পেটে লেবুর রস খেলে হতে পারে অ্যাসিডিটি। তাহলে কী করবেন? বরং জেনে নিন পুষ্টিবিজ্ঞানের ভাষায় কোন পানীয়টা সকালের নাস্তায় উপকারী।
কমলার রস
অ্যান্টি-অক্সিডেন্টের একটি আদর্শ উৎস কমলার রস। শরীর রোগ মুক্ত রাখতে এর অবদান অপরিসীম। ভিটামিন সি’য়ে ভরপুর এই পানীয় শরীরকে বায়ু দূষণ, অতি বেগুনি রশ্মি, পরিবেশ দূষণ ইত্যাদির ক্ষতিকর প্রভাব থেকে রক্ষা করে। মাত্র এক গ্লাস কমলার রস সারাদিনের ভিটামিন সি’য়ের চাহিদা মেটাতে যথেষ্ট।
অপকারিতা: সকালে ঘুম থেকে উঠে কমলার রস বানিয়ে পান করা বেশ ঝক্কির ব্যাপার। কমলার পুষ্টিগুণের একটি বড় অংশ থাকে এর পাতলা খোসায়, যা রস বানানোর আগেই ফেলে দিতে হবে।
বাড়ির উঠান বা ছাদে নিশ্চই কমলার গাছ নেই। অবশ্যই ফলটি দোকান থেকে কিনতে হবে। সেখানে আছে ফরমালিন এবং বিভিন্ন স্বাদবর্ধক রাসায়নিক উপাদানের ঝুঁকি।
অধিকাংশই হয়ত বেছে নেবেন বোতল কিংবা প্যাকেটজাত বিকল্প। বাড়তি চিনি, ক্ষতিকর রাসায়নিক উপাদান, প্রক্রিয়াজাতকরণ ইত্যাদির ক্ষতিকর দিকগুলো বাদ দিলেও ওই কমলার রস কিন্তু তাজা নয়।
এছাড়াও, বিভিন্ন গবেষণা বলছে, নিয়মিত কমলার রস পান করা দাঁতের এনামেলের জন্য ক্ষতিকর।
দুধ


অপকারিতা: দুধে থাকে ‘স্যাচুরেইটেড ফ্যাট’, যার কারণে হৃদরোগের ঝুঁকি বাড়ে।
বেশি দুধ পাওয়া জন্য গৃহপালীত পশুকে হরমোন ইঞ্জেকশন দেওয়ার ঘটনা আমাদের দেশে বিরল কিছু নয়। এই দুধ পান করলে আমাদের শরীরেও প্রবেশ করবে ওই ওষুধ। তাই দুধের গুণগত মান সম্পর্কে নিশ্চিত না হতে পারলে ওই দুধ নিশ্চিন্ত মনে পান করা উচিত হবে না।
দুধ বনাম কমলার রস


এক গ্লাস দুধ পান করে যতক্ষণ পেট ভরা থাকবে, এক গ্লাস কমলার রস পান করে ততক্ষণ পেট ভরা থাকবে না।
তবে ভালো উপায় হল, সম্ভব হলে দুটাই গ্রহণ করা। সকালে ঘুম থেকে উঠে খালি পেটে এক গ্লাস কমলার রস পান করে নিন। এবার এক ঘণ্টা বিরতি দিয়ে নাস্তা খেয়ে এক গ্লাস দুধ পান করুন। তবে কমলার রস পান করে গ্যাস হচ্ছে কিনা সেদিকে নজর দিতে হবে।
আবার আপনি ‘ল্যাকটোজ ইনটোলেরেন্ট’ বা দুধ হজমে অপারগ কিনা সেদিকেও নজর রাখতে হবে।
ছবি: রয়টার্স ও নিজস্ব।
আরও পড়ুন
দুধ ও কলা- একসঙ্গে ভালো না  
কলা যেন ধীরে কালো হয়  
