সাতটি সৃজনশীল লেখার নতুন পদ্ধতি অবিলম্বে বাতিলের দাবিতে বগুড়ায় সচেতন ছাত্র সমাজের উদ্যোগে মানববন্ধন কর্মসূচি পালন করেছেন শিক্ষার্থীরা।
বগুড়া: সাতটি সৃজনশীল লেখার নতুন পদ্ধতি অবিলম্বে বাতিলের দাবিতে বগুড়ায় সচেতন ছাত্র সমাজের উদ্যোগে মানববন্ধন কর্মসূচি পালন করেছেন শিক্ষার্থীরা।
বৃহস্পতিবার (০৬ অক্টোবর) দুপুরে শহরের সাতমাথায় এ কর্মসূচি পালন করা হয়। সৃজনশীল পদ্ধতিতে ২০১৭ সালের এসএসসি পরীক্ষায় শিক্ষা মন্ত্রণালয় প্রস্তাবিত সাতটি সৃজনশীল প্রশ্নের প্রস্তাবনা বাতিল চেয়েছেন তারা।
অতি দ্রুত তাদের যৌক্তিক দাবি মেনে নেওয়ার জন্য সংশ্লিষ্টদের প্রতি জোরালো আহ্বান জানানো হয়েছে। মানববন্ধনে শিক্ষার্থীরা বলেন, আমাদের শিক্ষা জীবন নিয়ে ছিনিমিনি খেলা হচ্ছে। অথচ এ পদ্ধতি চালু করে বাড়তি চাপ তৈরি করা হচ্ছে। যা কোনোভাবেই কাম্য নয়।
এতে শিক্ষা জীবনে মারাত্মক প্রভাব ফেলছে। তাই অবিলম্বে এ পদ্ধতি বাতিলের দাবি জানান তারা।
কর্মসূচিতে বিপুল সংখ্যক শিক্ষার্থী অংশ নেন।
