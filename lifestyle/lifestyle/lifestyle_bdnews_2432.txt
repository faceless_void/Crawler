মুখরোচক এই পিঠার রেসিপি দিয়েছেন রন্ধনশিল্পী তাসনুভা মাহমুদ নওরিন।


পদ্ধতি: ময়দা, চালের গুঁড়া, বেইকিং পাউডার ছাঁকনি দিয়ে চেলে নিন। অন্যদিকে ডিম, দুধ, চিনি, গুড়, লবণ, ঘি ভালো মতো মিশিয়ে নিন। খেয়াল রাখবেন গুড় ও চিনি যেন একদম মিশে যায়।
এখন শুকনা উপাদানগুলো গুড়-চিনির মিশ্রণে মিশিয়ে নারিকেল কোরানো দিন।
যে প্যানে পিঠা বসাবেন সেটাতে তেল ব্রাশ করে মিশ্রণটি ঢালুন। তারপর পুডিংয়ের মতো হাঁড়িতে পানি দিয়ে তার উপর পিঠার প্যান বসিয়ে উপরে ভারি কিছু দিয়ে চেপে রাখুন।
মাঝারি আঁচে আগুন জ্বেলে রাখুন। ৪০ থেকে এক ঘণ্টাও লাগতে পারে। মাঝে খুলে একবার দেখবেন হয়েছে কিনা। চুলায় সময় কম বেশি লাগতে পারে।
ওভেনে করতে চাইলে ১৮০ ডিগ্রি সেলসিয়াসে ৩০ থেকে ৪০ মিনিট লাগতে পারে। আর সেটা পিঠার আকারের উপর নির্ভর করবে। তবে মাঝে টুথপিক ঢুকিয়ে পরখ করে নিতে পারেন। টুথপিক পরিষ্কার হয়ে বের হলে পিঠা তৈরি।
নামিয়ে ঠাণ্ডা করে কেটে পরিবেশন করুন।
আরও রেসিপি
আরবীয় খানা খেবসা
কাশ্মিরি চা
