'শ্রীলংকান ইউথ' নামের একটি হ্যাকার দল ওয়েবসাইটটি হ্যাক করেছে বলে জানিয়েছে বিবিসি। আগামী বছরের এপ্রিলে অনুষ্ঠিতব্য 'এ-লেভেল' পরীক্ষার তারিখ পরিবর্তনের লক্ষ্যেই এই হ্যাকিং চালানো হয়। সিনহালা এবং তামিল নববর্ষ উৎসব একই সময়ে হওয়ায় পরীক্ষার তারিখ পরিবর্তনের আহবান জানায় শিক্ষার্থীরা।
গ্রেফতারকৃত ১৭ বছর বয়সী শিক্ষার্থীকে এর পেছনো অন্য কোনো উদ্দেশ্য আছে কি না সেটি নিয়ে প্রশ্ন করা হয়েছে বলে জানানো হয়।
এর আগে সিনহালা-তে একটি গ্রুপে পোস্টের মাধ্যমে পরীক্ষার নতুন সময়সূচী নিয়ে প্রতিবাদ জানানো হয়। এমনকি প্রেসিডেন্টকে হুমকি দিয়ে বলা হয় , "শ্রীলংকান ওয়েবসাইটগুলোর সুরক্ষার বিষয়ে যত্নশীল হোন অথবা সাইবার যুদ্ধের মুখোমুখি হোন।"
বার্তায় আরও বলা হয়, "আপনি যদি পরিস্থিতি নিয়ন্ত্রণ করতে না পারেন, প্রেসিডেন্ট নির্বাচন আয়োজন করুন। প্রধানমন্ত্রীর দায়িত্বজ্ঞানহীন কার্যকলাপ বন্ধ করুন এবং ইউনিভার্সিটি শিক্ষার্থীদের সমস্যাগুলো আরও খতিয়ে দেখুন।"
পরবর্তীতে দলটি আবার সক্রিয় হয় এবং এবার কোনো প্রকার বার্তা ছাড়াই আরেকবার ওয়েবসাইট হ্যাক করে।
সম্প্রতি বেশ কয়েকটি দেশের সরকারি ওয়েবসাইট হ্যাকের ঘটনা ঘটে। তবে শ্রীলংকান ওয়েবসাইটটি হ্যাকের ঘটনা নিয়ে প্রশ্ন দেখা দিয়েছে, প্রেসিডেন্টের ওয়েবসাইট এতটাই অরক্ষিত যে, সেটি একজন কিশোর হ্যাক করতে পারে?
