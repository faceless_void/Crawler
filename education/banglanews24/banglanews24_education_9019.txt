ঢাকা: অভিভাবকদের ৫৫ শতাংশ মনে করছেন, স্কুলে শাস্তির মাধ্যমে শিশুকে শৃঙ্খলার মধ্যে আনা যায়। আবার ২৭ শতাংশ মনে করছেন, শাস্তি না হলে শিশুরা বখে যায় এবং ২৫ শতাংশের মতে, শাস্তি দিলে শিশুরা শিক্ষকদের কথা শোনে।
রোববার (১৯ নভেম্বর) রাজধানীর সিরডাপ মিলনায়তনে আইনি সহায়তা প্রদানকারী প্রতিষ্ঠান ব্লাস্ট ও সেভ দ্য চিলড্রেন ‘শিশুর অধিকার সুরক্ষায় শারীরিক ও মানসিক শাস্তি নিরসন’ বিষয়ে অনুষ্ঠিত গণশুনানিতে এ তথ্য তুলে ধরা হয়।
ব্লাস্ট পরিচালিত এক গবেষণায় বলা হয়েছে, ‘৬৯ শতাংশ বাবা-মা নিয়মানুবর্তিতার জন্য স্কুলে শিশুদের বেত্রাঘাত সহ শাস্তির বিধানের পক্ষে।
গণশুনানিতে বিচারক, জাতীয় মানবাধিকার কমিশনের প্রতিনিধি, আইনজীবী, দাতা সংস্থার প্রতিনিধি, সাংবাদিক, শিক্ষক, মনোবিদ নির্যাতনের শিকার পরিবার ও নাগরিক সমাজের প্রতিনিধিরা অংশ নেন।
আলোচনায় জুরিবোর্ডের সদস্য গণসাক্ষরতা অভিযানের নির্বাহী পরিচালক রাশেদা কে চৌধুরী  বলেন, ‘যারা শিক্ষকদের প্রশিক্ষণ দেন, তারা নিজেরাই কতটুকু সচেতন, সে বিষয়ে প্রশ্ন থেকে যায়। আর শারীরিক নির্যাতন বন্ধ হলেও মৌখিকভাবে নির্যাতন বন্ধ হয়নি। এ বিষয়ে নীতিনির্ধারকদের এগিয়ে আসতে হবে।’
সভাপতির বক্তব্যে সাবেক বিচারপতি নিজামুল হক বলেন, গায়ে হাত না পড়লে শিশুরা মানুষ হবে না, এ ধারণা থেকে অভিভাবকদের বেরিয়ে আসা উচিত। 
এসময় আরও বক্তব্য দেন- ব্লাস্টের বোর্ড অব ট্রাস্টিজ সদস্য জেড আই খান, সেভ দ্য চিলড্রেনের ম্যানেজার একরামুল কবির, হিউমেন ডেভেলপমেন্ট ইনিশিয়েটিভের সভাপতি কাজী ফারুক আহমেদ প্রমুখ।
