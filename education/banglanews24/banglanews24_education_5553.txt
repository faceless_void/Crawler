টাঙ্গাইলের মধুপুর ও ধনবাড়ী উপজেলায় ট্রাক ও কাভার্ডভ্যান শ্রমিকদের এসএসসি এবং এইচএসসি পর্যায়ে জিপিএ-৫ প্রাপ্ত মেধাবী সন্তানদের মেধাবৃত্তি ও সম্মাননা ক্রেস্ট প্রদান করেছে বাংলাদেশ ট্রাক ও কভার্ডভ্যান ড্রাইভার্স ইউনিয়ন(রেজি-৬২৩) মধুপুর শাখা।
বৃহস্পতিবার (০৫ জানুয়ারি) সন্ধ্যায় এক অনাড়ম্বর অনুষ্ঠানে সংগঠনের মধুপুর ও ধনবাড়ী শাখার শ্রমিকদের এসএসসি পর্যায়ের ১৪ জন ও এইচএসসি পর্যায়ের তিন জন জিপিএ-৫ প্রাপ্ত সন্তানকে বৃত্তি ও সম্মাননা দেওয়া হয়।
সংগঠনের শাখা কার্যালয়ের হল রুমে এই অনুষ্ঠানে প্রধান অতিথি ছিলেন মধুপুর উপজেলা পরিষদের চেয়ারম্যান ছরোয়ার আলম খান আবু। সভাপতিত্ব করেন মধুপুর শাখা কমিটির সভাপতি মো. আব্দুল আজিজ।
সহকারি পুলিশ সুপার আলমগীর কবির, উপজেলা আওয়ামী লীগের সভাপতি ও জেলা পরিষদের সদস্য খন্দকার শফি উদ্দিন মনি, উপজেলা ভাইস চেয়ারম্যান হেলাল উদ্দিন, মধুপুর কলেজের অধ্যক্ষ মোন্তাজ আলী, শহীদ স্মৃতি উচ্চ মাধ্যমিক বিদ্যালয়ের অধ্যক্ষ বজলুর রশীদ খান, মধুপুর রানী ভবানী মডেল উচ্চ বিদ্যালয়ের প্রধান শিক্ষক আজিজুল হক লুলু, ট্রাক মালিক সমিতি মধুপুর শাখার সাধারণ সম্পাদক ফরহাদ হোসেন পলাশ প্রমুখ বক্তব্য রাখেন।
বাংলাদেশ ট্রাক ও কভার্ডভ্যান ড্রাইভার্স ইউনিয়ন মধুপুর শাখার সাধারণ সম্পাদক আনিছুর রহমান হীরা অনুষ্ঠান সঞ্চালনা করেন।
বাংলাদেশ সময় ০৪৩৮ ঘণ্টা, জানুয়ারি ০৬, ২০১৭ এমআইএইচ
