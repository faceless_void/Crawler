বনানী এলাকার এমন ছয়টি রেস্তোরাঁর ইফতার আয়োজন জানা গেছে তাদের ফেইসবুক পেইজ থেকে।
নান্দোস: ইফতারে এই রেস্তোরাঁয় ৩টি প্ল্যাটার।
১/৪ চিকেন, গরু-ডিমের চপ, মিনি পিৎজা, মালাই মুর্গ কাবাব, গরুর মাংসের কাবাব, চানা চাট, রাশিয়ান সালাদ, পিটা ব্রেড, পটেটো ওয়েজেস, আইসক্রিম, খেজুর, মাল্টা, আপেল, লেমোনেড ও পানি মিলে প্রথম প্ল্যাটার, দাম ১ হাজার টাকা।
চিকেন স্টেক, গরুর মাংস ও ডিমের চপ, মিনি পিৎজা, মালাই মুর্গ কাবাব, গরুর মাংসের কাবাব, চানা চাট, রাশিয়ান সালাদ, পিটা ব্রেড, পটেটো ওয়েজেস, আইসক্রিম, খেজুর, মাল্টা, আপেল, লেমোনেড ও পানি নিয়ে দ্বিতীয় প্ল্যাটার, দাম ১ হাজার ৩শ’ টাকা।
তৃতীয় প্ল্যাটারে আছে এস্পেদাতা, গরু-ডিমের চপ, মিনি পিৎজা, মালাই মুর্গ কাবাব, গরুর মাংসের কাবাব, চানা চাট, রাশিয়ান সালাদ, পিটা ব্রেড, পটেটো ওয়েজেস, আইসক্রিম, খেজুর, মাল্টা, আপেল, গোয়া লেমন ও পানি, দাম ১ হাজার ৬শ’ টাকা।
ফুল’স ডাইনার: রজমান মাসে ৭৮৫ টাকার প্ল্যাটার থাকছে এই রেস্তোরাঁয়। মিলবে চাইনিজ ক্লিয়ার সুপ, চিকেন সাত্তে, চিকেন স্ট্রিপস, তেমাকি, কাংকং সালাদ, ফ্রাইড রাইস, তেরিয়াকি চিকেন, ফল, কোকো রোল, খেজুর ও লেমোনেইড।
অ্যাবসোলুট থাই: ইফতারে এই রেস্তোরাঁয় থাকছে বাফেট। এছাড়াও আছে ২টি ‘টেকওয়ে মেন্যু’, দাম ৫শ’ টাকা।
‘সেট-এ’তে আছে বেগুনি, বেসিল লিফ চিকেন, বিফ ব্ল্যাক পেপার সস, চিকেন ফ্রাইড রাইস, সাগো উইফ কোকোনাট মিল্ক, খেজুর ও পানি।
‘সেট-বি’তে মিলবে চিকেন সাত্তে, গ্রিন কারি চিকেন, সবজি পাকোড়া, মিক্সড ভেজিটেবল, চিকেন ফ্রাইড রাইস, সাগো উইফ কোকোনাট মিল্ক, খেজুর ও পানি।
তড়কা: পাঁচটি প্ল্যাটার থাকছে এই রেস্তোরাঁয়।
ইফতার থালির মূল্য ৬শ’ টাকা। আছে মুরগির তাংরি কাবাব, মুরগির কাঠি কাবাব, চিকেন রোল, চিকেন ললিপপ, গরুর জালি কাবাব, চানা মসল্লা, খাসির হালিম, জাফরনি রেশমি জিলাপি, ডিম চপ, বেগুনি, পেঁয়াজু, মুড়ি, ফল, লাচ্ছি ও ডাবের পানি।
বিরিয়ানি থালিতে থাকবে খাসির হায়দ্রাবাদি দম বিরিয়ানি, গরুর জালি কাবাব, মাসালা চিকেন, সবজি সালাদ ও আচার। দাম ৪১০ টাকা।
৪৬০ টাকায় মিলবে নান থালি। আছে মাখন নানরুটি, তন্দুরি মুরগি, গরুর মাংস ভুনা, ডাল-মাখন ভাজা, সবজির দোপেঁয়াজা, সবজি সালাদ ও আচার।
চাপ থালির দাম সাড়ে ৪শ’ টাকা। আছে তাওয়া লাসা পরোটা, গরুর মাংসের চাপ, ডাল-মাখন ভাজা, সবজির দোপেঁয়াজা, সবজি সালাদ ও আচার।
আরও আছে রাইস থালি। এতে মিলবে সাদা ভাত, চিকেন মাসালা, গরুর মাংসের জালি কাবাব, ডাল চচ্চড়ি, সবজি দোপেঁয়াজা, সবজি সালাদ এবং আচার, দাম ৩৮০ টাকা।
ট্রি হাউস: দুটি সেট মেন্যু আছে এখানে।
প্রথমটি চিকেন ইফতার প্ল্যাটার, দাম ৮শ’ টাকা। মিলবে চেকেন স্টেক, সৌটেই ভেজিটেবল, দুটি বাফেলো উইঙস, দুটি প্রন রোল, সৌটেই মাশরুম, পটেটো ওয়েজেস, অ্যাপল স্কুপ আইসক্রিম, ফ্রুট সালাদ ও লেমোনেইড।
১ হাজার ২শ’ টাকায় আছে স্টেক বেনতো প্ল্যাটার। যাতে থাকছে বিফ সিরলোইন স্টেক, সালাদ, দুটি প্রন ফ্রাই, দুটি বাফেলো উইঙস, দুটি প্রন স্প্রিং রোল, সৌটেই মাশরুম, পটেটো ওয়েজেস, অ্যাপল স্কুপ আইসক্রিম, ফ্রুট সালাদ এবং লেমোনেইড।
