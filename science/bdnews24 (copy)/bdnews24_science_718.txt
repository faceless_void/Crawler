এ যাবৎ শোনা যাচ্ছিল নতুন আইফোন ৮-এর সামনে পুরোটাই পর্দা রাখবে মার্কিন টেক জায়ান্ট প্রতিষ্ঠানটি। এতে কোনো বেজেল না রাখায় পর্দার নিচে টাচ আইডি রাখা হবে বলে ধারণা করা হচ্ছিল। কিন্ত কুও-এর মতে আইফোন ৮-এর পর্দার নিচে টাচ আইডি বসানোর পরিকল্পনা  বাদ দিয়েছে অ্যাপল, বলা হয়েছে ভারতীয় সংবাদমাধ্যম আইএএনএস-এর প্রতিবেদনে।
আসন্ন বিভিন্ন প্রযুক্তি নিয়ে কুও একাধিকবার সঠিক আভাস দিয়েছেন। আইফোন ৫-এ টাচ আইডি বা ফিঙ্গারপ্রিন্ট সেন্সর যোগ করার ভবিষ্যদ্বাণীও কুওর ব্লগ থেকেই প্রথম এসেছিল।
ফিঙ্গারপ্রিন্ট স্ক্যানারের পরিবর্তে আইফোন ৮-এ স্যামসাংয়ের মতো ফেইস আনলক প্রযুক্তি ব্যবহার করা হতে পারে। তবে, নতুন আইফোন থেকে টাচ আইডি পুরোপুরি বাদ দেওয়া হচ্ছে কিনা তা স্পষ্ট করে বলেননি কুও।
তিনি বলেন, “জুলাইতে বিশ্লেষকরা আইফোন ৮-এর যে স্ট্যান্ডআউট ফিচারগুলো প্রকাশ করেছেন এত এম্বেডেড টাচ আইডি বাদ দেওয়া হয়েছে। কিন্তু এর মানে এই নয় যে অ্যাপলও তাদের সঙ্গে মিলে একই উদ্যোগ নিয়েছে।”
বিশ্লেষকদের ধারণা ক্যাপাসিটিভ সেন্সিং প্রযুক্তি বাদ দিতে পারে অ্যাপল। এর পরিবর্তে অপ্টিক্যাল সলিউশন আনতে পারে প্রতিষ্ঠানটি।
