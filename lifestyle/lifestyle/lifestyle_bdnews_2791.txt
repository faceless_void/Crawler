স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে এমনই কিছু খাবারের নাম উল্লেখ করা হয় যেগুলো ‘পিরিয়ড’ আগে হতে সাহায্য করে।
তবে এক্ষেত্রে মনে রাখতে হবে, এই ধরনের খাবার কোনো কোনো ক্ষেত্রে গর্ভপাতের কারণও হতে পারে। তাই বিবাহিতদের ক্ষেত্রে যদি পিরিয়ডের সময় পার হয়ে যায় তাহলে আগে ডাক্তারের পরামর্শ নিন। নিশ্চিত হন আপনি সন্তান ধারণে করেছেন কিনা। নতুবা এই খাবারের কারণে গর্ভপাত হওয়ার সম্ভাবনা বৃদ্ধি পাবে।
পার্সলে: পার্সলেতে রয়েছে অ্যাপিওল এবং মাইরিসটিসিন যা মাসিক চক্রকে স্বাভাবিক রাখতে সাহায্য করে। দিল্লির ফোর্টিস লা ফাম হাসপাতালের পুষ্টিবিদ ডা. লোভনিত বাত্রা বলেন, “পার্সলেতে থাকা উপাদানগুলো দ্রুত ইউট্রাস গঠনে সাহায্য করে। ফলে মাসিক চক্র প্রভাবিত হয়।”
জিরা: পার্সলে এবং জিরায় রয়েছে প্রায় একই ধরনের উপকারিতা। তাই একই প্রক্রিয়ায় মাসিকের সময় এগিয়ে আনতে সহায়তা করে।
আজওয়াইন: মাসিক চক্র দ্রুত করার পাশাপাশি ওই সময় হওয়া পেটে ব্যথা এবং অস্বস্তি দূর করতেও আজওয়াইন বেশ উপকারী। এক চা-চামচ আজওয়াইন দানা এক গ্লাস পানিতে ফুটিয়ে এক চা-চামচ গুঁড় মিশিয়ে সকালে খালি পেটে সেবন করলে উপকার পাওয়া যাবে।
পেঁপে: মাসিক এগিয়ে আসতে পেঁপে বেশ উপকারী ঘরোয়া টোটকা।
ডা. বাত্রার মতে পার্সলের মতো পেঁপেও ইউট্রাস গঠন প্রক্রিয়া গতিশীল করতে সাহায্য করে। পেঁপেতে থাকা ক্যারটিন ইস্ট্রোজেনের মাত্রা বৃদ্ধিতেও সহায়তা করে, আর তাই পিরিয়ড দ্রুততর হয়।
আদা: রয়েছে এমেইনাগোগ নামক একটি উপাদান। যা রজঃস্রাব হওয়ার ক্ষেত্রে জাদুমন্ত্রের মতো কাজ করে। ফলে মাসিকের প্রক্রিয়া দ্রুত করতে সাহায্য করে।
তবে পার্শ্বপ্রতিক্রিয়া হিসেবে পেটে গ্যাস হওয়ার সম্ভাবনা রয়েছে। তাই পার্সলের সঙ্গে আদা দিয়ে চা খাওয়ার সুপারিশ করা হয়।
ধনেগুঁড়া: যারা প্রায়ই পিরিয়ড দেরি হওয়ার সমস্যায় ভোগেন তাদের জন্য ধনে বেশ উপকারী একটি উপাদান। এতে আছে এমেইনাগোগ নামক একটি ঔষধি উপাদান যা পিরিয়ড নিয়মিত রাখতে সাহায্য করে।
দুই কাপ পরিমাণ পানিতে এক চা-চামচ ধনেগুঁড়া ফুটিয়ে নিন। পানি ফুটে এক কাপ পরিমাণে নেমে এলে ছেঁকে আলাদা করে রাখুন। পিরিয়ডের সময় এগিয়ে আসার কিছুদিন আগে থেকে দিনে তিনবার ওই পানি পান করুন।
মৌরি: মৌরি পানিতে ফুটিয়ে সুগন্ধি চা তৈরি করে নেওয়া যায়। প্রতিদিন সকালে খালি পেটে এই চা পান করলে অনিয়মিত মাসিকের সমস্যা থেকে রেহাই পাওয়া সম্ভাবনা বাড়বে। এছাড়া রাতে এক গ্লাস পানিতে দুই চা-চামচ মৌরি ভিজিয়ে রাখতে হবে। সকালে ছেঁকে নিয়ে ওই পানি পান করলেও উপকৃত হবেন।
অ্যালোভেরার শরবত: পেটের বিভিন্ন সমস্যা উপশনে অ্যালোভেরা বেশ উপকারী। তবে এই পাতার রসেও রয়েছে এমেইনাগোগ নামক উপাদানটি। যা মাসিক নিয়মিত করতে সাহায্য করে।
এক টেবিল-চামচ মধুর সঙ্গে পরিমাণ মতো অ্যালোভেরার রস মিশিয়ে সকালে নাস্তার আগে খেতে হবে। টানা এক মাস খেলে মাসিকের সমস্যা অনেকটাই কমে আসবে।
