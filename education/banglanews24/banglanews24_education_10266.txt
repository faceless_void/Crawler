রাজশাহী বিশ্ববিদ্যালয়ের ইংরেজি বিভাগের প্রফেসর রেজাউল করিম সিদ্দিকী হত্যাকাণ্ডের বিচারের দাবিতে মানববন্ধন করেছেন ইংরেজি বিভাগের শিক্ষক-শিক্ষার্থীরা।
রাবি: রাজশাহী বিশ্ববিদ্যালয়ের ইংরেজি বিভাগের প্রফেসর রেজাউল করিম সিদ্দিকী হত্যাকাণ্ডের বিচারের দাবিতে মানববন্ধন করেছেন ইংরেজি বিভাগের শিক্ষক-শিক্ষার্থীরা।
শনিবার (১৮ জুন) দুপুর ১২টার দিকে শহীদুল্লাহ কলা ভবনের সামনে আয়োজিত মনববন্ধনে সভাপতিত্ব করেন অধ্যাপক মাসউদ আখতার। 
এ সময় প্রফেসর রেজাউল করিম সিদ্দিকী হত্যাকারীদের দ্রুত গ্রেফতার করে বিচারের দাবি জানানো হয়।
এর আগে সাপ্তাহিক কর্মসূচির অংশ হিসেবে বিশ্ববিদ্যালয়ের একটি মৌন মিছিল বের করেন বিভাগের শিক্ষক-শিক্ষার্থীরা। মিছিলটি ক্যাম্পাসের বিভিন্ন সড়ক প্রদক্ষিণ শেষে বিশ্ববিদ্যালয়ের শহীদুল্লাহ কলা ভবনের সামনে মুকুল প্রতিবাদ ও সংহতি মঞ্চে এসে সমাবেশে মিলিত হয়।
এ সময় উপস্থিত ছিলেন- বিভাগের শিক্ষক অধ্যাপক অসীম কুমার দাস, অধ্যাপক শহীদুর রহমান, সহযোগী অধ্যাপক ড. শেহনাজ ইয়াসমীন, সহযোগী অধ্যাপক আব্দুল্লাহ আল মামুন, সহযোগী অধ্যাপক রুবাইদা আখতার প্রমুখ।
মানববন্ধন থেকে ইংরেজি বিভাগের অনিয়মিত ছোট কাগজ ‘সপ্তক’র পরবর্তী সংখ্যাটি অধ্যাপক রেজাউলকে উৎসর্গ করে প্রকাশ করা হবে বলে জানানো হয়।
গত ২৩ এপ্রিল রাজশাহীর শালবাগন এলাকায় নিজ বাসা থেকে একটু দূরে দুর্বৃত্তদের হাতে খুন হন অধ্যাপক রেজাউল করিম। 
