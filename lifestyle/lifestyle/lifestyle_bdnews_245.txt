রুপচর্চাবিষয়ক ওয়েবসাইটে প্রকাশিত প্রতিবেদন থেকে সন্তান জন্ম দেওয়ার  পরবর্তী সময়ে ঝুলে পড়া ত্বকের পরিচর্চার ঘরোয়া কিছু উপায় সম্পর্কে  জানা যায়।
এই প্রাকৃতিক পদ্ধতি অনুসরণের মাধ্যমে শরীরকে আগের আকারে ফিরিয়ে নেওয়া ও গর্ভকালীন দাগ দূর করা সম্ভব।
এক্সফলিয়েশন


ডিমের সাদা অংশের সঙ্গে দুই টেবিল-চামচ সামদ্রিক লবণ এবং আধা চা-চামচ লেবুর রস মিশিয়ে পেটে লাগিয়ে মালিশ করুন। ডিমের সাদা অংশ ত্বক টানটান করে, সামদ্রিক লবণ মৃত কোষ দূর করে।
চাইলে এর পরিবর্তে, দুকাপ সামদ্রিক লবণ, আধা কাপ ওটামিল পাউডার ও কয়েক ফোঁটা আঙুরের বীজের তেল মিশিয়ে পেটের ত্বকে এক্সফলিয়েটর হিসেবে ব্যবহার করতে পারেন।
জেল ব্যবহার করা


আদা চর্বি গলিয়ে ত্বক টান টান রাখতে সাহায্য করে,
মধু বার্ধক্য প্রতিরোধক ও অ্যান্টি অক্সিডেন্ট সমৃদ্ধ যা ঝুলে পড়া ত্বকে ভালো কাজ করে।
লেবুর রস কোলাজেনের বৃদ্ধি বাড়ায়, যা ত্বকের স্থিতিস্থাপকতা ফেরাতে সাহায্য করে।  
দুই টেবিল-চামচ আদার গুঁড়া, এক টেবিল-চামচ অ্যালোভেরার জেল, লেবুর রস ও মধু ভালোভাবে মিশিয়ে পেটে লাগিয়ে ২০ মিনিট অপেক্ষা করুন।
ভালো ফলাফল পেতে এই মিশ্রণ ব্যবহারের পাশাপাশি আধা ঘণ্টা শরীরচর্চা করতে পারেন।
সঠিক খাবার খান


কমলা, মাল্টা, স্ট্রবেরি, ব্ল্যাকবেরি এবং অন্যান্য বেরি-জাতীয় ফল শরীরে কোলাজেনের বৃদ্ধি করে ও খাবারের মানও স্বাস্থ্যকর করে।
খাবারে মটর, কুমড়ার বীজ, মাশরুম, কাজুবাদাম ইত্যাদি যোগ করুন। এগুলো উচ্চ জিংক সমৃদ্ধ খাবার যা ত্বক মসৃণ ও টানটান করতে সাহায্য করে।
খাবারে প্রোটিনের পরিমাণ বাড়াতে ডিম, মুরগির মাংস, ডাল, সয়াদুধ ও অঙ্কুরিত খাবার খান। এগুলো পেশি গঠনে সাহায্য করে। 
মালিশ করা


চাইলে দুই টেবিল-চামচ লেবুর রস, কাঠবাদামের তেল ও তিন-চার চামচ গোলাপ জল নিয়ে পেস্ট তৈরি করে মালিশ করতে পারেন।
ভালো ফলাফলের জন্য এই পদ্ধতির পাশাপাশি গরম পানি দিয়ে গোসল করুন।
ব্যায়াম করা ও আর্দ্র থাকা


পানি কেবল আপনাকে আর্দ্র রাখবে না বরং ত্বকের স্থিতিস্থাপকতা ও নমনীয়তা বাড়াতে সাহায্য করে।
শক্তি ও সুস্বাস্থ্যের জন্য ধীরে ধীরে ব্যায়ামের দিকে মনোযোগ দিন। দড়ি লাফ, সাঁতার কাটা, দৌড়ানো, হালকা ব্যায়াম ইত্যাদির মাধ্যমে শরীরচর্চা শুরু করতে পারেন। পরে ওজন উত্তোলনের মতো ব্যায়াম করতে পারেন।
অ্যারোবিক্স অথবা এক ঘণ্টা কার্ডিও ব্যায়াম যেমন হাঁটা, শরীরের জন্য চমৎকার কাজ করে।
ছবি: রয়টার্স ও নিজস্ব।
আরও পড়ুন
গর্ভবতী মায়ের মানসিক যত্ন  
