ঢাকা: জুনিয়র স্কুল সার্টিফিকেট (জেএসসি) ও জুনিয়র দাখিল সার্টিফিকেট (জেডিসি) পরীক্ষায় ২০১৬ সালের চেয়ে এবছর ৫৫ হাজার ৯৬০টি জিপিএ (গ্রেড পয়েন্ট অ্যাভারেজ) পাঁচ কমেছে।
শিক্ষামন্ত্রী নূরুল ইসলাম নাহিদ প্রধানমন্ত্রীর কাছে ফল হস্তান্তরের পর শনিবার (৩০ ডিসেম্বর) এ তথ্য জানান। 
তিনি বলেন, গত বছর জেএসসি ও জেডিসিতে মোট জিপিএ পেয়েছিল ২ লাখ ৪৭ হাজার ৫৮৮জন। এবার পেয়েছে ১ লাখ ৯১ হাজার ৬২৮জন। গতবারের চেয়ে কমেছে ৫৫ হাজার ৯৬০টি।
২৪ লাখ ৮২ হাজার ৩৪২ জন পরীক্ষার্থীর মধ্যে এবার পাস করে করেছে ২০ লাখ ১৮ হাজার ২৭১ জন।
ফলাফল বিশ্লেষণে দেখা যায়, আটটি সাধারণ শিক্ষা বোর্ডে ১ লাখ ৮৪ হাজার ৩৯৭ জন জিপিএ পাঁচ পেয়েছে। গত বছর পেয়েছিল ২ লাখ ৩৫ হাজার ৫৯ জন। গতবছরের চেয়ে কমেছে ৫০ হাজার ৬৬২টি।
মাদরাসা শিক্ষা বোর্ডে এবার জিপিএ পাঁচ পেয়েছে ৭ হাজার ২৩১ জন, গতবছর পেয়েছিল ১২ হাজার ৫২৯ জন। অর্থাৎ কমেছে ৫ হাজার ২৯৮টি।
এদিকে এবারও ছাত্রের তুলনায় ছাত্রীরা জিপিএ পাঁচ বেশি পেয়েছে। তবে গতবছরের তুলনায় কমেছে।   এ বছর জেএসসি ও জেডিসিতে মোট জিপিএ পাঁচ পাওয়া ছাত্রের সংখ্যা ৮০ হাজার ৮৯৮ জন, গত বছর ছিল ১ লাখ ৬ হাজার ৩৪৫ জন। কমেছে ২৫ হাজার ৪৪৭টি।
