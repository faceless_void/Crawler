বরিশাল বিশ্ববিদ্যালয়ে গ্রীষ্মকালীন অবকাশ, রমজান, শব-ই-কদর, জুমা’তুল বিদা ও ঈদুল-ফিতর’১৬ উপলক্ষে একাডেমিক তথা ক্লাশ কার্যক্রমের ছুটি ১৯ জুন থেকে শুরু হয়ে চলবে আগামী ১৪ জুলাই পর্যন্ত।
বরিশাল: বরিশাল বিশ্ববিদ্যালয়ে গ্রীষ্মকালীন অবকাশ, রমজান, শব-ই-কদর, জুমা’তুল বিদা ও ঈদুল-ফিতর’১৬ উপলক্ষে একাডেমিক তথা ক্লাশ কার্যক্রমের ছুটি ১৯ জুন থেকে শুরু হয়ে চলবে আগামী ১৪ জুলাই পর্যন্ত।
একইসঙ্গে প্রশাসনিক তথা অফিস কার্যক্রমের ছুটি ২৬ জুন থেকে শুরু হয়ে চলবে ১৪ জুলাই পর্যন্ত।
১৭ জুলাই ২০১৬ রোববার থেকে যথারীতি বিশ্ববিদ্যালয়ের একাডেমিক ও প্রশাসনিক কার্যক্রম শুরু হবে।
ছুটি চলাকালীন সময়ে সিকিউরিটিসহ অন্যান্য জরুরি সেবাসমূহ বলবৎ থাকবে।
বিষয়টি নিশ্চিত করেছেন বিশ্ববিদ্যালয়ের জনসংযোগ কর্মকর্তা মো. ফয়সাল রুমি।
বাংলাদেশ সময়: ২০২১ ঘণ্টা, জুন ১৫, ২০১৬ আরএ
 
