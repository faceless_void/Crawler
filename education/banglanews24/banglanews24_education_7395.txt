ময়মনসিংহ: শিক্ষা জাতীয়করণের দাবিতে বাংলাদেশ শিক্ষক সমিতির ময়মনসিংহ বিভাগীয় শিক্ষক সমাবেশ সফল করতে সংবাদ সম্মেলন অনুষ্ঠিত হয়েছে।  
শনিবার (১৪ জানুয়ারি) বেলা ১১টায় নগরীর সিকে ঘোষ রোডে সমিতির আঞ্চলিক কার্যালয়ে শামসুন্নাহার বেগমের সভাপতিত্বে এ সংবাদ সম্মেলন অনুষ্ঠিত হয়।
সংবাদ সম্মেলনে লিখিত বক্তব্য পাঠ করেন বাংলাদেশ শিক্ষক সমিতির সভাপতি আবু বক্কর ছিদ্দিক।
উপস্থিত ছিলেন শিক্ষক নেতা মুকুল চন্দ্র দেব, মো. চাঁন মিয়া ও মো. আনোয়ার হোসেন প্রমুখ।
আগামী সোমবার (১৬ জানুয়ারি) সকাল ১০টায় নগরীর রেলওয়ে কৃষ্ণচূড়া চত্বরে ময়মনসিংহ বিভাগীয় শিক্ষক সমাবেশ অনুষ্ঠিত হবে বলে সংবাদ সম্মেলনে জানানো হয়।
