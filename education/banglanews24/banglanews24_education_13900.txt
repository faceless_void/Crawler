ইসলামী বিশ্ববিদ্যালয়ের (ইবি) ২০১৬-১৭ শিক্ষা বর্ষের স্নাতক প্রথম বর্ষ ভর্তি পরীক্ষার ফর্ম বিতরণ ১৫ সেপ্টেম্বর থেকে শুরু হয়ে ২০ অক্টোবর পর্যন্ত চলবে।
ইসলামী বিশ্ববিদ্যালয়: ইসলামী বিশ্ববিদ্যালয়ের (ইবি) ২০১৬-১৭ শিক্ষা বর্ষের স্নাতক প্রথম বর্ষ ভর্তি পরীক্ষার ফর্ম বিতরণ ১৫ সেপ্টেম্বর থেকে শুরু হয়ে ২০ অক্টোবর পর্যন্ত চলবে।
বিশ্ববিদ্যালয়ের তথ্য ও জনসংযোগ অফিসের উপ পরিচালক আতাউল হক এ তথ্য জানান।
বুধবার (৩১ আগস্ট) সকাল ১১টার দিকে বিশ্ববিদ্যালয়ের উপাচার্য ও কেন্দ্রীয় ভর্তি পরীক্ষা কমিটির আহ্বায়ক অধ্যাপক ড. রাশিদ আসকারীর সভাপতিত্বে ভর্তি পরীক্ষা কমিটির এক সভা অনুষ্ঠিত হয়।
বিশ্ববিদ্যালয় প্রশাসন ভবনে উপাচার্যের কনফারেন্স কক্ষে এ সভায় আরো উপস্থিত ছিলেন- উপ উপাচার্য অধ্যাপক ড. শাহিনুর রহমান, ট্রেজারার অধ্যাপক ড. সেলিম তোহা, প্রক্টর অধ্যাপক ড. মাহবুবর রহমান, ছাত্র উপদেষ্টা অধ্যাপক ড. আনোয়ার হোসেন, ভারপ্রাপ্ত রেজিস্ট্রার এসএম আব্দুল লতিফ ও সব অনুষদের ডিন ও বিভাগীয় সভাপতিরা।
ভর্তি পরীক্ষা ১৯ নভেম্বর শুরু হয়ে চলবে ২৪ নভেম্বর পর্যন্ত। সব ইউনিটের ভর্তি পরীক্ষার ফলাফল ২৯ নভেম্বরের মধ্যে প্রকাশিত হবে বলেও সভায় সিদ্ধান্ত হয়।
এছাড়া ০৪ ডিসেম্বর থেকে ১৫ ডিসেম্বর মেধা তালিকা ও ২১ ডিসেম্বর থেকে ১২ জানুয়ারি পর্যন্ত অপেক্ষমান তালিকা থেকে ভর্তি কার্যক্রম চলবে। ১৫ জানুয়ারি থেকে নতুন এ শিক্ষাবর্ষের ক্লাস শুরু হবে।
এ বছর বিশ্ববিদ্যালয় কর্তৃপক্ষ ভর্তি ফরমের মূল্য নির্ধারণ করেছেন ৪৫০ টাকা।
ভর্তিচ্ছুরা মোবাইল অপারেটর টেলিটকের মাধ্যমে ভর্তি আবেদন করতে পারবে।
এদিকে, ভর্তি পরীক্ষা কমিটির সভায় শারীরিক প্রতিবন্ধী কোটার আসন সংখ্যা ১৫ থেকে বাড়িয়ে ২০ জন করা হয়েছে।
এছাড়া ভর্তি পরীক্ষা সংক্রান্ত বিস্তারিত তথ্য বিশ্ববিদ্যালয়ের নিজেস্ব ওয়েব সাইট (www.iu.ac.bd) থেকে জানা যাবে।
