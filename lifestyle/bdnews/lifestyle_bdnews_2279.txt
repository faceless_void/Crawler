ব্যস্ত নাগরিক জীবনে সবারই সাধারণ সমস্যা-- উদ্যমহীনতা। এ থেকে কাটিয়ে ওঠার জন্য কিছু সাধারণ কৌশল উল্লেখ করা হয় স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে।
- আমিষ দিয়ে দিনের শুরু করুন। এটি কাজে সঠিক মনোনিবেশ নিশ্চিত করবে। প্রতিদিনের খাদ্যাভ্যাসে দুধ, ডিম, দই, সয়া ইত্যাদি আমিষসমৃদ্ধ খাবার রাখুন।
- কর্মস্পৃহা অটুট রাখতে শরীরচর্চার কোনো বিকল্প নেই। যদিও সকালে ব্যায়াম করতে অনেকেই গড়িমসি করেন। তবে ব্যায়াম করার জন্য সকালই সবচেয়ে ভালো সময়। সকালে ব্যায়াম করার মাধ্যমে দিনের একটা সুন্দর সূচনা করা যায়, সেই সঙ্গে পুরো দিন সজাগ ও সচেতন থাকা যায়।
- প্রতিদিন কমপক্ষে ১০ মিনিট করে শরীরে রোদ লাগানো উপকারী। বাইরে গিয়ে রোদ পোহানো সম্ভব না হলে, ঘরের দরজা-জানালা খুলে দিনের প্রাকৃতিক আলো ভেতরে আসতে দিন। দেহে ভিটামিন ডি উৎপন্ন করার জন্য সূর্যালোক জরুরি। আর ভিটামিন ডি’র অভাব কর্মস্পৃহার অভাবের কারণ হতে পারে।
- যদি সম্ভব হয় বিকেলে একটা সংক্ষিপ্ত ঘুম দিন। এমন কি ১৫ মিনিটের ঘুমও অনেকখানি চাঙ্গা করে দিতে পারে। শুধু নিশ্চিত করুন ঘুমানোর ঘরটি যেন নীরব হয়।
- শরীরে জড়ো হওয়া কুঁড়েমি আর গুমোটভাব কাটিয়ে উঠতে দিনের মাঝখানে ছোটখাটো ব্যায়াম করে নিতে পারেন। ব্যায়ামটি হতে পারে, সিঁড়ি বেয়ে দ্রুত ওঠানামা করা কিংবা ১০ মিনিট সময় আশপাশে কোথাও হেঁটে আসা। 
- পানিশূন্যতাও উদ্যমহীনতার কারণ হতে পারে। দিনে পর্যাপ্ত পানি খাচ্ছেন কিনা, সেদিকে লক্ষ রাখুন।
