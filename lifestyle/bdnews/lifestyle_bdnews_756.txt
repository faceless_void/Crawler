রেসিপি দিয়েছেন ফারহানা রহমান।
উপকরণ
চিজ কেকের জন্য: সব উপকরণ স্বাভাবিক তাপমাত্রার হতে হবে। ঠাণ্ডা কিছু ব্যবহার করা যাবে না।
ক্রিম চিজবার ১টি (২৫০গ্রাম)। ২টা ডিম। গলানো মাখন ২ টেবিল-চামচ। গুঁড়া করা চিনি ৩ থেকে ৪ কাপ। দুধ ২ টেবিল-চামচ। ভ্যানিলা এসেন্স ১ চা-চামচ।
কেকের নিচের স্তরের জন্য: ডাইজেস্টিভ বিস্কুটের গুঁড়া ১ কাপ। মাখন আধা কাপ (গলানো)।
পদ্ধতি
প্রথবমেই ওভেন প্রিহিট করে নিন। এবার বিস্কুটের গুঁড়ার সঙ্গে মাখন মিশিয়ে কেক প্যান বা যে পাত্রে কেক তৈরি করবেন তাতে চেপে চেপে বসান। মোটা একটা স্তর করুন। তবে যতটা সম্ভব চেপে চেপে বসাতে হবে। এবার ১৭০ডিগ্রি সেলসিয়াসে ১০ মিনিট বেইক করুন। হয়ে গেলে বাটি বের করে ঠাণ্ডা হতে দিন।
এবার কেকের মিশ্রণ তৈরি করুন। বড় বাটিতে ক্রিম-চিজ খুব ধীরে ভালোমতো ফেটিয়ে নিন। ইলেক্ট্রিক বিটার ব্যবহার করলে সবচেয়ে ভালো হয়। তবে স্পিড বেশি দেওয়া যাবে না।
দুধ মিশিয়ে দিন। এবার চিনি এবং ভ্যানিলা দিয়ে ফেটাতে থাকুন। চিনি গলে গেলে মাখন মিশিয়ে নিন। একটা একটা করে ডিম মেশান। আস্তে আস্তে ভালো ভাবে মেশাবেন।
এই মিশ্রণ বেইক করা বিস্কুটের উপর ঢেলে প্রিহিটেড ইলেক্ট্রিক ওভেনে ১৭০ থেকে ১৮0 ডিগ্রি সেলসিয়াসে ২৫ মিনিট বেইক করুন।


অরেঞ্জ সিরাপ: আধা কাপ চিনি এক কাপ পানিতে গুলিয়ে আগুনে জ্বাল দিন। একটি কমলার কুচি এবং একটি কমলা গোল গোল করে কেটে সিরাপের ভেতর দিন। কিছুক্ষণ জ্বাল দিয়ে কমলা সেদ্ধ হয়ে গেলে গোল টুকরাগুলো তুলে নিন। আর বাকি কমলাসহ পানি ব্লেন্ড করে ফেলুন। এরপর ব্লেন্ড করা পানি আবার জ্বাল দিয়ে ঘন করুন।
কেকের উপর সিরাপ ছড়িয়ে পরিবেশন করুন। পরিবেশনের সময় সিদ্ধ করা কমলার টুকরাগুলো উপরে সাজিয়ে দিন।
সমন্বয়ে: ইশরাত মৌরি।
রেড ভেলভেট কেক
বানানা মার্বেল কেক
হার্ট চকলেট কেক
রেইনবো কেক
অরেঞ্জ কেক
