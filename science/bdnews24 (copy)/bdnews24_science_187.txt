গাড়িতে প্রয়োজনীয় ক্যামেরা, সেন্সর এবং রেডার সংযুক্ত করা হলেও গাড়িগুলো পরিপূর্ণভাবে স্বয়ংক্রিয় হতে এখনও বেশ কয়েক বছর সময় লাগবে বলে জানিয়েছে বিবিসি।
গত বছর গাড়িতে চালকের সহায়তাকারী সিস্টেম 'অটোপাইলট' ফিচার উন্মোচন করে টেসলা। কিন্তু নতুন গাড়িতে আপাতত অটোপাইলট ফিচার বন্ধ রাখছে এই প্রতিষ্ঠান। নতুন হার্ডওয়্যার সিস্টেমের 'জোরালো' পরীক্ষা চলাতেই এই সিদ্ধান্ত নেওয়া হয়েছে।
টেসলা প্রধান ইলন মাস্ক বলেন, "এই হার্ডওয়্যারগুলো আসলে গাড়িতে একটি সুপার কম্পিউটার।" কিন্তু নীতিনির্ধারক এবং জনগণই সিদ্ধান্ত নেবে আসলে ঠিক কখন স্ব-চালিত গাড়িগুলো রাস্তায় ব্যাবহার করা হবে।
তিনি আরও জানান, আপাতত হার্ডওয়্যারগুলো "শ্যাডো মোডে" চালানো হবে। এই হার্ডওয়্যারগুলো দিয়ে তথ্য সংগ্রহ করা হবে কখন এই প্রযুক্তির কারণে দূর্ঘটনা ঘটছে অথবা দূর্ঘটনা এড়িয়ে যাচ্ছে।
মাস্ক আত্মবিশ্বাসী যে, তিনি একদিন নীতিনির্ধারকদের এমন তথ্য দেখাতে পারবেন যে স্ব-চালিত গাড়ির প্রযুক্তি মানুষ চালকের চেয়ে বেশি নিরাপদ।
