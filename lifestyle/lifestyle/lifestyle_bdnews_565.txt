ইন্দ্রপ্রষ্ঠা অ্যাপোলো হাসপাতালে জ্যেষ্ঠ অর্থোপেডিক সার্জন রাজু ভাইশ্যা বলেন, “ধূমপান হাড়ের স্বাস্থ্যের উপর ক্ষতিকর প্রভাব ফেলে, হাড়ের ঘনত্ব কমে যায়। ফলাফল, অল্প বয়সেই হাড়ক্ষয় রোগ।”
স্কুল-কলেজে পড়ার সময়ে অনেকের ধূমপানের বদোভ্যাস গড়ে ওঠে, যেসময় হাড় উন্নয়নশীল অবস্থায় থাকে। শরীরের ক্যালসিয়াম ও ভিটামিন-ডি শোষণ প্রক্রিয়াতে কুপ্রভাব ফেলে ধূমপান।
ডাক্তারদের মতে, “হাড় ক্ষতিগ্রস্ত হলে সেই ক্ষয়পূরণ হতে অধূমপায়ীদের তুলনায় ধূমপায়ীদের সময় বেশি লাগে, বাড়ে জটিলতা বৃদ্ধির ঝুঁকিও।”
ভাইশ্যা বলেন, “হাড়ের গঠন চলার সময়ে ধূমপানের অভ্যাস হাড়ক্ষয় রোগ হওয়ার ঝুঁকি কয়েকগুণ বাড়িয়ে দেয়। আবার ত্রিশ পেরোনোর পর ধূমপানের অভ্যাস অব্যহত থাকলে হাড় ক্ষয়ের গতি বেড়ে যায় দ্বিগুণ।”
দ্য গ্লোবাল অ্যাডাল্ট টোবাকো সার্ভে ইন্ডিয়া’র প্রতিবেদন অনুসারে, ধূমপানের কারণে শূধু ভারতেই প্রতিবছর ১০ লাখ লোক মারা যায়।
সম্প্রতি অ্যানালস অফ আমেরিকান থোরাসিক সোসাইটি শীর্ষক জার্নালে প্রকাশিত এক জরিপে বলা হয়, পুরুষ ও নারীদের হাড়ের ক্ষয়ের একটি স্বতন্ত্র ঝুকিপূর্ণ কারণ ধূমপান।
বছরে ধুমপানের পরিমাণ এক প্যাকেট বৃদ্ধিতেই হাড়ের ঘনত্ব কমে যাওয়ার ঝুঁকি বাড়ে ০.৪ শতাংশ।
