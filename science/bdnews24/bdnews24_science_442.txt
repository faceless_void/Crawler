পোর্টালটির সূত্রমতে, এই খবর প্রকাশের আগের সপ্তাহে দেশটির দিল্লি ও পাটনার মধ্যে ৯০টিরও বেশি ট্রেন কুয়াশার কারণে নির্ধারিত সময়ের দেরিতে পৌঁছেছে।
এক প্রেস বিজ্ঞপ্তিতে প্রতিষ্ঠানটি বলে, “নতুন ‘ফগ অ্যালার্ট’ ফিচার যাত্রীদের রুটে কুয়াশার আশংকা, তীব্রতা ও ট্রেন শিডিউলে পড়া সম্ভাব্য প্রভাব নিয়ে জানাবে। এই ফিচার যাত্রীদের তাদের ভ্রমণ ভালোভাবে করার পরিকল্পনা করতে ও তাদের কাছের লোকদের বিলম্বের বিষয়ে জানার সুযোগ দেবে।
ফিচারটি নির্দিষ্ট কোনো স্টেশনে কোনো ট্রেনের সম্ভাব্য দেরি নিয়েও জানাবে বলে জানিয়েছে আইএএনএস।
ওই প্রতিষ্ঠানটির সহ-প্রতিষ্ঠাতা ও প্রধান নির্বাহী মানিশ রাঠি বলেন, “আমরা ব্যবহারকারীদের তাদের ভ্রমণ ভালোভাবে ব্যবস্থাপনায় সহায়তা করতে এই ফিচার চালু করেছি।”
২০১৮ সালের মধ্যেই ভারতের কলকাতায় স্বচালিত পাতাল ট্রেন চালু হতে পারে বলে এক খবরে জানা যায়।
ইন্ডিয়ান রেলওয়েস-এর রিসার্চ ডিজাইন এবং স্ট্যান্ডার্ড অর্গানাইজেশনস-এর পরিচালক ইয়াতিশ কুমার বলেন, “২০১৮ সালের মার্চ থেকে শহরের সব নতুন মেট্রো রেইকগুলো (পাতাল ট্রেন) গ্রেড অফ অটোমেশন থ্রি (জিওএ থ্রি) চালকবিহীন পাতাল ট্রেনে পরিবর্তিত করার লক্ষ্য নেওয়া হয়েছে।”
