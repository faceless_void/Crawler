এই ঘটনার সঙ্গে সংশ্লিষ্ট তিন ব্যক্তির বরাতে এ খবর প্রকাশ করেছে রয়টার্স। সাধারণত কোডিংয়ের ছোটখাট কোনো ত্রুটি শনাক্তে ‘বাগ বাউন্টি’ প্রোগ্রাম চালানো হয়। উবারও ‘কথিত বাগ বাউন্টি’ প্রোগ্রামের মাধ্যমে এই ডেটা ধ্বংসের জন্য অর্থ পরিশোধ করেছে বলে সংবাদমাধ্যমটির প্রতিবেদনে উল্লেখ করা হয়।
২০১৬ সালে এক হ্যাকিংয়ের মাধ্যমে ৫.৭ কোটি উবার ব্যবহারকারীর ব্যক্তিগত ডেটা ফাঁস হয়ে যায় বলে চলতি বছর ২১ নভেম্বর এক ঘোষণায় জানায় উবার। হ্যাকিংয়ে শিকারদের মধ্যে ছয় লাখ মার্কিন চালকও ছিলেন। এ ঘটনা জানার পরও চেপে থাকার কথা স্বীকার করে প্রতিষ্ঠানটি।
রয়টার্স-এর প্রতিবেদন মতে, ফাঁস হওয়া ডেটা ধ্বংসে ওই হ্যাকারকে এক লাখ ডলার পরিশোধ করেছিল উবার। কিন্তু প্রতিষ্ঠানটি ওই হ্যাকার আর কীভাবে এই অর্থ পরিশোধ করা হয়েছিল তা নিয়ে কোনো তথ্য প্রকাশ করেনি। ২০১৬ সালে প্রতিষ্ঠানের সফটওয়্যারের কোনো ত্রুটি খোঁজার জন্য নিরাপত্তা গবেষকদের পুরষ্কার দেওয়ার একটি প্রোগ্রাম চালু করে, এর মাধ্যমেই ওই অর্থ পরিশোধ করা হয় বলে রয়টার্স-এর সূত্র থেকে জানানো হয়। 
উবারের এই ‘বাগ বাউন্টি’ প্রোগ্রাম আয়োজন করে হ্যাকারওয়ান নামের একটি প্রতিষ্ঠান। এ প্রতিষ্ঠান আরও কয়েকটি প্রতিষ্ঠানের জন্যও তাদের প্লাটফর্মের সেবা দিয়ে থাকে।  
ওই হ্যাকারকে শনাক্ত করা যায়নি বলে প্রতিবেদনে জানিয়েছে রয়টার্স। আর উবারের মুখপাত্র ম্যাট কালম্যান এ বিষয়ে মন্তব্য করতে অস্বীকৃতি প্রকাশ করেছেন।
উবারের প্রধান নির্বাহী দারা খাসরোশাহী হ্যাকিংয়ের ঘটনা নিয়ে গত বছরেই জানানো উচিৎ ছিল মন্তব্য করে চলতি বছর নভেম্বরে দুঃখ প্রকাশ করেন। সেই সঙ্গে প্রতিষ্ঠানের নিরাপত্তা বিভাগে শীর্ষ দুই কর্মকর্তাকে অব্যাহতি দেন তিনি।
হ্যাকিংয়ের ঘটনা গোপন রাখা আর হ্যাকারকে অর্থ পরিশোধ করার বিষয়ে চূড়ান্ত সিদ্ধান্ত কারা নিয়েছিল তা এখনও অস্পষ্ট। তবে রয়টার্সের সূত্রমতে, ২০১৬ সালের নভেম্বরে তৎকালীন উবার প্রধান ট্রাভিস কালানিক এই ডেটা ফাঁস ও বাগ বাউন্টি প্রোগ্রাম সম্পর্কে জানতেন।
চলতি বছর জুনে উবার প্রধানের দায়িত্ব থেকে অব্যাহতি পাওয়া কালানিক এ নিয়ে কোনো মন্তব্য করতে অস্বীকৃতি প্রকাশ করেছেন বলে জানান তার মুখপাত্র।
বাগ বাউন্টি প্রোগ্রামে এক লাখ ডলার পুরস্কার দেওয়াকে অনেকটা নজিরবিহীনই বলা চলে। হ্যাকারওয়ান-এর সাবেক এক নির্বাহী বলেছেন, এই অংকটা ‘সর্বকালের জন্য রেকর্ড’। এ ধরনের প্রোগ্রামে সাধারণত পাঁচ হাজার থেকে ১০ হাজার ডলার পুরস্কার দেওয়া হয়। সেইসঙ্গে নিরাপত্তা পেশাদাররা বলছেন, কোনো বাউন্টি প্রোগ্রামে যে হ্যাকার ডেটা হাতিয়ে নিয়েছেন তাকে অর্থ দেওয়া নিয়মের বাইরে।
হ্যাকারওয়ান প্রধান নির্বাহী মার্টেন মিকোস বলেন, তিনি কোনো একক গ্রাহকের প্রোগ্রাম নিয়ে আলোচনা করতে পারেন না।
রয়টার্স-এর তিন সূত্রের মধ্যে দুই সূত্র জানিয়েছে, উবার ওই হ্যাকারের পরিচয় শনাক্তে অর্থ পরিশোধ করেছে আর তার সঙ্গে সামনে যাতে অপরাধ না করা হয় তা নিয়ে অপ্রকাশিত চুক্তি করেছে। পুরো ডেটা ধ্বংস হয়েছে নিশ্চিত করতে উবার হ্যাকারের মেশিনে বিশ্লেষণা চালিয়েও দেখেছে।
প্রতিবেদনে আরও বলা হয়, উবার ২০১৬ সালে পরিচয় গোপন রাখা একজনের কাছ থেকে একটি ইমেইল পায়। এই ইমেইলে ব্যবহারকারীদের ডেটার বিনিময়ে অর্থ চাওয়া হয়।
 
আরও খবর
পদ ছাড়লেন তিন উবার নিরাপত্তা কর্মী
কর্মীদের গোপন আলাপ ‘বন্ধে’ উবার প্রধান
উবার হ্যাকিং: আক্রান্ত ২৭ লাখ ব্রিটিশ
