রাজশাহী:  রাজশাহী প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ের (রুয়েট) ১৪তম প্রতিষ্ঠাবার্ষিকী ও বিশ্ববিদ্যালয় দিবস শুক্রবার (১ সেপ্টেম্বর)। নানা কর্মসূচির মধ্য দিয়ে দিনটি উদযাপন করবে বিশ্ববিদ্যালয় কর্তৃপক্ষ।
রুয়েট প্রেস অ্যান্ড ইনফরমেশন সেকশনের উপ-পরিচালক গোলাম মর্তুজা বৃহস্পতিবার (৩১ আগস্ট) বিকেলে এই তথ্য নিশ্চিত করেছেন।
তিনি জানান, শুক্রবার সকাল সাড়ে ৯টায় জাতীয় ও বিশ্ববিদ্যালয়ের পতাকা উত্তোলনের মধ্যে দিয়ে রুয়েটের প্রতিষ্ঠাবার্ষিকী ও বিশ্ববিদ্যালয় দিবসের অনুষ্ঠান শুরু হবে। 
দিনব্যাপী কর্মসূচির উদ্বোধন করবেন উপাচার্য প্রফেসর ড. মোহা. রফিকুল আলম বেগ। এরপর বিশ্ববিদ্যালয়ের শহীদ ছাত্রদের স্মরণে তাদের কবরে পুস্পস্তবক অর্পণ ও দোয়া অনুষ্ঠিত হবে। সকাল পৌনে ১০টায় বৃক্ষরোপণ কর্মসূচির উদ্বোধন করা হবে। 
পবিত্র ঈদুল আজহার ছুটি থাকায় এবার বিশ্ববিদ্যালয় দিবসের অনুষ্ঠান সংক্ষিপ্ত করা হয়েছে। সংশ্লিষ্টদের অংশ নেওয়ার জন্য আহ্বান জানানো হয়েছে। 
