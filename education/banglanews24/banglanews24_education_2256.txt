ঢাকা বিশ্ববিদ্যালয়ে (ঢাবি) ২০১৬-১৭ শিক্ষাবর্ষে স্নাতক সম্মান শ্রেণির ‘গ’ ইউনিটের ভর্তি পরীক্ষার ফল বৃহস্পতিবার রাত ৯টায় প্রকাশ করা হবে।
ঢাকা বিশ্ববিদ্যালয়: ঢাকা বিশ্ববিদ্যালয়ে (ঢাবি) ২০১৬-১৭ শিক্ষাবর্ষে স্নাতক সম্মান শ্রেণির ‘গ’ ইউনিটের ভর্তি পরীক্ষার ফল বৃহস্পতিবার রাত ৯টায় প্রকাশ করা হবে। বৃহস্পতিবার (৬ অক্টোবর) বিকেলে বিশ্ববিদ্যালয়ের জনসংযোগ দফতরের ভারপ্রাপ্ত পরিচালক নুরুল ইসলাম বাংলানিউজকে এ তথ্য নিশ্চিত করেন।
শুক্রবার (৩০ সেপ্টেম্বর) অনুষ্ঠিত ‘গ’ ইউনিটে পরীক্ষায় ১২শ ৫০টি আসনের জন্য ভর্তিচ্ছু আবেদনকারীর সংখ্যা ৪২ হাজার ১শ ৪৭জন। প্রতি আসনের জন্য লড়ে ৩৩ জন শিক্ষার্থী।
ভর্তি পরীক্ষা বিশ্ববিদ্যালয় ক্যাম্পাস ও ক্যাম্পাসের বাইরের মোট ৫৫টি কেন্দ্রে অনুষ্ঠিত হয়।
পরীক্ষা অনুষ্ঠিত হওয়ার তিনদিনের মধ্যে সাধারণত ফল প্রকাশ করা হলেও এবার ছয়দিন সময় নেওয়া হলো।
বাংলাদেশ সময়: ১৬১১ ঘণ্টা, অক্টোবর ৬, ২০১৬ এসকেবি/এএ
