ত্বক ও চুল পরিচর্যার অন্যতম উপাদান কলা। সাজসজ্জা-বিষয়ক একটি ওয়েবসাইটের প্রকাশিত প্রতিবেদন অবলম্বনে এই ফল দিয়ে চুল পরিচর্যার কয়েকটি প্রচলিত পন্থা জানানো হল।
কলা ও মধু
শুষ্ক ও রুক্ষ চুলের যত্নে কার্যকর সমাধান পাওয়া যাবে এই মাস্ক থেকে। মসৃণ করার পাশাপাশি এটা চুলের স্থিতিস্থাপকতাও রক্ষা করে।
পদ্ধতি: দুটি পাকাকলা কাঁটাচামচের সাহায্যে পিষে নিন। সঙ্গে দুই টেবিল-চামচ মধু যোগ করুন এবং ভালো মতো মেশান যেন কোনো দানাভাব না থাকে।
মিশ্রণটি চুলে লাগিয়ে ‘শাওয়ার ক্যাপ’য়ের সাহায্যে চুল ঢেকে রাখুন। আধ ঘণ্টা অপেক্ষা করে ধুয়ে ফেলুন।
কলা ও জলপাইয়ের তেল
দুর্বল চুলের সুস্থতা ফিরিয়ে আনতে এবং চুলের কোঁকড়াভাব কমাতে এই মাস্ক সাহায্য করে।
পদ্ধতি: একটি পাকাকলা কাঁটাচামচের সাহায্যে পিষে দুই টেবিল-চামচ খাঁটি জলপাইয়ের তেল মিশিয়ে নিন যেন কোনো দানাভাব না থাকে।
ব্রাশের সাহায্যে সম্পূর্ণ চুলে এই মিশ্রণ লাগিয়ে ২০ মিনিট অপেক্ষা করুন। তারপর শ্যাম্পু দিয়ে চুল ধুয়ে ফেলুন।
চুলে মসৃণভাব আনতে নারিকেলের তেল বা কোনো অর্গান তেল ব্যবহার করতে পারেন।
কলা, পেঁপে ও মধু
প্রোটিন সমৃদ্ধ এই মাস্ক উজ্জ্বলতা বাড়ানোর পাশাপাশি চুল শক্তিশালী করে।
পদ্ধতি: একটি পাকাকলা পিষে সঙ্গে চার-পাঁচ টুকরা পাকাপেঁপের মিশ্রণ যোগ করুন। তারপর দুই চা-চামচ মধু নিয়ে ভালোভাবে মিশিয়ে নিন।
সম্পূর্ণ চুল ও মাথার ত্বকে মিশ্রণটি লাগান। সব চুল উঁচু করে পেঁচিয়ে নিন এবং একটি টুপির সাহায্যে চুল ঢেকে রাখুন।  
কিছুক্ষণ অপেক্ষা করে প্রথমে হালকা গরম পানি দিয়ে চুল ধুয়ে নিন। তারপর শ্যাম্পু করুন।  


খুশকি মুক্ত রাখার পাশাপাশি আর্দ্রতা রক্ষা করতে এই মাস্ক সাহায্য করে।
পদ্ধতি: একটি পাকাকলা ভর্তা করে, চার টেবিল-চামচ তাজা এবং কোনো রকমের ঘ্রাণ যুক্ত নয় এমন দই এবং দুএক চা-চামচ মধু নিন। এগুলো খুব ভালোভাবে ব্লেন্ড করুন।
চুলের আগা থেকে গোড়া পর্যন্ত ভালোভাবে এই মাস্ক লাগান। ২০ থেকে ২৫ মিনিট অপেক্ষা করার পর চুলে শ্যাম্পু করে নিন।    
কলা, ডিম ও মধু
শুষ্ক চুলে অতিরিক্ত আর্দ্রতা সরবারহ করতে এই মাস্ক কার্যকর।
পদ্ধতি: দুটি পাকাকলা পিষে তার মধ্যে একটি ডিম নিন। সঙ্গে দুই টেবিল-চামচ মধু যোগ করে ভালোভাবে মিশিয়ে নিন। ডিমের গন্ধ দূর করতে চাইলে এতে যে কোনো সুগন্ধি তেল যেমন- ল্যাভেন্ডার, কমলা বা লেবুর তেল মেশাতে পারেন।
সম্পূর্ণ চুলে ব্রাশের সাহায্যে মাস্কটি লাগিয়ে নিন। ২০ মিনিট অপেক্ষা করে শ্যাম্পু করুন। 
কলা ও নারিকেলের দুধ
এই মাস্ক ‘ডিপ কন্ডিশনিং’য়ের কাজ করে চুলকে কোমল ও মসৃণ করতে সাহায্য করবে।
পদ্ধতি: দুটি পাকাকলা ও আধ কাপ নারিকেলের দুধ ভালো মতো মিশিয়ে নিন। চাইলে এতে কয়েক ফোঁটা মধু মেশাতে পারেন।
হালকা ভেজা চুলে সামান্য মালিশ করে এই মাস্ক লাগান। দেড় ঘণ্টা অপেক্ষা করে হালকা শ্যাম্পুর সাহায্যে চুল ধুয়ে নিন।
আরও পড়ুন
কলা তর্ক  
