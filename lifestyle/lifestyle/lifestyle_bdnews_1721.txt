সম্পূর্ণ তেল ছাড়া এই খাবার খুবই স্বাস্থ্যসম্মত। রেসেপি দিয়েছেন প্যারিস প্রবাসী রন্ধনশিল্পী ফারহা সামিন।   
উপকরণ: মুরগির মাংসের কিমা ১ কাপ। পেঁয়াজকুচি ২ টেবিল-চামচ। ধনেপাতা, লবণ ও কাঁচামরিচ স্বাদ মতো। ধনে, আদা ও রসুন বাটা- ১ চা-চামচ করে। টমেটো সস ১ চা-চামচ, সয়া সস ১ চা-চামচ। চিলি সস আধা চা-চামচ। গোলমরিচের গুঁড়া আধা চা-চামচ। বাসমতী চাল আধা কাপ। খাবার রং অল্প।
পদ্ধতি: পানিতে ৩০ মিনিট চাল ভিজিয়ে রাখতে হবে।
মাংসসহ সব উপকরণ মেখে বলের মতো গোল করে নিন। এরপর বলগুলো চালের উপর গড়িয়ে হালকা করে চেপে ভাপে ঢাকানা দিয়ে রান্না করুন ২০/২৫ মিনিট।
তারপর সাবধানে সার্ভিং ডিশে তুলে সস দিয়ে পরিবেশন করুন মজাদার ঝাল-কদম।
আরও রেসেপি
ওটসে তৈরি ভাপা পিঠা  
