ইইউ'র অভিযোগ- গুগলের মোবাইল অপারেটিং সিস্টেম অ্যান্ড্রয়েড প্রতিদ্বন্দ্বীদের চাপের মুখে রেখে নিজেদের আধিপত্যের অপব্যবহার করছে।
এর আগে এই প্রযুক্তি প্রতিষ্ঠানকে ইইউ একটি সময় বেধে দিলেও গুগল জানায় তাদের আরও সময় প্রয়োজন আর তারা ৭ অক্টোবরের মধ্যে এ নিয়ে জবাব দেবে। ইইউ কমিশন-এর একজন মুখপাত্র জানান, গুগল এ নিয়ে তৃতীয়বারের মতো তাদের সময়সীমা বাড়াল।
২০১৬ সালের এপ্রিলে কমিশন গুগলের বিরুদ্ধে অভিযোগ আনে যে, তারা ব্যবহারীদের ক্ষতি করছে। এর পেছনে গুগল ক্রোম ব্রাউজার এবং গুগল সার্চ ফোন নির্মাতাদের জন্য প্রি-ইনস্টলেশনের জন্য বাধ্য করে দেওয়ার কথা উল্লেখ করা হয়।
ওই মুখপাত্র বলেন, “আমরা নিশ্চিত করেছি যে কমিশনের দেওয়া নতুন সময়সীমা ৭ অক্টোবর। গুগল মামলা করা নথিগুলো পুনরায় পর্যবেক্ষণের জন্য আরও সময় চেয়েছে।"
বিশ্বের সবচেয়ে প্রভাবশালী সার্চ ইঞ্জিন গুগল ইইউ কমিশনের কাছে অন্য একটি অভিযোগের জন্য ১৩ অক্টোবর পর্যন্ত সময় পেয়েছে, জানিয়েছে রয়টার্স। গুগল তাদের নিজেদের সার্চ ইঞ্জিনের সার্চ ফলাফলে প্রতিদ্বন্দ্বী প্রতিষ্ঠানগুলোর তুলনায় নিজের শিপিং সার্ভিসকে বাড়তি সুবিধা দিচ্ছিল বলে ওই অভিযোগে বলা হয়।
অনলাইন প্রতিদ্বন্দ্বীদের সার্চ বিজ্ঞাপনকে ব্লক করে দেওয়ার অভিযোগের জবাব দিতে গুগল ৫ অক্টোবর পর্যন্ত সময় পেয়েছে।
