বরিশাল: বরিশাল সরকারি বিএম কলেজে ছাত্র সংসদ (বাকসু) নির্বাচন না দেওয়ায় বিক্ষোভ মিছিল ও সমাবেশ করেছে শিক্ষার্থীরা। কর্মসূচি থেকে তারা অবিলম্বে বাকসু নির্বাচনের তফসিল ঘোষণার দাবি জানায়।
সাধারণ শিক্ষার্থীরা বুধবার (৮ ফেব্রুয়ারি) সকাল ১১টার দিকে কলেজের জিরো পয়েন্ট থেকে ব্যানার হাতে বিক্ষোভ মিছিল নিয়ে কলেজের বিভিন্ন সড়ক ঘুরে প্রশাসনিক ভবনের সামনে গিয়ে সমাবেশ করে।
অধ্যক্ষকে না পেয়ে শিক্ষার্থীরা প্রশাসনিক ভবনের প্রধান গেটে তালা ঝুলিয়ে দেয়। পরে কলেজের অধ্যক্ষ স.ম. ইমানুল হাকিমের নেতৃত্বে একটি প্রতিনিধি দল বাকসু নির্বাচনের তফসিল ঘোষণার আশ্বাস দিলে শিক্ষার্থীর‍া কর্মসূচি স্থগিত করে।
কলেজের সমাজ কর্ম বিভাগের ছাত্র মো. সায়েম বলেন, ১৫ বছর আগে সর্বশেষ বাকসু নির্বাচন হয়েছিলো। গত পাঁচ বছর আগে অস্থায়ী কর্ম পরিষদের মাধ্যমে ৩ মাসের জন্য বাকসু’র কার্যক্রম শুরু হয়। তারপর থেকে ওই অস্থায়ী কর্ম পরিষদই চলে আসছে এখন পর্যন্ত।
নিয়ম অনুযায়ী, ওই কর্ম পরিষদ বহু আগেই অবৈধ হয়ে গেছে। নেতাদের কলেজে পাওয়া যায় না বলে যেকোন দাবি আদায়ে সাধারণ শিক্ষার্থীদের বেগ পেতে হয়।
