ময়মনসিংহ: শীতকালীন ছুটি শেষে রোববার (২১ জানুয়ারি) থেকে খুলছে জাতীয় কবি কাজী নজরুল ইসলাম বিশ্ববিদ্যালয়। ওইদিন থেকে ফের বিশ্ববিদ্যালয়ের একাডেমিক ও প্রশাসনিক কার্যক্রম চালু হবে।
শনিবার (২০ জানুয়ারি) বিকেলে বাংলানিউজকে এ তথ্য নিশ্চিত করেন বিশ্ববিদ্যালয়ের রেজিস্ট্রার (ভারপ্রাপ্ত) ড. মো. হুমায়ুন কবির। 
তিনি জানান, শীতকালীন ছুটি উপলক্ষে গত রোববার (০৭ জানুয়ারি) থেকে বৃহস্পতিবার (১৮ জানুয়ারি) পর্যন্ত বিশ্ববিদ্যালয়ের সব বিভাগের একাডেমিক কার্যক্রম বন্ধ ছিলো। 
একইসঙ্গে রোববার (১৪ জানুয়ারি) থেকে বৃহস্পতিবার (১৮ জানুয়ারি) পর্যন্ত বন্ধ ছিলো প্রশাসনিক কার্যক্রম। ১৪ দিনের ছুটি শেষে রোববার (২১ জানুয়ারি) থেকে বিশ্ববিদ্যালয়ের সব কার্যক্রম শুরু হবে। 
