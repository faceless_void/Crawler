রেসিপি দিয়েছেন রন্ধনশিল্পী তাসনুভা মাহমুদ নওরিন।
উপকরণ: শুকনা লালমরিচ ৩-৪টি পানিতে ভেজানো। পেঁয়াজ ১টি। রসুন আস্ত ৩-৪ কোঁয়া। পেঁয়াজপাতা ১/৩ কাপ। ধনেপাতা আধা কাপ। মুরগি/বিফ/মাটন কিমা ৩ কাপ। লবণ ১ চা-চামচ বা স্বাদ মতো। লালমরিচ টালা ১ চা-চামচ বা ঝাল অনুয়ায়ী। টমেটো সস – ২ টেবিল-চামচ। টমেটো ফালি করা কয়েকটি। কয়েকটা আস্ত কাঁচামরিচ।
পদ্ধতি: ব্লেন্ডারে পেঁয়াজকুচি, ধনেপাতা, ভেজানো লালমরিচ, পেঁয়াজপাতা, রসুন-কোঁয়া মিশিয়ে নেবেন। তবে একদম পেস্ট করা যাবে না।
এবার মুরগির কিমার সঙ্গে এই মিক্স করা উপাদান, লবণ, লাল-টালা-মরিচ, টমেটো সস দিয়ে মিশিয়ে নিন।
বেইকিং প্যানে হালকা তেল ব্রাশ করে এই মিশ্রণ ঢেলে চেপে পিৎজার আকার করে নিন। তারপর পিৎজার মতো তিন কোণা করে কেটে প্রতিটি টুকরার উপর একটা করে টমেটো ফালি ও একটা আস্ত কাঁচামরিচ বসিয়ে দিয়ে ওভেনে ১৮০ ডিগ্রি সেলসিয়াসে ২৫ থেকে ৩০ মিনিট বা যতক্ষণ লাগে বেইক করুন।
মাঝে মুরগির কিমা সিদ্ধ হয়ে গেলে উপরে একটু টমেটো সস ব্রাশ করে দিন। এতে দেখতে ও খেতে আরও বেশি ভালো লাগবে।
মুরগির কিমা সিদ্ধ হয়ে গেলে নামিয়ে কেটে পোলাও, বিরিয়ানি, নানরুটি বা পরোটার সঙ্গে পরিবেশন করুন এই ট্রে কাবাব।
আরও রেসিপি
পুর ভরা বেগুন  
