রেসিপি দিয়েছেন আফরোজ সাইদা।
উপকরণ
বড় ডাব ২টি। চিনি ২ টেবিল-চামচ (কম বা বেশি দিতে পারেন, নাও দিতে পারেন)। অথবা ডায়াবেটিক চিনি ২ টেবিল-চামচ। চায়না গ্রাস ১০ গ্রাম। পানি ১ কাপ। সবুজ রং ২,৩ ফোঁটা (ইচ্ছা)।
পদ্ধতি
ডাব ভেঙে চার কাপ পানি আলাদা করে রাখুন।
নারিকেল কুড়ুনি দিয়ে লম্বা করে শাঁস বের করে নিতে হবে। কুড়ুনি না থাকলে ছুরি দিয়ে ছোট ছোট ‍টুকরা করে কাটুন।
একটা হাঁড়িতে এককাপ পানি ও চায়না গ্রাস চিনিসহ জ্বাল দিন। চায়না গ্রাস গলে মিশে গেলে তাতে ডাবের পানি যোগ করে একটা জ্বাল দিয়ে নামিয়ে ফেলুন।
একটা কেকের মোল্ডে কুড়ানো ডাবের শাঁস অর্ধেকটা ছড়িয়ে দিয়ে এর উপর ডাবের পানির মিশ্রণ ঢেলে দিন। কিছুটা ঠাণ্ডা হলে বাকি শাঁসগুলো দিয়ে ফ্রিজে রাখুন।
জমে গেলে প্লেটে উল্টিয়ে ঢেলে নিজের পছন্দ মতো আকারে কেটে পরিবেশন করুন।
সমন্বয়ে: ইশতার মৌরি।
ব্রোকেন গ্লাস পুডিং
