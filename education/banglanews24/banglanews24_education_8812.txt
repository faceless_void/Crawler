রাজশাহী প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ে (রুয়েট) দু’টি নতুন বিভাগ খোলার সিদ্ধান্ত নিয়েছে কর্তৃপক্ষ।
রাজশাহী: রাজশাহী প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ে (রুয়েট) দু’টি নতুন বিভাগ খোলার সিদ্ধান্ত নিয়েছে কর্তৃপক্ষ।
চলতি ২০১৬-২০১৭ শিক্ষাবর্ষ থেকেই বিল্ডিং ইঞ্জিনিয়ারিং অ্যান্ড কনস্ট্রাকশন ম্যানেজমেন্ট ও ম্যাটারিয়াল সায়েন্স অ্যান্ড ইঞ্জিনিয়ারিং নামে এ দু'টি বিভাগের শিক্ষা কার্যক্রম শুরু হবে।
মঙ্গলবার (৩০ আগস্ট) বিকেলে রুয়েটের ভাইস চ্যান্সেলর প্রফেসর ড. রফিকুল আলম বেগ এ তথ্য জানান।   তিনি জানান, দেশে দক্ষ জনবল সৃষ্টি এবং যুগের চাহিদার সঙ্গে সামঞ্জস্য রেখে এই বিভাগ দু’টি খোলা হয়েছে। চলতি শিক্ষাবর্ষে নতুন বিভাগে ভর্তি কার্যক্রম শুরু হবে। দুই বিভাগে ৩০ জন করে মোট ৬০ জন শিক্ষার্থী ভর্তি করা হবে।
তিনি আরও জানান, ২০১৬-২০১৭ শিক্ষাবর্ষে প্রথম বর্ষ স্নাতক কোর্সে শিক্ষার্থী ভর্তির জন্য যে বিজ্ঞপ্তি প্রকাশ করা হবে, তাতে নতুন এই বিভাগ দু'টিও অন্তর্ভুক্ত করা হবে। এই দু’টি নতুন বিভাগ নিয়ে রুয়েটে বিভাগের সংখ্যা মোট ১৪টি  দাঁড়ালো বলেও জানান তিনি।
