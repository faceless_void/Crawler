ঢাকা বিশ্ববিদ্যালয়ের ২০১৬-১৭ শিক্ষাবর্ষে ‘ঘ’ ইউনিটের ভর্তি পরীক্ষায় তৃতীয় হওয়া এক ভর্তিচ্ছু শিক্ষার্থীকে শাহবাগ থানায় সোপর্দ করা হয়েছে।
ঢাকা বিশ্ববিদ্যালয় (ঢাবি):  ঢাকা বিশ্ববিদ্যালয়ের ২০১৬-১৭ শিক্ষাবর্ষে ‘ঘ’ ইউনিটের ভর্তি পরীক্ষায় ‘জালিয়াতি’র মাধ্যমে তৃতীয় হওয়া অয়ন কুমার দাস নামে এক ভর্তিচ্ছু শিক্ষার্থীকে শাহবাগ থানায় সোপর্দ করা হয়েছে।
শনিবার (০৩ ডিসেম্বর) সকালে তাকে পুলিশের হাতে তুলে দেন প্রক্টোরিয়াল বডির সদস্যরা।
এর আগে অয়নসহ ভর্তি পরীক্ষায় জালিয়াতির ঘটনা নিয়ে বাংলানিউজে একটি সংবাদ প্রকাশিত হয়। ওই সংবাদের সূত্র ধরে তাকে শনিবার ফের পরীক্ষায় বসতে হয়।
আগে ভর্তি পরীক্ষায় তৃতীয় হলেও পরবর্তীতে শুধুমাত্র বাংলা বিষয়ে তার পরীক্ষা নেওয়া হয়। কিন্তু এ পরীক্ষায় ৩০ নম্বরের মধ্যে মাত্র ৫ নম্বর পান তিনি। কিন্তু ম‍ূল ভর্তি পরীক্ষায় পেয়েছিলেন ২৭ নম্বর। 
ডিন অফিস সূত্র জানায়, অয়নের ভর্তি পরীক্ষার রোল নম্বর হচ্ছে-১২৯৬৩৬। ডেমরার হাবিবুর রহমান মোল্লা কলেজ থেকে এইচএসসি পাস করেছেন তিনি। ইউসিসি কোচিং সেন্টারে ভর্তি কোচিং করেছেন অয়ন।
এ বিষয়ে বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক আ আ ম স আরেফিন সিদ্দিক বাংলানিউজকে বলেন, ভর্তি পরীক্ষায় জালিয়াত চক্রের বিরুদ্ধে আইনগত ব্যবস্থা নেওয়া হবে।
