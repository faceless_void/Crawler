এই কমিশন ২০১৬ সালের অগাস্টে মার্কিন টেক জায়ান্ট অ্যাপলকে অপরিশোধিত কর পরিশোধে আদেশ দিয়েছে। আয়ারল্যান্ডের কাছ থেকে প্রতিষ্ঠানটি `অবৈধ রাষ্ট্রীয় সুবিধা’ পাচ্ছে বলে ওই আদেশে উল্লেখ করা হয়, খবর রয়টার্স-এর।
এক বিবৃতিতে ইইউ কম্পিটিশন কমিশনার মারগ্রেথ ভেস্টেগার বলেন, “কমিশন এই সিদ্ধান্ত নেওয়ার এক বছরেরও বেশি সময় পর, আয়ারল্যান্ড এখনও এই অর্থ পরিশোধ করেনি, আর এটি অংশ হিসেবে না।” তিনি বলেন, “অবশ্যই আমরা এটি বুঝি যে নির্দিষ্ট কিছু ক্ষেত্রে এই অর্থ আদায় করা অন্য ক্ষেত্রের চেয়ে বেশি জটিল হতে পারে, আর আমরা সবসময় সহায়তা করতে প্রস্তুত। কিন্তু সদস্য রাষ্ট্রগুলোর উচিৎ প্রতিযোগিতা পুনরুদ্ধারে যথেষ্ট পদক্ষেপ নেওয়া।”
কমিশন-এর পক্ষ থেকে বলা হয়, এই সিদ্ধান্ত বাস্তবায়নে আয়ারল্যান্ডকে বেঁধে দেওয়া সময়সীমা ছিল চলতি বছর ৩ জানুয়ারি পর্যন্ত। এই অর্থ আদায় না করা পর্যন্ত মার্কিন প্রতিষ্ঠানিটি অবৈধ সুবিধা পেতে থাকবে।
অ্যাপল, মিলে যাচ্ছে ইইউ আর আইরিশ হিসাব
ইইউ-র সিদ্ধান্তে আপিল করবে অ্যাপল
অ্যাপল নিয়ে আনুষ্ঠানিক আপিলে আয়ারল্যান্ড
অ্যাপলের পক্ষে আইরিশ সংসদ
বিষয় অ্যাপল, আগেই বসছে আইরিশ সংসদ
অ্যাপলের বিরুদ্ধে রায় ‘প্রচলিত আইনেই’
আপিলে অ্যাপলের পাশে আয়ারল্যান্ড
ইইউ'র রায় "পুরোই রাজনৈতিক বর্জ্য"
আপিলের সিদ্ধান্তে সময় নেবে আয়ারল্যান্ড
ইউরোপিয়ান কমিশনকে জবাব দিলেন কুক
