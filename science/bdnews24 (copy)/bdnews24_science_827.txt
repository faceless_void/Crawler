২০১৩ সাল থেকে কর ফাঁকি দেওয়ার জন্য আয়ারল্যান্ডে অর্থ জমা রাখা নিয়ে বিতর্কের মুখে পড়ে অ্যাপল।
পানামা পেপার্সের পর দুনিয়াজুড়ে নতুন আলোড়ন তুলেছে প্যারাডাইস পেপার্স, যেখানে ব্রিটেনের রানীসহ বিশ্বের বহু রাষ্ট্র ও সরকার প্রধান, রাজনীতিবিদ ও প্রভাবশালী ব্যক্তির গোপন সম্পদের তথ্য বেরিয়ে এসেছে। এতে দেখা যায়, অ্যাপল এই বিতর্ক এড়াতে আরেকটি কর স্বর্গে অর্থ রাখা শুরু করেছে। ইংল্যান্ড ও ফ্রান্সের মাঝে অবস্থিত জার্সি নামের এই চ্যানেল আইল্যান্ডস-এ কর না দিয়ে অ্যাপলের রাখা মোট অফশোর অর্থের পরিমাণ এখন ২৫২০০ কোটি ডলার।
যদিও এ নিয়ে অ্যাপলের দাবি, নতুন এই কর কাঠামো প্রতিষ্ঠানটির কর পরিশোধের পরিমাণ কমায়নি। প্রতিষ্ঠানটি বলে, শেষ তিন বছর ধরে অ্যাপল বিশ্বের সবচেয়ে বেশি কর পরিশোধকারী প্রতিষ্ঠান হিসেবে প্রায় ৩৫০০ কোটি ডলার কর্পোরেট কর পরিশোধ করে আসছে। এ ক্ষেত্রে আইন ও এর পরিবর্তনগুলো অনুসরণ করা হয়েছে। এর ফলে “কোনো দেশেই আমাদের কর পরিশোধ কমেনি” বলে মন্তব্য প্রতিষ্ঠানটির। 
আরেকটি বিবৃতিতে প্রতিষ্ঠানটি বলে, আয়ারল্যান্ড থেকে অ্যাপলের কোনো কার্যক্রম বা বিনিয়োগ সরানো হয়নি।
২০১৪ সাল পর্যন্ত অ্যাপল যুক্তরাষ্ট্রের বাইরের লাভ আয়ারল্যান্ডে জমা রাখার মাধ্যমে কর কমিয়ে আনছিল। প্রতিষ্ঠানটির মোট আয়ের ৫৫ শতাংশই যুক্তরাষ্ট্রের বাইরে থেকে আসে। আয়ারল্যান্ডে ১২ শতাংশ করপোরেট কর দেওয়ার জায়গায় নতুন কাঠামো অ্যাপলকে যুক্তরাষ্ট্রের বাইরের লাভের উপর হওয়া করের পরিমাণ কমাতে সহায়তা করেছে। 
ইউরোপিয়ান কমিশন-এর হিসাব মতে, এক বছরে অ্যাপলের একটি আয়ারল্যান্ডের প্রতিষ্ঠানের কর দেওয়ার হার হয়ে যায় ০.০০৫ শতাংশ।
২০১৩ সালে অ্যাপল মার্কিন সিনেটের চাপের মুখে পড়ে। এ সময় অ্যাপল প্রধান টিম কুক প্রতিষ্ঠানটির কর ব্যবস্থা নিয়ে আত্মপক্ষ সমর্থনে বাধ্য হয়েছিলেন। যুক্তরাষ্ট্র বড় অংকের কর হারাচ্ছে বলে দাবি করে অ্যাপলের এমন কর ব্যবস্থায় ক্ষুদ্ধ সে সময়কার মার্কিন সিনেটর কার্ল লেভিন কুক-কে বলেছিলেন, “আপনি সোনার হাঁসকে আয়ারল্যান্ডে পাঠিয়ে দিয়েছেন। আপনি এমন তিন প্রতিষ্ঠানের কাছে এটি পাঠিয়েছেন যারা আয়ারল্যান্ডে কোনো কর দেয় না। এগুলো অ্যাপলের জন্য মুকুটের রত্ন। এটি ঠিক নয়।”
এ নিয়ে ক্ষোভের সঙ্গে কুক জবাব দিয়েছিলেন, “আমরা আমাদের সব কর পরিশোধ করি, প্রতিটি ডলার। আমরা কর ফাঁকির উপর নির্ভর করি না…আমরা কিছু ক্যারিবিয়ান দ্বীপে অর্থ লুকিয়ে রাখি না।” 
২০১৩ সালে ইউরোপিয়ান ইউনিয়ন (ইইউ) এক ঘোষণায় বলে তারা অ্যাপলের আইরিশ কর ব্যবস্থাপনা নিয়ে তদন্ত চালাচ্ছে। সে সময় করের হার কমাতে, অ্যাপলকে অফশোর অর্থ রাখার এমন একটি কেন্দ্র খোঁজা প্রয়োজন ছিল যা প্রতিষ্ঠানটির আইরিশ অঙ্গপ্রতিষ্ঠানগুলোর কর রাখার জায়গা হিসেবে ব্যবহৃত হবে।
২০১৪ সালে অ্যাপলের আইনি উপদেষ্টারা অ্যাপলবাই’র কাছে একটি প্রশ্নপত্র পাঠান। অ্যাপবাই হচ্ছে একটি শীর্ষস্থানীয় অফশোর আর্থিক সেবাদাতা প্রতিষ্ঠান। প্যারাডাইস পেপার্স ফাঁসের বড় একটি সূত্রও এই প্রতিষ্ঠান। এই প্রশ্নপত্রে জিজ্ঞাসা করা হয়, ব্রিটিশ ভার্জিন আইল্যান্ডস, বারমুডা, ক্যায়ম্যান আইল্যান্ডস, মরিশাস, আইল অফ ম্যান, জার্সি আর গার্নেসি- এসব ভিন্ন ভিন্ন অফশোর বিচারব্যবস্থার দেশের কোনটিতে অ্যাপলবাই অ্যাপল-কে কী কী সুবিধা দিতে পারবে। ওই দেশগুলোর কোনোটির সরকার পরিবর্তনের সম্ভাবনা আছে কিনা, কোন কোন তথ্য প্রকাশ হবে আর বিচারব্যবস্থা থেকে বের হওয়া কতোটা সহজ হবে- এগুলোও জিজ্ঞাসা করা হয় ওই প্রশ্নপত্রে।  
অ্যাপল এই পদক্ষেপ গোপন রাখতে চেয়েছিল, ফাঁস হওয়া মেইলগুলো এই তথ্যও স্পষ্ট করে দিয়েছে বলে বিবিসি’র প্রতিবেদনে উল্লেখ করা হয়েছে। 
শেষে অ্যাপল জার্সিকেই বেছে নেয়। জার্সি হচ্ছে যুক্তরাজ্যের অধীনস্থ একটি স্বায়ত্ত্বশাসিত দ্বীপরাষ্ট্র। এই দ্বীপরাষ্ট্রের নিজস্ব কর আইন আছে, যাতে বিদেশি প্রতিষ্ঠানগুলোর জন্য শুন্য শতাংশ করপোরেট করের নিয়ম রাখা হয়েছে।  
প্যারাডাইস পেপার্স-এর দলিলে দেখা যায়, অ্যাপলের দুই আইরিশ অঙ্গপ্রতিষ্ঠানের মধ্যে অ্যাপল অপারেশনস ইন্টারন্যাশনাল (এওআই) অ্যাপলের ২৫২০০ কোটি ডলার বিদেশি আয়ের সবচেয়ে বেশি অংশ রাখে। আর অন্যটি হচ্ছে অ্যাপল সেলস ইন্টারন্যাশনাল (এএসআই), যা ২০১৫ সালের শুরু থেকে ২০১৬ সালের শুরু পর্যন্ত জার্সিতে থাকা অ্যাপলবাই’র কার্যালয় থেকে পরিচালনা করা হতো। 
এর মাধ্যমে অ্যাপল বিশ্বব্যাপী শত শত কোটি ডলার কর এড়ানো অব্যাহত রেখেছিল বলে প্রতিবেদনে উল্লেখ করা হয়েছে।
অ্যাপলের ২০১৭ সালের হিসাব অনুযায়ী, প্রতিষ্ঠানটি যুক্তরাষ্ট্রের বাইরে ৪৪৭০ কোটি ডলার আয় করে। এর মধ্যে ১৬৫ কোটি ডলার কর বিদেশি সরকারগুলোর কাছে পরিশোধ করে, যা আয়ের অংকের ৩.৭ শতাংশ। করের এই হার বিশ্বব্যাপী করপোরেট করের গড় হারের ছয় ভাগের এক ভাগেরও কম। 
দুই অঙ্গপ্রতিষ্ঠান তাদের কর জার্সিতে সরিয়ে নিচ্ছে কিনা তা নিয়ে জিজ্ঞাসা করা হলেও অ্যাপলের পক্ষ থেকে কোনো মন্তব্য করা হয়নি বলে জানিয়েছে বিবিসি। অ্যাপল-এর পক্ষ থেকে বলা হয়, “২০১৫ সালে আয়ারল্যান্ড যখন তাদের কর আইন পরিবর্তন করে, আমরা আমাদের আইরিশ অঙ্গপ্রতিষ্ঠানগুলোর কর দেওয়ার দেশ পরিবর্তন করি আর এটি নিয়ে আমরা আয়ারল্যান্ড, ইউরোপিয়ান কমিশন ও যুক্ররাষ্ট্রকে অবহিত করেছি।”
“আমরা যে পরিবর্তন করেছি তা কোনো দেশে আমাদের কর কমায়নি। বরং আয়ারল্যান্ডে আমাদের কর পরিশোধের পরিমাণ উল্লেখযোগ্যভাবে বেড়েছে আর শেষ তিন বছরে আমরা সেখানে ১৫০ কোটি ডলার কর দিয়েছি।”
আরও খবর-
প্যারাডাইস পেপার্স: বিশ্বের অনেক ক্ষমতাধরের গোপন তথ্য ফাঁস
যাদের গোমর ফাঁস করল প্যারাডাইস পেপার্স 
অ্যাপল নিয়ে আদালতের মুখে আয়ারল্যান্ড
অ্যাপল, মিলে যাচ্ছে ইইউ আর আইরিশ হিসাব
ইইউ-র সিদ্ধান্তে আপিল করবে অ্যাপল
অ্যাপল নিয়ে আনুষ্ঠানিক আপিলে আয়ারল্যান্ড
অ্যাপলের পক্ষে আইরিশ সংসদ
বিষয় অ্যাপল, আগেই বসছে আইরিশ সংসদ
অ্যাপলের বিরুদ্ধে রায় ‘প্রচলিত আইনেই’
আপিলে অ্যাপলের পাশে আয়ারল্যান্ড
ইইউ'র রায় "পুরোই রাজনৈতিক বর্জ্য"
আপিলের সিদ্ধান্তে সময় নেবে আয়ারল্যান্ড
ইউরোপিয়ান কমিশনকে জবাব দিলেন কুক
