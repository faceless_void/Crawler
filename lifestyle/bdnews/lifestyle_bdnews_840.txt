

মধু-লেবু-আদার জেলি
উপকরণ : মধু, লেবু আর আদা।
পদ্ধতি : একটি কাচের বোতলে লেবু গোল গোল করে কেটে তাতে আদা টুকরো টুকরো করে রাখুন। এর মধ্যে মধু ঢেলে দিন। এরপর ফ্রিজে রাখুন। এভাবে প্রায় ৩ মাস ফ্রিজে রেখে খেতে পারবেন।
শীতে চা, দুধ কিংবা এরকম কিছুর সঙ্গে এই জেলি খেতে মজা। যাদের ঠাণ্ডার সমস্যা আছে তারা এটা নিয়মিত খেতে পারেন। অনেক উপকার পাবেন।
 
দুধ-মধু
উপকরণ : দুধ ও মধু।
পদ্ধতি : ১ গ্লাস দুধ গরম করুন। এরপর দুধে ২ চামচ মধু মেশান। এই পানীয়তে চিনি না দিলেও হবে।


হট চকলেট
উপকরণ :  দুধ ৩ কাপ। কফি বা চকলেটের গুঁড়ো ৪ চামচ। মধু বা চিনি ৩ চা-চামচ। ডিম ১টি। কফি ১ চা-চামচ। ভেনিলা এসেন্স আধা চা-চামচ।
পদ্ধতি : দুধ গরম করুন। ওভেনে করলে ২ মিনিট। ব্লেন্ডারে গরম দুধ, ডিম, চিনি বা মধু, ভ্যানিলা এসেন্স, কফি বা চকলেটর গুঁড়ো দিয়ে ভালো করে মিশিয়ে ওভেনে আরও ৩ থেকে ৪ মিনিট গরম করুন। দেখবেন ফেনা ফেনা হয়ে আসবে।
হয়ে গেল মজাদার হট চকলেট ।


মসলাদার চা
উপকরণ : আদাকুচি ২ চা-চামচ। দারুচিনি ২টি। এলাচ ২টি। গোলমরিচ ৪-৫টি। লবঙ্গ ৩টি। দুধ (যদি দুধ চা খান) পরিমাণ মতো। চা-গুঁড়ো ৩ চা-চামচ। লেবু ১ চা-চামচ (যদি রং চা হয়। দুধ চায়ে লেবু দিতে হবে না)। মধু ২ চা-চামচ। 
পদ্ধতি : ৩ কাপ পানি গরম করে ফুটিয়ে চা-পাতা দিয়ে দিন। এরপর মধু আর দুধ বাদে বাকি সব উপকরণ দিয়ে ১০ মিনিট ফুটান।
রং চা তৈরি করতে একটু কম সময় জ্বাল দিন।
