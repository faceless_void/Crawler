শীতেও চুল ঝলমলে রাখতে চাই বাড়তি যত্ন। ধরণ বুঝে তাই পরিচর্যা করতে হবে। বিস্তারিত জানাচ্ছেন মিউনি’জ ব্রাইডালের রূপ বিশেষজ্ঞ তানজিমা শারমিন মিউনি।
তৈলাক্ত চুল: শ্যাম্পু করলেও তৈলাক্ত চুল নির্জীব থাকে। তাই শ্যাম্পুর সঙ্গে সামান্য বেকিং পাউডার মিশিয়ে নিলে চুলের গোড়ার অতিরিক্ত তেল শুষে যায়।
শুষ্ক চুল: সপ্তাহে দুবার রাতে চুলের গোড়ায় অলিভ অয়েল বা ক্যাস্টর অয়েল লাগাতে হবে এবং সকালে শ্যাম্পু করতে হবে। এতে চুলে আদ্রভাব বজায় থাকবে। চুলে প্রোটিন সমৃদ্ধ কন্ডিশনার ব্যবহার করতে হবে। তাহলে চুলের পুষ্টি বজায় থাকবে। শুষ্ক চুলের জন্য ভিটামিন-ই, অ্যালোভিরা সমৃদ্ধ শ্যাম্পু এবং কনডিশনার ব্যবহার করতে হবে।
শীত ঋতুতে চুলের জন্য সবচেয়ে বড় সমস্যা হল চুলে খুশকি। চুলের গোড়ায় ময়লা জমে খুশকি হয়। আর শীতকালে চুলের গোড়ায় খুব তাড়াতাড়ি ময়লা জমে। তাই খুশকির প্রবণতাও বাড়ে।
শুষ্ক মৌসুমে খুশকিকে টাটা বাইবাই বলতে চাই একটু বাড়তি যত্ন।
উপরের যে কোনো একটি পদ্ধতি অনুসরণ করলেই হয়। তবে খুশকির পরিমাণ বেশি হলে দুই তিনটি পন্থা একসঙ্গে চালিয়ে গেলে দ্রুত উপকার পাওয়া যাবে।
শীতে চুলের উজ্জ্বলতা
সতর্কতা
এছাড়াও বাড়িতে পরিচর্যার পাশাপাশি মাসে একবার পার্লারে গিয়ে ট্রিটমেন্ট নিতে পারেন। এতে মাথায় ত্বকের কোষগুলো সক্রিয় থাকে, চুলে খুশকি হয় না এবং অনেক সমস্যা থেকে সমাধান পাওয়া যায়।
চুলের ধরণ অনুযায়ী বেছে নিন প্রয়োজনীয় যত্ন। ভেজা চুল কখনও বেঁধে রাখা বা আঁচড়ানো ঠিক না। গোড়া নরম থাকে বলে চুল ঝরে যেতে পারে এবং চুল রুক্ষ্ম হয়ে যাওয়ার সম্ভাবনা থাকে।
সুন্দর ও ঝলমলে চুল পেতে এবং চুলের আর্দ্রতা ধরে রাখতে পরিচর্যার পাশাপাশি প্রচুর শাকসবজি, পানি ও ফলমূল খাওয়ার অভ্যেস করুন।
