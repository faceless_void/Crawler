মঙ্গল গ্রহে মিশন চালাতে ইতোমধ্যেই ওই গ্রহের আদলে হলোগ্রাফিক পরিবেশ তৈরি করেছে প্রতিষ্ঠানটি। এছাড়া বিভিন্ন ধরণের ৩ডি বস্তু তৈরি করতে ডেভেলপার এবং শিক্ষকদের কাছে প্রকল্প দিয়েছে নাসা।
এবার নভোচারীদের প্রশিক্ষণ দিতে নতুন প্রকল্প হাতে নিয়েছে নাসা। মিক্সড রিয়ালিটি প্রযুক্তিতে আন্তর্জাতিক মহাকাশ কেন্দ্রের সিমুলেটর তৈরি করতে গেইম নির্মাতা প্রতিষ্ঠান এপিক গেইমস-এর সঙ্গে কাজ করেছে প্রতিষ্ঠানটি, জানিয়েছে প্রযুক্তিবিষয়ক সাইট টেকক্রাঞ্চ।
এপিক গেইমস-এর গেইমিং ইঞ্জিন ‘আনরিয়াল ইঞ্জিন’ ব্যবহার করে তৈরি করা হবে এ মিক্সড রিয়ালিটি সিমুলেটর।
যোগ্যতাসম্পন্ন নাসা নভোচারী হতে প্রার্থীদেরকে দুই বছরের বেশি  সময় ধরে প্রশিক্ষণ দেওয়া হয়ে থাকে। এই সময়ের মধ্যে বিভিন্ন ধাপে তাদেরকে সিমুলেশন এবং ক্লাসের মাধ্যমে মহাকাশে পাঠানোর জন্য প্রস্তুত করা হয়।
পূর্বে নভোচারীদেরকে নিরপেক্ষ প্লবতা ল্যাব (নিউট্রাল বয়ান্সি ল্যাব)-এ প্রশিক্ষণ দেওয়া হত। এই ল্যাবে নভোচারীদেরকে ৬২ লাখ গ্যালন পানি ধারণ ক্ষমতার বিশাল পুলে নামানো হত। এছাড়া আন্তর্জাতিক মহাকাশ কেন্দ্রের একটি অংশের মতো স্পেস শাটলে তাদেরকে প্রশিক্ষণ দেওয়া হত।
অনেক ক্ষেত্রেই এসব প্রশিক্ষণ যন্ত্রের সীমাবদ্ধতা রয়েছে। তাই মিক্সড রিয়ালিটি প্রযুক্তির মাধ্যমে নভোচারীদের প্রশিক্ষণের জন্য রিসোর্চ আরও বাড়বে। আর এর সঙ্গে পুরানো প্রশিক্ষণও থাকবে বলে জানানো হয়।
