বলছিলাম মহারাজা শ্রীকান্ত আচার্যের অনন্য স্থাপনা ‘শশীলজ’র কথা। ঊনবিংশশতকের শেষ দিকে মুক্তাগাছার জমিদার মহারাজ সূর্যকান্ত আচার্য চৌধুরী দৃষ্টিনন্দন দ্বিতল ভবন নির্মাণ করেন। মহারাজসূর্যকান্ত নিঃসন্তান ছিলেন। তিনি তার দত্তক ছেলে মহারাজ শশীকান্ত আচার্য চৌধুরীর নামে এর নাম দেন ‘শশী লজ’।
১৮৯৭ সালে ভূমিকম্পে প্রাসাদটি ক্ষতিগ্রস্ত হয়। পরে শশীকান্ত আচার্য চৌধুরী বাড়িটি পুনরায় নির্মাণ করেন।
গিয়েছিলাম আমরা চার বান্ধবী। আমি সনি, সৌমি আর ঝুমুর। আমরা তিন জন এখানে প্রথমবার আসলাম। তবে আনন্দমোহন কলেজের শিক্ষক সৌমি (এখন সে বিদূষীর মা) আগেও এসেছে। যেহেতু সে পাশেই থাকে। তাই সেই আমাদের গাইড।
গাড়ি থেকে নেমে গেটের ফোঁকর দিয়ে দেখলাম বিশাল এক অনন্য স্থাপনা। দেখে চক্ষু অনেকটা চড়কগাছ।
পরিচয় নিয়ে ঢুকতে দিল দায়িত্বরত ব্যক্তিরা। গেটের ভেতরে পা দিয়ে খানিকক্ষণ দাঁড়িয়ে চারিদিকে একবার চোখ বুলিয়ে নিলাম। মাথার উপরে বিশাল বিশাল গাছ। কিছুক্ষণ হা করে তাকিয়ে রইলাম। কত যে পুরানো এসব গাছ কে জানে!


আমরা এগুতে লাগলাম একটু একটু করে। শশীলজেরমূল ভবনের সামনে রয়েছে দৃষ্টিনন্দন বাগান। বাগানের মাঝখানে আছে শ্বেতপাথরের ফোয়ারা। যার মাঝখানে আবার বিশাল গ্রিক দেবী ভেনাসের এক মর্মর মূর্তি। যদিও গায়ে খানিকটা শেওলা পড়েছে।
ভবনের কোনো কোনো ঘরের জানালা খোলা দেখতে পেলাম। মনে হল ভেতরে মানুষ আছে। আর এখনি মীর্জা সাহেবের (অয়োময় নাটকের আসাদুজ্জামান নূর) মা ডাক দিয়ে বলবে- কে যায়? কথা বলেনা কেনো, কে যায়?
শুনতে পাব মীর্জা সাহেব আর মোবারকের কথোপকথন-
পাখির যত্ন হইতেছে?


এগুলো মনে হতে হাসলাম খানিকক্ষণ। ঘাড় ঘুরে তাকিয়ে দেখল অনেকেই। পাগলই ভাবল কিনা কে জানে!
শশীকান্ত আচার্য চৌধুরীকে ছাপিয়ে আমার কেবলি অয়োময় নাটকের কথাই মনে হতে লাগল। বড় বউ, এলাচি বেগম মনে হচ্ছে ভেতরেই আছে। এখনি শুনতে পাব তাদের আওয়াজ।
ধীরে ধীরে দেখতে লাগলাম শমীলজের সামনের রাস্তা। প্রধান প্রবেশে দ্বার। স্নানঘর, স্নানঘাট। চারপাশ ছিল সাজানো গোছানো। যদিও পেছনের দিকে কোথাও কোথাও বেশ বড় বড় ঘাস গজিয়েছে। হয়ত পরিষ্কার করা হয়নি অনেক দিন। তবে অনেক শ্রমিককে কাজও করতে দেখলাম।
এরই মধ্যে সনিকে আমরা জ্বালিয়ে মারলাম ছবি তুলে দিতে। কারণে অকারণে এখানে সেখানে দাঁড়িয়ে গেলাম আমি ঝুমুর আর সৌমি। একটা মোক্ষম অস্ত্র আমাদের কাছে আছে, আর সেটা হল বিদূষী।
খুব করুণ মুখ করে বলতাম- ‘বড় হয়ে বিদূষী দেখবে না, মা-খালাদের সঙ্গে কোথায় ঘুরতে গিয়েছিল।’- বলেই হাসি মুখে আমরা পোজ দিয়ে দাঁড়িয়ে যেতাম।


সব থেকে ভালো লাগল ভবনের পেছন দিকে থাকা দোতলা স্নানঘর, ঘাট ও পুকুর। পুকুরের ঘাটটি মার্বেল পাথরের। যদিও স্নানঘরের দোতালায় ওঠার সিঁড়িতে বরই গাছের কাটাযুক্ত ডাল দিয়ে বন্ধ করে দেওয়া আছে। তাই আমরা আর উপরে যেতে পারলাম না।
সৌমি নাকি শুনেছে, এখানে বসেই রানি পাশের পুকুরে ভেসে বেড়ানো হাঁসের খেলা দেখতেন। সত্যি এই জায়গায় বসলে আর উঠতে ইচ্ছেই করে না।
বেশ কয়েকজন দর্শনার্থীকেও দেখলাম এখানে। ঘাটে বসে ছবি তুলে যাচ্ছেন তো যাচ্ছেই। সূর্য হেলে পড়ছে আমরা ছবি তোলার পর্যাপ্ত আলো পাব না। এতো ছবি তোলার কি আছে! একটা দুটো তুললেই তো হয়।
আর আমরা যখন জায়গা পেলাম শ’খানেক ছবি তো তুলেই ফেললাম।
১৯৫২ সালে এখানে মহিলা টিচার্স ট্রেনিং কলেজ প্রতিষ্ঠিত হয়। বাড়িটির মূল অংশ মহিলা টিচার্স ট্রেনিং কলেজের অধ্যক্ষের কার্যালয় ও দপ্তর হিসেবে ব্যবহৃত হয়ে আসছিল। ২০১৫ সালে ৪ এপ্রিল বাংলাদেশ সরকারের প্রত্নতত্ত্ব অধিদপ্তর জাদুঘর স্থাপনের জন্য শশীলজটি নিয়ে নেয়।
এখান থেকে বেরিয়ে আমরা গেলাম ব্রহ্মপুত্র নদে। নদীর ধারে পার্কটি অসাধারণ। শহরের সার্কিড হাউজ সংলগ্ন ব্রহ্মপুত্র নদকে ঘিরে শিল্পাচার্য জয়নুল আবেদিন সংগ্রহশালা এলাকায় গড়ে তোলা হয়েছে এই পার্ক। প্রতিদিনই সকাল-থেকে সন্ধ্যা পর্যন্ত এ পার্কে ঘুরে বেড়ায় লোকজন। সকাল বিকেলের হালকা জগিংও সেরে নেয় অনেকে।


কীভাবে যাবেন: সড়ক ও রেলপথ দুই মাধ্যমেই ভ্রমণে যেতে পারেন ঢাকা থেকে ময়মনসিংহে। ঢাকার মহাখালী থেকে লোকাল ও গেইটলক দুই ধরনের বাস ছাড়ে। বাস থেকে নামবেন মাসাকান্দা বাসস্ট্যান্ডে। সেখান থেকে অটোতে বা রিকশায় চলে যেতে পারবেন শশীলজ বা ব্রহ্মপুত্র নদের পাড়ে।
