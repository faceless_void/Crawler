জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ে প্রতিবন্ধী শিক্ষার্থীদের জন্য তিন মাস মেয়াদী কম্পিউটার প্রশিক্ষণ শুরু হয়েছে।
জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ে প্রতিবন্ধী শিক্ষার্থীদের জন্য তিন মাস মেয়াদী কম্পিউটার প্রশিক্ষণ শুরু হয়েছে।
শনিবার (৬ আগস্ট) দুপুর সাড়ে ৩টায় জহির রায়হান মিলনায়তনে বিশ্ববিদ্যালয়ের কোষাধ্যক্ষ আবুল খায়ের প্রশিক্ষণ পর্বের উদ্বোধন করেন।
এর ‍আয়োজনে রয়েছে প্রতিবন্ধী শিক্ষার্থীদের সংগঠন ফিজিক্যালি চ্যালেন্সড ডেভেলোপমেন্ট ফাউন্ডেশন (পিডিএফ)।
সংগঠনের উপদেষ্টা পদার্থ বিজ্ঞানী অধ্যাপক এ এ মামুন বলেন, প্রতিবন্ধীরা এ দেশের নাগরিক- তাদেরও অধিকার রয়েছে। আমাদের সবার চেষ্টায় এ বিষয়টি বাস্তরে রূপ দিতে হবে। বিশ্ববিদ্যালয়গুলোতে অনেকে প্রতিবন্ধী সেজে ভর্তি হওয়ায় প্রকৃত প্রতিবন্ধীরা সুবিধা বঞ্চিত হচ্ছে।
উদ্বোধনী অনুষ্ঠানে প্রত্নতত্ত্ব বিভাগের সভাপতি অধ্যাপক মোকাম্মেল হোসেন ভূঁইয়া, পিডিএফ’র কেন্দ্রীয় সভাপতি মিজানুর রহমান কিরণ, পিডিএফ’র জাবি শাখার সভাপতি কাউসার হামিদ প্রমুখ উপস্থিত ছিলেন।
