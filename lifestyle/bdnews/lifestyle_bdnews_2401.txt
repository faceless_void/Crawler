পেঁপেতে রয়েছে ব্যাকটেরিয়া ও ছত্রাকরোধী গুণাবলী। আবার ডেঙ্গুজ্বর রোধে কার্যকর পেঁপেগাছের পাতা।  এসব তথ্য পুষ্টিবিজ্ঞানে স্বীকৃত।
এত গুণের মাঝেও এই ফলের কিছু খারাপ দিকও আছে। যেমন- গর্ভবতী নারীদের পেঁপে এবং আনারস খেতে মানা করা হয়।
এরকমই কিছু ক্ষতিকর দিক সম্পর্কে জানিয়েছে স্বাস্থ্যবিষয়ক একটি ওয়েবসাইট।
গর্ভপাত: পেঁপে অত্যন্ত পুষ্টিকর হলেও এর বীজ ও শেকড় গর্ভপাত ঘটাতে পারে। কাঁচাপেঁপে জরায়ু সংকুচিত করে ফেলে। পাকাপেঁপেতে এই ঝুঁকি কিছুটা কম। তবে গর্ভবতী হলে পেঁপে এড়িয়ে চলাই ভালো।
খাদ্যনালীতে বাধা: পুষ্টিকর বলে কোনকিছুই অতিরিক্ত খাওয়া উচিত নয়। পেঁপে অতিরিক্ত খেলে খাদ্যনালীর উপর ক্ষতিকর প্রভাব ফেলে। দিনে এক কাপের বেশি পেঁপে খাওয়া উচিত নয়।
জন্মদোষ: পেঁপে পাতায় থাকা ‘পাপাইন’ নামক উপাদান গর্ভের সন্তানের জন্য বিষাক্ত হতে পারে। সন্তান বুকের দুধ খাওয়ানোর বয়সে মায়ের পেঁপে খাওয়া ক্ষতিকর কি না তা নিশ্চিত নয়। তবে সাবধানের মার নেই, তাই গর্ভাবস্থায় এবং সন্তান জন্মের কয়েক মাস পর্যন্ত পেঁপে এড়িতে চলা উচিত।
অ্যালার্জি: কাঁচাপেঁপের বোটা থেকে বের হওয়া সাদা তরল চামড়ায় অ্যালার্জির সৃষ্টি করতে পারে।
