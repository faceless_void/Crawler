স্বাস্থ্যবিষয়ক একটি ওয়েবসাইট থেকে জানা যায়, পেটের চারপাশের বারতি অংশ সবসময় চর্বি নাও হতে পারে। আজেবাজে খাবার খাওয়া বাদ দিয়ে স্বাস্থ্যকর ও পরিমাণ মতো খাবার খেয়েও ওজন নিয়ন্ত্রণে রাখা যায়।
স্ফীতভাব কমাতে: ভারতীয় পুষ্টিবিদ সুরাজ বিষ্ট’য়ের মতে, ওজন কমানোর প্রধান ধাপ হল এর মূল কারণ খুঁজে বের করা। অধিকাংশ মানুষের পেটের স্ফীতভাব সৃষ্টি হয় গ্যাস্ট্রিকজনিত সমস্যা যেমন কোষ্ঠ্যকাঠিন্য ও বদহজম। পেট স্ফীত হওয়ার অন্যতম সাধারণ কারণ হল সকালের ও রাতের খাবারকে প্রধান খাবার হিসেবে খাওয়া এর মধ্যবর্তী সময়ে ক্ষুধা লাগলেও হালকা নাস্তা না করা। তিনি দিনে মোট ছয়বার খাবার খাওয়ার পরামর্শ দেন, এতে তিরিক্ত খাবার খাওয়ার সম্ভাবনা থাকে না।
শুধুই চর্বি নয়: তাছাড়া, স্ফীতভাব কেবল মাত্র চর্বির জন্য হয় না, ভিটামিন ডি’র অভাব ও হাইপোথাইরোডিজম’য়ের জন্যও হতে পারে। তাই ডায়েট শুরু করার আগে এই বিষয়ে নিশ্চিত হয়ে নেওয়া উচিত। অনেকসময় ডায়েটে আঁশের ঘাটতি থাকায় এবং শস্যজাতীয় খাবার যেমন- চাপ্টি ও রুটি ইত্যাদি বা শ্বেতসার-জাতীয় খাবার বেশি থাকায় ওজন বৃদ্ধি পায়।
প্রয়োজনীও পুষ্টি উপাদান থেকে শরীরকে বঞ্চিত করতে না চাইলে খাবারে প্রয়োজনীয় চর্বি যেমন ঘি রাখা উচিত বলে জানান, তিনি।
শরীরের স্থুলতা কমানোর জন্য পুষ্টিবিদরা দিনে ছয়বার খাওয়ার পরামর্শ দেন। সেই অনুযায়ী সকালের নাস্তা থেকে শুরু করে প্রতি দুই ঘণ্টা বিরতিতে খাওয়া উচিত। এবং ঘুমাতে যাওয়ার অন্তত তিন ঘণ্টা আগে রাতের খাবার খাওয়া উচিত। প্রতিবেলার খাবার খাওয়ার পর গ্রিনটি খাওয়া ভালো। এটি শরীরের বিপাক ও হজম বাড়াতে সাহায্য করে।
ওজন কমাতে এই খাবারগুলো খেতে পারেন
- এক টেবিল-চামচ জিরা এক গ্লাস কুসুম গরম পানিতে মিশিয়ে সকালে খালি পেটে খেতে পারেন। এটি পেটের স্ফীতভাব কমাতে সাহায্য করে।
- এক চিমটি দারুচিনি, গোলমরিচ, হলুদ এবং আধ টেবিল-চামচ আদাগুঁড়া এক গ্লাস হালকা গরম পানিতে মিশিয়ে খালি পেটে খান।
