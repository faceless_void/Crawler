খাদ্য ও পুষ্টিবিষয়ক ওয়েবসাইটে প্রকাশিত প্রতিবেদন থেকে কয়েকটি স্মুদি সম্পর্কে জানা যায়। যা ওজন কমাতে সাহায্য করে এবং খেতেও বেশ মজা। 
স্ট্রবেরি, গাজর ও বিটরুটের স্মুদি


গাজরে খুব অল্প পরিমাণে ক্যালরি ও উচ্চ পরিমাণে আঁশ থাকে। স্ট্রবেরির ‘অ্যান্থোসায়ানিনস’ এবং ‘অ্যালাজিক অ্যাসিড’ প্রদাহ কমায়, ওজন কমাতে সাহায্য করে এবং হরমোনের কার্যকারিতা বাড়ায়।
এই স্মুদি খেতে বেশ মজাদার এবং এটা ওজন কমাতে আশানুরূপ কাজও করে থাকে।
আদা-লেবুর শরবত


অন্যদিকে, সুস্বাস্থ্যের জন্য লেবু গুরুত্বপূর্ণ ভূমিকা রাখে। এর ভিটামিন সি, যা একটি অ্যান্টিঅক্সিডেন্ট শরীর থেকে বর্জ্যপদার্থ অপসারণ করতে সাহায্য করে। এছাড়া লেবু শরীরে পানি ধারণ কমাতে ও শরীরের বিষাক্ত পদার্থ বের করে পেটের ফোলাভাব কমাতে সাহায্য করে।  
আপেল ও ক্যাপ্সিকামের স্মুদি


আপেলে চর্বি ও সোডিয়ামের পরিমাণ কম থাকে। আর আছে উচ্চমানের খাদ্যআঁশ। যা ওজন কমাতে সহায়ক। এছাড়া আপেল ভিটামিন ও খনিজ উপাদান সমৃদ্ধ।
আপেল ও ক্যাপ্সিকামের স্মুদি কেবল ওজন কমাতে নয় বরং স্বাস্থ্য রক্ষাতেও সাহায্য করে।
শসা ও আনারসের স্মুদি


শসায় জলীয় উপাদান বেশি এবং এতে ক্যালরির পরিমাণ কম, এটা স্বাদ যোগ করতে সাহায্য করে।
পানীয়র মাধ্যমে ওজন কমানোর ক্ষেত্রে এটা অন্যতম মজাদার খাবার।
ছবি: রয়টার্স ও নিজস্ব।
আরও পড়ুন
ওজন কমানোর কৌশল  
ওজন কমাতে সহায়ক খাবার  
