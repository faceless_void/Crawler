দিনাজপুর: দিনাজপুর মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষা বোর্ডের অধীনে ২০১৭ সালের মাধ্যমিক স্কুল সার্টিফিকেট (এসএসসি) পরীক্ষার ফলাফল পুনঃনিরীক্ষণের জন্য ৩৭ হাজার ৩৯৮টি আবেদন জমা পড়েছে। 
পুনঃনিরীক্ষণের ফলাফল আগামী ৩০ মে প্রকাশিত হবে।
গত ৪ মে চলতি বছরের এসএসসি ও সমমানের পরীক্ষার ফলাফল প্রকাশ করা হয়। তারপর ৫ মে থেকে ১১ মে পর্যন্ত পুনঃনিরীক্ষণের আবেদন গ্রহণ করা হয়।
দিনাজপুর মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষা বোডের্র উপ-পরীক্ষা নিয়ন্ত্রক রাকিবুল ইসলাম বাংলানিউজকে জানান, দিনাজপুর শিক্ষা বোর্ডের অধীনে ২০১৭ সালের এসএসসির ফলাফল পুনঃনিরীক্ষণের জন্য বিভিন্ন বিষয়ে ৩৭ হাজার ৩৯৮টি আবেদন পাওয়া গেছে। চলতি সপ্তাহেই আবেদন অনুযায়ী ওই পরীক্ষার্থীদের খাতাগুলোর পুনঃনিরীক্ষণের কাজ শুরু হবে। আগামী ৩০ মে এই পুনঃনিরীক্ষণের ফলাফল প্রকাশিত হবে।
তিনি জানান, এবার সবচেয়ে বেশি আবেদন পড়েছে গণিতে ৪ হাজার ১৯৭টি। এরপর ধর্মে ৩ হাজার ৭৮৩ ও রসায়নে ৩ হাজার ৪০৬টি। রয়েছে অন্যান্য বিষয়েও।
এ ব্যাপারে দিনাজপুর শিক্ষা বোর্ডের চেয়ারম্যান প্রফেসর আহমেদ হোসেন বাংলানিউজকে বলেন, বিগত দিনে ফলাফল পুনঃনিরীক্ষণের জন্য এতো বেশি আবেদন জমা হয়নি। ডিজিটাল পদ্ধতিতে আবেদন সহজ হওয়ায় এবং এবার দিনাজপুর বোর্ডে পাশের হার গতবারের  চেয়ে কম ও জিপিএ-৫ কম হওয়ায় এতো আবেদন পড়েছে।
২০১৫ সালের এসএসসি পরীক্ষায় দিনাজপুর শিক্ষা বোর্ডে জিপিএ-৫ পেয়েছিল ১০ হাজার ৮৪২ জন। ২০১৬ সালে জিপিএ-৫ পেয়েছিল ৮ হাজার ৮৯৯ জন। চলতি বছর এ সংখ্যা কমে দাঁড়িয়েছে ৬ হাজার ৯২৯- এ। 
