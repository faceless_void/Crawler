তিন থেকে চার কোটি মার্কিন ডলারে প্রতিষ্ঠানটি কিনতে পারে স্ন্যাপচ্যাট বলে রবিবার এক প্রতিবেদনে জানিয়েছে রয়টার্স।
সিমাজিন একটি স্টার্টআপ প্রযুক্তি যার সাহায্যে ব্যবহারকারীরা তাদের মোবাইল ফোন দিয়ে ভার্চুয়ালভাবে খুব সহজেই তাদের বাড়ির শূন্যস্থানে তারা কিনতে ইচ্ছুক এমন আসবাবপত্র ও যন্ত্রপাতি স্থাপন করে দেখতে পারেন যে ঠিক কেমন দেখাচ্ছে বা কী কী পরিবর্তন লাগতে পারে।
সিমাজিন হবে ইসরায়েলে স্ন্যাপচ্যাটের গবেষণা ও উন্নয়ন কেন্দ্র। বর্তমানের এর কর্মীসংখ্যা ২০ জন থেকে সামনে আরও উন্নীত করা হবে বলে জানা গেছে। প্রযুক্তির চেয়ে সিমাজিন-এর উচ্চ দক্ষতাসম্পন্ন কর্মীদলই এই অধিগ্রহণের প্রধান কারণ বলে ধারণা করা হচ্ছে।
