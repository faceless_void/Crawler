ত্বক শুষ্ক হয়ে গেলে দেখতে যতটা খারাপ লাগে তেমনি তৈলাক্ত ত্বকের কারণেও অস্বস্তিতে পড়তে হয়। তৈলাক্ত ত্বকে ময়লা বেশি জমে যেতে পারে ফলে ব্রণ এবং ব্ল্যাক হেডসের সমস্যাও বেশি হয়। তাই প্রয়োজন হয় বাড়তি যত্ন।
রূপচর্চাবিষয়ক একটি ওয়েবসাইটে ত্বকের বিশেষ যত্নের কিছু বিষয় তুলে ধরা হয়।
-  ক্রিমজাতীয় ময়েশ্চারাইজারের বা প্রসাধনীর বদলে জেলজাতীয় প্রসাধনী ব্যবহার করা উচিত। পাশাপাশি প্রাইমার ব্যবহার করা উচিত এটি ত্বকের চকচকে ও তেলতেলেভাব দূর করবে।
- রাতে ঘুমাতে যাওয়ার আগে অবশ্যই ত্বক ভালোভাবে পরিষ্কার করতে হবে। নইলে লোমকূপে তেল জমে ব্যাক্টেরিয়ার সংক্রমণের সম্ভাবনা বেড়ে যাবে।
- হালকা ক্লিনজার ব্যবহার করতে হবে। বেশি ক্ষারযুক্ত ক্লিনজার ব্যবহারে ত্বক থেকে অতিরিক্ত তেল ধুয়ে যাবে যা ত্বক রুক্ষ করে তুলবে।
- অনেকেই মনে করেন তৈলাক্ত ত্বকের জন্য ময়েশ্চারাইজার ব্যবহারের প্রয়োজন নেই। তবে এই ক্ষেত্রেও আর্দ্রতা ধরে রাখার জন্য ময়েশ্চারাইজার ব্যবহার করা জরুরি। নতুবা ত্বকেও ডিহাইড্রেশনের সমস্যা দেখা দিতে পারে। তখন ত্বকের গ্রন্থিগুলো আরও বেশি তেল নিঃসৃত করবে।
- প্রতি সপ্তাহে ত্বক উপযোগী মাস্ক ব্যবহার করতে হবে। ক্লে বা দই রয়েছে এমন মাস্ক ত্বকের জন্য উপকারী।
