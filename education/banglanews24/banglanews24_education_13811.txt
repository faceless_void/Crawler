বরিশাল বিভাগের ৭৪টি কলেজের অধ্যক্ষকে কারণ  দর্শানোর নোটিশ দিয়েছে বরিশাল মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষাবোর্ড কর্তৃপক্ষ।
বরিশাল: বরিশাল বিভাগের ৭৪টি কলেজের অধ্যক্ষকে কারণ  দর্শানোর নোটিশ দিয়েছে বরিশাল মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষাবোর্ড কর্তৃপক্ষ।
সোমবার (১৬ মে) বিকেলে বোর্ডের কলেজ পরিদর্শক ড. মো. লিয়াকত হোসেন সাক্ষরিত এ সম্পর্কিত একটি নোটিশ জারি করা হয়।
সূত্র জানায়, ২০১৫-১৬ শিক্ষাবর্ষে ওই ৭৪টি কলেজের উচ্চ মাধ্যমিক পর্যায়ে ২৫ জনের কম শিক্ষার্থী রয়েছে। শিক্ষার্থী কম থাকায় এসব প্রতিষ্ঠানের শাখা বা উচ্চ মাধ্যমিক পর্যায়ে পাঠদানের অনুমতি কেন বাতিল করা হবে না তা জানতে চেয়ে আগামী ২২ মে সকাল ১০টায় সভাপতি বা সভাপতির প্রতিনিধি এবং অধ্যক্ষকে শিক্ষাবোর্ডে হাজির হওয়ার নির্দেশ দেওয়া হয়েছে।
নোটিশে বলা হয়েছে, নির্ধারিত সভায় কেউ অংশ না নিলে সংশ্লিষ্ট কলেজে উচ্চ মাধ্যমিক শ্রেণির ভর্তি কার্যক্রম বন্ধ রাখা হবে।
