ভারতীয় সংবাদমাধ্যম আইএএনএস জানায়, খবরের সত্যতা যাচাইকারী সংস্থার কাছে কোনো খবর মিথ্যা মনে হলে তাতে ‘ডিসপিউটেড’ ট্যাগ যোগ করা হবে। নির্দলীয় তৃতীয় পক্ষের মাধ্যমে খবরের সত্যতা যাচাই করা হবে বলে জানানো হয়।
ফেইসবুকের সহায়তা কেন্দ্র পেইজে ‘কিভাবে ফেইসবুক পোস্টে ডিসপিউটেড ট্যাগ দেওয়া হবে’ এমন একটি প্রশ্ন উল্লেখ করা হয়েছে। তবে, এতে বলা হয়েছে এটি এখন সবার জন্য উন্মুক্ত নয়।
আপাতত শুধু যুক্তরাষ্ট্রেই ফিচারটি চালু করেছে ফেইসবুক। তবে সেখানেও সেটি সবার জন্য উনুক্ত নয়। বর্তমানে ঠিক কতজন গ্রাহক এটি ব্যবহার করতে পারছেন তাও স্পষ্ট নয়।
আগের বছর ডিসেম্বরের মাঝামাঝি ভুয়া খবর বন্ধে আরেকটি সমাধান আনে ফেইসবুক। এবার সেটি আরও জোরালো করতে যাচ্ছে প্রতিষ্ঠানটি।
ব্যবহারকারী কোনো খবরকে ভুয়া হিসেবে চিহ্নিত করলে তার সত্যতা যাচাই করতে নির্দিষ্ট সংস্থার কাছে পাঠানো হবে। এক্ষেত্রে সত্যতা যাচাই করতে এবিসি নিউজ, ফ্যাক্টচেক ডট অর্গ এবং স্নুপস-এর মতো সংস্থাগুলোর সহায়তা নেবে ফেইসবুক।
