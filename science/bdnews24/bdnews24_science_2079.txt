এই ব্ল্যাকআউটে কারণ হিসেবে আগের দিনের ভারী বৃষ্টিপাতের কথা জানিয়েছেন আয়োজক কর্তৃপক্ষ, বলা হয়েছে বিবিসি’র প্রতিবেদনে।
বিদ্যুত বিভ্রাটের কারণে তাৎক্ষণিকভাবে অংশগ্রহণকারীদের ভবন থেকে সরিয়ে নেওয়া হয়।
ইভেন্টের যে অংশে বিভ্রাট দেখা দিয়েছে সেখানে এলজি, স্যামসাং, সনি-সহ অন্যান্য প্রতিষ্ঠানের বুথ ছিল। বিবিসি’র এক প্রতিবেদক জানিয়েছেন, এ সময় নিরাপত্তা কর্মীরা এলভিসিসি’র নর্থ হল-এও কাউকে প্রবেশ করতে দেননি। এই অংশে টয়োটা, নিসান, ফোর্ড-সহ অন্যান্য গাড়ি নির্মাতা প্রতিষ্ঠান প্রদর্শনীতে অংশ নিয়েছে।
সিইএস ২০১৮ পরিচালনা করছে কনজিউমার টেকনোলজি অ্যাসোসিয়েশন। ধৈর্য্য ধরার জন্য দর্শণার্থীদের ধন্যবাদ জানিয়েছেন আয়োজকরা। কিন্তু এনিয়ে ক্ষোভও প্রকাশ করেছেন অনেকে।
মঙ্গলবার ভারী বৃষ্টিপাতের কারণেই এই বিদ্যুৎ বিভ্রাট ঘটেছে বলে দাবী করেছেন আয়োজকরা। এদিন বৃষ্টির কারণে এলভিসিসি গাড়ি পার্কিংয়ে অবস্থিত গুগলের একটি বুথও বন্ধ করে দেওয়া হয়।
১১৬ দিনে এটিই লাস ভেগাসে প্রথম বৃষ্টি। আর একে জানুয়ারির সবচেয়ে ভেজা দিন বলা হয়েছে লাস ভেগাস রিভিউ জার্নাল সংবাদপত্রের প্রতিবেদনে।
নাম প্রকাশে অনিচ্ছুক এক এলভিসিসি কর্মী বলেন বৃষ্টির পানি ট্রাংক লাইনের ক্ষতি করেছে। এর মাধ্যমেই অনুষ্ঠানে বিদ্যুত সরবরাহ করা হচ্ছিল।
