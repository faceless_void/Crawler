ভারতের অনলাইনভিত্তিক মিষ্টিবিক্রির প্রতিষ্ঠান ‘সেলভাই ডটকম’য়ের সহ-প্রতিষ্ঠাতা পুর্বা কালিতা এবং অনলাইনভিত্তিক ভেষজ উপাদান বিক্রির প্রতিষ্ঠান ‘ডিভাইন অর্গানিকস’য়ের প্রধান ও প্রশিক্ষক সোনিয়া মাথুর জানিয়েছেন নারিকেল কাজে লাগানোর বিভিন্ন উপায়।
ভোজ্য ব্যবহার
* যে কোনো মিষ্টিজাতীয় খাবারে নারিকেলের দুধ কিংবা নারিকেল-কুচি যোগ করা যেতে পারে। সাধারণ আটা-ময়দার পরিবর্তে নারিকেলের আটা-ময়দা ব্যবহার করতে পারেন। পাই বা পিঠা বানানোর ক্ষেত্রে পুরে নারিকেলের ছোট করে কাটা টুকরা দিলে স্বাদে আসবে ভিন্ন মাত্রা।
চকলেট কুকিজ গায়ে নারিকেলের কুচি মাখিয়ে নিয়ে বেইক করলে উপভোগ করতে পারবেন নারিকেলের ভিন্ন এক স্বাদ।
* তরমুজ, কমলা, লেবু ইত্যাদির শরবতে নারিকেলের পানি যোগ করলে ওই সরবত আপনাকে করবে আরও সতেজ।
* ফলের স্মুদিতে চমক চাইলে বেছে নিতে পারেন নারিকেলের পানি কিংবা নারিকেলের দুধ মেশানো গ্রীষ্মকালীন ফলের স্মুদি। এরসঙ্গে যোগ করতে পারেন নারিকেলের শাঁসের ছোট টুকরাও।
* তরকারিতেও মেশাতে পারেন নারিকেলের দুধ কিংবা কুচি। সালাদ, চাটনি, দইভিত্তিক তরকারি, ডাল ইত্যাদি রাঁধতে অলিভ অয়েলের বদলে ব্যবহার করতে পারেন নারিকেল তেল।
ত্বকের যত্নে
* প্রাকৃতিক ময়েশ্চারাইজার হিসেবে নারিকেল অনন্য। ভার্জিন নারিকেল তেল সরাসরি কিংবা অন্য ময়েশ্চারাইজারের সঙ্গে মিশিয়ে গায়ে মাখতে পারেন।
* আর্দ্রতা রক্ষার পাশাপাশি টোনার হিসেবেও বেশ কার্যকর নারিকেল। এতে থাকা চর্বি লোপকূপে ময়লা ও ব্যাকটেরিয়ার আক্রমণ কমায়, ফলে ত্বকের ক্ষয় রোধ হয়।
* নারিকেলের টোনিং উপদান ব্রণ দূরে রাখে।
* আঙুলের মাথা নারিকেল তেলে ডুবিয়ে তা মুখে আলতোভাবে ঘষলে মেইক-আপ উঠে আসবে। পাশাপাশি ত্বক হবে মসৃণ ও নমনীয়।
ছবি: রয়টার্স।
আরও পড়ুন
ওজন কমাতে নারিকেল  
নারিকেল দিয়ে ফেইস মাস্ক  
