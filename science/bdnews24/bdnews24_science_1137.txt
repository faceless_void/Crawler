বিবিসি’র প্রতিবেদনে বলা হয়, ম্যাকইওয়ান ইউনিভার্সিটি’র কর্মীরা ফিশিং ইমেইল পেয়েছিলেন। এসব ইমেইলে বলা হয়, বিশ্ববিদ্যালয়টির এক গ্রাহক তাদের ব্যাংক অ্যাকাউন্টের তথ্য পরিবর্তন করছে। এরপর কর্মীরা ওই ভুয়া অ্যাকাউন্টগুলোতে অর্থ পাঠিয়ে দেয়।
কোনো বিশ্বাসযোগ্য পরিচয়ের আড়ালে তথ্য হাতিয়ে নিতে পাঠানো প্রতারণামূলক ইমেইলগুলোকে ফিশিং ইমেইল বলা হয়।
কানাডার আলবার্টা’র এডমনটন-এর বিশ্ববিদ্যালয়টি এখন তাদের ব্যবসায়ের হিসাব করছে। 
অধিকাংশ অর্থ হং কং আর কানাডার মনট্রিয়াল-এর বিভিন্ন অ্যাকাউন্টে গেছে গেছে বলে শনাক্ত করেছে পুলিশ। তবে, এ নিয়ে এখনও কোনো অভিযোগ দায়ের করা হয়নি।
আসল গ্রাহক কোনো অর্থ পরিশোধ করা হচ্ছে না বলে অভিযোগ উঠানোর উপর এই জালিয়াতির বিষয়টি সামনে উঠে আসে।
এক বিবৃতিতে বিশ্ববিদ্যালয়টির মুখপাত্র ডেভিড বিহ্যারি বলেন, “এমন কিছু হওয়ার মতো উপযুক্ত সময় এটি কখনোই ছিল না। আমাদের শিক্ষার্থীরা তাদের নতুন শিক্ষাবর্ষ শুরু করতে যাচ্ছে, আমরা চাই তাদেরকে ও সম্প্রদায়কে নিশ্চিত করতে যে আমাদের আইটি ব্যবস্থা এই ঘটনায় লঙ্ঘন হয়নি।”
