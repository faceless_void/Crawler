শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ে (শাবিপ্রবি) ‘বাংলাদেশ গণিত সমিতি এএফ মুজিবুর রহমান’ ৮ম জাতীয় স্নাতক অলিম্পিয়াড উৎসব সম্পন্ন হয়েছে।
সিলেট (শাবিপ্রবি): শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ে (শাবিপ্রবি) ‘বাংলাদেশ গণিত সমিতি এএফ মুজিবুর রহমান’ ৮ম জাতীয় স্নাতক অলিম্পিয়াড উৎসব সম্পন্ন হয়েছে।
শুক্রবার (০৯ ডিসেম্বর) বিকেল ৫টায় শাবিপ্রবির মিনি অডিটোরিয়ামে এ উৎসব সম্পন্ন হয়। শাবিপ্রবি গণিত সমিতি উৎসবের আয়োজন করে।
সকাল ১০টায় হ্যান্ডবল গ্রাউন্ডে প্রতিযোগিতার মধ্য দিয়ে উৎসবের উদ্বোধন করেন উপাচার্য অধ্যাপক আমিনুল হক ভূইয়া।
বিভাগীয় প্রধান অধ্যাপক আনোয়ারুল ইসলামের সভাপতিত্বে ও সহযোগী অধ্যাপক পাবেল শাহরিয়ারের সঞ্চালনায় সমাপনী অনুষ্ঠানে বক্তব্য রাখেন- অধ্যাপক ইলিয়াস উদ্দীন বিশ্বাস, অধ্যাপক ড. সাজেদুল করিম, বুয়েটের অধ্যাপক আব্দুল হাকিম, ঢাকা বিশ্ববিদ্যালয়ের অধ্যাপক সাখাওয়াত আলী, অধ্যাপক রাশেদ তালুকদার প্রমুখ।
অনুষ্ঠানের শেষদিকে শীর্ষ দশ প্রতিযোগীর নাম ঘোষণা করা হয়। বিজয়ীরা ২৩ ডিসেম্বর (শুক্রবার) বাংলাদেশ প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ে (বুয়েট) চূড়ান্তপর্বে অংশগ্রহণের সুযোগ পাবেন।
