তবে আন্দাজে বা জরিপে নয়, গবেষণা করে পাওয়া গেছে এই তথ্য। শুধু আবেগ নয় মস্তিষ্কের গঠনের ভিন্নতার কারণে উদাসীন হওয়ারও প্রবণতা পুরুষদের বেশি।
অপরের প্রতি সহানুভূতির অভাব ও অন্যের অনুভূতির তোয়াক্কা না করাকেও আবেগহীন-উদাসীনতার উদাহরণ হিসেবে ধরা যায়। পুরুষের অপরাধ বোধও নারীর তুলনায় কম হয়।
এই বিষয়গুলো আসলে বিবেক ও সহমর্মীতার উন্নয়নে বাধাগ্রস্ততার সঙ্গে সম্পর্কিত।
গবেষণা অনুযায়ী, মস্তিষ্কের যে অংশ অপরের আবেগ এবং অনুভূতি বোঝার সঙ্গে জড়িতে সেই অংশে ‘অ্যান্টেরিয়র ইন্সুলা’ বা ‘গ্রে ম্যাটার’য়ের ঘনত্ব, সাধারণভাবে বেড়ে ওঠা ছেলেরা যাদের মধ্যে আবেগ বর্জিত আচরণ দেখা যায়- তাদের বেশি।
ছেলেদের আবেগ বর্জিত আচরণের তারতম্যের ১৯ শতাংশের ব্যাখ্যা মেলে এই ঘনত্বের তারতম্য থেকে। তবে মেয়েদের ক্ষেত্রে তা প্রযোজ্য নয়।
গবেষণার প্রধান, সুইজারল্যান্ডের ইউনিভার্সিটি অফ বেজেল’য়ের নোরা মারিয়া‌ রাশলে বলেন, “আমাদের গবেষণা দেখায় যে, আবেগবর্জিত আচরণ বাড়ন্ত ছেলেদের মস্তিষ্কের গঠনের তারতম্যের সঙ্গে সম্পর্কযুক্ত।”
এই গবেষণায় ‘ম্যাগনেটিক রেজোনান্স ইমেজিং (এমআরআই)’য়ের মাধ্যমে ১৮৯ জন কিশোরের মস্তিষ্কের বিকাশ নিবিড়ভাবে পর্যবেক্ষণ করেন। উদ্দেশ্যে ছিল আবেগ বর্জিত আচরণের কারণে মস্তিষ্কের গঠনে পরিবর্তন আসে কি না তা দেখা। 
দেখা যায়, ছেলে ও মেয়েদের ক্ষেত্রে আবেগ বর্জিত আচরণ ও মস্তিষ্কের গঠনের মধ্যে সম্পর্ক ভিন্ন।
গবেষণাটি বলে, “অংশগ্রহণকারীদের অ্যান্টেরিয়র ইন্সুলাতে গ্রে ম্যাটারের মাত্রার তারতম্যের রিপোর্ট কিংবা ছেলেদের তুলনায় মেয়েদের এই তারতম্যের মাত্রার ভিন্নতা, সেই সঙ্গে আবেগবর্জিত আচরণ একজন ব্যাক্তির পরিণত বয়সের আচরণের পূর্বাভাস দিতে পারে।”
রাশলে বলেন, “পরবর্তী ধাপে আমরা দেখব, কোন ধরনের কারণে এই কিশোর অংশগ্রহণকারীদের মধ্যে মানসিক সমস্যার উদ্রেক করে।”
ছবি: রয়টার্স।
পুরুষের যেসব অদ্ভূত বিষয় নারীর কাছে আকর্ষণীয়  
চওড়া মুখের পুরুষ হতে পারে প্রতারক
