২৬ ফেব্রুয়ারি বার্সেলোনায় অনুষ্ঠিতব্য মোবাইল ওয়ার্ল্ড কংগ্রেসে  নতুন ফ্ল্যাগশিপ গ্যালাক্সি এস৯ উন্মোচন করতে যাচ্ছে ইলেক্ট্রনিক পণ্য নির্মাতা দক্ষিণ কোরীয় প্রতিষ্ঠানটি। ইতোমধ্যেই ডিভাইসটির রিটেইল বাক্সের ছবি ফাঁস হয়েছে। এবার ডিভাইসের ছবিই ফাঁস করেছে ভেনচারবিট, বলা হয়েছে প্রযুক্তিবিষয়ক সাইট ভার্জ-এর প্রতিবেদনে।
ছবিতে দেখা গেছে গ্যালাক্সি এস৮ ও এস৮ প্লাসের সঙ্গে বাহ্যিক নকশায় খুব বেশি পার্থক্য রাখা হয়নি গ্যালাক্সি এস৯ ও এস ৯ প্লাসে। ডিভাইসের সামনের ছবি প্রকাশ করা হলেও পেছনের কোনো ছবি দেওয়া হয়নি।
ডিভাইসের পৃথক একটি ডামি ভিডিওতে দেখা গেছে গ্যালাক্সি এস৯-এর পেছনে ফিঙ্গারপ্রিন্ট সেন্সরের অবস্থান পরিবর্তন করা হয়েছে। এবারে ক্যামেরার নীচে বসানো হয়েছে ফিঙ্গারপ্রিন্ট সেন্সর।
এস৮-এর মতোই এজ-টু-এজ ইনফিনিটি ডিসপ্লে থাকবে গ্যালাক্সি এস ৯-এ। এর পর্দার মাপ বলা হয়েছে ৫.৮ ইঞ্চি। আর এস৯ প্লাসের পর্দার মাপ হবে ৬.২ ইঞ্চি। এস৯-এ আগের মতোই পেছনে একটি ক্যামেরা ব্যবহার করা হবে। আর এস৯ প্লাস মডেলে পেছনে দুইটি ক্যামেরা রাখা হবে বলে ধারণা করা হচ্ছে।
এর আগে ফাঁস হওয়া রিটেইল বাক্সে দেখা গেছে ‘সুপার স্পিড ডুয়াল পিক্সেল ১২ ওআইএস’ ক্যামেরা রাখা হয়েছে গ্যালাক্সি এস৯-এ। আর সামনে ৮ মেগাপিক্সেল সেলফি ক্যামেরা থাকবে এতে।
৪ জিবি র‍্যামের পাশাপাশি ৬৪ জিবি ইনটার্নাল স্টোরেজ থাকতে পারে গ্যালাক্সি এস ৯-এ। আর ডিভাইসটির ব্যাটারিও উন্নত করা হবে বলে ধারণা করা হচ্ছে।
আর এতে স্যামসাংয়ের নতুন এক্সিনস ৯৮১০ বা স্ন্যাপড্রাগন ৮৪৫ চিপ ব্যবহার করা হতে পারে। এই চিপের মাধ্যমে আইফোন X-এর মতো ফেসিয়াল-রিকগনিশন ও অ্যানিমোজি ফিচার যোগ করতে পারে স্যামসাং।
চলতি বছরের ১৬ মার্চ ডিভাইসটি বাজারে আসতে পারে বলে জানিয়েছে ভেনচারবিট।
 
আরও খবর-
উন্নত ক্যামেরা থাকবে গ্যালাক্সি এস৯-এ  
গ্যালাক্সি এস৯-এর রিটেইল বাক্সের ছবি ফাঁস  
