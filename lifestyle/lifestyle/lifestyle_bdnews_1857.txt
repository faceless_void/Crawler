জোলি হলেন হলিউড তারকা। আমাদের দেশে বিষয়টি আবার সম্পূর্ণ ভিন্ন। একেইতো সচেতনতার অভাব, এর উপর স্তন ক্যান্সার নিয়ে লজ্জাবোধ, সামাজিক বিপত্তি আর বাধা তো আছেই।
তবে এসব লজ্জা, কুণ্ঠা ঝেড়ে ফেলে খুব সহজেই এ সম্পর্কে সচেতন হওয়া যায়।
স্তন ক্যান্সার বিষয়ে বিস্তারিত জানান ঢাকা মেডিকেল কলেজের প্লাস্টিক সার্জন অধ্যাপক রায়হানা আউয়াল সুমি ও বঙ্গবন্ধু শেখ মুজিব মেডিকেল বিশ্ববিদ্যালয়ের প্রাক্তন সার্জন মোহাম্মদ আনোয়ার হোসেন।
হোসেন বলেন, “স্তন ক্যান্সারের সঙ্গে জেনেটিক কিছু সম্পর্ক পাওয়া যায়।”
উদাহরণ হিসেবে হলিউড তারকা জোলির কথাই উল্লেখ করলেন তিনি। জোলির পরিবারে স্তন ক্যান্সারের জেনটিক সম্ভবনা ‘পজিটিভ’ ছিলো।
“রক্তের সম্পর্ক আছে এরকম কারো যদি স্তন ক্যান্সারে আক্রান্ত হওয়ার ইতিহাস থাকে তাহলেও একটু বিশেষ সতর্কতা অবলম্বন করা উচিত।” বললেন ডা. হোসেন।
হোসেনের মতে, অল্পবয়সী নারীর স্তন ক্যান্সারে আক্রান্ত হওয়ার সম্ভবনা সাধারণত কম থাকে। তবে কেউ যদি মনে করেন তরুণীদের স্তন ক্যান্সার হবে না, তাহলে হবে মস্ত বড় ভুল।
ডা. হোসেনের ভাষায়, “মাঝ বয়সীদের একটু বিশেষ সতর্ক থাকতে হবে। উল্টো বলা যায় তরুণ বয়স থেকেই সচেতনতা সৃষ্টি করতে হবে।”
পরিবেশগত বা শারীরিক যত্নের সঙ্গে এই রোগের সরাসরি সম্পর্ক পাওয়া না গেলেও স্থুলকায় নারীদের একটু বিশেষ সতর্ক থাকতে পরামর্শ দেন এই সার্জন।
কীভাবে সচেতন থাকা যাবে? উত্তরে আনোয়ার হোসেন বলেন, “মাসিক পেরিয়ে যাওয়ার দুই থেকে তিনদিন পরে স্তন ও বগলের পুরো অংশ হাত দিয়ে নিজেই পরীক্ষা করা যেতে পারে। দেখতে হবে কোনো ধরনের পিণ্ড বা চাকারমতো কিছু হাতে ঠেকছে কিনা! যদি শক্ত কিছু টের পাওয়া যায়, সেক্ষেত্রে একজন বিশেষজ্ঞের পরামর্শ নেওয়া উচিত।”
রায়হানা আউয়াল সুমির মতে এই ‘সেল্ফ এক্সামিনেশন’-এর ব্যাপারে ডাক্তারের সঙ্গে কথা বলা যেতে পারে। কারণ বহুভাবেই এই পরীক্ষা করা সম্ভব।
ব্রেস্টক্যান্সার ডটওআরজি সাইটে এই রোগের লক্ষণ সম্পর্কে কয়েকটি দিক উল্লেখ করেছে।
প্রাথমিকভাবে এই রোগের কোনো লক্ষণ চোখে পড়ে না। স্তনের ভিতর কোনো পিণ্ড থাকলেও প্রথমদিকে এটা এত ছোট থাকে যে হাতের স্পর্শে সহজে বোঝা যায় না। আবার দেহের ছোটখাট পরিবর্তনও চোখে পড়ে না।
তারপরও এই ক্যান্সার বোঝার প্রাথমিক উপায় হচ্ছে স্তনের ভিতর কোনো প্রকার চাকা বা পিণ্ড। এটি হবে ব্যথাহীন, শক্ত এবং অমসৃণ। তবে কোনো কোনো সময় এই পিণ্ড হতে পারে নরম ও গোল। এছাড়া স্তন অস্বাভাবিক ভারিও লাগতে পারে। তাই কোনো প্রকার সন্দেহ হলেই চিকিৎসকের পরামর্শ নেওয়া উচিত।
অ্যামেরিকান ক্যান্সার সোসাইটির দেওয়া কিছু লক্ষণ এখানে দেওয়া হল। যদি একটি লক্ষণও মিলে যায় তবে দ্রুত পরীক্ষা করার পরামর্শ দিয়েছেন তারা।
 
* পুরো স্তন বা কোনো অংশ স্ফিত হওয়া বা ফুলে যাওয়া।
* চামড়ায় যন্ত্রণা বা গর্ত হওয়া।
* স্তনে ব্যথা।
* স্তনবৃ্ন্তে ব্যথা বা ভিতর দিকে ঢুকে যাওয়া।
* স্তনবৃন্তে বা স্তনের ত্বক পুরু হওয়া, লালচে আভা বা অসমভাব হওয়া।
* স্তনবৃন্ত থেকে দুধ ছাড়া অন্য কোনো তরল বের হওয়া।
* হাতের তলার বা বগলে নিচে কোনো প্রকার পিণ্ড অনুভুত হওয়া।
 
এই ওয়েবসাইটে আরও বলা হয়েছে, দুজন মানুষ যেমন দেখতে একরকম নয়, তেমনি দুই স্তনের ক্যান্সারও একরকম হয় না। তাই ডাক্তারই সবরকম পরীক্ষা করে চিকিৎসার ব্যাপারে সিদ্ধান্ত নিবেন।
আর চিকিৎসকের কাছে পরামর্শের ব্যাপারে ডাক্তার সুমি ও হোসেনের মত হচ্ছে, মেয়েদের এই বিষয় নিয়ে ডাক্তারের কাছে পরামর্শ নেওয়ার বেলায় লজ্জা ভেঙে ফেলা উচিত। ডাক্তার ছেলে বা মেয়ে যেই হোক, পরামর্শ নেওয়ার ব্যাপারে এই সংকোচ থেকে যদি বেরিয়ে না আসতে পারেন সেক্ষেত্রে সম্ভাব্য রোগীরই ক্ষতি হবে।
অধ্যাপক রায়হানা আউয়াল সুমি বলেন, “দুর্ভাগ্যজনকভাবে অনেক রোগী শুধুমাত্র সংকোচের কারণে ডাক্তারের কাছে আসেন না। বিশেষ করে গ্রামাঞ্চলে  এই সমস্যাটি বেশি।”  
দুই বিশেষজ্ঞই মনে করেন, সচেতনতা বাড়াতে মা বড় ভুমিকা রাখতে পারেন। তরুণী মেয়ের সঙ্গে বন্ধুত্বপূর্ণ মনোভাবে এই বিষয়ে পরামর্শ করা উচিত।
বাংলাদেশে এখন স্তন ক্যান্সার শনাক্ত করতে যথেষ্ট ভালো ব্যবস্থা রয়েছে বলে মনে করেন রায়হানা আউয়াল সুমি।
তিনি বলেন, “বাংলাদেশের সব মেডিকেল কলেজেই সর্বাধুনিক শনাক্তকরণ ব্যবস্থা রয়েছে। আগেভাগে নিশ্চিত হতে পারলে চিকিৎসা দেওয়া সম্ভব।”
