২০০৭ সালে যাত্রা শুরু করা প্রতিষ্ঠানটি ২০১৬ সালের ডিসেম্বরে ‘লুসিড এয়ার’ নামে প্রথম গাড়ি উন্মোচন করে। ধারণা করা হচ্ছে এ গাড়ি দিয়েই টেসলাকে টক্কর দেবে প্রতিষ্ঠানটি, জানিয়েছে প্রযুক্তিবিষয়ক সাইট টেকক্রাঞ্চ।
লুসিডই প্রথম প্রতিষ্ঠান নয় যে টেসলাকে টেক্কা দেওয়ার প্রয়াশ করেছে। তবে, এখন পর্যন্ত তেমন সাফল্য পায়নি কোনো প্রতিষ্ঠানই। ইতোপূর্বে আউডি, বিএমডাব্লিউ, পোর্শ-এর মতো শীর্ষস্থানীয় গাড়ি নির্মাতা প্রতিষ্ঠানও তাদের বৈদ্যুতিক গাড়ির মডেল দেখিয়েছে। কিন্তু সেগুলো বাজারে আসলেই আসল অবস্থা বিচার করা যাবে।
এবার প্রশ্ন উঠতে পারে- তাহলে লুসিড এয়ার-এর সম্ভাবনা কোথায়?
ভবিষ্যত প্রজন্মের নকশার কারণে ইতোমধ্যেই গ্রাহকের নজর কেড়েছে লুসিড এয়ার। তবে সেটি এই গাড়ির বাজার দখলে মূল অস্ত্র নয়।
লুসিড এয়ার-এর সবেচেয়ে ইতিবাচক দিক হতে পারে এর মূল্য। বৈদ্যুতিক গাড়ির ওপর থাকা ফেডারেল কর যোগ হওয়ার আগে এর দাম শুরু হবে ৬০ হাজার মার্কিন ডলার থেকে। যেখানে টেসলা মডেল এস ৬০- এর দাম শুরু হয় ৮০ হাজার মার্কিন ডলার থেকে।
দামে স্পষ্ট পার্থক্য থাকলেও একবার পূর্ণ চার্জে দুই গাড়ির সর্বোচ্চ যাত্রা সীমা প্রায় একই। পূর্ণ চার্জে  টেসলা মডেল এস-৬০ চলতে পারে ২১০ মাইল সেখানে লুসিড এয়ার চলবে ২৪০ মাইল।
