সিবিসি নিউজ ধূমপান নিয়ে একটা উদ্বেগজনক তথ্য দিয়েছে। তথ্যটি হচ্ছে, কানাডার মতো একটি উন্নত দেশে প্রতি বছর যত লোক মারা যায় তার অন্তত ১৮ শতাংশের মৃত্যু হয় ধূমপানজনিত কারণে। 
এছাড়াও যে আর্থিক ক্ষতি হয় তার পরিমাণ বছরে ১৬ দশমিক ২ বিলিয়ন কানাডিয়ান ডলার। 
রিপোর্টে বলা হয়েছে, ধূমপায়ীর সংখ্যা কিছুটা হ্রাস পেলেও ধূমপানজনিত রোগ ব্যাধিতে মৃত্যু হার বৃদ্ধি পাচ্ছে। ধূমপানজনিত রোগ ব্যাধিতে মৃত্যু হার বৃদ্ধিতে যেসব রোগ ব্যাধি ভূমিকা রাখছে তা হচ্ছে, ক্যান্সার, হূদরোগ ও শ্বাসযন্ত্রের রোগ। 
তাই বিশেষজ্ঞগণ ধূমপান রোধে কার্যকর পদক্ষেপ গ্রহণ এবং ধূমপান বর্জন করে ধূমপানজনিত মৃত্যু বহুলাংশে রোধ করা যায় বলে মনে করেন। আর এজন্য প্রয়োজন নিজের সদিচ্ছা।
লেখক: ডা. মোড়ল নজরুল ইসলাম, চুলপড়া, এলার্জি, চর্ম ও যৌন রোগ বিশেষজ্ঞ
ইত্তেফাক/আনিসুর
