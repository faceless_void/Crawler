ঢাকা: ঢাকা বিশ্ববিদ্যালয়ের কবি জসীমউদ্‌দীন হল অ্যালামনাই অ্যাসোসিয়েশনের প্রথম পুনর্মিলনীর আয়োজন হচ্ছে আগামী ১০ ফেব্রুয়ারি। 
দিনব্যাপী পুনর্মিলনী অনুষ্ঠানের উদ্বোধনী পর্ব ও স্মৃতিচারণ অনুষ্ঠিত হবে বিশ্ববিদ্যালয়ের নবাব নওয়াব আলী চৌধুরী সিনেট ভবনের অ্যালামনাই ফ্লোরে। বিকেলে সমাপনী অনুষ্ঠান ও সাংস্কৃতিক আয়োজন থাকবে হলের খেলার মাঠে।
হলের সাবেক শিক্ষার্থীদের মিলনমেলায় অংশ নিতে নিবন্ধন ফি দিতে হবে ১০০০ টাকা। স্ত্রী-সন্তানদের অংশগ্রহণে ফি দেওয়া লাগবে মাথাপিছু ৫০০ টাকা। নিবন্ধনের শেষ সময় ৫ ফেব্রুয়ারি।
গত শুক্রবার (১৯ জানুয়ারি) হল প্রভোস্টের কক্ষে বিশেষ সভায় পুনর্মিলনী নিবন্ধনের উদ্বোধন করেন প্রধানমন্ত্রীর উপ-প্রেস সচিব আশরাফুল আলম খোকন। অনুষ্ঠানকে বর্ণিল করে ‍তুলতে বেশ কিছু সিদ্ধান্ত গৃহীত হয় সভায়।
সিদ্ধান্ত অনুযায়ী, পুনর্মিলনী উপলক্ষে প্রকাশিত হবে বিশেষ স্যুভেনির ‘নিমন্ত্রণ’। এতে সাবেক শিক্ষার্থীদের হল জীবনের স্মৃতি প্রকাশিত হবে। তাই সাবেকদের পাঠিয়ে দিতে হবে তাদের নিজের লেখাটি। সংশ্লিষ্ট ছবি থাকলে তাও পাঠানো যাবে। লেখা পাঠানোর ইমেইল অ্যাড্রেস- kjhaadu@gmail.com
সার্বিক যোগাযোগ করতে হবে শেখ সোহেল রানা টিপু (০১৭১১০৬২৪৩৯), মো. কামাল হোসেন (০১৭৩৯৩৬০১০২), নূরনবী সিদ্দিক সুইন (০১৭১১২৮১২৮৫), রাসেল খান (০১৭৭০৭০১০৭৯), রবি (০১৭৩৪২২৩০০৪) ও উবায়দুল্লাহ বাদলের (০১৬১৫০৬১৫২৬) সঙ্গে।
