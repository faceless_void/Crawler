আইএএনএস-এর প্রতিবেদনে বলা হয়, গুগলের এই স্পিকারটিকে কেউ যদি জিজ্ঞাসা করেন “যীশু কে?”, তখন গুগল হোম-কে জবাব দিতে দেখা যায়- “আপনাকে এক্ষেত্রে কীভাবে সহায়তা করতে পারি তা নিয়ে আমি নিশ্চিত না।”
এক বিবৃতিতে গুগলের পক্ষ থেকে বলা হয়, তারা ডিভাইসটিতে ধর্মীয় সব বড় চরিত্রগুলো নিয়ে জবাব দেওয়ার ফিচার বন্ধ করে দেবে।
শুক্রবার গুগলের সার্চ বিভাগের উপদেষ্টা ড্যানি সালিভান এক টুইটে বলেন, “‘যীশু কে? বা ‘যীশখ্রিস্ট কে’ এমন প্রশ্নের জবাব গুগল অ্যাসিস্ট্যান্ট-এর না দেওয়ার পেছনে কোনো অসম্মানের বিষয় নেই, বরং সম্মান নিশ্চিত করতেই এটি করা হয়েছে।”
“অ্যাসিস্ট্যান্ট-এর বলা কিছু জবাব ওয়েব থেকে আসে আর নির্দিষ্ট কিছু বিষয়ে এই কনটেন্ট ক্ষতিকর বা স্প্যাম জাতীয় কিছুর কাছে অনেক লঙ্ঘনযোগ্য হয়ে উঠতে পারে।
গুগল হোম গৌতম বুদ্ধ, হজরত মুহাম্মদ (স.) আর শয়তান সম্পর্কে বলতে পারে কিন্তু যীশুকে নিয়ে কোনো ধরনের তথ্য দিতে পারে না, বিভিন্ন সংবাদ প্রতিবেদনের বরাতে ভারতীয় সংবাদমাধ্যমটি প্রতিবেদনে এমনটা বলা হয়।
