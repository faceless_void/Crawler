মেডিকেল সার্ভিস অ্যান্ড আরঅ্যান্ডডি, কায়া লিমিটেড’য়ের প্রধান এবং সহসভাপতি সঙ্গীতা ভেলাস্কর কয়েকটি চমৎকার খাবারের তালিকা দেন। যার উপকারিতা বর্তমানে দৈনন্দিন জীবনের সঙ্গে জড়িত।










‘পিওরসেন্স’য়ের প্রতিষ্ঠাতা রিশাব মারিওয়ালা দুইটি ফলের উপকারিতা ও খাদ্যতালিকায় যোগ করার মাধ্যমে কাঙ্ক্ষিত ও মসৃণ ত্বক পাওয়ার কারণ সম্পর্কে জানান-  


এর অ্যান্টিঅক্সিডেন্ট ত্বক কোমল রাখে ও ক্ষয় পূরণ করে। আঙুরজাতীয় ফলের পটাসিয়াম ত্বকের বলিরেখা ও দাগছোপ দূর করে এবং রোদের ক্ষতিকর অতিবেগুনি রশ্মি থেকে ত্বককে সুরক্ষিত রাখে।


কিউই ত্বকের ‘অক্সিডেটিভ’ চাপ দূর করে এবং দূষণের কারণে হওয়া ক্ষতি পূরণ করে। কিউই ভিটামিন ই’য়ের ভালো উৎস যা ত্বকের মুক্ত মৌল গঠন প্রতিরোধে সহায়তা করে আরও সুন্দর ত্বক পেতে সাহায্য করে।
প্রচ্ছদের প্রতীকী মডেল: সোনিয়া। ছবি: দিপ্ত।
আরও পড়ুন
রূপচর্চায় ফল ও সবজি
ভালো যৌনজীবনের জন্য প্রাকৃতিক খাবার  
