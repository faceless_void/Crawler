রেসিপি দিয়েছেন রন্ধনশিল্পী তাসনুভা মাহমুদ নওরিন।
উপকরণ: সুজি আধা কাপ। টক দই ৪ টেবিল-চামচ। লবণ ১/৪ চা-চামচ। বেইকিং সোডা এক চিমটি। বেইকিং পাউডার ১ চা-চামচ। আাদাকুচি ১ চা-চামচ। তেল ১ চা-চামচ। পানি পরিমাণ মতো।
তার্কার জন্য লাগবে– তেল ২ টেবিল-চামচ। লাল সরিষা আধা চা-চামচ। কাঁচামরিচ ২/৩টি। কারিপাতা কিছু। তিল আধা চা-চামচ।
পদ্ধতি: সুজির সাথে টক দই, লবণ, আদাকুচি, তেল আর পরিমাণ মতো পানি দিয়ে মিশিয়ে নিন। ২০ মিনিট ঢেকে রাখুন। এরমধ্যে সুজি ফুলে উঠবে।
সুজির ব্যাটার যদি একদম ঘন হয়ে যায় তাহলে আবার সামান্য পানি দিয়ে একটা কেকের ব্যাটারের মতো লেই তৈরি করে নিন।
যে পাত্রে ঢোকলা বসাবেন সেটা তেল ব্রাশ করে নিন। এখন ব্যাটারে বেইকিং সোডা এবং বেইকিং পাউডার মিশিয়ে আবার ভালো মতো মিশিয়ে সঙ্গে সঙ্গে বাটিতে ঢেলে নিন।
বেইকিং সোডা এবং বেইকিং পাউডার মেশানোর পর বেশিক্ষণ রাখা যাবে না। সাথে সাথে চুলায় বসিয়ে দিতে হবে।
ঠিক পুডিংয়ের মতো বড় হাঁড়িাতে পানি দিয়ে তার উপর ঢেকে ঢোকলার বাটি বসিয়ে দিন। ২৫ থেকে ৩০ মিনিট বা কম সময়ও লাগতে পারে। টুথপিক দিয়ে একবার পরখ করে দেখতে পারেন। ঢোকলা হয়ে আসলে টুথপিকে আর কাঁচাসুজি লেগে আসবে না।
ঠাণ্ডা করে বাটি থেকে বের করে নিন।
এবার ২ টেবিল-চামচ তেল গরম করে তার মধ্যে সরিষা, কারিপাতা, তিল, কাঁচামরিচ ভেজে ঢোকলার উপর ছড়িয়ে দিন। চাইলে নারিকেল কুড়ানো দিতে পারেন। 
দই, রাইতা, চাটনির সাথে পরিবেশন করুন মজার ঢোকলা।
আরও রেসেপি
শাক্কাপারা
