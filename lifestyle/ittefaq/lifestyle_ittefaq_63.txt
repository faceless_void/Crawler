প্রতিদিন খাবার খাচ্ছেন। কিন্তু কোন খাবারে কতটুকু ক্যালোরি আছে জানেন ? আর এটি না জানার ফলে বেশি ক্যালোরি গ্রহণ করাই শারীরিক অসুবিধা হচ্ছে। এই জন্য এখনি জেনে নিন কোন খাবারে কত ক্যালোরি আছে।
ছোট এক বাটি সুজির হালুয়া: খাঁটি ঘি এবং চিনি দিয়ে তৈরি ছোট এক বাটি সুজির হালুয়াতে থাকে ৩৭৯ ক্যালোরি।
এক প্লেট ভাত: এক প্লেট ভাত, অর্থাৎ ৮০ গ্রাম ভাতে ২৭২ ক্যালোরি থাকে।
১টি সিঙ্গাড়া: এক্কেবারে ডুবো তেলে ভাজা আলুর পুর ভরা মাঝারি মাপের শিঙাড়াতে থাকে ১২৩ ক্যালোরি।
এক কাপ দুধ চা: ৩০ মিলিলিটার দুধ, ২ চা-চামচ চিনি দিয়ে তৈরি এক কাপ দুধ চায়ে থাকে ৩৭ ক্যালোরি। যদি দুধ এবং চিনির পরিমাণ আরও বাড়ানো বা কমানো হয় সে ক্ষেত্রে ক্যালোরি পরিমাণের তারতম্যও ঘটে।
লুচি: ১০ গ্রাম ময়দা দিয়ে তৈরি ১টি মাঝারি মাপের লুচিতে ক্যালোরির মাত্রা ১২৫। যদি আপনি ৩টি লুচি খান তবে এর মানে আপনি ৩৭৫ ক্যালোরি গ্রহণ করছেন।
১টি চাপাটি এবং ১টি পরোটা: ২০ গ্রামের একটি মাঝারি মাপের আটার চাপাটিতে থাকে ৭০ ক্যালোরি। আর ৩০ গ্রাম আটায় তৈরি একটি বড় চাপাটিতে থাকে ১০০ ক্যালোরি। আলুর পরোটা, যা ৩০ গ্রাম আটায় তৈরি এবং যাতে আলুর পুর ভরা থাকে। এমন আলুর পরোটায় ২১০ ক্যালোরি থাকে।
ইত্তেফাক/জামান
