নতুন এক গবেষণায় দেখা গেছে, শরীরের অতিরিক্ত মেদের কারণে হৃদরোগ, ডায়াবেটিস, স্ট্রোক, কিডনি ও লিভার অসুখের মতো মারাত্মক রোগ তরুণ বয়সেই হতে পারে।
স্বাভাবিক ওজনের মানুষের চাইতে যারা ‘ক্লাস থ্রি’ মাত্রার স্থূলতায় ভুগছেন বা অতিরিক্ত ওজনের অধিকারী, তাদের আয়ূ নাটকীয়ভাবে কমে যায়।
যুক্তরাষ্ট্রের ন্যাশনাল ইনিস্টিটিউটস অফ হেল্থের অঙ্গ প্রতিষ্ঠান ন্যাশনাল ক্যান্সার ইনিস্টিটিউটের (এনসিআই) কারি কিতাহারা বলেন, “ক্লাস থ্রি’ মাত্রার স্থূলতা বা অতিরিক্ত মেদ, বিরল হলেও এটা ক্রমাগত বাড়ছে।”
উদাহরণ হিসেবে তিনি জানান, যুক্তরাষ্ট্রের শতকরা ছয়জন ‘ক্লাস থ্রি’ মাত্রার স্থূলতা শ্রেণির অন্তর্ভুক্ত। সাধারণ উচ্চতার এসব মানুষের ওজন স্বাভাবিকের চাইতে একশ গুণ বেশি।
নির্দিষ্ট শ্রেণির মানুষের ‘বডি ম্যাস ইনডেক্স’ বা বিএমআই নিয়ে এই নীরিক্ষা চালানো হয়।
বিএমআই হল, মানুষের উচ্চতা অনুযায়ী ওজন পরিমাপের পদ্ধতি।
এই গবেষণা চালানোর জন্য যুক্তরাষ্ট্র, সুইডেন ও অস্ট্রেলিয়ার ২০টি বড় জরিপ থেকে তথ্য নেওয়া হয়।
