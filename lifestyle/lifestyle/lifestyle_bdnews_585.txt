গবেষণার ফলাফলে দাবি করা হয়, মাসিকের বিভিন্ন পর্যায়ে মস্তিষ্ক বিভিন্নভাবে কাজ করে এমনটাও জানিয়েছেন অনেকে নারী। এমনকি গর্ভাবস্থায়, সন্তান প্রসবের পর, রজোবন্ধের পরও এরকম হতে পারে।
কানাডার কিউবেক’য়ের কনকর্ডিয়া ইউনিভার্সিটির অধ্যাপক ওয়েইন ব্রেক বলেন, “আমাদের গবেষণায় দেখা গেছে, নারীদের যৌনতা বিষয়ক হরমোন ইস্ট্রোজেন ও প্রোজেস্টেরন পুরোপুরি স্মৃতিভ্রম ঘটায় না, বরং একটি নির্দিষ্ট স্মৃতি বা কৌশলকে বেশি গুরুত্ব দেয়।”
নিয়মিত ঋতুস্রাব হয় এমন ৪৫ জন নারীর অংশগ্রহণে গবেষণাটি করা হয়।
প্রথমেই অংশগ্রহণকারীরা একটি হরমোনবিষয়ক প্রশ্ন উত্তর পর্বে যোগ দেন। এই পর্বে মাসিক চলাকালীন তাদের মাসিক, গর্ভকালীন অবস্থা, জন্ম বিরতিকরণ ওষুধ খাওয়া, সিনথেটিক হরমন গ্রহণ ইত্যাদি বিষয়ে তথ্য সংগ্রহ করা হয়।
এরপর অংশগ্রহণকারীদের মৌখিকভাবে স্মৃতি পরীক্ষা নেওয়া হয়। যেমন- কিছু শব্দের তালিকা মনে করা। রাস্তা মনে রাখতে পারার ক্ষমতা।
ফলাফলে দেখা যায়, জরায়ু থেকে ডিম্বানু বেরিয়ে যাওয়া অবস্থায় থাকা অংশগ্রহণকারীরা মৌখিক স্মৃতি পরীক্ষায় ভালো ফল করেছেন। আবার মাসিক শুরু হওয়ার আগ মুহূর্তে থাকা নারীরা রাস্তা খুঁজে বের করায় ভালো ফল করেন।
এথেকে বোঝা যায়, সমস্যা সমাধানে নারীরা বিভিন্ন কৌশল ব্যবহার করেন, যা নির্ভর করে ঋতুস্রাবের কোন পর্যায়ে
