রূপচর্চাবিষয়ক একটি ওয়েবসাইটে এই বিষয়ের উপর প্রকাশিত প্রতিবেদন অবলম্বনে ’ফেইস ওয়াশ’য়ের প্রয়োজনীয় উপাদানগুলোর নাম এখানে দেওয়া হল।
হলুদ: রূপচর্চায় হলুদের অবদান অনস্বীকার্য। নানী-দাদীদের রূপচর্চার এই উপাদানই ছিল গোপন রহস্য। হলুদ ত্বক ভালো রাখে, দাগছোপ দূর করে, ব্রণের বিরুদ্ধে কাজ করে, ত্বকে টানটানভাব আনে এবং এটা এক ধরনের অ্যান্টিসেপ্টিক যা ত্বক সুন্দর ও কোমল রাখাতে সাহায্য করে। তাই ফেইস ওয়াশ নির্বাচনের সময় হলুদ আছে কিনা দেখে নিন।
অ্যালোভেরা: ত্বক ভালো রাখাতে অ্যালোভেরা জাদুর মতো কাজ করে। রোদপোড়া-ভাব, ব্রণ, ত্বকের সংক্রমণ, বয়সের ছাপ, দাগ হালকা করা ইত্যাদি কাজে অ্যালোভেরার জুড়ি নেই। তাই ত্বকের সুরক্ষায় অ্যালোভেরাযুক্ত ফেইস ওয়াশ ব্যবহার করুন।
লেমন এসেনশিয়াল অয়েল: রোদপোড়া-দাগ, মলিন চেহারা, ব্রণ, বয়সের ছাপ ইত্যাদি থেকে মুক্তি পেতে লেবুর তেল খুব ভালো সমাধান। ত্বকের নানারকম সমস্যা দূর করতে ফেইস ওয়াশে ‘লেমন এসনশিয়াল অয়েল’ আছে কিনা দেখে নিন। এটা ত্বকের প্রাকৃতিক পিএইচ’য়ের মাত্রা ঠিক রাখে এবং ত্বক অতিরিক্ত আর্দ্র ও শুষ্ক হওয়া থেকে রক্ষা করে। এছাড়া ত্বকের ধরন ও মান ভালো করতে সহায়ক। ফেইস ওয়াশ’য়ে এই উপাদান থাকলে চিন্তাভাবনা ছাড়াই সেটা নিতে পারেন।
টি-ট্রি অয়েল: বর্তমানে টি-ট্রি তেল সমৃদ্ধ ফেইস ওয়াশ বেশ জনপ্রিয়তা পেয়েছে। যাদের ত্বকে ব্রণের সমস্যা বেশি তাদের জন্য এটা বিশেষ উপযোগী। টি-ট্রি তেল বিশিষ্ট ফেইস ওয়াশ ত্বক গভীর থেকে পরিষ্কার করে। আমাদের আবহাওয়া ও পরিবেশের ধুলাবালি, জীবাণু এবং ময়লা থেকে ত্বক রক্ষা করতে টি-ট্রি তেল বিশিষ্ট ফেইস ওয়াশ কার্যকর।
ফলের নির্যাস: কমলা, স্ট্রবেরি, ব্লুবেরি ইত্যাদি ফলের নির্যাস সমৃদ্ধ ফেইস ওয়াশ বাজারে পাওয়া যায়। ফলের নির্যাস অ্যান্টিআক্সিডেন্ট সমৃদ্ধ এবং এটা ত্বক পরিষ্কার ও উজ্জ্বল করতে সাহায্য করে। এটা প্রাকৃতিকভাবেই ত্বকে ফেরাতে পারে দীপ্তি। আর তাজা গন্ধে সতেজ অনুভব হয়।
এইসব উপাদান-ই যে একটি ফেইস ওয়াশ’য়ে থাকবে, তা নয়। তবে উপরের যে কোনো একটি বা দুটি উপাদান যুক্ত ফেইস ওয়াশগুলো ব্যবহারে কার্যকর ফলাফল পাওয়ার সম্ভাবনা বেশি থাকে। আর কোন উপাদান কী কাজ করে তা জেনেই গেলেন। তাই প্রয়োজন বুঝে লেবেল পড়ে ফেইস ওয়াশ কিনুন।
ছবি: রয়টার্স।
আরও পড়ুন
জুতা কেনার আগে  
অনলাইনে জুতা কেনার আগে  
ব্লেজার কিনতে বা বানাতে  
