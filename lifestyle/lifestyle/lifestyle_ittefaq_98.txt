মানব দেহের ৫টি ভাইটাল অর্গান হচ্ছে হার্ট, লাং, ব্রেইন, লিভার ও কিডনি। এর যে কোনো একটি অর্গান না থাকলে মানুষ বাঁচতে পারে না। আজ আমরা আলোচনা করবো হার্ট কিভাবে ভালো রাখবেন। আর হার্ট ভাল রাখার জন্য রয়েছে ৪টি প্রধান করণীয়।
ইংরেজিতে বলা হয়, ‘ফোর মেইন পিলারস অব হার্ট হেলথ’। এই ৪টি স্তম্ভ হচ্ছে স্বাস্থ্যকর খাবার আহার, ধূমপান বর্জন, ফিজিক্যালি অ্যাকটিভ থাকা ও শরীরের ওজন নিয়ন্ত্রণে রাখা।
হার্ট ভালো রাখতে আমাদের দেশের কতজন মানুষ এই চারটি নিয়ম মেনে চলেন তার কোনো পরিসংখ্যান নেই। তবে যুক্তরাষ্ট্রে এক গবেষণা রিপোর্টে উল্লেখ করা হয়েছে, ৩ ভাগেরও কম মার্কিন নাগরিক দাবি করতে পারবেন তারা হার্ট ভাল রাখতে ৪টি প্রধান শর্ত মেনে চলেন। মাত্র ২ দশমিক ৭ ভাগ মার্কিনী ননস্মোকার বা অধূমপায়ী। অর্থাৎ প্রায় ৯৮ ভাগ মার্কিনী ধূমপান করে থাকেন।
এদিকে হার্ট ভাল রাখতে হার্টবান্ধব খাবার বলতে প্রচুর পরিমাণ সবুজ শাক সবজি, তাজা ফলমূল আহার, অধিক চর্বিযুক্ত খাবার পরিহার, সপ্তাহে অন্তত: ১৫০ মিনিট এক্সারসাইজ করা, শরীরের ওজন স্বাস্থ্যকর পর্যায়ে রাখা এবং ধূমপান একেবারেই বর্জন করা। তবে হার্ট ভালো রাখতে মার্কিনীদের অনিহা সংক্রান্ত তথ্যে কিছুটা হতাশা ব্যক্ত করেছেন যুক্তরাষ্ট্রের ওরিগন অব পাবলিক হেলথ এন্ড হিউম্যান সায়েন্সের সিনিয়র অথার ইলেন স্মিথ।
বিশেষজ্ঞদের মতে, হার্ট সুস্থ রাখতে স্বাস্থ্যকর ও পুষ্টিকর খাবার আহার, প্রতিদিন ব্যায়াম করা, শরীরের ওজন কাঙ্খিত পর্যায়ে রাখা ও ধূমপান পরিহার করার কোনো বিকল্প নেই।
লেখক- ডা. মোড়ল নজরুল ইসলাম, 
চুলপড়া, এলার্জি, চর্ম ও যৌন রোগ বিশেষজ্ঞ
ইত্তেফাক/আনিসুর
