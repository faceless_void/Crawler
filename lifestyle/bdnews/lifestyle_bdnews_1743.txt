শীতমৌসুমে ওজন বৃদ্ধি এড়ানোর জন্য এর পেছনের কারণ জানা থাকা চাই। স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে এই বিষয়ের উপর প্রকাশিত প্রতিবেদন অবলম্বনে কারণগুলো এখানে দেওয়া হল।
ঋতু পরিবর্তনের প্রভাব: ‘সিজনাল অ্যাফেক্টিভ ডিজওর্ডার (এসএডি)’ হল এক ধরনের ঋতুনির্ভর হতাশাগ্রস্ততা, যা আমাদের মন-মেজাজের উপর বিরূপ প্রভাব ফেলে এবং কর্মস্পৃহা কমিয়ে দেয়। আর শীতকালে রোদ কম হয় বলে স্বাস্থ্যের উপরও তা ক্ষতিকর প্রভাব ফেলে। মানুষের পরিশ্রম কিংবা ব্যায়াম করার আগ্রহও কমে যায় এই ঋতুতে। ফলাফল, ওজন বৃদ্ধি।
আবহাওয়া: ওজন নিয়ন্ত্রণে রাখার আগ্রহ কমে যাওয়ার একটি বড় কারণ হল ঠাণ্ডা আবহাওয়া। শরীরচর্চার পরিবর্তে এই ঋতুতে সিংহভাগ মানুষই লেপ-কম্বল মুড়ি দিয়ে আয়েসে দিন কাটাতেই পছন্দ করেন। আর কনকনে ঠাণ্ডায় শারীরিক পরিশ্রম করাটা বেশ কঠিনও বটে। এজন্য চাই দৃঢ় প্রতীজ্ঞাবদ্ধতা।
বড় রাত: রাত বড় মানেই ঘুম বেশি, আর ঘুম বেশি মানে পরদিন শরীর ম্যাজম্যাজ করা। যত বেশি ঘুমাবেন, শরীরের স্বাভাবিক চক্রের উপর তা ততই প্রভাব ফেলবে, ফলে আলসেমি বাড়বে। এই অলসতা আপনার ব্যায়ামের রুটিনও নষ্ট করবে।
উষ্ণ খাবার: শীতকালে পরিশ্রম কমার পাশাপাশি ভারী এবং উষ্ণ খাবার খাওয়ার মাত্রা বেড়ে যায়। এই খাবারগুলো শরীরের তাপমাত্রা এবং খাওয়ার ইচ্ছা দুটোই বাড়িয়ে দেয়। মৌসুমি খাবার যে স্বাস্থ্যকর তাতে কোনো সন্দেহ নেই, তবে সব খাবারই পরিমাণ মতো খাওয়া বুদ্ধিমানের কাজ।
বেশি খাওয়া: শীতকালে শরীরে খাবারের চাহিদা বেড়ে যায়। অসংখ্য গবেষণার ফলাফলে দেখা গেছে, শীতকালে খাবারের চাহিদা বাড়ে প্রাকৃতিকভাবেই। তাই খাবারের উপর ঝাঁপিয়ে পড়ার আগে তার ফল সম্পর্কে সচেতন হওয়া উচিত।
বর্ধিত বিপাকক্রিয়া: শুনতে ভালো মনে হলেও বিপাকক্রিয়ার আকস্মিক দ্রুততা চর্বি কমানোর বদলে আরও বাড়িয়ে দেয়। গবেষণা বলে, বিপাকক্রিয়া বাড়লে তা আগের চাইতে বেশি শক্তি খরচ করতে চায় যা শরীর উষ্ণ রাখতে সাহায্য করে। এতে ক্ষুধা বাড়তে পারে, ফলে খাওয়ার পরিমাণও বাড়ে।
ছবি: রয়টার্স।
আরও পড়ুন
পিৎজা খেয়েও ওজন কমান  
