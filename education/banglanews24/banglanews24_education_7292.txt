সন্তানদের প্রাইভেট পড়তে না দিয়ে ক্লাসে আরও আন্তরিকভাবে পড়াতে শিক্ষকদের উপর চাপ প্রয়োগের আহ্বান জানালেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ।
ঢাকা: সন্তানদের প্রাইভেট পড়তে না দিয়ে ক্লাসে আরও আন্তরিকভাবে পড়াতে শিক্ষকদের উপর চাপ প্রয়োগের আহ্বান জানালেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ।
সোমবার (২৪ অক্টোবর) বিকেলে রাজধানীর রমনায় বাংলাদেশ ইনস্টিটিউশন অব ইঞ্জিনিয়ার্স, বাংলাদেশ (আইইবি) মিলনায়তনে প্রকৌশলী পরিবারের কৃতি সন্তানদের সংবর্ধনা অনুষ্ঠানে প্রধান অতিথির বক্তব্যে তিনি এ আহ্বান জানান।
দেশ ও দেশের মানুষের কল্যাণে- এই মূলমন্ত্র নিয়ে দেশের বৃহৎ শিল্প প্রতিষ্ঠান বসুন্ধরা গ্রুপ আইইবি’র বিভিন্ন উন্নয়ণমূলক কর্মকাণ্ডের সঙ্গে যুক্ত থাকার অংশ হিসেবে বসুন্ধরা গ্রুপের বসুন্ধরা সিমেন্ট অনুষ্ঠানে সহযোগী পৃষ্ঠপোষক হিসেবে অংশগ্রহণ করে।
এসময় প্রায় সাড়ে ৫শ’ শিক্ষার্থীকে সংবর্ধনা দেওয়া হয়।
অনুষ্ঠানে অভিভাবকদের উদ্দেশে শিক্ষামন্ত্রী বলেন, সন্তানদের শুধু প্রাইভেট টিচারের কাছে নিয়ে যান কেন, শিক্ষকদের চাপ দেন না। শিক্ষকদের বাধ্য করবেন, ক্লাসে পড়াতে হবে- প্রাইভেট নয়। আমরা শিক্ষকদের বেতন দ্বিগুণ বৃদ্ধি করেছি।
বসুন্ধরা গ্রুপের পৃষ্ঠপোষকতায় আয়োজিত সংবর্ধনা অনুষ্ঠানের প্রশংসা করে শিক্ষামন্ত্রী বলেন, এটা খুবই মহৎ আয়োজন এবং ভবিষতে শিক্ষার্থীদের চলবার পথে বিরাট শক্তি যোগাবে।
পরীক্ষা পদ্ধতি এবং পাঠ্যক্রম পরিবর্তন নিয়ে শিক্ষামন্ত্রী অভিভাবকদের উদ্দেশে বলেন, ছেলে-মেয়েদের এত ভালবাসেন, নতুন কিছু করলে বাধা দেন। কিন্তু আধুনিক যুগের সঙ্গে তাল মিলিয়ে এগিয়ে যেতে হলে আধুনিক পদ্ধতিতে গ্রহণ করতে হবে। হা-হুতাশ করলে হবে না।
শিক্ষার্থীদের সাহসী করে তুলতে হবে জানিয়ে মন্ত্রী বলেন, আমরা সেই চেষ্টা করছি, যাতে ভালোভাবে তারা পড়ালেখা করতে পারে। আধুনিক জ্ঞান এবং প্রযুক্তি আজকের দুনিয়ায় সব থেকে বড় হাতিয়ার। আমরা মানুষের হাতের নাগালে প্রযুক্তি পৌঁছে দিয়েছি। আমাদের সন্তানদের বিশ্বমানের করে গড়ে তুলতে পারলে জগতের যে কোনো প্রতিযোগিতায় টিকে থাকতে পারবে। একই সঙ্গে তাদের দেশপ্রেমে উদ্বুদ্ধ ভালো মানুষ হিসেবে গড়ে তুলতে হবে।
প্রকৌশলী পরিবারের কৃতি সন্তানদের উৎসাহিত করার লক্ষ্যে আইইবি’র ঢাকা কেন্দ্র এই সংবর্ধনার আয়োজন করে।
প্রাথমিক শিক্ষা সমাপনী, জেএসসি, এসএসসি, এইচএসসি, ও-লেভেল এবং এ-লেভেলে উত্তীর্ণদের সংবর্ধনা দেওয়া হয়।
বসুন্ধরা গ্রুপের বসুন্ধরা সিমেন্ট সেক্টরের হেড অব ডিভিশন (সেলস) খন্দকার কিংশুক হোসেন অনুষ্ঠানে উপস্থিত ছিলেন।
অনুষ্ঠানের শুরুতে বঙ্গবন্ধুর জীবনী নিয়ে একটি ভিডিও চিত্র প্রদর্শন করা হয়।
আইইবি’র ঢাকা কেন্দ্রের চেয়ারম্যান মেসবাহুর রহমান টুটুলের সভাপতিত্বে শিক্ষা প্রকৌশল অধিদফতরের প্রধান প্রকৌশলী দেওয়ান মোহাম্মদ হানজালা, আইইবি ঢাকা কেন্দ্রের ভাইস চেয়ারম্যান খায়রুল বাশার, নজরুল ইসলাম, সম্মানী সম্পাদক আমিনুর রশীদ চৌধুরী মাসুদ উপস্থিত ছিলেন।
