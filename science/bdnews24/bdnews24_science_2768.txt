ট্যাপ-টু-আনব্লক, গ্রুপ চ্যাটে ব্যক্তিগত বার্তা দেওয়ার সুবিধাসহ অনেকগুলো ফিচার যোগ হচ্ছে নতুন আপডেটে। ফিচারগুলো শনাক্ত করেছে ওয়াবেটালইনফো। অ্যান্ড্রয়েড আপডেটে নতুন ফিচারগুলোর বেটা সংস্করণ যাচাই করে থাকে এই ওয়েবসাইটটি।
ট্যাপ-টু-আনব্লক ফিচারের মাধ্যমে ব্লক করা রয়েছে এমন কন্টাক্টস-এর ওপর কিছুক্ষণ চাপ দিয়ে ধরে রাখলে সেখানে আনব্লক করার অপশন দেখানো হবে। ফলে সহজে কন্টাক্টস আনব্লক করে তার সঙ্গে চ্যাট করতে পারবেন গ্রাহক, বলা হয়েছে ভারতীয় সংবাদমাধ্যম আইএএনএস-এর প্রতিবেদনে।
এর পাশাপাশি ‘প্রাইভেট রিপ্লাইস’ ফিচারের পরীক্ষা চালাচ্ছে হোয়াটসঅ্যাপ। এর মাধ্যমে গ্রুপ চ্যাটে কাউকে ব্যক্তিগত বার্তা পাঠানোর প্রয়োজন হলে সেটি সহজেই করা যাবে। এর জন্য গ্রুপ থেকে বের হওয়ার কোনো প্রয়োজন হবে না। ওয়াবেটালইনফো জানায়, শুধু অ্যাডমিনিস্ট্রেটর এই সুবিধা পাবেন।
চ্যাটিংয়ের সময় কোনো ত্রুটি দেখা গেলে ব্যবহারকারী তার ডিভাইসটি ঝাঁকিয়ে অভিযোগ জানাতে পারবেন- ভবিষ্যতে এমন ফিচারও যোগ হতে পারে বলে জানিয়েছে ওয়েবসাইটটি।
ত্রুটি দেখা দিলে ডিভাইস ঝাঁকানোর ফলে হোয়াটসঅ্যাপ-এর কন্টাক্ট আস সেকশন খুলবে। এর মাধ্যমে বিষয়টি তদন্ত করতে প্রতিষ্ঠানটিকে সহায়তা করবেন গ্রাহক।
সম্প্রতিক সময়ে বেশ কিছু নতুন ফিচারের পরীক্ষা চালিয়েছে হোয়াটসঅ্যাপ। ইতোমধ্যে নতুন আপডেটে কিছু ফিচার যোগও করা হয়েছে।
আইওএস আপডেটে গ্রুপ কল ফিচার আনার লক্ষ্যেও কাজ করছে প্রতিষ্ঠানটি।
২০০৯ সালে যাত্রা শুরু করে হোয়াটসঅ্যাপ। ২০১৪ সালে ২২০০ কোটি মার্কিন ডলারে প্রতিষ্ঠানটি কিনে নেয় ফেইসবুক। সে সময় এই ক্রয়মূল্য ছিল ইলেক্ট্রনিক পণ্য নির্মাতা প্রতিষ্ঠান সনি কর্পোরেশন-এর  বাজার মূল্যের চেয়েও বেশি।
