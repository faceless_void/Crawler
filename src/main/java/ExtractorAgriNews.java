import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class ExtractorAgriNews {
    private HashSet<String> links;
    private HashSet<String> linksForParsing;
    private List<List<String>> articles;
    private static final int MAX_DEPTH = 50;
    int count = 0;
    String filename = "data2/agri24/news_agri_";

    public ExtractorAgriNews() {
        links = new HashSet<>();
        articles = new ArrayList<>();
        linksForParsing = new HashSet<>();

    }

    //Find all URLs that start with "http://www.mkyong.com/page/" and add them to the HashSet
    public void getPageLinks(String URL,int depth) {
        if (!links.contains(URL) && (depth < MAX_DEPTH)) {
            try {
                Document document = Jsoup.connect(URL)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .timeout(10*1000).get();
                Elements otherLinks = document.select("a[href^=\"http://www.agrinews24.com/\"]");

                for (Element page : otherLinks) {
                     {
                        links.add(URL);
                        //Remove the comment from the line below if you want to see it running on your editor
                        //System.out.println(URL);
                        System.out.println("calling with this "+ page.attr("abs:href"));
                         if(!page.attr("abs:href").contains("/category/"))
                                linksForParsing.add(URLDecoder.decode( page.attr("abs:href"), StandardCharsets.UTF_8.name() )) ;
//                         if (links.contains(page.attr("abs:href")))
//                             continue;
                        getPageLinks(page.attr("abs:href"),++depth);
                    }

                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
            finally {
                //System.out.println("exception ");
//                getArticles();
//                writeToFile();
//                System.out.println("getArticle called");

            }
        }
    }

    //Connect to each link saved in the article and find all the articles in the page
    public void getArticles() {
        System.out.println("links count "+linksForParsing.size());

        linksForParsing.forEach(x -> {

            {
                System.out.println("calledArticle1");

                Document document;
                try {
                    document = Jsoup.connect(x).ignoreHttpErrors(true).get();
                    Elements articleLinks = document.select("div.entry>p");

                    System.out.println("calledArticle from"+x +"--connected");

                    if(articleLinks.size() >3)
                        articleLinks.remove(articleLinks.size()-1);

                    System.out.println("articles size " + articleLinks.size());
                    for (Element article : articleLinks) {
                        if(article.hasText())
                            System.out.println(article.text() + article.text().length());
                        {
                            ArrayList<String> temporary = new ArrayList<>();
                            temporary.add(article.text()); //The title of the article
                            System.out.println("length is "+article.text().length());
                            writeNow(article.text());
                            temporary.add(article.attr("abs:href")); //The URL of the article
                            articles.add(temporary);
                            System.out.println("article count : "+count++);
                        }
                    }
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
                finally {
//                    writeToFile();
                }
            }
        });
    }

    private void writeNow(String text) {
        FileWriter writer ;
        {

            {
                try {
                    writer= new FileWriter(filename+Integer.toString(count++));

                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer.write(text);
                    writer.close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
//            writer[0].close();
        }
    }

    public void writeToFile() {

        final FileWriter[] writer = new FileWriter[1];
         {

            articles.forEach(a -> {
                try {
                    writer[0] = new FileWriter(filename+Integer.toString(count++));
                    String temp = a.get(0) ;
                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer[0].write(temp);
                    writer[0].close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            });
//            writer[0].close();
        }
    }

    public static void main(String[] args) throws Exception {
        ExtractorAgriNews bwc = new ExtractorAgriNews();
        bwc.getPageLinks("http://www.agrinews24.com",0);
        bwc.getArticles();
        bwc.writeToFile();
        System.out.println("done.");
    }
}