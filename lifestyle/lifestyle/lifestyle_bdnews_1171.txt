ঘর থেকে বের হওয়ার আগে ঝটপট মেইকআপ করার কিছু পন্থা উল্লেখ করা হয় রূপচর্চাবিষয়ক একটি ওয়েবসাইটের প্রতিবেদনে।
বিবি ক্রিম
চট জলদি মেইকআপের সময় ফাউন্ডেশন বাদ দিয়ে বেছে নেওয়া যেতে পারে এসপিএফ যুক্ত বিবি ক্রিম। পরিমাণ মতো ক্রিম নিয়ে পুরো মুখে ফোঁটা ফোঁটা করে লাগিয়ে আঙুলের ডগা দিয়ে পুরো মুখে ছড়িয়ে দিতে হবে। বিবি ক্রিম ত্বককে রোদের হাত থেকে বাঁচাবে, পাশাপাশি ফাউন্ডেশনের কাজও করবে। আর এটি ত্বকের সঙ্গে মিশিয়ে দিতে বেশি কসরতও করতে হবে না।


ত্বকে কোনো ধরনের দাগ না থাকলে কনসিলারের প্রয়োজন হয় না। তবে অসম গায়ের রং এবং ব্রণ বা দাগ লুকাতে কনসিলার ব্যবহার জরুরি। চোখের নিচে, নাকের দু’পাশে এবং ব্রণ ও কালো দাগের উপর কনসিলার লাগিয়ে ভালোভাবে ব্লেন্ড বা মিশিয়ে নিতে হবে।
ব্লাশ
বেইস মেইকআপ হয়ে যাওয়ার পর গালে হালকা ব্লাশ বুলিয়ে নেওয়া যায়। এর সঙ্গে সামান্য ব্রোঞ্জার ব্যবহার করে মুখ কন্টুয়ার করে নেওয়া যেতে পারে। গালের উঁচু অংশ বা ‘অ্যাপল’য়ে মানানসই রংয়ের ব্লাশ ব্যবহার করলেই ত্বকে আলাদা এক ধরনের আভা যুক্ত হবে।
কাজল ও মাস্কারা
সকালে ভারি করে চোখ সাজানোর সময় যদি না হয় তবে হালকা কাজল এবং চোখের পাপড়িতে মাস্কারা বুলিয়ে নিলেই চোখের সাজ হয়ে যাবে।


ম্যাট লিপস্টিক লাগাতে সময় একটু বেশি লাগে। তাই তাড়াহুড়ার সময় ম্যাট লিপস্টিকের বদলে লিপগ্লস বেছে নেওয়া যেতে পারে। গোলাপি বা পিচ রংয়ের বিভিন্ন শেইড বা পছন্দ মতো অন্য যে কোনো রংয়ের লিপগ্লস ব্যবহার করে ঠোঁটের মেইকআপ শেষ করা যায়।
ছবি: রয়টার্স।
লিপস্টিক আকর্ষণীয় করার উপায়
মাস্কারা ব্যবহারের ভুলগুলো
