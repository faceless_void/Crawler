দক্ষিণ অস্ট্রেলিয়ার ১০০ মেগাওয়াটের এই ব্যাটারি অঞ্চলটির বৈদ্যুতিক গ্রিডের নিরাপত্তার জন্য নকশা করা হয়েছে বলে প্রতিবেদনে জানিয়েছে বিবিসি।
এক ঘন্টার জন্য ৩০ হাজার বাড়িতে বিদ্যুৎ সরবরাহ ক্ষমতা রয়েছে এই ব্যাটরির।
বৈদ্যুতিক গাড়ি নির্মাতা মার্কিন প্রতিষ্ঠানটির প্রধান ইলন মাস্ক বলেন, প্রকল্পের চুক্তি স্বাক্ষরের ১০০ দিনের মধ্যে এটির কাজ শেষ না হলে ব্যাটারির জন্য কোনো মূল্য নেওয়া হতো না।
প্রকল্পের শেষ তারিখ ১ ডিসেম্বরের আগেই এটি শেষ হবে বলে ধারণা করা হচ্ছে। ব্যাটারিটি পুনব্যবহারযোগ্য শক্তি উৎপাদনকারী ফরাসী প্রতিষ্ঠান নিওয়েন এর একটি বায়ু বিদ্যুত প্ল্যান্টের সঙ্গে যুক্ত থাকবে বলে প্রতিবেদনে বলা হয়েছে।
এই প্রকল্প বাস্তবায়নে স্যামসাংয়ের ব্যাটারি সেল ব্যবহার করেছে টেসলা।
মাইক্রো ব্লগিং সাইট টুইটারের মাধ্যমেই মূলত এই প্রকল্পের প্রস্তাব দেন মাস্ক। চলতি বছরের ফেব্রুয়ারিতে দক্ষিণ অস্ট্রেলিয়ায় বেশ কিছু লোড শেডিংয়ের পর এই প্রস্তাব করা হয়। এছাড়া ২০১৬ সালের সেপ্টেম্বরে ব্ল্যাকআউটের মুখে পরে অঞ্চলটি, যাতে ১৭ লাখ বাসিন্দাকে বিদ্যুৎ ছাড়া থাকতে হয়।
লোড শেডিং এড়াতে কর্তৃপক্ষ ব্যাটারির পুরো শক্তি ব্যবহারের সুযোগ পাবেন বলে প্রতিবেদনে জানানো হয়েছে।
প্রাথমিকভাবে এটি পরীক্ষার মধ্য দিয়ে যাবে। তবে, এটি চালু করার সঙ্গে সঙ্গে “সিস্টেম সিকিউরিটি সার্ভিসেস”-এর জন্য ব্যবহার করা হবে।
