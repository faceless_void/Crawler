(শাবিপ্রবি): শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ে ১০ দিনব্যাপী বই উৎসব শুরু হয়েছে।
রোববার (১৫ অক্টোবর) সকাল ১১টার দিকে বিশ্ববিদ্যালয়ের স্বেচ্ছাসেবী সংগঠন ‘কীনে’র উদ্যোগে এ উৎসবের উদ্বোধন করেন উপাচার্য অধ্যাপক ফরিদ উদ্দিন আহমেদ।
দ্বাদশ শ্রেণির ছাত্র ব্লাড ক্যান্সারে আক্রান্ত জিদান আহমেদের সাহায্যার্থে এ উৎসবের আয়োজন করে ‌‌'কীন'। উৎসব শেষ হবে ২৪ অক্টোবর।
আয়োজকরা জানান, বিশ্ববিদ্যালয়ের পার্শ্ববর্তী এলাকা করেরপাড়ার শফিক আহমেদের ছেলে জিদান চিকিৎসার জন্য অনেক টাকা প্রয়োজন।
এ উৎসব থেকে প্রাপ্ত টাকা তার চিকিৎসার জন্য ব্যয় করা হবে। এছাড়াও কেউ সাহায্য পাঠাতে চাইলে সোনালী ব্যাংক শাবিপ্রবি শাখায় কীনে'র সঞ্চয়ী হিসাব নং-৩৪০৭৭০৪১ ও সংগঠনের নেতাদের সঙ্গে যোগাযোগ করার অনুরোধ করা হয়েছে।
রোববার থেকে বৃহস্পতিবার পর্যন্ত প্রতিদিন সকাল ১০টা থেকে রাত ৮টা এবং শুক্র ও শনিবার বিকেল ৩টা থেকে রাত ৮টা পর্যন্ত স্টলগুলো খোলা থাকবে।
এবারের আয়োজন বই নাটোরের বইপ্রেমী পলান সরকারকে উৎসর্গ করা হয়েছে।
