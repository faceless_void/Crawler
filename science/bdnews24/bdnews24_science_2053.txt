আরটেম ভাউলিন নামের ওই ব্যক্তি ফাইল শেয়ারিং সাইটটির মালিক বলে বলা হচ্ছে। তার বিরুদ্ধে মুদ্রা পাচার আর কপিরাইট আইন লঙ্ঘনের অভিযোগ আনা হয়েছে। অভিযোগে তিনি শেষ আট বছরে শতকোটি পাউন্ড মূল্যের পাইরেটেড গান আর চলচ্চিত্র বিতরণ করেছেন বলে দাবি করা হয়েছে। 
বুধবার ৩০ বছর বয়সী ভাউলিন-কে পোল্যান্ডে আটক করা হয়, জানিয়েছে বিবিসি। মার্কিন বিচার বিভাগ জানিয়েছে তারা তাকে যুক্তরাষ্ট্রে আনার চেষ্টা চালাবে। 
কিকঅ্যাস টরেন্টস ২৮টি ভাষায় পরিচালিত হয় আর এর বাজার মূল্য ৫.৪ কোটি ডলারেরও বেশি, দাবি মার্কিন কর্মকর্তাদের। 
মার্কিন রাষ্ট্রপক্ষের আইনজীবী জ্যাচারি ফার্ডন এক বিবৃতিতে বলেন, "ভাউলিন ইন্টারনেট ব্যবহার করে শিল্পীদের প্রচুর ক্ষতি করেছেন।" 
কিকঅ্যাস টরেন্টস-এ অবৈধ ফাইল রাখা না হলেও, এখানে ওই ফাইলগুলোর ইন্টারনেট লিঙ্ক দিয়ে দেওয়া হয়। এর ফলে ব্যবহারকারীরা সেখান থেকে কোনো অনুমোদন ছাড়াই কনটেন্ট ডাউনলোড করে নিতে পারেন। 
ওয়েব মনিটরিং প্রতিষ্ঠান অ্যালেক্সা -এর মতে, কিকঅ্যাস টরেন্টস-এর মালিকানাধীন একটি ডোমেইন সারা বিশ্বের সবচেয়ে জনপ্রিয় ওয়েবসাইটগুলোর মধ্যে ৭০তম।   
