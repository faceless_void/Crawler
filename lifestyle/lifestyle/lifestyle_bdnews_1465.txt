স্বাস্থ্যবিষয়ক একটি ওয়েবসাইট থেকে জানা যায় মুখে ব্যাক্টেরিয়া জন্মানোর অন্যতম কারণ হচ্ছে দাঁতের ফাঁকে জমে থাকা খাবার।
নিয়মিত প্রতিটি দাঁতের মাঝে ব্রাশ বা পরিষ্কার না করা হলে তা পচে ব্যাক্টেরিয়ার জন্মের পরিবেশ তৈরি করে দেয়। এ ছাড়াও কিছু খাবার যেমন পেঁয়াজ, রসুন, চীনাবাদামের তেল ইত্যাদি তীব্র গন্ধের খাবার মুখে দুর্গন্ধ সৃষ্টি করে।
ওয়েবসাইটটি দুর্গন্ধ থেকে বাঁচার কিছু নিধানও জানিয়েছে।
- দিনে ৮ থেকে ১০ গ্লাস পানি খাওয়া উচিত। এতে মুখ এবং জিহবা আর্দ্র থাকে। মুখের ভিতরে লালা সৃষ্টি হয়। এই লালা মুখকে দ্রুত পরিষ্কার করে গন্ধ হওয়া রোধ করে।
- পানিতে টইটুম্বুর এমন সব ফল সবজি যেমন- তরমুজ, শসা ইত্যাদি বেশি করে খেতে হবে। এগুলো দাঁতের ফাঁকে জমা ব্যাক্টেরিয়া বাসা বাধতে বাঁধা দেয়।
- মিষ্টি খাবারকে বিদায় করে দিলে দুর্গন্ধ সৃষ্টিকারী ব্যাকটেরিয়া মুখে বাসা বাঁধার পরিবেশই পাবে না। চিনি ছাড়া যে সেগুলো বাঁচতেই পারে না।
- কফি খাওয়া বাদ দিলে সমস্যা অনেকটাই মিটে যায়। তবে যদি কফি একদম বাদ না দেওয়া যায় তবে কফির পরে তৈরি হওয়া গন্ধ দূর করতে সঙ্গে সঙ্গেই পানি বা কোনো ফল খেয়ে ফেললে গন্ধ জাদুর মতো মিলিয়ে যায়।
