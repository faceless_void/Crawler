বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজের খাদ্য ও পুষ্টি বিজ্ঞান বিভাগের প্রধান ফারাহ মাসুদা রোজার মাসে স্বাস্থ্যকর সেহেরি খাওার পরামর্শ দেন।
তিনি বলেন, “যেহেতু গরমকালে রোজা তাই শরীরের পানির চাহিদার প্রতি বিশেষ মনযোগ দিতে হবে। শরীরের পুষ্টি ও ভিটামিনের চাহিদা পূরণের জন্য খাদ্য তালিকায় সব ধরণের খাদ্য উপাদান যোগ করতে হবে।”
ফারাহ মাসুদা, সেহেরিতে সুষম খাবার খাওয়ার পরামর্শ দেন।
তিনি বলেন, “খাদ্য তালিকায় খাবারের ছয়টি উপাদান- শর্করা, স্নেহ, আমিষ, ভিটামিন, খনিজ ও পানি ইত্যাদি উপাদানগুলো প্রয়োজন মতো রাখতে হবে। যেহেতু সারাদিন কোনো কিছু খাওয়া হয় না তাই সেহেরিতে পর্যাপ্ত পরিমাণে খাবার খেতে হবে।”
তিনি আরও বলেন, “অনেকে মনে করেন যে, সারাদিন কিছু খাওয়া হবে না তাই সেহেরিতে বেশি করে খেতে হবে। এটা একটা ভুল ধারণা। বরং প্রয়োজনের তুলনায় বেশি খাওয়া হলে শরীরের জন্য ক্ষতিকর।”
সেহেরিতে আঁশ বহুল খাবার বা হজম দেরিতে হয় এমন খাবার খাওয়া ভালো। এতে ক্ষুধা দেরিতে লাগে।
শরীর ঠিক রাখতে সেহেরিতে শর্করাজাতীয় খাবার যেমন- ভাত, রুটি ইত্যাদি খেতে পারেন। এর সঙ্গে আমিষ ও স্নেহ পদার্থের চাহিদা মেটানোর জন্য মাছ, মাংস, ডিম, দুধ ইত্যাদি খাওয়া ভালো।
তবে যেহেতু এখন গরমকাল তাই যতটা সম্ভব ‘রেড মিট’ অর্থাৎ গরু, মহিষ, খাসি ইত্যাদির মাংস এড়িয়ে চলুন।
