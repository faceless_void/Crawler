এর মাধ্যমে প্রতিষ্ঠানটি তাদের মেসেজিং ব্যবসায়ের কৌশলে নজর বাড়াতে যাচ্ছে, বলা হয়েছে প্রযুক্তি সাইট ভার্জ-এর প্রতিবেদনে।
এই অ্যাপ কোনো নির্দিষ্ট বিষয়ে ছোট ছোট গ্রুপে আলোচনা চালানোর জন্য আনা হয়েছিল। এটি টেক্সটিং অ্যাপের চেয়ে ছোট গ্রুপ ফোরাম অ্যাপ হিসেবেই বেশি ব্যবহৃত হয়। এখানে ব্যবহারকারী একটি বিষয় ঠিক করে বন্ধু আর সহকর্মীদের আমন্ত্রণ জানাতে ও তাদের নিয়ে আলোচনা করতে পারেন। এর মূল ফিচার হচ্ছে এটি থেকে স্বয়ংক্রিয়ভাবে গুগল সার্চ, ইমেইজ ও ইউটিউব থেকে কনটেন্ট নিয়ে আসা যায়।
গুগল প্লাস-এ এক পোস্টে গুগল-এর পণ্য ব্যবস্থাপক জন কিলক্লাইন বলেন, “আমরা স্পেসেস থেকে যা শিখেছি তা নিয়ে আমাদের অন্যান্য পণ্যে প্রয়োগের সিদ্ধান্ত নিয়েছি।”
স্পেসেস-এর মতো ফেইসবুকেও একটি পরীক্ষামূলক অ্যাপ ছিল, এর নাম ছিল রুমস। ২০১৫ সালের ডিসেম্বরে সেটি বন্ধ করে সোশাল জায়ান্টটি।
