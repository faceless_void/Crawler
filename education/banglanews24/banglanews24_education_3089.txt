ঢাকা: বাংলাদেশ জনতাত্ত্বিক ডিভিডেন্ডের সুবিধা নিতে প্রস্তুত বলে জানিয়েছেন ব্রিটিশ কাউন্সিলের দক্ষিণ এশিয়া অঞ্চলের ডেপুটি রিজিওনাল ডিরেক্টর রবিন রিকার্ড। 
মঙ্গলবার (২ মে) ব্রিটিশ কাউন্সিল আয়োজিত দ্বিতীয় বছরের জন্য প্লে লার্ন অ্যাক্ট-এর উদ্বোধন অনুষ্ঠানে সংস্থাটির ঢাকা বিশ্ববিদ্যালয় ক্যাম্পাস কার্যালয়ে একথা বলেন তিনি। 
তিনি বলেন, বাংলাদেশ জনতাত্ত্বিক ডিভিডেন্ডের সুবিধা নিতে প্রস্তুত। এ লক্ষ্য অর্জনের প্রথম ধাপ হবে তরুণদের দক্ষতা বাড়িয়ে তুলে একটি নির্দিষ্ট স্তরে নিয়ে যাওয়া। যেখানে তারা বৈশ্বিকভাবে নিজেদের উপস্থাপন করতে পারবে। এমন একটি উদ্যোগের অংশ হতে পেরে ব্রিটিশ কাউন্সিল গর্বিত। 
পেশাগত জীবনে ইংরেজি ভাষা খুবই জরুরি বলে মন্তব্য করেছেন হংকং অ্যান্ড সাংহাই ব্যাংকিং করপোরেশন (এইচএসবিসি) বাংলাদেশের প্রধান নির্বাহী ফ্রাঁসোয়া দ্য মারিক্যু। তিনি বলেন, আমার মাতৃভাষা কিন্তু ইংরেজি না। কিন্তু ইংরেজি ভাষা জানার কারণে আমি বিশ্বের বিভিন্ন দেশে কাজ করতে পারছি। তাই শিশু বয়স থেকেই মাতৃভাষার পাশাপাশি ইংরেজি শিখতে হবে। 
প্রথম বছর ১০টি বিদ্যালয় নিয়ে এ প্রকল্পের কাজ শুরু করে ব্রিটিশ কাউন্সিল। চলতি বছর ১৫টি বিদ্যালয় অংশ নেবে এ প্রকল্পে। ২০১৭ সালের শেষে এ প্রকল্পের আওতায় ২০টি সরকারি প্রাথমিক বিদ্যালয়ের শিক্ষক ও শিক্ষার্থী উপকারিতা পাবেন বলে আশা করছেন ব্রিটিশ কাউন্সিলের কর্মকর্তারা। 
এ সময় অন্যদের মধ্যে উপস্থিত ছিলেন ব্রিটিশ কাউন্সিলের পরিচালক বারবারা উইকহ্যাম, ব্রিটিশ কাউন্সিলের হেড অফ কালচারাল সেন্টার সারাওয়াত রেজা। 
অনুষ্ঠানে ঢাকা জেলার শিক্ষা কর্মকর্তাদের ক্রেস্ট প্রদান করে ব্রিটিশ কাউন্সিল। 
