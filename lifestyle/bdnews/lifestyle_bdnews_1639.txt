নতুন এক গবেষণা অনুযায়ী, যে নারীরা স্বাভাবিকের তুলনায় এক ঘণ্টা বেশি ঘুমিয়েছেন, পরের দিন তাদের যৌনাকাঙ্ক্ষা বেড়েছে।
প্রতি এক ঘণ্টা বাড়তি ঘুমানোর ফলে যৌনাকাঙ্ক্ষা বাড়ে প্রায় ১৪ শতাংশ, যা যৌনমিলনের চাহিদার উপর ঘুমের প্রভাব প্রতিফলিত করে।
জার্নাল অফ সেক্সুয়াল মেডিসিনে প্রকাশিত এই গবেষণার ফলাফলে আরও জানা গেছে, যেসব নারী গড়ে বেশি সময় ঘুমান তাদের যৌনাঙ্গের উত্তেজনাজনিত সমস্যা কম ঘুমানো নারীদের তুলনায় কম।
গড়ে ৭ ঘণ্টা ২২ মিনিট ধরে ঘুমানোর কথা জানিয়েছেন এই নারীরা। 
ইউনিভার্সিটি অফ মিশিগানের স্লিপ অ্যান্ড সার্কাডিয়ান রিসার্চ ল্যাবরেটরির গবেষক ডেভিড কামবাক বলেন, “যৌন চাহিদা এবং উত্তেজনার উপর ঘুমের প্রভাব নিয়ে পর্যবেক্ষণের আগ্রহ কম। তবে এই গবেষণায় ফলাফল ইঙ্গিত করে যে, অপর্যাপ্ত ঘুম নারীদের যৌনাকাঙ্ক্ষা এবং উত্তেজনার উপর ক্ষতিকর প্রভাব ফেলে।”
তিনি আরও বলেন, “পর্যাপ্ত ঘুম নারীদের যৌনাকাঙ্ক্ষার উপর ইতিবাচক প্রভাব ফেলে এবং উত্তেজনা বাড়িয়ে যৌনমিলনকে আরও সুখকর করে।”
গবেষণার জন্য কামাবাক ও তার সহকর্মীরা কলেজ পড়ুয়া ১৭১ জন নারীকে পর্যবেক্ষণ করেন। যারা প্রতিদিন ঘুমের সময় ডায়রিতে লিখে রাখতেন এবং পরেরদিন কারও সঙ্গে যৌনমিলনে লিপ্ত হয়েছেন কিনা তা জানাতেন।  
কামবাক বলেন, “আমি মনে করি, এই গবেষণা থেকে বাড়তি ঘুম ভালো এই শিক্ষা নেওয় উচিৎ নয়। বরং আমাদের শরীর এবং মনের জন্য প্রয়োজনীয় পরিমাণ ঘুমানো দরকার, এই শিক্ষা নেওয়া জরুরি।”
বর্তমানে গবেষকরা ঘুমজনিত সমস্যা যৌনবিষয়ক কোনো জটিলতার জন্য দায়ি কি না তা নিয়ে গবেষণা করছেন।
ছবি: রয়টার্স।
 
ভালো ঘুমের জন্য
ঘুম না হলে!
রাতে ভালো ঘুমের খাবার
