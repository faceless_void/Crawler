কোনো মাসে হয়ত ব্যস্ততার কারণে ফেইশল করানোর সময় হয়ে উঠলো না। এক্ষেত্রে ঘরেই বরফশীতল পানি দিয়ে এই ফেইশল করে ফেলতে পারেন।
ঠাণ্ডা পানি ভর্তি বড় বাটিতে ৩০ সেকেন্ড মুখ ডুবিয়ে রাখলে ত্বকের নানান সমস্যা থেকে মুক্তি পাওয়া যায়। এতে ত্বক যে শুধু শীতল হয় তা নয়, আরও কিছু সমস্যাও দূর হয়।
রূপচর্চাবিষয়ক একটি ওয়েবসাইটে বরফশীতল ফেইশলের পদ্ধতি উল্লেখ করা হয়।
কীভাবে করবেন
- একটি পাত্রে পরিষ্কার ঠাণ্ডা পানি নিন।
- পানির মধ্যে বেশ কয়েকটা বরফের টুকরা ছেড়ে দিন।
- এই পানিতে ১০ থেকে ৩০ সেকেন্ড মুখ ভিজিয়ে নিন। একবারে না পারলে বিরতি দিয়ে কয়েকবার মুখ ডুবিয়ে রাখতে পারেন। এর আগে অবশ্যই চুল একটি ব্যন্ড দিয়ে আটকে নিতে হবে।
উপকারিতা
- চোখের ফোলাভাব ও ক্লান্তি দূর করে। দিনের শুরুতে এই পদ্ধতি অনুসরণ করলে চোখের ফোলাভাব থেকে মুক্তি পাওয়া যায়। বরফশীতল পানি ত্বক জাগিয়ে তোলে ও রক্ত সঞ্চালন বৃদ্ধি করে।
- রোদে পোড়া ত্বকের যত্নেও এই পদ্ধতি বেশ উপকারী। তাপের কারণে ত্বকে লালচে ছোপ পড়ে, তাছাড়া অনেকের ত্বকের স্বাভাবিক রংয়ের সঙ্গে নীলচে বা হলদে রংও হয়। এ ধরনের সমস্যা সমাধানেও বেশ কার্যকর এই শীতল ফেইশল।
- তাছাড়া মেইকআপের শুরুতে ত্বক প্রস্তুত করার জন্যও এই ফেইশল করা যেতে পারে।
- যারা খোলা লোমকূপের ঝামেলায় ভুগছেন তাদের জন্য এই পদ্ধতি বেশ উপকারী। ঠাণ্ডা পানি লোমকূপ সংকুচিত করতে সাহায্য করবে।
