ঢাকা বিশ্ববিদ্যালয় (ঢাবি): ঢাকা বিশ্ববিদ্যালয় অধিভুক্ত সরকারি সাত কলেজে ২০১৭-২০১৮ শিক্ষাবর্ষে প্রথম বর্ষ স্নাতক (সম্মান) এবং পাস কোর্সে বাণিজ্য ইউনিটের ভর্তি পরীক্ষা শনিবার (৯ ডিসেম্বর) অনুষ্ঠিত হবে।
বৃহস্পতিবার (৭ ডিসেম্বর) বিকালে বিশ্ববিদ্যালয়ের জনসংযোগ দফতর থেকে পাঠানো এক প্রেসবিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
এতে বলা হয়, ওইদিন সকাল ১০টা থেকে ১১টা পর্যন্ত ভর্তি পরীক্ষা মোট ৮টি কেন্দ্রে অনুষ্ঠিত হবে। বাণিজ্য ইউনিটে ৮ হাজার ৭৮৫টি আসনের বিপরীতে ভর্তিচ্ছু আবেদনকারীর সংখ্যা প্রায় ২২ হাজার  ১০৩ জন।
পরীক্ষা কেন্দ্রগুলো হচ্ছে- ঢাবির বিজনেস স্টাডিজ অনুষদ, ইডেন মহিলা কলেজ, বেগম বদরুন্নেসা সরকারি মহিলা কলেজ, ঢাকা কলেজ, কবি নজরুল সরকারি কলেজ, সরকারি শহীদ সোহরাওয়ার্দী কলেজ, সরকারি তিতুমীর কলেজ, সরকারি বাঙলা কলেজ।
 বিজ্ঞান ইউনিটে ৮ হাজার ৬০০টি আসনের বিপরীতে ভর্তিচ্ছু আবেদনকারীর সংখ্যা প্রায় ২৭ হাজার ১২৬ জন। এ পরীক্ষা ঢাকা বিশ্ববিদ্যালয় ক্যাম্পাসসহ মোট ২৪টি কেন্দ্রে অনুষ্ঠিত হবে।
ভর্তি সংক্রান্ত বিস্তারিত তথ্য www.7college.du.ac.bd ওয়েবসাইটে পাওয়া যাবে বলেও জানানো হয়েছে ওই প্রেসবিজ্ঞপ্তিতে। 
