জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি): জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের সঙ্গে সরকারের আইসিটি বিভাগ ও আর্নস্ট অ্যান্ড ইয়ং ইন্ডিয়ার মধ্যে একটি সমঝোতা স্মারক (এমওইউ) সই হয়েছে।
রোববার (১৯ নভেম্বর) বিশ্ববিদ্যালয়ের কাউন্সিল কক্ষে ত্রি-পক্ষীয় এ এমওইউ সই করা হয়।
সমঝোতায় জাবি কোষাধ্যক্ষ অধ্যাপক শেখ মো. মনজুরুল হক, আইসিটি বিভাগের যুগ্ম-সচিব রেজাউল করিম এবং আর্নস্ট অ্যান্ড ইয়ং ইন্ডিয়ার প্রজেক্ট ডিরেক্টর কামাল মানসারামানি নিজ নিজ পক্ষে সই করেন। 
এমওইউ অনুযায়ী, জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের সরকার ও রাজনীতি বিভাগের শিক্ষার্থীরা আইসিটি ক্ষেত্রে দক্ষতা বাড়াতে প্রশিক্ষণের সুযোগ পাবে। 
অনুষ্ঠানে বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ফারজানা ইসলাম, সরকার ও রাজনীতি বিভাগের সভাপতি সহযোগী অধ্যাপক শামসুন্নাহার খানম এবং বিভাগীয় অন্যান্য শিক্ষকরা উপস্থিত ছিলেন।
