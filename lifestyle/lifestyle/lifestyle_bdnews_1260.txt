কদিন আগেই গেল বিদ্রোহী কবির মৃত্যুবার্ষিকী। তার স্মৃতিময় শৈশবের দরিরামপুর, কাজির শিমলা আর ত্রিশাল ঘুরে ফিরে দেখে আসতে পারেন ‘নজরুল স্মৃতিকেন্দ্র’।
ত্রিশালের শাখুয়া গ্রামে বাস করে বন্ধু মানুষ চিকিৎসক আবু সায়েম আরিফ। তার কাছ থেকেই জানলান, এখন রাস্তা ভালো। গাজীপুরের যানজটে না পড়লে যেতে সময় লাগবে না। তার এমন নিশ্চয়তায়, ভোরবেলা ত্রিশালের উদ্দেশ্যে ঢাকা ছাড়লাম। তারপর সকাল ১১টায় ত্রিশাল বাজারে পৌঁছে সকালের নাস্তা খেয়ে চলে আসি দরিরামপুর হাই স্কুলে।


সেই সময়ে দরিরামপুর হাই স্কুলের প্রথম কক্ষটির দেয়ালে কবির হাতের লেখা মর্মর পাথরে খোদাই করে রাখা হয়েছে। কবি এখানে লিখেছেন- ‘আমি এক পাড়াগেঁয়ে স্কুল পালানো ছেলে/ তার ওপর পেটে ডুবুরি নামিয়ে দিলেও/ ‘ক’ অক্ষর খুঁজে পাওয়া যাবে না/ স্কুলের হেডমাস্টারের চেহারা মনে করতেই আমার আজও জল তেষ্টা পেয়ে যায়।’
মর্মর পাথরটি যতটা অযত্নে আছে, ঠিক পাশের নতুন স্কুল ভবনটি ততটাই ঝকঝকে তকতকে।


দরিরামপুর হাইস্কুল আর নজরুল মঞ্চের বেহাল দশা দেখে মন খারাপ হয়ে যায়।  সেখান থেকে রিকশায় চেপে চলে আসি যাই নজরুল বিশ্ববিদ্যালয়ে। ঢোকার মুখের নাম এখন বটতলা। কবির স্মৃতিমাখা বটগাছটি এখনও দাঁড়িয়ে কবির অস্তিত্ব জানান দিচ্ছে।
গাছটিকে বট বলা হলেও এটি আসলে একটি পাকুড় গাছ। জনশ্রুতি আছে, কবি এই গাছর তলায় বসে বাঁশি বাজাতেন! বিচুতিয়া বেপারীবাড়িতে থাকার সময় তৎকালীন সুকনীবিলের পারের এই বটগাছ তলায় প্রায়ই ছুটে আসতেন। একাকী বসে সময় কাটাতেন, বাঁশিতে তুলতেন সুরের মূর্ছনা।
কবির প্রিয় সেই বটতলায় কিছু সময় কাটিয়ে চলে আসি নজরুল বিশ্ববিদ্যালয়ে। ভবন ঘুরে ঘুরে দেখি। থমকে দাঁড়াই একটি ক্লাসরুমের কাছে। সঙ্গীতচর্চা চলছে- আমি চিরতরে দূরে চলে যাব/ তবু আমারে দেব না ভুলিতে।


যখন নজরুল স্মৃতিকেন্দ্রে ঢুকতে যাব তখন তত্ত্বাবধায়ক আক্তারুজ্জমান স্মৃতিকেন্দ্রের মূল ফটক বন্ধ করে বাসায় চলে যাওয়ার প্রস্তুতি নিচ্ছিলেন। আমাদের পেয়ে তিনি বন্ধ স্মৃতি কেন্দ্রটি তার পিয়নকে খুলে দিতে বলেন। তারপর নিজেই সেই ভর দুপুরে আমাদের স্মৃতিকেন্দ্রটি ঘুরে ঘুরে দেখালেন।
স্মৃতিকেন্দ্রের নিচতলায় রয়েছে মিলনায়তন, আর দোতলা ব্যবহৃত হয় আর্কাইভ হিসেবে। এখানে আমরা কবির ব্যবহৃত একটি গ্রামোফোন দেখে অভিভূত হই। স্মৃতিকেন্দ্রের দোতলায় দাঁড়িয়ে দেখি সামনের পুকুরটি যেখানে কবি দাপিয়ে সাঁতার কাটতেন। পুরো স্মৃতিকেন্দ্রটি নানারকম গাছগাছালিতে ঘেরা। এখানে শুধুই মুগ্ধতা। অনেক মুগ্ধতা আর আক্তারম্নজ্জামান সাহেবের যথেষ্ট আন্তরিকতা নিয়ে আমরা চলে যাই বিচুতিয়ার সে বাড়িতে, যেখানে কবি জায়গির ছিলেন।


সিএনজি চালিত অটো রিকশায় কাজির শিমলা যেতে সময় লাগে দশ মিনিট। ময়মনসিং মহাসড়ক থেকে যখন আমরা কাজির শিমলার পথ ধরি তখন রাস্তার দুপাশের সবুজ মন কেড়ে নেয়।
ছোট মাঝারি বা বিশাল কত রকমের গাছের সঙ্গে দুপাশেই পুকুড় আর ডোবায় ভরা, সঙ্গে মেঘলা আকাশ আর ঝিরিঝিরি বৃষ্টির ছাট সব কিছু অসাধারণ মনে হতে থাকে, আহা কত দিন এমন অসাধারন পিচ পথে চলি না।


দারোগা রফিজ উদ্দিনের বাড়িতে কবি নজরুল ছিলেন। কবির কৈশরের সেই স্মৃতি ধরে রাখার জন্যই বাড়িটি হয়েছে নজরুল স্মৃতিকেন্দ্র। তবে সবকটা ফটক তালা মারা দেখে হতাশ হয়ে ফিরেই আসছিলাম, সে সময় এখানকার তত্ত্বাবধায়ক আল-আমিন তালুকদারের ডাক। আমরা স্মৃতিকেন্দ্রের ভেতর প্রবেশ করি।
২০০৮ সালের ২৫ অগাস্ট স্মৃতিকেন্দ্রটিকে বর্তমান রূপ দেওয়া হলেও পাশেই রয়েছে পুরাতন ভবনটি। অবশ্য পুরাতন ভবনটির ভঙ্গাবশেষ ছাড়া এখন আর কিছুই নেই।
আমরা শুরুতে ভগ্নপ্রায় ভবনটি ঘুরে দেখি। তারপর চলে আসি নতুন ভবনে।


খাটের পাশে কিছুটা সময় কাটিয়ে চলে যাই দোতলায়। সেখানে রয়েছে অফিস, হলরুম আর স্টোররুম।
আমরা হলরুমে ঢুকে দেখি কবির সব বইয়ে ঠাঁসা। রয়েছে কবির সব দুঃসপ্রাপ্য ছবি। আমরা সে সব বই আর ছবি দেখি। আল-আমিন তালুকদার স্মৃতিকেন্দ্রের গল্প করেন।


“শুধু জয়ন্তীতে সব কিছু নতুন করে গোছানো হয়, যেমন করা হয় একুশে ফেব্রুয়ারির আগে শহীদ মিনারকে। সে সময় এখানে ইউএনও আর ম্যাজিস্ট্রেট সাহেবের ঠেলাঠেলি আর মন্ত্রণালয়ের খবরাখবরে দম ফেলার ফুরসত মেলেনা।” আমরা আল আমিন তালুকদারের কথা শুনি আর ভাবি কেনো এমন সব অবহেলা আমাদের জাতীয় কবিকে নিয়ে!
প্রয়োজনীয় তথ্য
ঢাকার মহাখালি থেকে সরাসরি ত্রিশাল বাস থাকলেও ময়মনসিংহগামী এনা বা আলম এশীয়ায় চড়ে বসলেই ত্রিশাল চলে যেতে পারবেন। ভাড়া জনপ্রতি ২২০ টাকা। অবশ্য দলবেঁধে নিজস্ব বাহনে যাওয়ার মজাই আলাদা। দিনে গিয়ে দিনেই ফেরত আসা যায়।


সেখান থেকে রিকশায় যেতে হবে নজরুল বিশ্ববিদ্যালয়। এখানে বটতলা আর বিশ্ববিদ্যালয় ঘুরে বিচুতিয়া বেপারী বাড়ির নজরুল স্মৃতিকেন্দ্রে চলে যান। সেখান থেকে ত্রিশাল হয়ে কাজির শিমলায় দারোগাবাড়ির নজরুল স্মৃতিকেন্দ্র, তারপর ফিরতি পথ।
মনে রাখবেন এখানকার রিকশা চালক আপনাকে দেখে তিনগুন ভাড়া চেয়ে ফেলবে। ফাঁদে পা না দিয়ে দরদাম করে তবেই রিকশায় উঠুন।
লোকসংখ্যা বেশি হলে একটা ভ্যানগাড়ি অথবা ব্যাটারি-চালিত-অটোরিকশায় চড়ে বসতে পারেন।
বটতলায় মোটামুটি মানের খাবার রেস্তোরাঁ আছে। আরও খাবার দোকান পাবেন ত্রিশাল ও কাজির শিমলার কাছে মেডিকেল রোডে।
