বেরোবি (রংপুর): বেগম রোকেয়া বিশ্ববিদ্যালয়ে (বেরোবি) ‘জনসেবক হিসেবে অভিজ্ঞতা বিনিময়’ শীর্ষক সেমিনার অনুষ্ঠিত হয়েছে।
রোববার (১৭ সেপ্টেম্বর) বিকেলে বিশ্ববিদ্যালয়ের লোকপ্রশাসন বিভাগের আয়োজনে এ সেমিনার অনুষ্ঠিত হয়। 
লোক প্রশাসন বিভাগের প্রভাষক জুবায়ের ইবনে তাহেরের সভাপতিত্বে সেমিনারে প্রধান অতিথি হিসেবে উপস্থিত ছিলেন বিশ্ববিদ্যালয়ের উপাচার্য প্রফেসর ডক্টর নাজমুল আহসান কলিমউল্লাহ। 
সেমিনারে মুখ্য আলোচক ছিলেন বাংলাদেশ ন্যাশনাল সায়েন্টিফিক অ্যান্ড টেকনিক্যাল ডকুমেন্টেশন সেন্টারের (ব্যান্সডক) মহাপরিচালক জেসমীন আক্তার।   এ সময় অন্যদের মধ্যে উপস্থিত ছিলেন লোকপ্রশাসন বিভাগের প্রভাষক আসাদুজ্জামান মন্ডলসহ শিক্ষক ও শিক্ষার্থীরা।
