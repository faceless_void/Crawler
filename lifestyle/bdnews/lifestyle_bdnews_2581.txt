গবেষণার জ্যেষ্ঠ লেখক, ওয়াশিংটন ইউনিভার্সিটি স্কুল অফ মেডিসিন ইন সেইন্ট লুইসের মেডিসিন বিভাগের অধ্যাপক লিন্ডা পিটারসন বলেন, “গবেষণাটি ছোট, তবে রোগীকে বিটের শরবত খাওয়ানোর দুই ঘণ্টার মধ্যেই মাংসপেশীতে জোরালো পরিবর্তন লক্ষ করেছি আমরা।”
গবেষকদের বিশ্বাস, বিটের শরবতে থাকা উচ্চ নাইট্রেইটযুক্ত উপাদান গবেষণায় অংশগ্রহণকারীদের মাংসপেশীর শক্তি বাড়ানোর কারণ হতে পারে।
পুর্ববর্তী গবেষণায় দেখা গেছে, অনেক ক্রীড়াবিদের মাংসপেশীর কর্মক্ষমতা বাড়াচ্ছে ‘ডায়েটারি নাইট্রেইট’।
বিটের শরবত, পালংশাক ও অন্যান্য সবুজ পাতাবিশিষ্ট সবজিতে থাকা নাইট্রেইট শরীরে প্রক্রিয়াজাত হয়ে নাইট্রিক অক্সাইড তৈরি করে। যা রক্তনালী শিথিল করতে সহায়ক এবং শরীরের বিপাকক্রিয়ার উপর আছে বিভিন্ন উপকারী প্রভাব।
গবেষণায় অংশগ্রহণকারী রোগীদের দেওয়া হয় বিটের শরবত। সবাইকে সাধারণ বিটের জুস এবং আরেকটা জুস থেকে নাইট্রেইট উপদান সরিয়ে দেওয়া হয়।  
প্রথম চিকিৎসার প্রভাব যাতে পরের চিকিৎসার উপর না পড়ে এজন্য দুটি পরীক্ষামূলক সেশনের মাঝে ছিল এক থেকে দুই সপ্তাহের ব্যবধান।
উচ্চমাত্রায় নাইট্রেট উপদানযুক্ত বিট শরবত খাওয়ার দুই ঘণ্টা পরেই রোগীদের হাটু সম্প্রসারণকারী পেশীর শক্তি ১৩ শতাংশ বাড়তে দেখা গেছে। 
গবেষকরা সর্বোচ্চ সুফল দেখেছেন যখন পেশি সর্বোচ্চ গতিতে পৌঁছায়।
মাংসপেশির কার্যক্ষমতা বৃদ্ধির পরিমাণ উল্লেখযোগ্য ছিল দ্রুত এবং শক্তি প্রয়োজন হয় এমন কাজে। তবে দীর্ঘসময় ধরে পরীক্ষায় গবেষকরা রোগীদের কার্যক্ষমতায় কোনো উন্নতি দেখেননি, যা মাংসপেশির অবসাদ পরিমাপ করে।
গবেষকরা আরও দেখেন, অংশগ্রহণকারীরা বড় ধরনের কোনো পার্শ্বপ্রতিক্রিয়া যেমন, হঠাৎ করেই হৃৎস্পন্দন বেড়ে যাওয়া, রক্তচাপ কমে যাওয়া ইত্যাদি অনুভব করেননি, যে বিষয়গুলো হৃদরোগীদের ক্ষেত্রে গুরুত্বপূর্ণ।
সার্কুলেশন: হার্ট ফেইলিয়র জার্নালে এই গবেষণা প্রকাশিত হয়।
আরও খবর:
বয়স ধরে রাখার খাবার
