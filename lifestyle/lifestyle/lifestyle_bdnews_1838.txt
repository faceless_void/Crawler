আর কাদের এই আয়কর বিবরণী বা ট্যাক্স বিবরণী জমা দিতে হবে এই বিষয়ে বিস্তারিত জানাচ্ছেন পেশায় চার্টার্ড অ্যাকাউটেন্ট জসীম উদ্দিন রাসেল।
২০১৬-১৭ অর্থ বছরে আপনার করযোগ্য আয় কি ২ লাখ ৫০ হাজার টাকা অতিক্রম করেছে? বা আপনি কি সরকারি কোনো প্রতিষ্ঠানে চাকরি করেন এবং আপনার মূল বেতন ১৬ হাজার টাকা বা তার বেশি? বা কোনো ব্যবসায় কিংবা পেশায় নির্বাহী বা ব্যবস্থাপনা পদে চাকরি করছেন?
উপরের প্রশ্নের উত্তর যদি হ্যা হয়ে থাকে তাহলে আপনাকে বাধ্যতামূলকভাবে এ বছর জাতীয় রাজস্ব বোর্ডে আয়কর বিবরণী বা ট্যাক্স বিবরণী দিতে হবে।
এর বাইরেও যেসব ব্যক্তির বাধ্যতামূলকভাবে আয়কর বিবরণী দাখিল করতে হয় তা আয়কর অধ্যাদেশ ১৯৮৪-তে উল্লেখ রয়েছে।
আয়কর বিবরণী কি এবং কোথায় পাওয়া যায়
একজন করদাতার বার্ষিক আয়ের তথ্যাবলী জাতীয় রাজস্ব বোর্ড’য়ের  নির্ধারিত ফরমে উপস্থাপন করার মাধ্যম হল আয়কর বিবরণী। আয়কর বিবরণী দাখিল করার জন্য নির্ধারিত ছাপানো ফর্ম আছে। জাতীয় রাজস্ব বোর্ড প্রতি বছর করদাতাদের কাছ থেকে আয়কর সংগ্রহ করার জন্য এই ফর্ম প্রকাশ করে থাকে। বিনামূল্যে যে কোনো আয়কর অফিস থেকে এই ফর্ম সংগ্রহ করা যায় বা জাতীয় রাজস্ব বোর্ড’য়ের ওয়েবসাইট থেকেও ডাউনলোড করা যায়।
যাদের জন্য আয়কর বিবরণী দাখিল করা বাধ্যতামূলক
সাধারণভাবে বলতে গেলে, কোনো ব্যক্তির কোনো বছরে যদি করযোগ্য আয় ২ লাখ ৫০ হাজার টাকা অতিক্রম করে তাহলে তাকে আয়কর বিবরণী দাখিল করতে হবে। তবে করমুক্ত সীমা ২ লাখ ৫০ হাজার টাকা ক্ষেত্র বেধে ভিন্ন হতে পারে।
যেমন, মহিলা এবং ৬৫ বছর বা তদূর্ধ্ব বয়সের করদাতার ক্ষেত্রে এই সীমা ৩ লাখ টাকা, প্রতিবন্ধী করদাতার ক্ষেত্রে ৪ লাখ টাকা এবং গেজেটভুক্ত যুদ্ধাহত মুক্তিযোদ্ধা করদাতার ক্ষেত্রে ৪ লাখ ২৫ হাজার টাকা।
এখন ধরুন আপনি কোনো বছর আয়কর বিবরণী দাখিল করেছিলেন কিন্তু পরবর্তি কোনো বছর আপনার আয় সেই সীমা অতিক্রম করেনি। তাহলেও আয়কর বিবরণী দাখিল করে যেতে হবে।
এই অবস্থায়, আয় বছরের পূর্ববর্তী তিন বছরের যে কোনো বছর আপনার যদি করযোগ্য আয় হয়ে থাকে এবং তার জন্য কর দিয়ে থাকেন তাহলে আয়কর বিবরণী দাখিল করতে হবে।
তবে ব্যতিক্রমও রয়েছে। আয়ের পরিমান যা-ই হোক না কেনো, ব্যক্তি করদাতাকে সংশ্লিষ্ট আয় বছরের জন্য অবশ্যিই আয়কর বিবরণী দাখিল করতে যাদের-
১। কোনো কোম্পানির শেয়ার হোল্ডার পরিচালক বা শেয়ার হোল্ডার চাকরিজীবী।
২। কোন ফার্মের অংশীদার।
৩। সরকারি কোনো প্রতিষ্ঠানের কর্মচারী হয়ে আয় বছরের যে কোনো সময় ১৬ হাজার টাকা বা তদূর্ধ্ব পরিমাণ মূল বেতন আহরণ করে থাকলে।
৪। কোনো ব্যবসায় বা পেশায় নির্বাহী বা ব্যবস্থাপনা পদে, যে নামেই অভিহিত হোক না কেনো, বেতনভোগী কর্মী হয়ে থাকলে।
এর বাইরে কিছু ক্ষেত্রে শর্ত স্বরূপ আয়কর বিবরণী দেখাতে হয়। যেমন, জাতীয় নির্বাচন থেকে শুরু করে স্থানীয় পর্যায়ের নির্বাচনে অংশগ্রহণকারী, দরপত্রে অংশগ্রহণকারী, সমাজের কোনো প্রতিষ্ঠীত ক্লাবের সদস্য ইত্যাদি।
যেসব ক্ষেত্রে এমন শর্ত রয়েছে যে বাধ্যতামূলকভাবে জাতীয় রাজস্ব বোর্ডে আয়কর বিবরণী দাখল করতে হবে সেগুলো হল-
* মোটর গাড়ির মালিক (মোটর গাড়ি বলতে জিপ বা মাইক্রোবাসকেও বোঝাবে)।
* মূল্য সংযোজন কর আইনের অধীন নিবন্ধিত কোনো ক্লাবের সদস্য।
* কোনো সিটি কর্পোরেশন, পৌরসভা বা ইউনিয়ন পরিষদ হতে ট্রেড লাইসেন্স গ্রহণ করে কোনো ব্যবসা বা পেশা পরিচালনা করে থাকেন এমন ব্যক্তি।
* চিকিৎসক, দন্তচিকিৎসক, আইনজীবী, চার্টার্ড অ্যাকাউন্ট্যান্ট, কস্ট অ্যান্ড ম্যানেজমেন্ট অ্যাকাউন্ট্যান্ট, প্রকৌশলী, স্থপতি অথবা সার্ভেয়ার হিসেবে বা সমজাতীয় পেশাজীবী হিসেবে কোনো স্বীকৃত পেশাজীবী সংস্থার নিবন্ধনভূক্ত ব্যক্তি।
* জাতীয় রাজস্ব বোর্ডে নিবন্ধিত আয়কর পেশাজীবী।
* কোনো বণিক বা শিল্প বিষয়ক চেম্বার বা ব্যবসায়িক সংঘ বা সংস্থার সদস্য।
* কোনো পৌরসভা বা সিটি কর্পোরেশনের কোনো পদে বা সংসদ সদস্য পদে প্রার্থী হওয়া।
* কোনো সরকারি, আধা-সরকারি, স্বায়ত্বশাসিত সংস্থা বা কোনো স্থানীয় সরকারের কোনো দরপত্রে অংশগ্রহণকারী।
* কোনো কোম্পানির বা কোন গ্রুপ অফ কোম্পানিজের পরিচালনা পর্ষদে থাকা।
বিবরণী দাখিলের সময়সীমা
আমাদের আয়-বর্ষ জুলাই থেকে জুন পর্যন্ত। এরপর থেকেই অর্থাৎ জুলাই মাসের ১ তারিখ থেকেই আয়কর বিবরণী দাখিল করা শুরু হয়ে যায়।
টানা চলতে থাকে কর দিবস পর্যন্ত। বাংলাদেশে কর দিবস পালিত হয় ৩০ নভেম্বর। অর্থাৎ আগামী ৩০ নভেম্বর পর্যন্ত যে কোনো ব্যক্তি করদাতা তার আয়কর বিবরণী দাখিল করতে পারবেন।
বিবরণী দাখিল না করলে কী হয়
উপরে বর্ণিত কোনো ব্যক্তি যদি সময় মতো আয়কর বিবরণী দাখিল করতে ব্যর্থ হন তাহলে জরিমানাসহ অতিরিক্ত সরল সুদ এবং বিলম্ব সুদে দণ্ডিত হবেন। তবে কোনো করদাতা অতিরিক্ত সময়ের জন্য উপ কর-কমিশনারের নিকট সময় চেয়ে আবেদন করতে পারেন। যদি উপ কর-কমিশনার তার আবেদনে সন্তুষ্ট হয়ে অতিরিক্ত সময় মঞ্জুর করেন এবং সেই বর্ধিত সময়ের মধ্যে আয়কর বিবরণী দাখিল করেন সেক্ষেত্রে করদাতার উপর জরিমানা আরোপিত হবে না। তবে তার উপর অতিরিক্ত সরল সুদ এবং বিলম্ব সুদ আরোপিত হবে।
