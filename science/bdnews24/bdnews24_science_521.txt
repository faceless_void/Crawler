শুক্রবার এক টুইটে এই ধনকুবের বলেন, “আপনারা যদি এআই নিরাপত্তা নিয়ে উদ্বিগ্ন না হন, তবে আপনাদের তা হওয়া উচিৎ। এটি উত্তর কোরিয়ার চেয়েও অনেক বেশি ঝুঁকিপূর্ণ।” সম্প্রতি যুক্তরাষ্ট্র সরকারের সরাসরি নিয়ন্ত্রণে থাকা প্রশান্ত মহাসাগরীয় দ্বীপ গুয়াম-এ মিসাইল হামলা চালানোর হুমকি দেন উত্তর কোরিয়া প্রেসিডেন্ট কিম জং উন। এই হুমকি দেওয়ার পর নতুন করে সৃষ্ট উত্তেজনার মধ্যে এ কথা বললেন মাস্ক, খবর মার্কিন সাময়িকী ফরচুন-এর।
ওই টুইটের সঙ্গে একটি ছবিও জুড়ে দেওয়া হয়। এতে বলা ছিল- “সবশেষে যন্ত্রগুলোই জিতবে।”
জনগণের বিপদ সৃষ্টি করতে পারে এমন যেকোনো কিছু যেভাবে নিয়ন্ত্রণ করা হয়, ঠিক সেভাবেই কৃত্রিম বুদ্ধিমত্তা বা এআই নিয়ন্ত্রণ করা উচিৎ বলেও সতর্ক করেন মাস্ক।
এআই নিয়ে মাস্ক-এর এমন সতর্কবাণী এটিই প্রথম নয়। চলতি বছর জুলাই মাসেই এর ঝুঁকি নিয়ে ফেইসবুক প্রধান জাকারবার্গ-এর সঙ্গে বিতণ্ডায় জড়িয়ে পড়েন তিনি। মাস্ক, মাইক্রোসফট সহ-প্রতিষ্ঠাতা বিল গেটস আর ব্রিটিশ পদার্থবিদ স্টিফেন হকিং বরাবরই এআই-এর ঝুঁকি নিয়ে বলে আসছেন। এমনকি এটি মানব সভ্যতার অস্তিত্ব হুমকির মুখে ঠেলে দিতে পারে বলেও আশংকাও প্রকাশ করা হয়েছে। কিন্তু এক্ষেত্রে জাকারবার্গের অবস্থান ঠিক বিপরীত।
 
আরও খবর
যুক্তরাষ্ট্রের গুয়ামে ক্ষেপণাস্ত্র হামলার হুমকি উ. কোরিয়ার  
১৪ মিনিটেই গুয়ামে আঘাত হানতে পারে উ কোরিয়ার ক্ষেপণাস্ত্র  
আতঙ্কিত হন: উত্তর কোরিয়াকে ট্রাম্প  
উ. কোরিয়াকে মোকাবেলায় যুক্তরাষ্ট্রের সেনাবাহিনী প্রস্তুত: ট্রাম্প  
উত্তর কোরিয়াকে ‘বড় ধরনের সমস্যায়’ ফেলার হুমকি ট্রাম্পের  
এআই নিয়ে জাকারবার্গ-এর জ্ঞান সীমিত: মাস্ক  
‘মানব সভ্যতা রক্ষায় উচিৎ এআই নিয়ন্ত্রণ’  
চার বছরেই মস্তিষ্ক নিয়ন্ত্রিত কম্পিউটার: মাস্ক  
এআই-কে ‘ভয় পায় না’ জাপান  
এআই সবচেয়ে ভাল, বা সবচেয়ে খারাপ: হকিং  
"আমাদের মুছে ফেলতে পারে এলিয়েন"- হকিং  
