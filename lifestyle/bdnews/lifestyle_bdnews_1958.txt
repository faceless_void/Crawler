রেসিপি দিয়েছেন আফরোজা মেরী ।
উপকরণ
আটা বা ময়দা ২ কাপ। বেকিং পাউডার ২ চা-চামচ। লবণ ১/৪ চা-চামচ। মাখন আধা কাপ। তেল ২ টেবিল-চামচ। গুঁড়াচিনি ১ কাপ এবং এক কাপের তিন ভাগের ১ ভাগ। ডিম ৪টি। কমলার চামড়াকুচি ১ টেবিল-চামচ। কমলার রস আধা কাপ। ভ্যানিলা এসেন্স ১ টেবিল-চামচ। কমলার এসেন্স আধা চা-চামচ (যদি থাকে)। হলুদ খাবার রং ৩ ফোঁটা ।
পদ্ধতি
ওভেন প্রিহিট করুন ১৮০ ডিগ্রি সেলসিয়াস তাপমাত্রায়।
একটা পাত্রে ময়দা, লবণ এবং বেইকিং পাউডার একসঙ্গে মিশিয়ে নিন।
এখন একটা বড় পাত্রে মাখন নিন এবং বিটার দিয়ে বিট করুন যতক্ষণ না এটা একদম নরম না হয়ে যায়।
এক মিনিট পর পর একটা একটা করে ডিম যোগ করতে থাকুন। ভ্যানিলা এসেন্স দিন এবং এক মিনিটের মতো বিট করুন।
এরপর একে একে কমলার চামড়াকুচি, কমলা এসেন্স দিন। একটু বিট করে ময়দার মিশ্রণের অর্ধেকটা মিশিয়ে দিন। এখানে বিট না করে স্প্যাচুলা দিয়ে ভালো করে মিশিয়ে দেবেন এবং কমলার রসের এক তৃতীয়াংশ দিয়ে দিন।


কেকের মিশ্রণ ঢেলে ইলেকট্রিক ওভেনে দিন। ১৮০ ডিগ্রি সেলসিয়াস তাপমাত্রায় এটা হতে সময় লাগবে ৫০ থেকে ৬০ মিনিট।
৫০ মিনিট পর ওভেন থামিয়ে, একটা টুথপিক কেকের ভেতর ঢুকিয়ে বের করবেন। যদি কাঠিটা পরিষ্কার উঠে আসে, তাহলে বুঝবেন কেক হয়ে গেছে।
