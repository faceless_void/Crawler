হোটেল ওয়েস্টিনের ইতালিয়ান রেস্তোরাঁরা ‘প্রেগো’য়ের কুইজিন শেফ কারমেইন দে ফিলিপো দেখাচ্ছেন চিলিয়ান সি বাস ফিলেত বেকেড উইথ পটেটো এবং মিডটেরেনিয়ান সস তৈরির পদ্ধতি।
চারজনের এই ডিশটি তৈরি করতে প্রয়োজন হবে
সি বাস ফিলেত ৯৬০ গ্রাম। বেইকড পটেটো ৪০০ গ্রাম। মটরশুঁটি ২৬০ গ্রাম। চেরি টমেটো ২০০ গ্রাম। ব্ল্যাক অলিভ ৮০ গ্রাম। কেইপা ৪০ গ্রাম। আনচোভি মাছ (সামুদ্রিক মাছ) ৪৩ গ্রাম। রসুন ৪০ গ্রাম। পার্সলে ২০ গ্রাম। লেবু ২ টেবিল-চামচ। লবণ স্বাদ মতো। গোলমরিচ ২০ গ্রাম। অলিভ অয়েল ১২০ গ্রাম।
