এই সেবায় প্রতি মাসে গ্রাহকদের হয়তো গুণতে হবে পাঁচ মার্কিন ডলার আর এটি শুধু মার্কিন ই-কমার্স জায়ান্ট প্রতিষ্ঠানটির নিজস্ব ইকো হার্ডওয়্যার পণ্যগুলোতেই পাওয়া যাবে, বিভিন্ন সূত্রের বরাতে এক প্রতিবেদনে খবর জানিয়েছে প্রযুক্তি সাইট রিকোড।
ওই প্রতিবেদনে আরও জানানো হয়, অ্যামাজন চলতি বছর সেপ্টেম্বরে এই সার্ভিস চালু করতে যাচ্ছে। কিন্তু এখনও এ নিয়ে বড় সঙ্গীত লেবেল আর প্রকাশনা প্রতিষ্ঠানগুলোর সঙ্গে চুক্তি সম্পন্ন করা হয়নি।
চলতি বছর জুনে রয়টার্স এক প্রতিবেদনে জানিয়েছিল, অ্যামাজন একটি নতুন ‘স্ট্যান্ডঅ্যালোন’ মিউজিক স্ট্রিমিং সার্ভিস আনতে যাচ্ছে। বড় প্রতিদ্বন্দ্বীদের সঙ্গে পাল্লা দিতে এর মাসিক চার্জ ৯.৯৯ ডলার করা হবে তখন জানা যায়।
অ্যামাজন মাসিক চার বা পাঁচ ডলারের চেয়েও কম মূল্যে এই সেবা দেবে কিনা টা নিশ্চিত করা যায়নি।
এ নিয়ে তাৎক্ষণিকভাবে অ্যামাজনের কোনো মন্তব্য পাওয়া যায়নি।
২০১৪ সালের জুনে যুক্তরাষ্ট্রে নিজস্ব মিউজিক-স্ট্রিমিং সেবা চালু করে অ্যামাজন।
