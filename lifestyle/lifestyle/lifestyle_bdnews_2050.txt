চেপে ব্রণের ভেতরের আঁশ বের করে ফেলা একটা সমাধান হলেও এতে সেরে উঠতে সময় লাগে।
তাছাড়া অতিরিক্ত চাপাচাপির ফলে ব্রণ ছড়িয়ে যেতে পারে এবং দাগ পড়তে পারে। তাই রূপচর্চাবিষয়ক একটি ওয়েবসাইটে দ্রুত ব্রণ থেকে মুক্তি পাওয়ার জন্য করণীয় কিছু বিষয় উল্লেখ করা হয়। 
- হালকা এবং সাবান বিহীন কোনো ক্লিনজার দিয়ে প্রথম মুখ ভালোভাবে পরিষ্কার করতে হবে নিয়মিত। মুখে যতটা সম্ভব হাত কম লাগাবেন এবং চুলও যেন মুখে বেশি না লাগে সেদিকে খেয়াল রাখতে হবে।
- গ্রিন টি ব্রণ উপশমে বেশ উপকারী। গ্রিণ টি জ্বলুনি ও পোড়াভাব দূর করতে সাহায্য করে। তাই ভেজা গ্রিন টি একটি টিস্যুতে ভিজিয়ে ব্রণের উপর লাগালে উপকার পাওয়া যাবে।
- নখ দিয়ে কখনও ব্রণ খোটানো ঠিক না। ভালোভাবে পরিষ্কার করার পর তুলা দিয়ে দুপাশে চেপে ধরতে হবে। এতে ভিতরের আঁশ ও পুঁজ বেরিয়ে আসবে। তবে বেশি চাপাচাচি করা ঠিক হবে না।
- দ্রুত ব্রণ থেকে রেহাই পেতে সিলিসাইলিক অ্যাসিড সমৃদ্ধ কোনো স্পট ট্রিটমেন্ট ব্যবহার করা যেতে পারে। ট্রিটমেন্টের উপর উল্লেখিত পদ্ধতি অনুসারে ব্যবহার করতে হবে।
