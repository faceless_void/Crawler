নিউ ইয়র্ক মেডিক্যাল কলেজের চর্ম বিশেষজ্ঞ ডেন্ডি এঙ্গেলম্যান ওমেনসহেলথম্যাগ ডটকমের এক প্রতিবেদনে নখের শোভা বাড়াতে বেশ কিছু খাবারের নাম দিয়েছেন। এই খাবারগুলো নখ সুস্থ রাখে।
ডিম
এঙ্গেলম্যান জানান, ডিম হল প্রোটিনের সবচাইতে ভালো উৎস। নখ স্বাস্থ্যকর করে তুলতে ডিমের জুরি নেই।
নখের যে অংশটি আমরা দেখতে পাই তার পুরোটাই প্রোটিন দিয়ে তৈরি। তাছাড়া ডিমে প্রচুর পরিমাণে ভিটামিন বি থাকে যা তাড়াতাড়ি নখ বড় করতে সাহায্য করে।
ব্রোকোলি
শরীরে প্রোটিনকে কাজে লাগাতে সিসটেইন নামের এক প্রকার অ্যামাইনো অ্যাসিড প্রয়োজন। আর এই অ্যাসিডটির সব চাইতে ভেলো উৎস হল ব্রোকোলি।
স্যামন মাছ
প্রোটিন ও জিঙ্ক সমৃদ্ধ এই মাছটি শরীরে প্রোটিন কাজে লাগানোর জন্য দারুণ উপযোগী একটি খাবার। তাছাড়া পুষ্টিকর সেলেনিয়াম এবং কপারে সমৃদ্ধ এ মাছ নখ শক্ত করে ও তাড়তাড়ি বড় হতে সাহায্য করে।
বাংলাদেশে সুপার শপগুলোতে টিনজাত স্যামন মাছ পাওয়া যায়।
নারকেল তেল
এঙ্গেলম্যান বলেন, “ভোজ্য নারকেল তেল খাওয়ার অভ্যাস গড়ে তুললে তা শরীরে পুষ্টি উপাদান গ্রহণের ক্ষমতা বৃদ্ধি করতে পারে।”
তাছাড়া নারকেল তেলে আছে ভিটামিন এ, ডি, ই এবং কে। আর এ উপাদানগুলো স্বাস্থ্যকর নখ গঠনে দারুণ কার্যকর।
মুরগীর মাংস
ডিমের মতো মুরগীর মাংসেও আছে প্রচুর পরিমাণে ভিটামিন বি এবং প্রোটিন। তাছাড়া আরও আছে জিঙ্ক। এই তিনটি উপাদান নখের স্বাস্থ্যের জন্য অত্যন্ত উপকারী।
পালং শাক
