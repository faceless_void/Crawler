‘এম’ এখন থেকে ব্যবহারকারীদের অ্যানিমেটেড জিআইএফ, ‘ধন্যবাদ’ বলার মতো প্রচলিত শব্দের ‘সাজেশন’ দেখাবে, মার্কিন সাময়িকী ফরচুন এক প্রতিবেদনে এ খবর প্রকাশ করে। সেই সঙ্গে ‘হ্যা’, ‘না’ বা ‘আমিও তাই মনে করি’- এ ধরনের তাৎক্ষণিক জবাবও দিতে সক্ষম হবে অ্যাপটি। 
চলচ্চিত্র টিকেট কেনার সেবাদাতা প্রতিষ্ঠান ফ্যানড্যানগো’র সঙ্গে চুক্তি করেছে মেসেঞ্জার। কোনো ব্যবহারকারীর বার্তালাপের মধ্যে সম্প্রতি বের হওয়া চলচ্চিত্রের নাম উল্লেখ করা হলে এই ফিচার ব্যবহারকারীদের আশপাশের হলগুলোতে চলচ্চিত্রের সময়সূচী জানাবে- বলা হয়েছে আইএএনএস-এর প্রতিবেদনে। 
এই ফিচার মেসেঞ্জার অ্যাপ থেকে ব্যবহারকারীকে পছন্দের চলচ্চিত্রের টিকেট বুকিং দেওয়ার সুযোগও দেবে।
ব্যবহারকারী চাইলে অ্যাপের সেটিংসে গিয়ে এই ভার্চুয়াল অ্যাসিস্ট্যান্টকে মিউট করে দিয়ে কোনো ‘সাজেশন’ পাওয়া বন্ধও করে দিতে পারেন।
