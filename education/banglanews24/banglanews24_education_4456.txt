দ্বিতীয়বারের মতো ঢাকা বিশ্ববিদ্যালয়ের (ঢাবি) কোষাধ্যক্ষ পদে নিয়োগ পেয়েছেন একই বিশ্ববিদ্যালয়ের ইন্টারন্যাশনাল বিজনেস বিভাগের অধ্যাপক ড. মো. কামাল উদ্দীন।
ঢাকা: দ্বিতীয়বারের মতো ঢাকা বিশ্ববিদ্যালয়ের (ঢাবি) কোষাধ্যক্ষ পদে নিয়োগ পেয়েছেন একই বিশ্ববিদ্যালয়ের ইন্টারন্যাশনাল বিজনেস বিভাগের অধ্যাপক ড. মো. কামাল উদ্দীন।
অধ্যাপক কামালকে কোষাধ্যক্ষ পদে নিয়োগ দিয়ে বৃহস্পতিবার (৩০ জুন) প্রজ্ঞাপন জারি করেছে শিক্ষা মন্ত্রণালয়।
আদেশে বলা হয়েছে, ঢাকা বিশ্ববিদ্যালয় অধ্যাদেশ অনুযায়ী রাষ্ট্রপতি ও চ্যান্সেলর কাম‍াল উদ্দীনকে চার বছরের জন্য এ পদে নিয়োগ দিয়েছেন।
কোষাধ্যক্ষ পদে তিনি তার বর্তমান পদের সমপরিমাণ বেতন-ভাতা ও অন্যান্য সুযোগ-সুবিধা ভোগ করতে পারবেন বলেও আদেশে বলা হয়।
বাংলাদেশ সময়: ২০৩০ ঘণ্টা, জুন ৩০, ২০১৬ এমআইএইচ/টিআই  
