গবেষকরা আরও মন্তব্য করেন, “সমবয়সি অন্যান্য শিশুরা মোটা শিশুদের পছন্দ করে না, পরিণত হয় সহপাঠীদের উপহাসের পাত্রে। ফলে নিজের স্থূলতা নিয়ে আরও বেশি উত্তেজিত হয়ে পড়তে পারে।
গবেষণার প্রধান গবেষক, যুক্তরাষ্ট্রের ওকলাহোমা স্টেট ইউনিভার্সিটির অ্যামান্ডা ডাব্লিউ. হ্যারিস্ট বলেন, “অতিরিক্ত স্থূলতা শিশুদের মানসিক স্বাস্থ্যের জন্য মারাত্বক ঝুঁকিপূর্ণ, এমনকি তাদের বয়স ছয় পেরোলেও।”
ওজনদার শিশুরা অন্যদের কাছ থেকে প্রত্যাখ্যাত হওয়ার কষ্ট এড়াতে খাবারের প্রতি ঝুঁকে পড়তে পারে। কিংবা সমবয়সিদের উপহাস থেকে বাঁচতে খেলাধুলা বন্ধ করে দিতে পারে। ফলে তাদের শারীরিক পরিশ্রম কমে যায়। উভয় ক্ষেত্রেই ফলাফল ওজন বেড়ে যাওয়া।
‘চাইল্ড ডেভেলপমেন্ট’ জার্নালে প্রকাশিত এক প্রতিবেদনে হ্যারিস্ট বলেন, “আমাদের গবেষণায় দেখা গেছে, একঘরে হয়ে থাকা শিশুরা একাকিত্ব, হতাশা এবং রাগের সঙ্গে মানসিক যন্ত্রণায় ভোগে। পাশাপাশি, এই শিশুদের স্কুল ফাঁকি দেওয়ার এবং শিক্ষাজীবন থেকে ঝরে পড়ার সম্ভাবনা বাড়ে।”
স্থূল শিশুদের সামাজিক এবং মানসিক অবস্থা জানার উদ্দেশ্যে এই গবেষণায় ওকলাহোমার গ্রামাঞ্চলের ২৯টি স্কুলের ১ হাজার ১৬৪ জন প্রথম শ্রেণির শিক্ষার্থীদের পর্যালোচনা করা হয়।
ফলাফলে দেখা যায়, স্থূল শিশুরা প্রতিনিয়ত তাদের সমবয়সিদের কাছ থেকে প্রত্যাখ্যাত হয়।
ওজন যত বেশি উপহাসের মাত্রাও ততটাই বেশি।
মানসিক স্বাস্থ্যের দিক থেকে বিবেচনা করলে দেখা যায়, অতিরিক্ত মোটা শিশুদের মধ্যে হতাশার পরিমাণ স্বাস্থ্যবান শিশুদের তুলনায় অনেক বেশি।
