রাবি: টানা কয়েক ঘণ্টার বৃষ্টিতে রাজশাহী বিশ্ববিদ্যালয় (রাবি) ক্যাম্পাসে জলাবদ্ধতার সৃষ্টি হয়েছে। এতে ভোগান্তিতে পড়েছেন বিশ্ববিদ্যালয়ের শিক্ষক-শিক্ষার্থীরা।
সরেজমিনে দেখা যায়, বিশ্ববিদ্যালয়ের শহীদুল্লাহ ও মমতাজ উদ্দীন কলা ভবনের সামনে, কেন্দ্রীয় গন্থাগারের পেছনে, টুকিটাকি চত্বর, পরিবহন এলাকা ও চারটি বিজ্ঞান ভবনের সামনে জলাবদ্ধতার সৃষ্টি হয়েছে।
এছাড়া বিনোদপুর গেট, হবিবুর রহমান হলের সামনের সড়ক ও কেন্দ্রীয় গ্রন্থগারের সামনের সড়ক বৃষ্টির পানিতে তলিয়ে গেছে।
শিক্ষার্থীরা অভিযোগ করেছেন, ক্যাম্পাসে দুর্বল ড্রেনেজ ব্যবস্থার কারণে বৃষ্টি হলেই জলাবদ্ধতার সৃষ্টি হয়। এতে চলাচলে বিড়ম্বনায় পড়েন শিক্ষক-শিক্ষার্থীরা।
ক্ষোভ প্রকাশ করে বিশ্ববিদ্যালয়ের আইন বিভাগের শিক্ষার্থী মঈন উদ্দীন বলেন, ‘বিশ্ববিদ্যালয়ের ড্রেনেজ ব্যবস্থা খুবই খারাপ। সামন্য বৃষ্টি হলেই জলবদ্ধতার সৃষ্টি হয়। এতে করে আমাদের ক্লাসে যেতে সমস্যা হয়, ক্যাম্পাসে চলাফেরায় সমস্যা হয়। কর্তৃপক্ষের উচিত দ্রুত বিশ্ববিদ্যালয়ের ড্রেনেজ ব্যবস্থা ঠিক করা।’
শামীম রেজা নামে আরেক শিক্ষার্থী বলেন, ‘ক্যাম্পাসে এমন জলবদ্ধতা সৃষ্টি হলে আমাদের শরীরে পানি বাহিত রোগ দেখা দিবে। তাই প্রশাসনের উচিত এ সমস্যার দ্রুত সমাধান করা।’
এ বিষয়ে যোগাযোগ করা হলে বিশ্ববিদ্যলয়ের ছাত্র উপদেষ্টা প্রফেসর মিজানুর রহমান বাংলানিউজকে বলেন, ‘জলাবদ্ধতার এ সমস্যা সমাধানে বিশ্ববিদ্যালয়ের একটা মাস্টার প্ল্যান রয়েছে। তা কার্যকর হলে এ সমস্যাটি আর থাকবে না।’
