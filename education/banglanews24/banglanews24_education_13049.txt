পারিবারিক সহিংসতা আইন-২০১০ সম্পর্কে নারীরা জানলে পারিবারিক নির্যাতনের শিকার হওয়া নারীকে সহায়তা করা সম্ভব হবে বলে মনে করছেন বিশেষজ্ঞরা।
ঢাকা বিশ্ববিদ্যালয়: পারিবারিক সহিংসতা আইন-২০১০ সম্পর্কে নারীরা জানলে পারিবারিক নির্যাতনের শিকার হওয়া নারীকে সহায়তা করা সম্ভব হবে বলে মনে করছেন বিশেষজ্ঞরা।
বুধবার (৭ ডিসেম্বর) সন্ধ্যায় ঢাকা বিশ্ববিদ্যালয়ের ছাত্র-শিক্ষক কেন্দ্র মিলনায়তনে প্রামাণ্যচিত্র আয়নাঘর প্রদর্শনীতে এই অভিমত প্রকাশ করেন তারা।
কেয়ার বাংলাদেশের উদ্যোগে অমিতাভ রেজা পরিচালিত এই প্রামাণ্যচিত্র প্রথমবারের মতো ঢাকা বিশ্ববিদ্যালয়ে প্রদর্শিত হয়। পরবর্তীতে বিভিন্ন প্রাইভেট বিশ্ববিদ্যালয়, সারা দেশের বিভিন্ন শিক্ষা প্রতিষ্ঠানে নারী নির্যাতনরোধে সচেতনতা সৃষ্টিতে এই প্রামাণ্যচিত্র প্রদর্শিত হবে বলে আয়োজকদের পক্ষ থেকে জানানো হয়। প্রামাণ্যচিত্রে নারীর প্রতি সহিংসতার কারণে দেশের জিডিপি কমে যাওয়া, পারিবারিক সহিংসতা আইনের বিভিন্ন ধারা, নারীদের ওপর বিভিন্ন ধরনের নির্যাতনের বিষয় তুলে ধরা হয়।
প্রদর্শনীর পর অনুষ্ঠিত হয় আলোচনা ও প্রশ্নোত্তর পর্ব। এতে বক্তব্য রাখেন- আইনজীবী তাপস্বী রাবেয়া, কেয়ার বাংলাদেশের নারীর ক্ষমতায়নের পরিচালক হুমায়রা আজিজ।
তাপস্বী রাবেয়া তার বক্তব্যে বলেন, সরকারের পক্ষ থেকে নারী নির্যাতনের বিরুদ্ধে আইনি সহায়তা দেওয়ার বিষয়টি অনেক নারীরাই জানে না। বাংলাদেশ সরকার ইউনিয়ন থেকে শুরু করে জেলা পর্যায়ে নারীদেরকে বিনামূল্যে আইনি সহায়তা প্রদান করছে।
এছাড়াও ১০৯২১ নম্বরে ফোন করে আইনি সহায়তা পাওয়া যাবে বলে জানান তিনি।
