বরিশাল: বরিশাল বিশ্ববিদ্যালয়ের সমাজবিজ্ঞান বিভাগের চেয়ারম্যান সহকারী অধ্যাপক সাদিকুর রহমানকে বাধ্যতামূলক ছুটিতে পাঠানো হয়েছে।
তার বিরুদ্ধে ক্লাসে শিক্ষক ও ছাত্রীদের যৌন হয়রানি করার অভিযোগ উঠেছে। 
এ বিষয়ে বিশ্ববিদ্যালয়ের একজন শিক্ষক বিশ্ববিদ্যালয় কর্তৃপক্ষ বরাবর অভিযোগ দিয়েছেন ও থানায় সাধারণ ডায়েরি করেছেন।
পাশাপাশি ওই শিক্ষকের বিরুদ্ধে শিক্ষার্থীদেরও অভিযোগ রয়েছে এমনটাই জানিয়েছে বিশ্ববিদ্যালয় প্রশাসন।
বাধ্যতামূলক ছুটিতে যাওয়ার বিষয়টি নিশ্চিত করে সমাজবিজ্ঞান বিভাগের সহকারী অধ্যাপক সাদিকুর রহমান জানান, গত ১৫ মে ওই শিক্ষককে একটি চিঠি দেওয়া হয়। যেখানে তাকে বাধ্যতামূলক ছুটিতে যেতে বলা হয়েছে। তবে ওই চিঠিতে কি কারণে বা কেন ছুটিতে যেতে বলা হয়েছে তা উল্লেখ করা হয়নি। 
তিনি বলেন, হঠাৎ করে এ ধরনের ঘটনায় তিনি মর্মাহত। তবে এখানে কোনো রাজনৈতিক বিষয় কিংবা কারো কোনো লাভের বিষয় থাকতে পারে। যা কর্তৃপক্ষ খতিয়ে দেখবে বলে আশা করি।
এদিকে শিক্ষক ও ছাত্রীদের যৌন হয়রানির বিষয়ে বিশ্ববিদ্যালয় কর্তৃপক্ষ বরাবর লিখিত অভিযোগ এবং নিজের নিরাপত্তা চেয়ে থানায় লিখিত অভিযোগ দেওয়ার বিষয়টি স্বীকার করলেও অভিযোগকারী সমাজবিজ্ঞান বিভাগের শিক্ষক এ বিষয়ে বিস্তারিত কিছুই বলতে রাজি নন। তিনি জানান, বিষয়টি যেহেতু তদন্তাধীন তাই এ বিষয়ে অভিযোগকারী হিসেবে কিছুই বলা ঠিক হবে না। তবে আশা করি অল্পদিনের মধ্যেই তদন্ত প্রতিবেদন সবার সামনে তুলে ধরা হবে।
অপরদিকে অভিযুক্ত শিক্ষকের বিরুদ্ধে পরবর্তী আইনগত কিংবা বিভাগীয় ব্যবস্থা নেওয়া হবে কিনা সে বিষয়ে কোনো তথ্য জানাতে পারেননি শিক্ষক সমিতির নেতারা। তাদের দাবি বিষয়টি ভিসি স্যার দেখছেন। তিনিই এ বিষয়ে সিদ্ধান্ত নেবেন।
