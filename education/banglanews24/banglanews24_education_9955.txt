বেসরকারি শিক্ষক নিবন্ধন ও প্রত্যয়ন কর্তৃপক্ষের (এনটিআরসিএ) আওতায় ত্রয়োদশ শিক্ষক নিবন্ধন পরীক্ষার কলেজ পর্যায়ের প্রিলিমিনারি টেস্টের তারিখ পরিবর্তন করা হয়েছে।
ঢাকা: বেসরকারি শিক্ষক নিবন্ধন ও প্রত্যয়ন কর্তৃপক্ষের (এনটিআরসিএ) আওতায় ত্রয়োদশ শিক্ষক নিবন্ধন পরীক্ষার কলেজ পর্যায়ের প্রিলিমিনারি টেস্টের তারিখ পরিবর্তন করা হয়েছে।
পূর্ব নির্ধারিত ৭ মে’র পরিবর্তে আগামী ১৩ মে শুক্রবার সকাল ১০ টা হতে ১১টা পর্যন্ত এই পরীক্ষা অনুষ্ঠিত হবে মঙ্গলবার (২৬ এপ্রিল) সংবাদ বিজ্ঞপ্তিতে জানিয়েছে শিক্ষা মন্ত্রণালয়। 
তবে, শিক্ষক নিবন্ধন পরীক্ষার স্কুল পর্যায়ের প্রিলিমিনারি টেস্ট পূর্ব নির্ধারিত সময়সূচি অনুযায়ী ৬ মে শুক্রবার সকাল ১০টা থেকে ১১ টা পর্যন্ত অনুষ্ঠিত হবে। প্রিলিমিনারিতে উত্তীর্ণদের লিখিত পরীক্ষা আগামী ১২ ও ১৩ আগস্ট অনুষ্ঠিত হবে বলে এর আগে জানিয়েছে শিক্ষা মন্ত্রণালয়।
এবার ২০টি জেলায় শিক্ষক নিবন্ধনের প্রিলিমিনারি এবং আটটি বিভাগীয় শহরে লিখিত পরীক্ষা নেওয়া হবে বলে জানায় এনটিআরসিএ। লিখিত পরীক্ষার পর উত্তীর্ণদের মৌখিক পরীক্ষা নিয়ে চূড়ান্ত ফল ঘোষণা করা হবে। 
পরবর্তীতে শূন্য পদের চাহিদা নিয়ে উপজেলা, জেলা, বিভাগ ও জতীয়- এসব ক্যাটাগরিতে মেধা তালিকা তৈরি সে অনুযায়ী নিয়োগ দেবে এনটিআরসিএ।  বেসরকারি স্কুল-কলেজে শিক্ষক নিয়েগের জন্য ২০০৫ সাল থেকে এনটিআরসিএ এই পরীক্ষা নেওয়া শুরু করে। 
