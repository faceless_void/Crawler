পৌষ সংক্রান্তি পুরান ঢাকায় সাকরাইন নামে পরিচিত। এর আরেক নাম মকরক্রান্তি। এই উৎসব দক্ষিণ এশিয়ার অনেক দেশেই পালিত হয়।
ভারতে পৌষ সংক্রান্তি নামেই পরিচিত। নেপালে এটা পরিচিত মাঘী নামে, থাইল্যান্ডে সংক্রান, লাওসে পিমালাও, মিয়ানমারে থিং ইয়ান এবং কম্বোডিয়ায় মহাসংক্রান নামে পরিচিত।


পুরান ঢাকায় সাকরাইন পৌষের শেষ দিন পালিত হয়। তাই বাংলা একাডেমির পঞ্জিকা মতে ১৪ জানুয়ারি এ উৎসব পালন করার কথা।


মেলা, নতুন ধানের পিঠে, চিরা, গুড়- গ্রামে মিলতে পারে, তবে পুরান ঢাকায় নতুন ধানের পিঠে না মিললেও ছাদে ছাদে চলবে বারবিকিউ।










উৎসবের আমেজ থাকবে পুরান ঢাকার সর্বত্র। গেণ্ডারিয়া, তাঁতীবাজার, লক্ষ্মীবাজার, চকবাজার, লালবাগ, সূত্রাপুর মাতবে ঐতিহ্যের এই উৎসবে। আকাশে উড়বে ঘুড়ি আর বাতাসে দোলা জাগাবে গান।






ছবি: লেখক।
আরও পড়ুন
মহেড়া জমিদারবাড়ি  
আহসান মঞ্জিল  
ঘুরে আসুন হোসেনী দালান  
পুরান ঢাকার খাবার হোটেল  
