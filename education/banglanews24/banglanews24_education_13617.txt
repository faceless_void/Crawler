দীর্ঘ প্রায় ৪০ দিনের ছুটি শেষে খুলতে যাচ্ছে শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয় (শাবিপ্রবি)। রোববার (১৭ জুলাই) থেকে শুরু হচ্ছে বিশ্ববিদ্যালয়ের একাডেমিক ও প্রশাসনিক কার্যক্রম।
শাবিপ্রবি (সিলেট): দীর্ঘ প্রায় ৪০ দিনের ছুটি শেষে খুলতে যাচ্ছে শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয় (শাবিপ্রবি)। রোববার (১৭ জুলাই) থেকে শুরু হচ্ছে বিশ্ববিদ্যালয়ের একাডেমিক ও প্রশাসনিক কার্যক্রম।
শুক্রবার (১৫ জুলাই) বিশ্ববিদ্যালয়ের জনসংযোগ অধিদফতর বাংলানিউজকে বিষয়টি নিশ্চিত করেছে।
ফলে, শিক্ষার্থীরাও ইতোমধ্যে আবাসিক হল এবং মেসে ফিরতে শুরু করায় ক্যাম্পাস ফের শিক্ষার্থীদের পদচারণায় মুখর হয়ে উঠেছে।
পবিত্র রমজান ও ঈদ-উল-ফিতরের ছুটি উপলক্ষে গত ৯ জুন (বৃহস্পতিবার) থেকে বিশ্ববিদ্যালয় বন্ধ ঘোষণা করা হয়।
