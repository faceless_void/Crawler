রেসিপি দিয়েছেন বীথি জগলুল।
উপকরণ: বুটের / ছোলার ডাল আধা কাপ। মুগডাল আধা কাপ। মসুরডাল ১/৪ কাপ। মাষকলাইয়ের ডাল ১/৪ কাপ। আদাবাটা ১ টেবিল-চামচ। রসুনবাটা ১ চা-চামচ। কাঁচামরিচ ৩,৪টি। টমেটো বড় ১টি। হলুদগুঁড়া ১ চা-চামচ।
ফোঁড়নের জন্য: থেঁতো করা রসুন আস্ত ১টি। আস্ত জিরা ১ চা-চামচ অথবা পাঁচফোঁড়ন ১ চা-চামচ। আস্ত মুগডাল ১ চা-চামচ। তেজপাতা ২,৩টি। শুকনা-মরিচ ৪,৫টি। পেঁয়াজকুচি ২টি। তেল অথবা ঘি পরিমাণ মতো।
এছাড়াও লাগবে: গরমমসলা-গুঁড়া ১ চা-চামচ। ফালি করা কাঁচামরিচ ৫,৬টি। লেবুর রস ১ টেবিল-চামচ। লবণ স্বাদ মতো। ধনেপাতা-কুচি ইচ্ছা।
পদ্ধতি: সব ডাল একসঙ্গে ধুয়ে ৩০ থেকে ৪০ মিনিট ভিজিয়ে রাখুন। প্রেশার কুকারে ১ চামচ তেল ও ১ চামচ ঘি গরম করে আদা ও রসুনের পেস্ট দিয়ে নেড়েচেড়ে টমেটো-কুচি দিন।
কিছুক্ষণ টমেটো কষিয়ে সবরকমের ডাল আর দুতিন কাপ গরম পানি দিয়ে ঢাকনা দিয়ে ঢেকে দিন। ৩ থেকে ৪টি সিটি বাজলে নামিয়ে নিন।
ঢাকনা খুলে গরম মসলা ছাড়া বাকি সব গুঁড়ামসলা, লবণ দিয়ে মৃদু আঁচে ১০ থেকে ১৫ মিনিট রান্না করুন। মাঝে মাঝে নেড়ে দেবেন যেন তলায় লেগে না যায়।
যদি মনে করেন ডাল বেশি ঘন হয়ে গিয়েছে তাহলে আন্দাজ মতো গরম পানি মিশিয়ে দিবেন। ডাল সিদ্ধ হলে লেবুর রস মিশিয়ে ঢাকনা লাগিয়ে নামিয়ে রাখুন।
অন্য একটি প্যানে তেল অথবা ঘি গরম করে আস্ত মুগডাল, জিরা অথবা পাঁচফোঁড়ন, শুকনামরিচ ও তেজপাতা ফোঁড়ন দিয়ে, পেঁয়াজ-রসুন লাল করে ভেজে প্রেশার কুকারের ঢাকনা খুলে ডালের সঙ্গে মিশিয়ে নিন। ধনেপাতা, ফালি করা কাঁচামরিচ ও গরমসলা গুঁড়া মিশিয়ে ঢাকনা দিয়ে রাখুন।
