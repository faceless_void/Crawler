রাজশাহী বিশ্ববিদ্যালয়ের ইনস্টিটিউট অব বাংলাদেশ স্টাডিজ (আইবিএস) এলামনাই অ্যাসোসিয়েশনের উদ্যোগে ‘জাতীয় বাজেট ২০১৬-২০১৭ : পর্যালোচনা’ শীর্ষক সেমিনার অনুষ্ঠিত হয়েছে।
রাবি: রাজশাহী বিশ্ববিদ্যালয়ের ইনস্টিটিউট অব বাংলাদেশ স্টাডিজ (আইবিএস) এলামনাই অ্যাসোসিয়েশনের উদ্যোগে ‘জাতীয় বাজেট ২০১৬-২০১৭ : পর্যালোচনা’ শীর্ষক সেমিনার অনুষ্ঠিত হয়েছে।
বৃহস্পতিবার (১৬ জুন) দুপুরে ইনস্টিটিউটের সেমিনার কক্ষে অনুষ্ঠিত এ সেমিনারে প্রধান অতিথি ছিলেন রাবি উপাচার্য প্রফেসর মুহম্মদ মিজানউদ্দিন।
আইবিএস এলামনাই অ্যাসোসিয়েশনের সভাপতি প্রফেসর ড. এম. জয়নুল আবেদীনের সভাপতিত্বে অনুষ্ঠানে বিশেষ অতিথি ছিলেন আইবিএস’র পরিচালক প্রফেসর ড. স্বরোচিষ সরকার।
সঞ্চালনা করেন আইবিএস এলামনাই অ্যাসোসিয়েশনের সাধারণ সম্পাদক ড. মোহাম্মাদ নাজিমুল হক।
সেমিনারে মূল প্রবন্ধ উপস্থাপন করেন- আইবিএস’র পিএইচডি গবেষক তহিদুর রহমান। প্রবন্ধে জাতীয় বাজেটের সঙ্গে সম্পৃক্ত বিভিন্ন ধারণা ব্যাখ্যা করা হয় এবং একই সঙ্গে প্রস্তাবিত বাজেটে জাতীয় আয় ও ব্যয় বণ্টনের বিভিন্ন দিক পর্যালোচনা করা হয়। দেশের উন্নয়নের গতিধারা এবং আর্থ-সামাজিক অবস্থার ওপর প্রস্তাবিত বাজেটের সম্ভাব্য প্রভাবও উঠে আসে প্রবন্ধ থেকে।
প্রবন্ধের ওপর আলোচনায় অংশ নেন- বিশ্ববিদ্যালয়ের ফিনান্স বিভাগের শিক্ষক প্রফেসর ড. এএইচএম জিয়াউল হক, অর্থনীতি বিভাগের শিক্ষক প্রফেসর ড.  ইলিয়াছ হোসেন, ড. এএসএম রেজাউল হাসান করিম বকসী ও ড. ফরিদ উদ্দিন খান প্রমুখ।
সেমিনারে যৌথ একটি প্রবন্ধ উপস্থাপন করেন আবিএসের গবেষক তহিদুর রহমান, রিপন কুমার দে, রোস্তম আলী ও তাওহীদুল ইসলাম।
বাংলাদেশ সময়: ১৭৫৯ ঘণ্টা, জুন ১৬, ২০১৬
