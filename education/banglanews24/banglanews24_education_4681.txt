রাবি: রাজশাহী বিশ্ববিদ্যায়ের (রাবি) জনসংযোগ দপ্তরের নতুন প্রশাসক হিসেবে পরিসংখ্যান বিভাগের অধ্যাপক ড. প্রভাষ কুমার কর্মকার দায়িত্ব গ্রহণ করেছেন।
বৃহস্পতিবার (১৭ আগস্ট) দুপুরে দপ্তরে যোগদান করেন তিনি।
অধ্যাপক ড. প্রভাষ কুমার কর্মকার জনসংযোগ দপ্তরে পৌঁছালে সদ্য বিদায়ী প্রশাসক অধ্যাপক মো. মশিহুর রহমান তাকে স্বাগত জানান। পরে বিদায়ী প্রশাসক ড. প্রভাষ কুমারের কাছে দায়িত্ব হস্তান্তর করেন। এসময় সেখানে বিভিন্ন বিভাগের শিক্ষক, জনসংযোগ দপ্তরের কর্মকর্তা-কর্মচারীরা উপস্থিত ছিলেন।
নতুন জনসংযোগ প্রশাসক অধ্যাপক ড. প্রভাষ কুমার কর্মকার বলেন, আজ আমি দায়িত্ব গ্রহণ করেছি। সবার সহযোগিতা পেলে জনসংযোগ দপ্তরের কাজ এগিয়ে নিতে পারবো বলে আশা প্রকাশ করছি।
রোববার (১৩ আগস্ট) জনসংযোগ দফতরের প্রশাসক পদ থেকে অব্যাহতি চেয়ে পদত্যাগপত্র জমা দেন গণযোগাযোগ ও সাংবাদিকতা বিভাগের সহযোগী অধ্যাপক মশিহুর রহমান।
