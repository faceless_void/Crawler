জগন্নাথ বিশ্ববিদ্যালয় (জবি) ছায়া জাতিসংঘ অ্যাসোসিয়েশনের (জবিমুনা) উদ্যোগে প্রথম ‘মুন ওয়ার্কশপ’ অনুষ্ঠিত হয়েছে। একই সঙ্গে জবিমুনা’র আনুষ্ঠানিক যাত্রাও শুরু হয়েছে।
জগন্নাথ বিশ্ববিদ্যালয়: জগন্নাথ বিশ্ববিদ্যালয় (জবি) ছায়া জাতিসংঘ অ্যাসোসিয়েশনের (জবিমুনা) উদ্যোগে প্রথম ‘মুন ওয়ার্কশপ’ অনুষ্ঠিত হয়েছে। একই সঙ্গে জবিমুনা’র আনুষ্ঠানিক যাত্রাও শুরু হয়েছে।
 
শনিবার (২৬ নভেম্বর) সকাল ৯টায় জবির উপাচার্য ড. মিজানুর রহমান এ ওয়ার্কশপ ও সংগঠনের উদ্বোধন ঘোষণা করেন।
উপাচার্য বলেন, ‘এতোদিন এসব ওয়ার্কশপে বিবিএ’র শিক্ষার্থীদের বেশি প্রাধান্য ছিল। কিন্তু এখানে শুধু বিবিএ’র শিক্ষার্থীই নয়, সকল বিভাগের শিক্ষার্থীর অংশগ্রহণ দেখা যাচ্ছে। যা একটি ভালো লক্ষণ। এ ওয়ার্কশপ থেকে শিক্ষার্থীরা ডিবেট নিয়ে অনেক কিছু জানতে পারবে’।
উপাচার্য আরও বলেন, সাম্প্রতিক সময়ে আমাদের পার্শ্ববর্তী দেশগুলোতে বিভিন্ন ইস্যু নিয়ে যেসব সমস্যা দেখা দিয়েছে, সেগুলো নিয়ে আলোচনায় বসা উচিত। আলোচনার মাধ্যমে এসব বিষয়ের সুষ্ঠু সমাধান আসবে বলেও মনে করেন তিনি।
জবিমুনা’র মডারেটর বাংলা বিভাগের প্রভাষক শরিফুল ইসলামের সঞ্চালনায় ওয়ার্কশপে বক্তব্য দেন জাতিসংঘ যুব ও ছাত্র সংঘের সভাপতি মো. মামুন মিয়া ।
তিনি বলেন, বিশ্বের স্বনামধন্য বিশ্ববিদ্যালয়ে এ মুন চর্চা হয়ে থাকে। তারই ধারাবাহিকতায় ২০০৩ সালে প্রথম ছায়া জাতিসংঘ চর্চা শুরু হয়, যা দিন দিন ক্রমেই বিকাশ লাভ করছে। তারই ফল, জবি ছায়া জাতিসংঘ অ্যাসোসিয়েশন।
দ্বিতীয় পর্বে বিকেল সাড়ে ৩টা পর্যন্ত ওয়ার্কশপের বিভিন্ন দিক নিয়ে আলোচনা ও দিক-নির্দেশনা দেওয়া হয়।
