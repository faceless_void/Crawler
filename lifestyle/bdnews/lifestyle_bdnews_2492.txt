মুখের পরিচ্ছন্নতা নিয়ে সচেতন না হলে মুখে ব্যাকটেরিয়ার সংক্রমণ বৃদ্ধি পায়, যা দাঁত ও মাড়ির ক্ষতি করে।
স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটের একটি প্রতিবেদনে মুখের যত্ন নেওয়ার ক্ষেত্রে পাঁচটি বিষয় উল্লেখ করা হয়। এ প্রতিবেদনে সেই ধাপগুলোই উল্লেখ করা হলো।
দাঁত ব্রাশ
মুখের যত্নে নিয়মিত দাঁত ব্রাশ করার আর কোন বিকল্প নেই। ফ্লুরাইডসমৃদ্ধ টুথপেস্ট দিয়ে নিয়মিত দাঁত ব্রাশ করলে দাঁত ও মুখ সুস্থ থাকে এবং জীবাণুর সংক্রমণ রোধ করা যায়। এতে করে মুখের দুর্গন্ধ কম হয় এবং দাঁতের হলদেটেভাবও দূর হয়। তাছাড়া দাঁত ক্ষয় বা ক্যাভিটি হওয়ার সম্ভাবনা কমে যায়।
পানি পান
মুখের দুর্গন্ধ ও দাঁতের ক্ষয় রোধে প্রতিদিন পর্যাপ্ত পরিমাণ পানি পানের অভ্যাস তৈরি করতে হবে। বেশি করে পানি পান করার কারণে মুখে জীবানুর সংক্রমণ কম হয়। তাছাড়া পানি পান বা কুলিকুচি করার ফলে দাঁতের ফাঁকে লেগে থাকা খাদ্যকণা পরিষ্কার হয় এবং ক্ষতিকর ব্যকটেরিয়া দূর করে।
জিহ্বা পরিষ্কার করা
জিহ্বা মুখের স্বাস্থ্য ও দাঁতের জন্য ক্ষতিকর ব্যাক্টেরিয়ার আবাসস্থল। তাই নিয়মিত জিহ্বা পরিষ্কার না করলে  জীবাণুর সংক্রমণ বাড়তে থাকবে। তাই নিয়মিত জিহ্বার উপরের আস্তরণ পরিষ্কার করতে হবে।
আপেল ও গাজর
আপেল মুখে লালারসের প্রবাহ বৃদ্ধি করতে সাহায্য করে। যা মুখের ক্ষারভাব দূর করে এবং দাঁতের ফাঁকে জমে থাকা খাদ্যকণা পরিষ্কার করতে সাহায্য করে। গাজরও ঠিক একইভাবে দাঁতের জন্য উপকারী।
বেকিং সোডা
বেকিং সোডা একটি ক্ষারীয় উপাদান। সপ্তাহে একদিন বেকিং সোডা দিয়ে দাঁত মাজলে দাঁতের উপর জমে থাকা আস্তরণ দূর হয়। তাছাড়া বেকিং সোডা দাঁতের হলদেটেভাব দূর করতে সাহায্য করে।
ছবি: রয়টার্স।
ব্যাকটেরিয়ার বিস্তার
ছয়টি খাবার
