স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে ত্বকের পুষ্টির জন্য উপকারী কিছু খাবারের নাম উল্লেখ করা হয় যেগুলো ব্রণের সমস্যা থেকে মুক্তি পেতে সাহায্য করে।
পালংশাক: শরীরে জমে থাকা টক্সিন এবং ব্যাক্টেরিয়া থেকেই ব্রণের ‍উৎপত্তি। সবুজ শাক ও সবজিতে, বিশেষ করে পালংশাকে রয়েছে প্রচুর পরিমাণে ক্লোরোফিল যা শরীরে জমে থাকা অতিরিক্ত টক্সিন ও ব্যাক্টেরিয়া পরিষোধিত করতে সাহায্য করে। আরও রয়েছে প্রচুর পরিমাণে ভিটামিন এ যা ত্বকে পুষ্টি জোগায় এবং তারুণ্য ধরে রাখতে সাহায্য করে দীর্ঘদিন।
হলুদ: এই মসলা ত্বকের প্রদাহ রোধ করে এবং উজ্জ্বল করতে সাহায্য করে। তাছাড়া জমে থাকা টক্সিন এবং ব্যাক্টেরিয়া দূর করে ত্বকে

কোকোয়া: দারুণ স্বাদের কোকোয়া পছন্দ করেন না এমন মানুষ পাওয়া দুষ্কর। এই মজাদার উপাদানে বাড়তি কোন চিনি নেই, তাই ত্বকের জন্য দারুণ উপকারী। তাছাড়া ব্রণের জন্য দায়ী অ্যাসিডের কার্যকারীতা রোধ করে কোকোয়া। ত্বকে রক্ত সঞ্চালন বৃদ্ধি করে চকচকে করতে সাহায্য করে এই উপাদান। পুষ্টি জোগায়, ব্রণ দূর করে বলিরেখা কমিয়ে ত্বকের বয়স কমিয়ে আনে কোকোয়া।
গাজর: ভিটামিন এ এবং বিটা ক্যারোটিনে ভরপুর এই সবজি স্বাস্থ্য ও ত্বকের জন্য খুবই উপকারী। এছাড়া ব্রণের সমস্যা থেকে মুক্তিতেও এই উপাদান সাহায্য করে। স্বাস্থ্যকর খাদ্যাভ্যাসের পাশাপাশি প্রতিদিন গাজর খেলে ত্বক সুন্দর হয়ে উঠবে।

