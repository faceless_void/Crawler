বাংলাদেশ প্রাথমিক শিক্ষক সমিতির কেন্দ্রীয় কমিটির সভাপতি হিসেবে নির্বাচিত হয়েছেন অনোয়ারুল ইসলাম তোতা। তিনি এক হাজার তিনশ’ সাতাশ ভোট পেয়ে নির্বাচিত হন।
ঢাকা: বাংলাদেশ প্রাথমিক শিক্ষক সমিতির কেন্দ্রীয় কমিটির সভাপতি হিসেবে নির্বাচিত হয়েছেন অনোয়ারুল ইসলাম তোতা। তিনি এক হাজার তিনশ’ সাতাশ ভোট পেয়ে নির্বাচিত হন।
এছাড়া নির্বাচনে সিনিয়র যুগ্ম সম্পাদক পদে নির্বাচিত হয়েছেন আব্দুল ওয়াদুদ ভূঁইয়া, সাংগঠনিক সম্পাদক জুলফিকার আলী ও অর্থ সম্পাদক পদে নির্বাচিত হয়েছেন মো. সাইদুর রহমান।
শনিবার (১৫ অক্টোবর) সকাল ১১টার দিকে ঢাকা রিপোর্টার্স ইউনিটিতে (ডিআরইউ) বাংলাদেশ প্রাথমিক শিক্ষক সমিতির কেন্দ্রীয় কমিটির নির্বাচনের ফল প্রকাশ উপলক্ষে নির্বাচন কমিশন এক সংবাদ সম্মেলনের আয়োজন করে।
সংবাদ সম্মেলনে কেন্দ্রীয় নির্বাচন কমিশনের চেয়ারম্যান (ভারপ্রাপ্ত) মো. গোলাম মোস্তাফা বলেন, এবারও নির্বাচন সুষ্ঠু ও শান্তিপূর্ণভাবে হয়েছে। আমরা আশা প্রকাশ করছি এ নির্বাচনের মাধ্যমে যারা আমাদের প্রতিনিধি নির্বাচিত হয়েছেন। তারা সংগঠনের জন্য নিজ স্বার্থ ব্যতিরেকে কাজ করবেন।
