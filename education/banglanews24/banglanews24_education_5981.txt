ইবি: কর্মচারী দক্ষতা ও শৃংখলা ভঙ্গের দায়ে উকিল উদ্দীন নামে ইসলামী বিশ্ববিদ্যালয়ের এক কর্মচারীকে সাময়িক বরখাস্ত করেছে বিশ্ববিদ্যালয় প্রশাসন।
বিশ্ববিদ্যালয়ের সহায়ক কর্মচারী সমিতির সভাপতি উকিল উদ্দিন নামের ওই কর্মচারী ছাত্র-শিক্ষক সাংষ্কৃতিক কেন্দ্রে সাউন্ড সিস্টেম অপারেটর হিসেবে কর্মরত ছিলেন।
বুধবার (২৪ জানুয়ারি) দিনগত রাত নয়টায় বিশ্ববিদ্যালয়ের ভারপ্রাপ্ত রেজিস্ট্রার এস এম আব্দুল লতিফ স্বাক্ষরিত এক প্রজ্ঞাপনে এতথ্য জানানো হয়।
পাশাপাশি তাকে কেন চূড়ান্তভাবে বরখাস্ত করা হবে না এমর্মে আগামী সাত কার্য দিবসের মধ্যে কারণ দর্শাতে বলা হয়েছে।
লিখিত ওই প্রজ্ঞাপনে বলা হয়, বুধবার (২৪জানুয়ারি) অফিস চলাকালীন বিশ্ববিদ্যালয়ের প্রশসান ভবনের সামনে উকিল উদ্দিনের নেতৃত্বে কতিপয় কর্মচারী আকস্মিকভাবে উচ্চস্বরে অকথ্য ভাষায় শ্লোগান দিতে থাকে। এতে এক নৈরাজ্যকর পরিস্থিতি ও ত্রাসের সৃষ্টি হয়। 
এছাড়া তার নেতৃত্বে বিশ্ববিদ্যালয়ের মেইন গেট বন্ধ করে বিকেলের শিফটের সকল পরিবহন বন্ধ করে দেন ওই কর্মচারীরা। সাথে সাথে তারা কিছু কর্মকর্তার নাম উল্লেখ করে ‘ধর ধর জবাই কর’ বলে শ্লোগান দেন। এতে বিশ্ববিদ্যালয়ের দীর্ঘদিনের চলমান সুষ্ঠু ও স্বাভাবিক কর্মকাণ্ডকে বাধাগ্রস্ত করার অপচেষ্টা করেন। যা বিশ্ববিদ্যালয় কর্মচারী দক্ষতা ও শৃংখলা বিধির পরিপন্থি।
এদিকে ঘটনাটি তদন্ত করে এর সাথে জড়িত অন্যদেরকেও চিহ্নিত করতে তিন সদস্যের তদন্ত কমিটি গঠন করা হয়েছে। তদন্ত কমিটিতে বিশ্ববিদ্যালয়ের বায়োটেকনোলজি অ্যান্ড জেনেটিক ইঞ্জিনিয়ারিং বিভাগের অধ্যাপক ড. রেজওয়ানুল ইসলামকে আহবায়ক করা হয়েছে।
কমিটির অন্য সদস্যরা হলেন, কম্পিউটার সায়েন্স অ্যান্ড ইঞ্জিনিয়ারিং বিভাগের অধ্যাপক ড. আহসান উল আম্বিয়া ও পরীক্ষা নিয়ন্ত্রক অফিসের উপ-রেজিস্ট্রার আব্দুর রশিদ। কমিটিকে দ্রুত তদন্ত প্রতিবেদন জমা দিতে বলা হয়েছে।
