রেসিপি দিয়েছেন ফেইসবুকের ‘রকমারি রান্না শিখুন’ পেইজের মুহসিনা তাবাসসুম।
পুরির জন্য উপকণ: আটা সোয়া ১ কাপ। সুজি সিকি কাপের থেকে সামান্য কম। তেল ৬ চা-চামচ (স্টিলের চামচের)। বেইকিং পাউডার আধা চা-চামচ (স্টিলের চামচের)। কালোজিরা ১ চিমটির বেশি না (ইচ্ছা)। লবণ সামান্য। সাধারণ পানি পরিমাণ মতো।
পদ্ধতি: সব উপকরণ মিশিয়ে নরম ডো তৈরি করুন। রুটির আটার মতো হলেও হবে। নরম করে না মাখালে সুজি সহজে গলবে না।
ডো পলিথিনে মুড়ে ঢেকে রেখে দিন এক, দুই ঘণ্টা। তারপর বের করে খুব ভালো করে ময়ান দিন। পরোটার মতো মোটা ও ছোট ছোট রুটি বেলে বা বড় রুটি বেলে কাটার দিয়ে কেটে নিন।
সব বেলা শেষ হলে ভাজবেন। তিন থেকে চার মিনিট পরে ভাজলে ভেলপুরি অনেক সুন্দর মচমচে হয়। ডুবো তেলে দেওয়ার পরে হালকা চেপে চেপে দিন। তাহলে সহজেই ফুলে উঠবে।
মাঝারি আঁচে লালাচে বাদামি রং করে ভাজুন। ভেলপুরি একটু বেশি ভাজতে হবে। অল্প ভাজলে মচমচে কম হবে এবং সহজেই নরম হয়ে যাবে।
ভাজা হয়ে গেলে ঠাণ্ডা করে এয়ার টাইট বক্সে ভরে রাখুন।
টিপস: ভেলপুরি ফুচকার থেকে সাইজে দ্বিগুণ এবং মোটা হবে। ভাজার পরে এক পিঠ একটু বেশি মোটা থাকে এবং ওপর পিঠ সামান্য পাতলা থাকে। ফুচকার মতো পাতলা হয় না।


চাট মসলা তৈরির পদ্ধতি: জিরা, ধনিয়া, শুকনামরিচ তেল ছাড়া প্যানে টেলে নিয়ে গুঁড়া করে নিন।
পুর তৈরির পদ্ধতি: ভেজানো ডাল সব মসলা দিয়ে কষিয়ে সিদ্ধ করে নিন। সঙ্গে আলুও সিদ্ধ করুন। এবার এই সিদ্ধ ডাল ও আলু চাট মসলা দিয়ে ভালো করে মেশান। সঙ্গে পেঁয়াজকুচি, ধনেপাতা ও কাঁচামরিচ-কুচি মেশান।
