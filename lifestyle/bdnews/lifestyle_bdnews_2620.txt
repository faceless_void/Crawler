পরিমিত পুষ্টি সম্পন্ন খাবার ত্বক সুন্দর করতে সাহায্য করে। আর তারুণ্য শুধু ত্বক দেখেই বোঝা যাবে তা নয়, শারীরিক সুস্থতা এবং কর্মক্ষমতাও তারুণ্যেরই একটি অংশ।
স্বাস্থ্য বিষয়ক একটি সাইটে ত্বকের তারুণ্য ধরে রাখতে সাহায্য করে এমন কিছু খাবারের বিষয়ে উল্লেখ করা হয়।
রসুন
রসুনে থাকা হাইড্রোজেন সালফাইড স্বাস্থ্য ভালো রাখতে সাহায্য করে। হাইড্রোজেন সালফাইড হচ্ছে একটি অ্যান্টিঅক্সিডেন্ট যা ধমনী শিথিল করে এবং রক্ত চলাচল স্বাভাবিক রাখাসহ ক্যান্সার প্রতিরোধে সাহায্য করে।
আমাদের শরীর প্রাকৃতিকভাবেই হাইড্রোজেন সালফাইড তৈরি করে। তবে খাদ্য তালিকায় রসুন যোগ করলে শরীরের কোষ বেশি পরিমাণে উপকারি হাইড্রোজেন সালফাইড তৈরি করতে পারবে।
অ্যাভাকাডো
অ্যাভাকাডো চাইলে টুকরা করে বা ভর্তা বানিয়ে খাওয়া যায়। অ্যাভাকাডো ফলে রয়েছে বিশেষ কিছু উপাদান যা ত্বকে বয়সের ছাপ পড়তে বাঁধা সৃষ্টি করে। আছে প্রচুর পরিমাণে ভিটামিন ই, পটাশিয়াম এবং অ্যান্টিঅক্সিডেন্ট। পাশাপাশি এতে থাকা ভিটামিন এবং খনিজ পদার্থ কোলেস্টেরল কমাতে এবং রক্ত চাপ নিয়ন্ত্রণ করতে সাহায্য করে।
আভাকাডো ফলে আরও আছে ফলিক এসিড বা ভিটামিন-বি যা হার্ট অ্যাটাক প্রতিরোধ করে এবং অস্টিওপরোসিস রোগের ঝুঁকি কমায়।
শস্যজাতীয় খাদ্য
ভুট্টা, যব, বার্লি ইত্যাদি শরীরে ফাইবার ধরে রাখে এবং ক্ষতিকর কোলেস্টরেল ও চর্বি থেকে রক্ষা করে হজম প্রক্রিয়াকে ভালো রাখে। পাশাপাশি খাবারের আঁশ ক্ষুধা নিয়ন্ত্রণ করে এবং রক্তে চিনির পরিমাণ কমাতে সাহায্য করে। তাছাড়া ওটস, আটা ও বাদামি-চালে আছে প্রচুর ভিটামিন এবং খনিজ পদার্থ যা বয়স ধরে রাখতে সাহায্য করে। এছাড়া 'হোল গ্রেইন'জাতীয় খাবার খেলে হৃদরোগ, স্ট্রোক এবং ডায়াবেটিসের ঝুঁকি কমে যায়।
তবে অপরিশোধিত শস্যজাতীয় খাবারগুলো শরীরের জন্য বেশি উপকারী। কারণ খাদ্য শোধন করার সময় প্রয়োজনীয় ভিটামিন এবং খনিজ পদার্থ নষ্ট হয়ে যায়।
বাদাম
প্রোটিনের উৎস হিসেবে পরিচিত। তবে প্রোটিন ছাড়াও বাদামে আছে ওমেগা-থ্রি ফ্যাটি এসিড যা হৃদপিণ্ড ভালো রাখতে সাহায্য করে। বাদামে আরও আছে ভিটামিন, খনিজ ও পটাশিয়াম যা রক্ত চাপ কমায়। এছাড়াও বাদামে থাকা ভিটামিন-ই কোষকে ক্ষতির হাত থেকে রক্ষা করে এবং ক্যালসিয়াম হাঁড় মজবুত করে।
তাছাড়া বাদামে ক্যালরি থাকলেও তা শরীর সহজে গ্রহণ করে না। আর তাই ওজন বাড়ার ভয়ও থাকে না।
ফল ও শাকসবজি
আছে অ্যান্টিঅক্সিডেন্ট যা বয়সের প্রভাব কমাতে সাহায্য করে। সাধারণত সবুজ সবজি ও বিভিন্ন ধরনের শাকে অ্যান্টিঅক্সিডেন্ট বেশি থাকে।
মাছ
বর্তমানে খাদ্য তালিকায় মাছের তেল বেশ জনপ্রিয়। মাছ বা মাছের তেলে আছে ওমেগা-থ্রি ফ্যাটি এসিড যা হৃদরোগের ঝুঁকি ও ইনফ্লামেশন কমায় এবং রক্তচাপ নিয়ন্ত্রনে সাহায্য করে। ওমেগা-থ্রি ফ্যাটি এসিড সাধারণত মিষ্টি পানির মাছে বেশি থাকে।
এক গবেষণায় জানা যায়, যারা নিয়মিত মাছ খায় তারা দীর্ঘ দিন বাঁচেন। এছাড়াও মাছে আছে প্রোটিন। তবে মাংসের তুলনায় মাছে স্যাচুরেটেড ফ্যাটের পরিমাণ কম থাকে। তাই বাড়তি ওজন নিয়েও দুশ্চিন্তা করতে হয় না।
মটরশুঁটি, শিমের দানা
মটরশুটি, শিমের দানা স্বাস্থ্যের জন্য এত উপকারি যে এদেরকে ‘সুপার ফুড’ নামে ডাকা হয়। মটরশুঁটি ও শিমের দানাতে আছে প্রয়োজনীয় খনিজ ও ভিটামিন যা বিভিন্ন রোগ ও অসুস্থতার সঙ্গে লড়াই করে অ্যান্টি-এইজিং উপাদান তৈরিতে শরীরকে শক্তি জোগায়। এছাড়াও ব্লুবেরি, দই, ডিম, বাদাম ও মিষ্টি আলুও সুপার ফুড বলা হয়।
ডার্ক চকোলেট
বয়স বাড়ার কিছু লক্ষণ ত্বকে দেখা যায়। আর সূর্যের ক্ষতিকর রশ্মি দ্রুত ত্বকে বয়সের ছাপ ফেলে দেয়। তবে ডার্ক চকোলেট খেলে বা পান করলে সূর্যের ক্ষতিকর রশ্মির প্রভাব থেকে রক্ষা পাওয়া যায়।
