রেসিপি দিয়েছেন আতিয়া হোসেন তনি ।
সাদা পোলাও
উপকরণ: পোলাওয়ের চাল ১ কেজি। ঘি আধা কাপ। সয়াবিন তেল ২ টেবিল-চামচ। দারুচিনি ৩ টুকরা। এলাচ ৩ থেকে ৪টি। লবঙ্গ ২ থেকে ৩টি। আদাবাটা ১ টেবিল-চামচ। রসুনবাটা ১ চা-চামচ। লবণ ১ টেবিল-চামচ বা স্বাদমতো। পানি ফুটানো ৬ কাপ (প্রয়োজন হলে আরও এককাপ)। কেওড়ার জল ১ চা-চামচ (ইচ্ছা)। পেঁয়াজ কুচি ১ কাপ। পেঁয়াজ বেরেস্তা ২ টেবিল-চামচ (সাজানোর জন্য)। কিশমিশ ২ টেবিল চামচ। আস্ত কাঁচামরিচ ৪ থেকে ৫টি।
পদ্ধতি: চাল ভালো করে ধুয়ে আধা ঘণ্টা পানিতে ভিজিয়ে রাখুন। এরপর পানি ঝরিয়ে, পাতিলে তেল, ঘি দিয়ে গরমমসলা দিন। পেঁয়াজকুচি দিয়ে নরম করে ভেজে সব বাটামসলা দিন। কিছুক্ষণ ভেজে অল্প পানি দিয়ে ভালোমতো কষিয়ে  পানি ঝরানো চাল দিয়ে আরও কিছুক্ষণ ভাজুন।


ঢাকনা দেওয়ার ২০ থেকে ২২ মিনিট পর পোলাও চুলা থেকে নামিয়ে রাখুন। এই সময়ে অর্থাৎ ঢাকনা দেওয়ার পরে ঢাকনা না খুলে আধা ঘণ্টা দমে রাখবেন। পোলাও না নেড়ে সার্ভিং ডিশে নিয়ে উপরে বেরেস্তা ছিটিয়ে দিয়ে পরিবেশন করুন ।
* রাইস কুকারে করতে চাইলে চাল ভাজার পর, সেটা রাইস কুকারে পানিসহ দিয়ে দিন। রাইস কুকার চালু করুন। মাঝখানে একবার নেড়ে দিতে হবে।
কবুতরের রোস্ট
উপকরণ: কবুতর ২টি। পেঁয়াজকুচি ১ কাপ। পেঁয়াজবাটা ২ টেবিল-চামচ। আদাবাটা ১ টেবিল-চামচ। রসুনবাটা ১ চা-চামচ। হলুদগুঁড়া আধা চা-চামচ। মরিচগুঁড়া ১ চা-চামচ। ধনেগুঁড়া আধা চা-চামচ। গোলমরিচগুঁড়া আধা চা-চামচ। তেজপাতা ২,৩টি। এলাচ ৪টি। লবঙ্গ ৩,৪টি। দারুচিনি ২,৩ টুকরা। টক দই ২ টেবিল-চামচ। তেল আধা  কাপ। লবণ স্বাদমতো। পেঁয়াজ বেরেস্তা ২ টেবিল-চামচ।
পদ্ধতি: প্রথমে কবুতরের পালক তুলে পরিষ্কার করে নিন। এবার আগুনের ওপর ধরে চামড়ার ছোট পালকগুলো পুড়িয়ে পরিষ্কার করুন। মাঝখান থেকে কেটে দুই ভাগ করে ধুয়ে নিন। অথবা না কেটে আস্ত রোস্ট করতে পারেন।
এবার আস্ত গরমমসলা বাদে সব মসলা দিয়ে মাখিয়ে দু’তিন ঘণ্টা মেরিনেইট করে রাখুন।


বাকি মসলা দিয়ে ভাজা হলে এলাচ, দারুচিনি, তেজপাতা দিয়ে দিন। সব গুঁড়ামসলা সামান্য পানি দিয়ে গুলিয়ে দিয়ে দিন। কিছুক্ষণ রান্না করুন।
মসলা কষে, তেল উঠে আসলে কবুতর দিন। মাংস কষানো হলে পরিমাণমতো পানি দিয়ে ঢেকে দিন। ঝোল মাখা মাখা হলে বেরেস্তা দিয়ে কিছুক্ষণ আরও রান্না করে লবণ চেখে নামিয়ে ফেলুন। উপরে কিছু বেরেস্তা ছিটিয়ে দিন।
পোলাও কিংবা গরম ভাত দিয়ে পরিবেশন করুন।
সমন্বয়ে: ইশরাত মৌরি।
কাবুলি ঝাল পোলাও
ইলিশ পোলাও
ঝটপট বিরিয়ানি
ডিমের বিরিয়ানি
