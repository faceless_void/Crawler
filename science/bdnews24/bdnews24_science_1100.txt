এই এক লাখ ফলোয়ারের কারণে ভারতীয় টুইটারে যিনি ছিলেন একক ‘শাহেনশাহ’, তাকে এখন শীর্ষ স্থানটি ভাগাভাগি করে নিতে হচ্ছে বলিউড বাদশাহ্র সঙ্গে। টুইটার ফলোয়ার সংখ্যার দিক থেকে ৭৪ বছর বয়সী এই তারকা বর্তমানে শাহ রুখ খান-এর সঙ্গে সহাবস্থান করছেন। শাহ রুখের ফলোয়ার সংখ্যাও ৩.২৯ কোটি।
নিজের ফলোয়ার সংখ্যা ৩.৩০ কোটি থেকে ৩.২৯ কোটিতে নেমে আসার পর টুইটার ত্যাগের এই সিদ্ধান্ত নিয়েছেন ‘বিগ বি’ খ্যাত এই তারকা, বলা হয়েছে আইএএনএস-এর প্রতিবেদনে। 
বুধবার এক টুইটে অমিতাভ বলেন, “টুইটার! তুমি আমার ফলোয়ার সংখ্যা কমিয়ে দিয়েছ। হাহা! এটি একটি কৌতুক। তোমাকে ছাড়ার সময় হয়ে গিয়েছে।”
বলিউড তারকাদের মধ্যে এরপরের অবস্থানগুলোতে আছেন সালমান খান (৩.০৭ কোটি), আমির খান (২.২৮ কোটি), প্রিয়াংকা চোপড়া (২.১৬ কোটি) আর দিপিকা পাড়ুকোন (২.৩০ কোটি)।
 
আরও খবর-
