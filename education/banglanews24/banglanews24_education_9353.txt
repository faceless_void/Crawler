ঢাকা: বাংলা নববর্ষ বরণে ড্যাফোডিল ইন্টারন্যাশনাল ইউনিভার্সিটির (ডিআইইউ)  সকল ক্লাব ও এলিট পেইন্ট যৌথভাবে আয়োজন করেছে দু'দিনব্যাপী উৎসব ‘বৈশাখ পার্বণে- ১৪২৪’।
এবারের উৎসব হবে আরও বৃহৎ পরিসরে বাঙালির চিরচেনা গ্রামীণ সংস্কৃতির আবহে।
প্রতি বছরের মতো এবারো এ আয়োজনের মূল আকর্ষণ আল্পনা উৎসব। রাজধানীর ধানমণ্ডি ২৭ থেকে রাসেল স্কয়ার সিগন্যাল পর্যন্ত রাজপথে আল্পনা এঁকে স্বাগত জানানো হবে নতুন বছরকে।  
বৃহস্পতিবার (১৩ এপ্রিল) রাত সাড়ে দশটা থেকে শুক্রবার (১৪ এপ্রিল) ভোর পর্যন্ত চলবে আল্পনা উৎসব। এরপর পহেলা বৈশাখের সকালে বের হবে মঙ্গল শোভাযাত্রা।
উৎসবে আরো থাকছে গানে গানে নববর্ষ বরণ,  মেহেদি উৎসব, পুঁথিপাঠ, কনসার্টসহ আরো অনেক কিছু।
এবারের মঙ্গল শোভাযাত্রায় নানা রঙের তিন শতাধিক মুখোশ ছাড়াও থাকবে বাঙালি সাজ-সজ্জার বর্তমান প্রেক্ষাপটে নানা অশুভ শক্তি দূর করার বিভিন্ন সচেতনতামূলক প্ল্যাকার্ড।  
এবারের থিম অনুসারে মঙ্গল শোভাযাত্রায় থাকছে প্রতীকী বাঘ, ইলিশ, পেঁচা, মাটির হাঁড়ি-পাতিল ছাড়াও  বাঙালির সংস্কৃতি ও গ্রাম বাংলার ঐতিহ্যের আরও নানা অনুষঙ্গ।
পহেলা বৈশাখের আগে এসব মুখোশ প্রদর্শিত হচ্ছে ড্যাফোডিলের একাডেমিক ভবনের নিচতলায়।
