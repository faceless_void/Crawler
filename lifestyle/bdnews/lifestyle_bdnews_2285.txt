কারণ হচ্ছে, ফ্লাইটের সময় ছিল দুপুরে। কিন্তু আবহাওয়া ভালো না হওয়ার কারণে ঢাকা থেকে নেপালের সেই ফ্লাইট ছাড়তে ছাড়তে হয়ে যায় রাত। বিমান যখন মাঝ আকাশে তখন মাঝে মাঝেই অন্ধকারের বুক চিড়ে আলো ছড়াচ্ছিল বজ্রপাত। মাঝে মাঝে বেশ ঝাঁকুনিও দিচ্ছিল বিমান। সেটা ছিল এপ্রিল মাস।
খুব অল্প সময়ের মধ্যে কাঠমান্ডু চলে আসলেও বিমানটি অবতরণ করতে পারছিলনা। কাঠমান্ডুর উপর দিয়েই চক্কর দিচ্ছিল। কারণ সেই খারাপ আবহাওয়া। ঝড়ো বাতাস, বৃষ্টি আর বজ্রপাত। বেশ অনেকক্ষণ আকাশে চক্কর কাটার পর রাত সাড়ে ৯টার দিকে ত্রিভুবন আন্তর্জাতিক বিমান বন্দরে আমরা অবতরণ করি। আমি, আমার বর (অনিক), শাশুড়ি আর শ্বশুড়।
অনেকক্ষণ পর একটু প্রাণ খুলে নিশ্বাস নিলাম। যদিও এটা একটা অন্যরকম অভিজ্ঞতা। তবে সত্যি কথাটা হচ্ছে আমি ভয় পেয়েছিলাম। বেশ ভালো ভয় পেয়েছিলাম।
বিমান বন্দরটা সুন্দর, ছিমছাম। কিন্তু খুব ছোট্ট। খুব বেশি সময় লাগল না অন অ্যারাইভাল ভিসা নিতে। সব কাজ সেরে যখন বিমান বন্দরের বাইরে আসলাম তখন রাত প্রায় ১১টা।
বিমানবন্দর থেকে হোটেলে গেলাম। হোটেলের নাম ‘দা মাল্লা হোটেল’। পাঁচ তারকা হোটেল বলে ভাড়া একটু বেশি। বিমানবন্দর থেকে বেশিক্ষণ লাগল না। ১৫/২০মিনিট। রাতে ভালো ঘুম দিলাম।


সকালে গাড়ি এসে হাজির। নাস্তা সেরে রওনা দিলাম আমরা পোখরার উদ্দেশ্যে। কাঠমান্ডু থেকে পোখরার দূরত্ব ২০০ কিলোমিটার। চাইলে কেউ বিমানেও যেতে পারে। পোখরাতে ছোট একটি বিমানবন্দর রয়েছে। কিন্তু আমরা গিয়েছিলাম মাইক্রোতে। রাস্তার সৌন্দর্য দেখতে দেখতে।
পোখরাকে বলা হয় নেপালের রানী। পোখরার প্রাকৃতিক সৌন্দর্য্যের কোনো তুলনাই হয় না। কাঠমান্ডু থেকে পোখরা যাওয়ার রাস্তাটা পাহাড়ি আর আঁকাবাঁকা। মুগ্ধ হয়ে দেখতে লাগলাম। কি দারুণ!
রাস্তার ধারের দোকানগুলো খুবই সাদামাটা কিন্তু খুব পরিচ্ছন্ন আর সুন্দর। প্রাকৃতিক সৌন্দর্যের মাঝে চেয়ারে গা এলিয়ে দিয়ে বসে আছেন, আর বয়ে চলেছে ঝিরঝির বাতাস। ভাবুন তো একবার কি রকম আনাবিল প্রশান্তি পাওয়া যেতে পারে!
চোখের সামনে দেখা যায় সারি সারি পাহাড়। নিচে বয়ে চলেছে নদী। নদী উপর দিয়ে আবার ঝুলন্ত সেতু। শুধু মাত্র হেঁটে পারাপারের জন্য। স্থানীয়রা ওপাশ থেকে এপাসে আসে এই সেতু দিয়ে। আহা! কি যে সুন্দর!!
পোখরা
এই সব দেখতে দেখতে খাবার খেতে খেতে এক সময় পৌঁছে গেলাম নেপালের রানীর শহরে। পোখরার সমতল এলাকাটিও সমুদ্রপৃষ্ঠ হতে প্রায় আড়াই হাজার ফুট উপরে।


খানিকক্ষণ পরেই শুরু হল বৃষ্টি। সে কি বৃষ্টি! মনে হচ্ছে আকাশ ভেঙে পড়েছে। শুধু পানি আর পানি। পাহাড়ি রাস্তা দিয়ে গলগল করে পানি বয়ে যাচ্ছে।
বৃষ্টি থামা পর্যন্ত অপেক্ষা করতেই হল। উপায় নেই। সারি সারি দোকান হলেও এক দোকান থেকে বেরিয়ে যে অন্য দোকানে যাব সেটার ব্যবস্থা নেই। বেরুলেই ভিজতে হবে। দোকানে বসে বসে ঝুম বৃষ্টির শব্দ আর বৃষ্টি দেখতে দেখতে মনে পড়ে গেল দাদা বাড়ির কথা!
সবাই বলল ঝড় বৃষ্টিতে আকাশ পরিষ্কার হয়ে যাবে। বায়ুমণ্ডলের ধুলাবালি চলে যাবে। যার ফলে সকালে অন্নপূর্ণা পরিষ্কার দেখতে পাব। খুশি হয়ে গেলাম। যাকে দেখব বলে পোখরা আসা তাকে স্পষ্ট দেখতে পাব এর থেকে আনন্দের আর কি হতে পারে!


চারটার দিকে পুরো হোটেল হঠাৎ সরগরম হয়ে উঠল। সবাইকে ডেকে দিচ্ছে। যার যার ভাড়া করা গাড়ি চলে এসেছে। অন্নপূর্ণার জন্য রওনা দিতে হবে। চা দিয়ে গেল রুমে। চা খেয়ে ঘুম ঘুম চোখেই রেডি হলাম। সাড়ে ৪টার বাজার মধ্যেই রওনা দিয়ে দিলাম। প্রচুর লোক। পুরো হোটেলের লোক একসঙ্গে বের হলে যা অবস্থা হয় আর কি! সারি সারি গাড়ি। হৈচৈ। এতো ভিড়ের মধ্যে আমরা আমাদের ড্রাইভার বিজুকে খুঁজে পাচ্ছিলাম না।
গাড়ির ভিড় বেশ খানিকটা ফাঁকা হওয়া পরই পেলাম বিজুকে। তুমুল উত্তেজনা নিয়ে রওনা হলাম। অন্ধকার। পাহাড়ি রাস্তা এঁকেবেঁকে চলছে। বুকের ভেতরে ধুকপুক ধুকপুক করছে।
আমরা যাচ্ছি সারাংকোট এলাকায়। পোখরা পাহাড়ের একটি গ্রাম সারাংকোট। এখান থেকেই দেখা যাবে অন্নপূর্ণাকে। আমরা যখন পৌঁছলাম তখনও অন্ধকার। এরই মধ্যে প্রচুর পর্যটক চলে এসেছে। গাড়ি আমাদের যেখানে নামিয়ে দিল সেই জায়গাটা সমুদ্রপৃষ্ঠ থেকে প্রায় পাঁচ হাজার ফুট উঁচু।
গাড়ি এর বেশি আর উপরে উঠবেনা। বাকি রাস্তা আমাদের হেঁটেই যেতে হবে পাহাড়ি রাস্তা দিয়ে। ধাপ ধাপ করা সিঁড়ি আছে। একটু পর পরই বাড়ি। বাড়ি থেকে ডাক আসছে তাদের ছাদে বসে অন্নপূর্ণা দেখার আহবান। প্রতিটি বাড়ির সামনেই আছে চায়ের দোকান। সরগরম। অনেকেই যাচ্ছে। আমরা আরও উপরে উঠতে লাগলাম।


আহ! জীবন অনেক সুন্দর। সত্যি সুন্দর!
ধীরে ধীরে ফরসা হতে লাগল চারপাশ। সূর্য বাবাজি তখনও উঁকি দেয়নি। তখনও অনেক মানুষ আসছে। বসছে। উঠছে। আসলে ঠিক কোন দিকে যে তাকাব বুঝে উঠতে পারছিলাম না। কোনটা যে অন্নর্পর্ণা কিছুই তো দেখা যাচ্ছিল না। শুধু অন্ধকার।
কিছুক্ষণের মধ্যেই ফর্সা হতে থাকল চারপাশ। এই সময়টুকুতে সব কিছু এত দ্রুত ঘটে যায়! পুবের আকাশ লাল হতে লাগল ধীরে ধীরে। কালো কালো অবয়বগুলো ধীরে ধীরে চোখের সামনে ফুটে উঠতে লাগল।


বরফ ঢাকা শ্বেত শুভ্র শরীরে সূর্যের আলোয় সোনালি আভা দেখে দুচোখ বিস্ময়ে স্থির হয়ে গেছে ততক্ষণে। করতালি দিয়ে আনন্দ প্রকাশ করছে সবাই। আমি তো শুধু হা করেই দেখছিলাম। স্থানীয় একজন জানাল, সামনে যে পিরামিড আকৃতির পর্বত দেখা যাচ্ছে সেটা ‘অন্নপূর্ণা-১’। আর একটু পর ‘মৎসপুচ্ছ’ও দেখা যাবে। হা করে তাকিয়ে থাকতে থাকতে ছবি তোলার কথা ভুলেই গিয়েছিলাম। বেশি তুলতে পারলাম না। মনে হল ছবি তোলার দিকে মনযোগ বেশি চলে যাচ্ছে। এই সৌন্দর্য ঠিক মতো উপভোগ করতেই পারছি না। বাদ দিলাম ছবি তোলা।
সূর্যোদয় তো আমরা কতই দেখি। তবে এই সূর্যোদয়ের যেন কোনো তুলনাই নেই! একে দেখার জন্য মানুষের ভিড়, কোলাহল কি নেই? এর সৌন্দর্য লিখে প্রকাশ করা আমার পক্ষে সম্ভব নয়।
আমি একটা পাহাড় কিনতে চাই। সত্যি চাই। সুনীলের সেই কবিতার মতো। আর সেই পাহাড় হবে এইটা, যেটাকে আমি চোখের সামনে দেখতে পাচ্ছি।


বেশ কিছুক্ষণ পর সবাই ধীরে ধীরে নামতে শুরু করতে লাগল। বসার জন্য ভাড়া চায়ের বিল মিটিয়ে আমরাও নামতে লাগলাম। নিচে ছোট ছোট স্যুভিনিয়রের দোকান সাজিয়ে বসেছে স্থানীয়রা। সেখানে আছে হরেক রকমের জিনিস। সবথেকে ভালো লাগল ওদের হাতে বোনা কাপড়। খুব সুন্দর।
অন্নপূর্ণা মানে অন্নদাত্রী বা অন্নের দেবী। এক সময় এই পর্বতকে শষ্যভাণ্ডারের দেবী মনে করা হত। তাই তাকে এই নামে ডাকা হয়।
সারাংকোট এলাকাটা ঘুরে দেখলাম। লালমাটির দোতলা, দেড় চালা বাড়িই বেশি। বাড়ির সামনে আছে পাথর দেওয়া ছোট ছোট দেয়াল। ইটের ঘর চোখে পড়ল অল্প কয়েকটা। প্রতিটা বাড়ির সামনে ছোট ছোট গাছ।


ঘোরাঘুরি শেষে ফিরতে লাগলাম। ড্রাইভার বিজু জানাল এই গ্রামেই তার বাড়ি। আর পরিবার থাকে এখানেই। পাহাড় থেকে নামতে নামতে হাতের ইশারায় দেখিয়েও দিল তার বাড়ি।
তার বাড়িতে যেতে অনুরোধও করল। আহা কত মায়া ওদের। এই বিজুকেই খুশি হয়ে কিছু অতিরিক্ত টাকা দিতে চেয়েছিলাম আমরা। নিল না। বরং খুব আহত চোখে তাকিয়ে থাকল।


আপনি যদি সড়ক পথে ভ্রমণ করেন তাহলে, ঢাকার কল্যাণপুর থেকে বেশ কিছু বাস ছেড়ে যায় যেগুলো রাতে রওনা হয়ে ভোরে পৌঁছায় লালমনিরহাট বুড়িমারি সীমান্তে। সীমান্তের আনুষ্ঠানিকতা সেরে ওপারে গিয়ে উঠতে হবে শিলিগুড়ির বাসে।
শিলিগুড়ি থেকে আবার অন্য একটি বাসে রানীগঞ্জ সীমান্তে। সেখানে আনুষ্ঠানিকতা সেরে নেপালের কাকরভিটা। এরপরপ্রায় ১০ থেকে ১২ ঘণ্টার বাস জার্নিতে পৌঁছে যাবেন পোখারা।
আর বিমানে যেতে চাইলে, কোনো ঝামেলা নেই। টিকেট কেটে শুধু বসে পড়ুন বিমানে। বিমান ছাড়তে না ছাড়তেই পৌঁছে যাবেন।
প্রয়োজনীয় জিনিস
পোখরাতে হালকা শীত পড়ে। তাই হালকা কিছু শীতের কাপড় নিয়ে গেলে ভালো হয়। তাই বলে আবার অত ভারি ভারি কাপড় নেওয়ার দরকার নেই। এই শীতটা খুব মিষ্টি মিষ্টি একটা শীত।
খাবার
