ঢাকা: আমাদের দেশর অর্ধেকেরও বেশি শিক্ষক সৃজনশীল শিক্ষা ব্যবস্থা কী সেটা জানেন না। কিন্তু সৃজনশীলতাই একটি দেশের মূল উন্নয়নের হাতিয়ার। যেদেশ যতো বেশি সৃজনশীল তারা ততো বেশি উন্নত।
সোমবার (১২ জুন) জাতীয় প্রেসক্লাবের ভিআইপি লাউঞ্জে ‘বাজেট ২০১৭-১৮, শিক্ষা ও নারী’ শীর্ষক গোলটেবিল আলোচনায় বক্তারা এসব মন্তব্য করেন। আলোচনা সভায় প্রাথমিক শিক্ষার ওপর ২০১৬-১৭ অর্থবছরের জাতীয় বাজেটের প্রভাব নিয়ে একটি প্রবন্ধ উপাস্থপন করেন বাংলাদেশ উন্নয়ন গবেষণা সংস্থার সাবেক সিনিয়র রিসার্চ ফেলো ড. প্রতিমা পাল মজুমদার। আলোচনার আয়োজন করে বাংলাদেশ উন্নয়ন পরিষদ।
এ সময় তিনি বলেন, ২০১৬-১৭ জাতীয় বাজেটে শিক্ষা ক্ষেত্রে বরাদ্দ ছিল মোট বাজেটের ১৫ দশমিক ৪ শতাংশ। ২০১৭-১৮ অর্থবছরে তা বৃদ্ধি পেয়ে দাঁড়িয়েছে ১৬ দশমিক ৪ শতাংশ। কিন্তু এই বৃদ্ধি পর্যাপ্ত নয়।
এছাড়া যতোটুকু বৃদ্ধি পেয়েছে সেটাও কতোটা কাজে লাগছে তা নিয়ে প্রশ্ন তোলেন প্রতিমা পাল।
প্রবন্ধের ওপর আলোচনায় অন্য বক্তরা বলেন, সৃজনশীল শিক্ষা ব্যবস্থা যুগের সঙ্গে মিলিয়ে করা, কিন্তু আমাদের শিক্ষকদের এ সম্পর্কে তেমন কোনো প্রশিক্ষণ দেওয়া হয় না। ফলে অর্ধেকেরও বেশি শিক্ষক এই শিক্ষা সম্পর্কে জানেন না। এতে তারা কী শিক্ষা দেবেন।
বাজেটে জেন্ডার সংবেদনশীলতা নিয়ে আরেকটি প্রবন্ধ উপস্থাপন করেন বাংলাদেশ ব্যাংকের সাবেক বোর্ড মেম্বার আধ্যাপক হান্নানা বেগম।
বাংলাদেশ উন্নয়ন পরিষদের চেয়ারম্যান এম এ জলিলের সভাপতিত্বে সেখানে অতিথি হিসেবে বক্তব্য রাখেন ঢাকা স্কুল অব ইকোনমিকসের গভর্নিং কাউন্সিলের চেয়ারম্যান ড. কাজী খলীকুজ্জমান আহমদ।
আলোচনা সভায় বিশেষ আলোচক হিসেবে বক্তব্য রাখেন বাংলাদেশ প্রাণিসম্পদ গবেষণা ইনস্টিটিউটের সাবেক মহাপরিচালক ড. জাহাঙ্গীর আলম এবং বাংলাদেশ উন্নয়ন পরিষদের নির্বাহী পরিচালক ড. নিলুফার বানু।
