কোনো ব্যবহারকারী তার লিস্টে কাকে কীভাবে যোগ করছেন সে বিষয়ে ওই লিস্টে নাম ওঠা ব্যবহারকারীকে জানানো হবে না বলে জানিয়েছে টুইটার।
অন্যদিকে, ব্যবহারকারীদের অভিযোগের ভিত্তিতেই সাইটটি এই পদক্ষেপ নিয়েছিল বলে জানায় বিবিসি। সে সব অভিযোগে দাবি করা হয়েছিল যে এই পরিবর্তনের মাধ্যমেই হয়রানি বন্ধ করা সম্ভব।
এই পরিবর্তনকে একটি ‘ভুল পদক্ষেপ’ হিসেবে ব্যাখ্যা দিয়েছে টুইটার।
এই সামাজিক মাধ্যমে ব্যবহারকারীরা বিভিন্ন তালিকায় অন্য ব্যবহারকারীদের যোগ করার সুযোগ দেয়। কোনো ব্যবহারকারী, অন্য কারও এমন তালিকায় যুক্ত হলে তাকে সে বিষয়ে জানানোর ব্যবস্থা চালু করেছিল টুইটার। কিন্তু কিছু ব্যক্তি অন্যান্যদের হয়রানি করার ক্ষেত্রে তাদের বিভিন্ন তালিকায় যুক্ত করে এই ফিচারের ফায়দা নিতে শুরু করে।
সোমবার টুইটার-এর নিরাপত্তা দল জানান, এখন ব্যবহারকারীদের আর এ বিষয়ে জানানো হবে না। তবে কিছু ব্যবহারকারীর দাবি, এই পরিবর্তন হয়রানি বন্ধ করছিল।  এর মাধ্যমে কাউকে ইচ্ছাকৃতভাবে কোনো হয়রানিমূলক তালিকায় যুক্ত করা হচ্ছিল বলেও জানান তারা। 
