সিলেট (শাবিপ্রবি): তীব্র সেশনজটের কবলে পড়া শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়কে (শাবিপ্রবি) আগামী বছরের মধ্যে সেশনজট মুক্ত করার ঘোষণা দিয়েছেন উপাচার্য (ভিসি) অধ্যাপক ফরিদ উদ্দীন আহমেদ। একই সঙ্গে শিক্ষকদের প্রতি পরীক্ষার ফলাফল দ্রুততার সাথে প্রকাশের আহ্বান জানান।
বুধবার (১১ অক্টোবর) সন্ধ্যায় বিশ্ববিদ্যালয়ের কেন্দ্রীয় মিলনায়তনে পলিটিক্যাল স্টাডিজ (পিএসএস) বিভাগের ২০১৬-১৭ শিক্ষাবর্ষের শিক্ষার্থীদের নবীনবরণ অনুষ্ঠানে প্রধান অতিথির বক্তব্যে উপাচার্য এ ঘোষণা দেন।
এসময় বিভাগীয় প্রধান অধ্যাপক ড. এসএম হাসান জাকিরুল ইসলামের সভাপতিত্বে অনুষ্ঠানে বিশেষ অতিথি ছিলেন প্রক্টর জহির উদ্দিন আহমেদ।
শিক্ষকদের উদ্দেশ্যে উপাচার্য বলেন, আপনারা পরীক্ষার ফলাফল দ্রুততার সঙ্গে প্রকাশ করবেন। যাতে শিক্ষার্থীরা তাদের রেজাল্ট পেয়ে দ্রুত কর্মক্ষেত্রের জন্যে নিজেদের প্রস্তুত করতে পারেন। 
সেশনজট মুক্ত করার বিষয়টি দ্রুত বাস্তবায়ন করতে চাই উল্লেখ করে তিনি বলেন, শাবিপ্রবির বিভিন্ন বিভাগে ফলাফল প্রকাশে ধীরগতির কারণে বছরের পর বছর শিক্ষার্থীরা সেশনজটসহ অবর্ণনীয় দুর্ভোগের শিকার হয়ে আসছেন।
