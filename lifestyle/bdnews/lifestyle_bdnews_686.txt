কথা হচ্ছিল এই বোর্ডিংয়ের দীপক কুমার নাগের সঙ্গে। বলছিলেন শুরুর সময়ের কথা।
“চার আনায় যখন এখানে ভাত মাংস খাওয়া যেত তখন একটি চিতল মাছের পেটির দাম পড়ত পাঁচ টাকা। বোঝেন এবার মাছটা কত বড় ছিল দাদা। তেমন মাছ এখন আর পাওয়া যায় না। তারপরও আমরা আমাদের খাবার মেন্যুতে চিতল মাছের পেটি আর কোপ্তার ঐতিহ্য এখনও ধরে রেখেছি।”
“আমাদের আরেকটা ঐতিহ্য হচ্ছে ব্যবসার অংশীদারিত্ব। আমরা আর আমাদের কাকার সব ছেলেরা এর মালিক। ব্যবসা শুরু করেছিলেন আমার বাবাসহ তিন কাকা নন্দলাল নাগ, সিতানাথ নাগ আর বৃন্দাবন নাগ। দেশে বিভাগের পর, সেই ১৯৬৫ সাল থেকে এভাবেই চলে আসছে। কোনো ঝগড়া বিবাদ নাই, অংশীদারিত্বের ঝামেলা নাই। আমরা খুব ভালো আছি, কখনও ভাগাভাগির চিন্তা করিনা।”


কাঁসা, শংখ আর পূজামণ্ডপ পেছনে ফেলে সামনে পড়বে বিখ্যাত কালিমন্দির। এর পাশের গলিতেই দাঁড়িয়ে আছে এই কল্পনা বোর্ডিং।
এবার মন্দিরের পাশের সেই সরু গলি দিয়ে ভেতরে প্রবেশের পর চোখে পড়বে পুরানো আমলের চেয়ার-টেবিল আর তার ওপর স্টিলের থালা-গ্লাস সাজানো।
খালি দেখে যে টেবিল-চেয়ারে বসে পড়ুন। খাবার অর্ডার করার আগেই হাত ধোয়াবার লোক চলে আসবে। তার কাছে মেন্যু শুনে খাবার অর্ডার দিন।
শুরু হল আপনার কল্পনা বোর্ডিং এর ১২ পদের ভর্তা, পাঁচ পদের সবজি আর নানা রকম মুখরোচক খাবার দিয়ে ভূরিভোজন।  


রাজা তৈলক্ষ মহারাজ, সুবোধ মিত্র, আব্দুর রাজ্জাক, রনেশ দাস মৈত্রসহ বিভিন্ন নামিদামী লোকের যাতায়াত ছিল এখানে।
কল্পনা বোর্ডিংয়ের দোতালার এক ঘরে সে সময় মুজিব বাহিনী গঠিত হয়। যুদ্ধের এখানে বিভিন্ন সভা ছিল নিয়মিত। পশ্চিমবঙ্গ বিশেষত; কংগ্রেসের অনেক বিখ্যাত ব্যাক্তি এখানে থেকে গেছেন।
যুদ্ধের সময় ভারতীয় দূতাবাসের কর্মকর্তারা এখানে থাকতেন। সে সময় এখান থেকে ভারতের ভিসা দেওয়া হত। স্বাধীনতা যুদ্ধ শুরু হলে পাক হানাদার বাহিনী বোর্ডিংটি পুড়িয়ে দেয়। ২৯ মার্চের পর এখানে কেউ ছিলনা।
১৯৭২ সালের জানুয়ারি মাসের প্রথম সপ্তাহে এটি পুনরায় চালু হয়।
বর্তমানে এখানে আগের মতোই থাকা-খাওয়া চলে।
থাকার ব্যবস্থা আগের চাইতে ভালো। সিঙ্গেল রুম ভাড়া ২০০ টাকা। ডাবল ২৫০ থেকে ৪০০ টাকা। একটা এসি রুম আছে, ভাড়া ৮০০ টাকা।
শুরুতে কল্পনা বোর্ডিং দোতলা ছিল, এখন এটি চার তলা। এখানে সকালে নাস্তার কোনো ব্যবস্থা নাই। সকাল ১০টা থেকে রাত ১২টা পর্যন্ত ভাত-মাছ আর মাংসসহ নানা পদের খাবার বিকিকিনি চলে।
স্বর্ণশিল্পী, জুয়েলারির মালিক আর এর সঙ্গে যুক্ত বেপারীরা এখানকার প্রধান গ্রাহক। এ
খাবারের মধ্যে অন্যতম চিতল মাছের পেটি, চিতল মাছের কোপ্তা আর পাঁচ তরকারি। বেগুন, কুমড়া, আলু, ডাল, শিম, কপিসহ বিভিন্ন ধরনের সবজি দিয়ে তৈরি এই পাঁচ তরকারির চাহিদা অনেক।
চিতল মাছের কোপ্তা, ও পেটি দেশের বিভিন্ন বড় বড় হোটেলেও সরবরাহ করা হয়। বিশেষ করে এক সময়ের হোটেল ইন্টারকন্টিনেন্টাল ছিল এর অন্যতম খরিদ্দার, জানালেন দীপক কুমার নাগ।
তাছাড়া এখানকার মুড়িঘন্টরও খুব সুনাম। আরও কিছু অন্যতম খাবার হচ্ছে- ইলিশ, রুই, বোয়াল, আইড়, টাকি মাছের ভর্তা, শৈল মাছের ভর্তা ইত্যাদি।
কৈ মাছ এখানে ঝাল ও মিষ্টি দুভাবে রান্না করা হয়। ভোজন রসিকদের মিষ্টি কৈ মাছের কদর খুব। সঙ্গে পাবেন মুরগি ও খাসির মাংস।


তার ভাষায়, “দাদা খাবারের মান নিয়া আপোষ নাই। আমাদের খাবারের মানের জন্যই আজও আমরা টিকা আছি।”
এখন পর্যন্ত কল্পনা বোর্ডিয়ে না এসে থাকলে কিংবা এখানকার খাবারের স্বাদ এখনও না গ্রহন করে থাকলে সময় সুযোগ মতো চলে আসুন ৪৭/১ শাখারী বাজার, একেবারে কালি মন্দিরের পাশে!
পুনশ্চ: সঙ্গে নিজস্ব বাহন থাকলে বাহাদুর শাহ পার্কের সামনে পার্ক করুন। চালকসহ গাড়িটি সেখানে রেখে তবেই কল্পনা বোর্ডিংয়ে আসুন। কারণ শাখারীবাজারে গাড়িসহ ঢোকা সম্ভব নয়!
কল্পনা কল্পনা বোর্ডিংয়ের কোপ্তার রেসিপি
আরও রেস্তোরাঁ
এখনও বিউটি বোর্ডিং
