ঢাকা: জাতীয় বিশ্ববিদ্যালয়ের ২০১৭ সালের স্নাতক (সম্মান) শেষ বর্ষের পরীক্ষার সময়সূচি পরিবর্তন করা হয়েছে।
বুধবার (০৭ ফেব্রুয়ারি) বিশ্ববিদ্যালয়ের জনসংযোগ, তথ্য ও পরামর্শ দফতর থেকে পাঠানো এক প্রেস বিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
এতে জানানো হয়, জাতীয় বিশ্ববিদ্যালয়ের ২০১৭ সালের স্নাতক (সম্মান) শেষ বর্ষের পরীক্ষা আগামী ৫ মার্চ থেকে শুরু হয়ে ২২ এপ্রিল পর্যন্ত চলবে এ পরীক্ষা। প্রতিদিন দুপুর দেড়টা থেকে এ পরীক্ষা অনুষ্ঠিত হবে।
পরীক্ষার বিস্তারিত সময়সূচি বিশ্ববিদ্যালয়ের ওয়েবসাইট (www.nu.edu.bd) ও (www.nubd.info) থেকে জানা যাবে।
