পিরিয়ড নারীদের কাছে একটি পরিচিত শব্দ। পিরিয়ড প্রতি মাসে নারীকে গর্ভধারণের জন্য প্রস্তুত করে। এ সময় নারীদের পরিচ্ছন্ন থাকা যেমন জরুরি, তেমনি খাবারের বিষয়ে একটু গুরুত্ব দেয়া উচিত। কারণ পিরিয়ডের সময়ে পরিচ্ছন্নতা, খাবার, বিশ্রাম নেয়া ও হাঁটাচলায় সাবধানতা জরুরি।
পিরিয়ড কী
প্রতি চন্দ্র মাস পর পর হরমোনের প্রভাবে পরিণত মেয়েদের জরায়ু চক্রাকারে যে পরিবর্তনের মধ্য দিয়ে যায় এবং রক্ত ও জরায়ু নিঃসৃত অংশ যোনিপথে বের হয়ে আসে, তাকেই পিরিয়ড বা ঋতুচক্র বলে। মাসিক চলাকালীন পেটব্যথা, পিঠব্যথা ও বমি বমি ভাব হতে পারে। পিরিয়ডে ভালো মানের ন্যাপকিন ব্যবহার করা জরুরি। এ ছাড়া কোনোভাবেই একই কাপড় পরিষ্কার করে একাধিকবার ব্যবহার করা উচিত নয়। পিরিয়ডের সময় শরীর থেকে যে রক্ত প্রবাহিত হয়, তার মধ্যে ব্যাকটেরিয়া থাকে।
পিরিয়ডের সময়ে শরীর ঠিক রাখার জন্য খাদ্যের প্রতি হতে হবে সচেতন। নতুবা দৈনন্দিন জীবনে এর খারাপ প্রভাব পড়বে।
আসুন জেনে নেই দেহ ও মন সুস্থ রাখতে পিরিয়ডের সময় খাবেন যেসব খাবার-
শাকসবজি
পিরিয়ডের সময় শরীরকে প্রয়োজনীয় সুরক্ষা দিতে সবুজ শাকসবজি খাওয়া যেতে পারে।বিশেষ করে কচু শাক বেশি উপকারী। সবুজ শাকসবজিতে রয়েছে আয়রন এবং ভিটামিন-বি ও উচ্চ ফাইবার। এই ফাইবার আপনার হজম প্রক্রিয়াকে তরান্বিত করবে, যা পিরিয়ডকে সহনীয় করতে খুবই প্রয়োজন।
ফলমূল
পিরিয়ড সম্পর্কিত হজমের সমস্যা সমাধানে তাজা ফলমূল বেশ গুরুত্বপূর্ণ ভূমিকা রাখে। আপনার পছন্দসই নানা ধরনের ফলমূল নিয়মিত খাবার টেবিলে রাখুন।
লাল মাংস
পিরিয়ডের সময় লাল মাংস আপনার শরীরের জন্য বেশ প্রয়োজনীয়। লাল মাংস আপনার শরীরে প্রয়োজনীয় আয়রন জোগায়, যা প্রতি মাসে আপনার শরীর থেকে প্রচুর রক্তক্ষরণ পূরণে সহায়তা করে। আয়রনযুক্ত খাবার আপনাকে অনেক কঠিন রোগ থেকে রক্ষা করে থাকে।
বাদাম
পিরিয়ডের সময় বাদাম খাওয়া যেতে পারে।বাদাম শরীরের জন্য বেশ উপকারী।
গাইনি কনসালট্যান্ট,অ্যাপোলো হাসপাতাল।
