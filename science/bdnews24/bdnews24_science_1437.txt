শেয়ার বাজারে বর্তমানে শক্ত অবস্থানে থাকলেও ব্যবসার পরিধি বাড়াতে প্রতিষ্ঠানটি বিক্রি করার সম্ভাবনা রয়েছে। ২৮ অক্টোবর বিষয়টির সঙ্গে সংশ্লিষ্ট এক ব্যক্তি ব্যাপারটি নিশ্চিত করেছেন। ওই দিন বিকেল পর্যন্ত প্রতিষ্ঠানটির শেয়ার মূল্য ৮.৭ শতাংশ বৃদ্ধি পায়।
স্মার্টফোনের জন্য উচ্চ ক্ষমতাসম্পন্ন জায়রোস্কোপ সেন্সর তৈরি করে ইনভেনসেন্স। এই সেন্সর স্মার্টফোনকে গতি নিরূপণ করতে সাহায্য করে। এটি পোকিমন গো-এর মতো গেইম এবং বিভিন্ন লোকেশনভিত্তিক অ্যাপকে আরও উন্নত করে তুলতে পারে।
ব্যবসা বাড়াতে প্রতিষ্ঠানটি একটি বিনিয়োগকারী ব্যাংকের সঙ্গে তাদের পরবর্তী করনীয় এবং সম্ভাব্য বিকল্পগুলো নিয়ে আলোচনা চালিয়ে যাচ্ছে। ইতোমধ্যেই প্রতিষ্ঠানটি কিনতে আগ্রহ প্রকাশ করেছে চীনা এবং জাপানি বেশ কয়েকটি প্রতিষ্ঠান।
যদিও প্রতিষ্ঠানটির পক্ষ থেকে সতর্ক করা হয়েছে যে, এর কোন নিশ্চয়তা নেই আদৌ বিক্রয় প্রক্রিয়া সম্পন্ন হবে কি না।
