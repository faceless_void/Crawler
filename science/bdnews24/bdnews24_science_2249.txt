বিশ্বের সবচেয়ে বড় সামাজিক যোগাযোগ মাধ্যমটির এমন পদক্ষেপ ব্যবসায়বান্ধব সামাজিক মাধ্যম লিংকডইন-এর নিয়োগ ব্যবসায়কে চাপে ফেলে দিতে পারে বলে জানিয়েছে রয়টার্স।
ফেইসবুকের এক মুখপাত্র বলেন, "আচরণের উপর ভিত্তি করে আমরা দেখি, ফেইসবুকে অনেক ছোট ব্যবসায় প্রতিষ্ঠান তাদের লোক নিয়োগ নিয়ে পেইজে পোস্ট দেয়, আমরা একটি পরীক্ষা চালাচ্ছি যাতে পেইজ অ্যাডমিনরা নিয়োগ বিজ্ঞপ্তি দিতে পারে আর আগ্রহী প্রার্থীদের কাছ থেকে আবেদন গ্রহণ করতে পারে।"
লিংকডইন তাদের মোট আয়ের অধিকাংশই আসে চাকরিপ্রার্থী আর নিয়োগকারীদের পরিশোধ করা মাসিক ফি থেকে।
প্রযুক্তি সাইট টেকক্রাঞ্চ এ নিয়ে করা তাদের প্রতিবেদনে জানিয়েছে, ফেইসবুকের জব ফিচারগুলোর মাধ্যমে নিয়োগকারী প্রতিষ্ঠানগুলো তাদের পেইজে আরও বেশি ব্যবহারকারী টানতে পারবে। আর ফেইসবুককে অর্থ পরিশোধের মাধ্যমে তারা তাদের নিয়োগ বিজ্ঞপ্তি আরও বেশি মানুষের কাছে পৌঁছাতে পারবে।
