জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি) শহীদ সালাম বরকত হলের ১৪৮ নম্বর রুমে অভিযান চালিয়ে গাঁজা ও ইয়াবা সেবনের সরঞ্জামাদি জব্দ করেছে হল প্রশাসন।
জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি) শহীদ সালাম বরকত হলের ১৪৮ নম্বর রুমে অভিযান চালিয়ে গাঁজা ও ইয়াবা সেবনের সরঞ্জামাদি জব্দ করেছে হল প্রশাসন।
 
রোববার (২৩ অক্টোবর) রাত সাড়ে ১১টার দিকে শহীদ সালাম বরকত হলের ১৪৮ নম্বর রুম থেকে তা জব্দ করা হয়।
হল প্রশাসন সূত্র জানায়, গোপন সংবাদের ভিত্তিতে পাওয়া তথ্যের ভিত্তিতে এ অভিযান পরিচালনা করা হয়।
তবে অভিযানের সময় কাউকে আটক করা যায়নি। এ রুমে মীর মশাররফ হোসেন হলের অর্থনীতি বিভাগের ৪২তম ব্যাচের শিক্ষার্থী শরিফুল ইসলাম শিশির থাকেন বলে শনাক্ত করা গেছে।
এ বিষয়ে হল প্রাধ্যক্ষ সহযোগী অধ্যাপক সিকদার মো. জুলকারনাইন বাংলানিউজকে বলেন, ওই শিক্ষার্থীকে হলে অবাঞ্ছিত ঘোষণা কর হয়েছে। এছাড়া অবৈধভাবে হলে অবস্থান করে এ ধরণের কর্মকাণ্ড করায় বিশ্ববিদ্যালয় শৃঙ্খলা বিধি অনুয়াযী তার বিরুদ্ধে ব্যবস্থা গ্রহণ করা হবে।
এদিকে খোঁজ নিয়ে জানা যায়, বিশ্ববিদ্যালয় শাখা ছাত্রলীগের আইন বিষয়ক সম্পাদক আবু সুফিয়ান চঞ্চলের এলাকার ছোট ভাই শিশির। তার অনুমতিতেই মীর মশাররফ হোসেন হলের আবাসিক শিক্ষার্থী হয়েও সালাম বরকত হলে অবস্থান করছেন। এই রুমে অবস্থানকারী শিক্ষার্থীদের কোনো তালিকা হল প্রশাসনের কাছে নেই বলে জানা যায়।
এছাড়া সালাম বরকত হলের সাংগঠনিক সম্পাদক কার্তিক ঘোষ নীরব, আইআইটি বিভাগের মাহবুব, দর্শন বিভাগের মাহিন শাহরিয়ার ও হাসান ইশতিয়াক হৃদয়ের সঙ্গে তার ভাল সখ্য রয়েছে। তারা একইসঙ্গে নেশা করে বলে নির্ভরযোগ্য সূত্র থেকে জানা যায়।
এ বিষয়ে বিশ্ববিদ্যালয় শাখা ছাত্রলীগের আইন বিষয়ক সম্পাদক আবু সুফিয়ান চঞ্চল বাংলানিউজকে বলেন, তার হলে থাকার সমস্যা হচ্ছিল তাই শিশিরের বড় ভাই আমাকে বলেছিল তার জন্য একটি থাকার ব্যবস্থা করতে। তাই শিশিরকে ওই হলে উঠিয়েছিলাম। পরবর্তীকে সে গোপনে কি করছে এটাতো দেখভাল করা আমার পক্ষে সম্ভব নয়।
এ বিষয়ে বিশ্ববিদ্যালয় প্রক্টর অধ্যাপক তপন কুমার সাহা বাংলানিউজকে বলেন, এখন পর্যন্ত এ ব্যাপারে লিখিত কোনো অভিযোগ পাইনি। অভিযোগ পেলে ব্যবস্থা নেবো।
