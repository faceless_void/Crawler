এই বিষয়ে বিস্তারিত জানাচ্ছেন, মালয়েশিয়া প্রবাসী জনস্বাস্থ্য পুষ্টিবিদ শওকত আরা সাঈদা (লোপা)।
স্বাভাবিক ব্যক্তিদের ক্ষেত্রে লক্ষ রাখতে হবে যে ব্যাপারগুলো
ঈদ বা যেকোনো উৎসবের খাবারগুলো স্বাভাবিকভাবেই প্রচুর তেল মসলা, ঝাল এবং মিষ্টিজাতীয় হয়ে থাকে। যা আমাদের প্রতিদিনের খাবারের মধ্যে পড়ে না। তাই খাবারের ব্যাপারে কিছুটা সতর্কতা অবলম্বন করলে খুব সহজেই ভালো থাকা যায়।
* খাবারের তালিকায় সহজে হজম হয় এমন খাবার রাখতে হবে।
* রান্নার ক্ষেত্রে এমন খাবার বাছাই করুন যেগুলোতে তুলনামূলক ভাবে তেল মসলা কম ব্যবহৃত হয়। গ্রিল, রোস্টেড মুরগি বা গরু, অল্প তেলে ভাজা খাবার, সুপ, বিভিন্ন ধরনের সালাদ, সবজির বিভিন্ন পদ এবং তাজা ফল এবং ফলের তৈরি ডেজার্ট রাখতে পারেন।
* তেল মসলার ভারী খাবারের সঙ্গে পানীয় হিসেবে বেশিরভাগ মানুষের পছন্দ গ্যাসযুক্ত পানীয়গুলো। তবে এসব বাদ দিয়ে ঘরেই তৈরি করে নিন তাজা ফলের শরবত বা স্মুদি। এসব পানীয়তে চিনির পরিবর্তে মধু ব্যবহার করুন।
* সারাদিনে নিয়মিত বিরতিতে প্রচুর পানি পান করুন।
* গরু বা খাসির মাংস রান্নার সময় মাংসের সঙ্গে লেগে থাকা বাড়তি চর্বি কেটে বাদ দিন। চেষ্টা করুন কম তেলে রান্না করতে।
* ঈদে নিজের বাসায় দাওয়াতের আয়োজন বা আত্মীয় বন্ধুদের বাসায় দাওয়াত থাকেই। খাওয়ার ক্ষেত্রেও যাদের শারীরিক কোনো সমস্যা নেই তারা সবই খাবেন। তবে অবশ্যই নিয়ন্ত্রণে রেখে। খাবারের পদগুলো দেখে ঠিক করে নিন কোনগুলো খাবেন এবং এক খাবার বেশি পরিমাণে না নিয়ে খুব অল্প পরিমাণে সেই খাবারগুলো একসঙ্গে প্লেটে নিন। এভাবে খেলে পরিমিত ভাবে সব খাবারই খাওয়া হবে এবং নিয়ন্ত্রণ রেখেই খাওয়া যাবে।
স্থূলতা রয়েছে যাদের
যারা ওজন কমাতে চাচ্ছেন তারা যে উৎসবে মজাদার কিছু খাবেন না তা নয়। তবে খাবেন রয়েসয়ে। প্রতিবেলাই ভারী খাবার খাবেন না। দুপুরে বা রাতে যখন দাওয়াত বা বেশি খাবারের আয়োজন থাকে তখন অন্য বেলায় খুব সাধারণ খাবার খান।
কাঁচাসালাদ বা সবজির দিকে বেশি নজর দিন। দাওয়াত থাকলে চেষ্টা করুন ভাত না খেতে বা খুব অল্প পরিমাণে ভাত খেতে। আর কোমল পানীয়ের পরিবর্তে মধু দিয়ে গ্রিনটি খান।
উচ্চ রক্তচাপ বা হৃদরোগীদের ক্ষেত্রে
স্বাভাবিক ভাবেই উচ্চ রক্তচাপ বা হৃদরোগীদের ক্ষেত্রে গরু বা খাসির মাংস না খেতে বা খেলেও খুব কম খেতে বলা হয়ে থাকে। তাদের জন্য মুরগির মাংসটাই শুধু অনুমোদিত। তাই তারা ঈদে মাংস খেলেও অল্প পরিমাণে দিনে একবেলাই খাওয়া উচিত।
ডায়াবেটিক রোগীদের জন্য
গরু বা খাসির মাংসের মতো উচ্চমানের প্রোটিন খাবার ডায়াবেটিক রোগীদের ক্ষেত্রে পুরোপুরি নিষিদ্ধ না হলেও খুব বেশি খাওয়া যায় না।
তবে ডায়াবেটিক রোগীদের কিডনির সমস্যা থাকলে তারা সারাদিনে প্রোটিন খেতে পারবেন দেহের প্রতি গ্রাম ওজনের জন্য এক গ্রাম করে। সেক্ষেত্রে আসলে এক টুকরা মাংস খেলে সারাদিনের অনুমোদিত প্রোটিন শেষ হয়ে যায়। তাই এসব রোগীদের খাবারের ক্ষেত্রে অবশ্যই সতর্ক থাকতে হবে।
কোষ্ঠকাঠিন্য যাদের রয়েছে
তাদের বেশি মাংস ও তেল-মসলার ভারী খাবার খাওয়া ক্ষতিকর। তারা ঈদের সময় অন্যান্য খাবারের পাশাপাশি প্রচুর তরল খাবার এবং পানীয় খাবেন। গরম সুপ খেতে পারেন। বেশি সমস্যা হলে প্রতিদিন সকালে ইসবগুলের সরবত খেতে পারেন।
ঈদে বা উৎসবে গরু, খাসির মাংসসহ ভারী খাবারের আয়োজন থাকবেই। তবে প্রত্যেকের উচিত নিজেদের শরীর ও স্বাস্থ্যের কথা চিন্তা করে নিয়ন্ত্রণে থেকে খাবার খাওয়া।
প্রতীকী ছবির মডেল: আবু জাফর স্বপন।
মাংস খেতে নেই মানা
মাংস খান অল্প
