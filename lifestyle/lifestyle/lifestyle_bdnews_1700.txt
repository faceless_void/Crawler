রেসিপি দিয়েছেন ফারহানা শারমিন তৃষা।
উপকরণ: পাকাআম বড় ১টি। শসা ১টি। পেঁয়াজ বড় ১টি। কাঁচামরিচ-কুচি ১ টেবিল-চামচ। ধনেপাতা-কুচি ২ টেবিল-চামচ। লেবুর রস ২ টেবিল-চামচ। বিট লবণ স্বাদ মতো। গোলমরিচ স্বাদ মতো।
পদ্ধতি: আম ভালোভাবে ধুয়ে খোসা ছাড়িয়ে কিউব করে কেটে নিন। একই ভাবে শসা আর পেঁয়াজ কাটুন।
এবার একটি পাত্রে বাকি সব উপকরণ দিয়ে আম, পেঁয়াজ আর শসা হাল্কা হাতে একটু মাখিয়ে নিন।
