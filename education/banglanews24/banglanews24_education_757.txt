শাবিপ্রবি: শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ে (শাবিপ্রবি) পলিটিক্যাল স্টাডিজ বিভাগে হামলার ঘটনায় মানববন্ধন করেছে বিভাগের শিক্ষার্থীরা।
বৃহস্পতিবার (২ মার্চ) দুপুর ১২টায় বিশ্ববিদ্যালয়ের কেন্দ্রীয় গ্রন্থাগারের সামনে এ মানববন্ধন ও প্রতিবাদ সমাবেশের অয়োজন তারা।
এর আগে মঙ্গলবার (২৮ ফেব্রুয়ারি) বিভাগের সমিতি নির্বাচন কেন্দ্র করে সমিতির নব-নির্বাচিত ভিপি সরদার মনসুরের উপর পরাজিতদের হামলার প্রতিবাদে এ মানববন্ধন অনুষ্ঠিত হয়।
এ ঘটনায় বুধবার (১ মার্চ) সরদার মনসুর আহমেদ বিভাগীয় প্রধান অধ্যাপক দিলারা রহমান বরাবর লিখিত অভিযোগ দেন। সেখানে তিনি হামলার দায়ে বিভাগের সাবেক শিক্ষার্থী জাকারিয়া মাসুদ, পরাজিত প্রার্থী দেবাশীষ বর্মণ রুবেল ও মাজহারুল ইসলাম সবুজসহ বেশ কয়েকজনকে দায়ী করেন।
মানববন্ধনে প্রায় দুই শতাধিক শিক্ষার্থী অংশ নেন।
সমাবেশে বক্তারা অবিলম্বে হামলাকারীদের দৃষ্টান্তমূলক শাস্তি দাবি করেন।
