রেসিপি দিয়েছেন শারমিন হক।
চ্যাপাশুঁটকি ভর্তা
উপকরণ: চ্যাপাশুঁটকি ১ কাপ। পেঁয়াজকুচি ১ কাপ। রসুন আধা কাপ। ৮টি কাঁচামরিচ। লাল গুঁড়ামরিচ ২ চা-চামচ। ১ টেবিল-চামচ সয়াবিন তেল। ধনেপাতা প্রয়োজন মতো। লবণ স্বাদ মতো।
পদ্ধতি: হাঁড়িতে তেল দিয়ে চ্যাপাশুঁটকি, পেঁয়াজকুচি, রসুন, কাঁচামরিচ, লাল গুঁড়ামরিচ, সয়াবিন তেল ও লবণ দিয়ে ভালো করে ভেজে নিন। এবার ধনেপাতাসহ সবকিছু একসঙ্গে মিহি করে বেটে নিন।
হয়ে গেল ভর্তা।
চিংড়ি ভর্তা
উপকরণ: চিংড়িমাছ ১ কাপ। আধা কাপ পেঁয়াজকুচি। ৪ কোয়া রসুন। ৮টি কাঁচামরিচ। ১ টেবিল-চামচ সয়াবিন তেল। ধনেপাতা প্রয়োজন মতো। লবণ স্বাদ মতো।
