বাঙালি নারী এই শাড়ি পরা বিবর্তন নিয়েই বিডিনিউজ টোয়েন্টিফোর ডটকমের জন্য লিখেছেন তরুণ ইতিহাস গবেষক রিদওয়ান আক্রাম।
বাঙালি নারীর আটপৌরে পোশাক শাড়ি পরার প্রচলন হয়েছিল কবে? নিশ্চিত দিন-ক্ষণের হিসাব কেউ রাখেনি। তাই নানা তথ্য যাচাই-বাছাই করে খানিকটা অনুমান করে নেওয়া যায় বৈকি!
সূত্র হিসেবে নেওয়া যেতে পারে ‘শাড়ি’ শব্দটির উৎস-সময়কালকে। বলা হয়ে থাকে, ‘শাড়ি’ এসেছে সংস্কৃত শব্দ ‘শাটী’ থেকে। তবে অনেকের ধারণা, ‘শাটী’ শব্দটি সংস্কৃত হিসেবে পরিচিতি লাভ করলেও আদতে এটা ধার করা। আর্যরা ভারতবর্ষে আসার আগেই নাকি ‘শাটী’ শব্দটির অস্তিত্ব ছিল।


মধ্যভারতীয় আর্য ভাষায় ‘শাড়ি’কে আরও বিভিন্ন শব্দে আখ্যায়িত করা হয়েছে, যেমন-‘সাটক’, ‘সাটিকা’। আবার ‘মহাভারত’য়ে উল্লিখিত দ্রৌপদীর যে ‘বস্ত্রহরণ’ করা হয়েছিল, সেটাও অনুমিত হয়, শাড়িই ছিল।
গুপ্ত যুগের (আনুমানিক ৩০০ থেকে ৫০০ সাল পর্যন্ত) বিখ্যাত কবি কালীদাসের ‘কুমারসম্ভব’য়ে শাড়ির কথা উল্লেখ আছে। গুপ্ত আমলের ইলোরা অজন্তা গুহার প্রাচীন চিত্রাবলি বলে দেয়, খ্রিস্টাব্দ শুরুর সময়ে শাড়ির অস্তিত্ব ছিল। ইলোরা অজন্তার মতো পাহাড়পুর-ময়নামতির পোড়ামাটির ফলক থেকে প্রমাণ পাওয়া যায়, হাজার বছর আগে এই পূর্ববঙ্গে শাড়ির প্রচলন ছিল। তবে এসব শাড়ি পরার সঙ্গে আজকের শাড়ি পরার খানিকটা পার্থক্য রয়েছে।
ঐতিহাসিক নীহাররঞ্জন রায়ের মতে, আদিমকালে পূর্ব-দক্ষিণ ও পশ্চিম ভারতে সেলাই করা কাপড় পরার প্রচলন ছিল না। এই অখণ্ড বস্ত্রটি পুরুষের পরিধানে থাকলে হত ‘ধূতি’, আর মেয়েদের পরিধানে থাকলে ‘শাড়ি’। নারী-পুরুষ উভয়ের শরীরের ওপরের অংশ উন্মুক্তই থাকত। তবে কখনও কখনও উচ্চবংশের নারীরা পালা-পার্বণে ওড়নাজাতীয় কাপড়ে নিজেকে ঢেকে নিত।
রামচন্দ্র মজুমদারের কথায় এর সমর্থনও পাওয়া যায়— ‘তখন মেয়েরা আংটি, দুল, হার- এসবের সঙ্গে পায়ের গোছা পর্যন্ত শাড়ি পরেছে। ওপরে জড়ানো থাকত আধনা বা আধখানা।’


প্রথম মা হওয়া নারীকে উপহার হিসেবে দেওয়া হত লাল শাড়ি। শাড়ি পরার ক্ষেত্রে ধনী-গরিবের বিভাজন ছিল। ধনী মহিলাদের শাড়ি ছিল মলমলের মিহি কাপড়ের। আর গরিবের শাড়ি ছিল সাধারণ সুতি কাপড়ের; তাও আবার ছেঁড়া। প্রতিবেশীর কাছ থেকে ধার করা সুইয়ে তালি দেওয়া শাড়ি পরতে হত তাদের।
মুসলমানরা ভারতবর্ষে আগমনের ফলে এখানকার পোশাক-পরিচ্ছদেও পরিবর্তনের হাওয়া লাগে। তুর্কি-আফগান-মোগল সংস্কৃতির ধারা স্থানীয়রাও গ্রহণ করেছিল। পাগড়ি, সালোয়ার-কামিজ ও পায়জামার মতো পোশাক জনপ্রিয় হয়ে উঠলেও শাড়ির গুরুত্ব কমে যায়নি।
দক্ষিণ ও পূর্ব ভারতের নারীরা শাড়িকেই নিজেদের পরিধানের প্রধান বস্ত্র হিসেবে আঁকড়ে ধরেছে।
মোগলদের মধ্যে সম্রাট আকবরই ভারতবর্ষের সংস্কৃতির সঙ্গে খাপ খাওয়ানোর চেষ্টা শুরু করেন। তাঁর দেখানো পথ ধরেই হাঁটলেন মোগলরা। শুরু হল ভারতীয় নারীদের নিজের স্ত্রী হিসেবে গ্রহণ করার মধ্য দিয়ে মোগল সংস্কৃতির সঙ্গে ভারতীয় সংস্কৃতির এক মেলবন্ধন। সেই ধারাবাহিকতায় শাড়িতেও মোগলাই আভিজাত্যের সংযোজন। তবে সেসময়ে অভিজাতদের মধ্যে শাড়ি ব্যাপকভাবে চল না হলেও শাড়িতে আভিজাত্যের উপস্থিতি থাকত পূর্ণমাত্রায়।


মোগলদের বাংলা জয়ের পর ‘জমিদার’ একটি বিশেষ পদবি হিসেবে স্বীকৃতি পায়। জমিদার মানেই বিশাল সম্পত্তির মালিক। স্বাভাবিকভাবেই সেসময়ে ধনীক শ্রেণি হিসেবে ‘জমিদার’ গোষ্ঠীর আত্মপ্রকাশ ঘটে। তাদের সময়েও শাড়ি পরা হত এক প্যাঁচে। ব্লাউজও এ সময়ে শাড়ির গুরুত্বপূর্ণ অনুষঙ্গ হিসেবে আভির্ভূত হয়। তবে গ্রামবাংলার সাধারণ নারীরা কিন্তু সেই ধারা থেকে ছিলেন যোজন যোজন দূরে।
আর সেটাই ধরে পড়েছে মাদাম বেলনোসের আঁকা ছবিগুলোতে। তাঁর আঁকা ছবিগুলোতে চমৎকারভাবে পাওয়া যায় উনিশ শতকের প্রথমদিকে গ্রামবাংলার অন্তঃপুরের চালচিত্র। তাতে দেখা যায়, বাংলার নারীরা শাড়ি এক প্যাঁচেই পরেছেন। অধিকাংশের শাড়ির রংই সাদা। তবে শাড়ির পাড় হত চিকন এবং লাল রংয়ের। সায়া-ব্লাউজ কিংবা অন্তর্বাসের ব্যবহারের কোনো প্রচলন ছিল না। কখনও কখনও ঘোমটা দেওয়ার কাজটা সারা হতো সে শাড়ি দিয়েই।


তবে তখনও পেটিকোট বা সায়ার প্রচলন হয়নি অন্তত সাধারণ ঘরের নারীদের জন্য তো অবশ্যই।
উনিশ শতকের চল্লিশ দশক থেকে বিংশ শতাব্দী পর্যন্ত সময়কে বলা হয়ে থাকে ‘ভিক্টোরিয়ান যুগ’। এ যুগে ফ্যাশনের মূল কথাই ছিল কাপড়ে সারা শরীর ঢাকা থাকতে হবে। এমন কি গোড়ালি দেখানোটাও অসম্মানকর হিসেবে বিবেচিত হত। সেই সময় ভারতীয় গরমের মধ্যেও ইংরেজরা ভিক্টোরিয়ান ফ্যাশনের পোশাক পরতে পিছ পা হননি। ব্রিটিশদের সংস্পর্শে থাকা জমিদার ও স্থানীয় ধনীদের পোশাক-পরিচ্ছেদেও ভিক্টোরিয়ান ফ্যাশনের স্টাইল যোগ হয়। এ সময়ে শাড়ির সঙ্গে ফুলহাতা ব্লাউজ এবং পেটিকোট পরার চল শুরু হয়। এই রীতিতে শাড়ি পরাটা বাঙালি নারীর চিরায়ত এক প্যাঁচে শাড়ি পরার ধারণাকে সম্পূর্ণ পাল্টে দেয়। আর সেটা যে হুট করেই হয়েছিল তা কিন্তু নয়।
উনিশ শতকের ষাটের দশক থেকেই শাড়ি পরার ক্ষেত্রে পরিবর্তনের হাওয়াটা যে আসি আসি করছে তার একটা সূত্রপাত কিন্তু হয়ে গিয়েছিল। নারীরা যে ঊর্ধ্বাংশে কিছু পরে না, এজন্য বিরূপ মন্তব্য শুরু হয়ে গিয়েছিল সেসময়টা থেকেই।


এর সঙ্গে সুর মিলিয়ে ঠাকুরবাড়ির ছেলে সত্যেন্দ্রনাথ ঠাকুর ১৮৬৪ লিখেছেন, ‘আমাদের স্ত্রীলোকেরা যেরূপ কাপড় পরে, তাহা না পরিলেও হয়।’
আসলে আধুনিক শিক্ষায় শিক্ষিত বাঙালি তরুণরা নারীদের আধুনিকভাবে দেখতে চাইছিলেন। এই আধুনিকতার রেশ ধরেই বাঙালি সমাজে ব্লাউজ এবং সায়া বা পেটিকোটের প্রচলন শুরু হয়। আর তা শুরু হয় কলকাতার জোড়াসাঁকোর ঠাকুরবাড়ির অন্দরমহল থেকেই। শাড়ি এবং ব্লাউজ মিশ্রিত রূপটি এক ধরনের বিলাতি পোশাকের ভাব এনে দিত। ফুলহাতা ব্লাউজ এবং কুচিছাড়া শাড়িই ছিল সেই সময় উঁচু সমাজের নারীদের প্রধান ফ্যাশন।
১৮৮৯ সালে ঢাকা জেলার একটি হিন্দু পরিবারের ওপর করা জরিপে দেখা যায়, একজন মহিলা বছরে পাঁচ,ছয়টি শাড়ি ব্যবহার করে থাকেন। এগুলোর পেছনে খরচ হত চার রুপি।
ব্রিটিশ মুক্ত ভারতে ১৯৪০ দশকের শেষের দিক থেকে ১৯৫০ সাল পর্যন্ত সময়টা ছিল হিন্দি চলচ্চিত্রের ‘সোনালি সময়’। এসব চলচ্চিত্রের নায়িকারা ছিল তখনকার ফ্যাশন আইকন। নার্গিস, মধুবালা এবং বৈজয়ন্তীমালার মতো নায়িকাদের পোশাক ভাবনা ভারত উপমহাদেশের নারীদেরকে দারুণভাবে অনুপ্রাণিত করা শুরু করে।


গত শতাব্দীর ষাটের দশকের শেষ থেকে ১৯৭০ এর প্রথমদিকে হিপ্পীদের শ্লোগান ছিল ‘ফুলের শক্তি’। সমসাময়িক সময়ে চলা ভিয়েতনাম যুদ্ধের প্রতিবাদ আন্দোলনের প্রতীক হিসেবে ‘ফুল’ জনপ্রিয় হয়ে ওঠে। পোশাকের নকশাতেও থাকত ফুলের প্রাধান্য, এমনকি সাজসজ্জায়ও। পরে এ ফ্যাশনে নতুন মাত্রা আনে হিন্দি সিনেমাতে বহুল প্রচলিত ‘মিনি শাড়ি’। বিশেষ করে বলা যেতে পারে ‘সত্যম শিভম সুন্দরম’ সিনেমাতে জিনাত আমানের পরা শাড়ির কথা।
‘মিনি শাড়ি’র পাশাপাশি ‘টেডি শাড়ি’র কথাও বলা যায়, যা অভিনেত্রী মমতাজ জনপ্রিয় করেছিলেন। শাড়ির সঙ্গে সঙ্গে ব্লাউজেও লাগে পরিবর্তনের হাওয়া। ব্লাউজের গলা করা হয় অনেকটা নৌকার আদলে এবং ব্লাউজের পেছনের দিকটা আটকানোর জন্য থাকতো মেগি ক্যাপের বোতাম।
দীর্ঘ এক সংগ্রামের পর বাংলাদেশে ১৯৭১ সালে স্বাধীন এক দেশ হিসেবে আত্মপ্রকাশ করে। ফলে সত্তর দশকের শাড়ি শুধু দেশি, বিশেষ করে বললে বাংলাদেশি। বাংলাদেশিরা নিজেদের সবকিছুর মধ্যে বাংলাদেশকে উপস্থাপন করা শুরু করে। তাদের পছন্দের শাড়ির মধ্যে জায়গা করে নেয় দেশি খাদি এবং তাঁতের শাড়ি।


সত্তর দশকের শেষ থেকে আশি দশকের প্রথমভাগ, আন্তর্জাতিক সংগীতে একদিকে তখন ডায়ানা রসের ডিস্কো এবং অন্যদিকে পপ দল বিজিসের জনপ্রিয় গান। এসবের সঙ্গে যোগ হওয়া ঝলমলে, জমকালো এবং চকচকে পোশাক পরার রীতি এ সময়ের শাড়ির ধরণকে নির্ধারিত করে। ঠিক তখনই দৃশ্যপটে ঝড়ের মতো আগমন করে হিন্দি চলচ্চিত্র ‘সিলসিলা’য় অভিনেত্রী রেখার পরা ‘সিলসিলা’ শাড়ি। ঘন রংয়ের এই শাড়ির সঙ্গে পরা হত হাতাহীন কিংবা হোল্টার গলার ব্লাইজ। আর সাজসজ্জার অনুষঙ্গ হিসেবে থাকতো সুরমা, গাঢ় রংয়ের লিপষ্টিক আর চিকন ভ্রু। তাছাড়া এসময়ে শাড়ির সঙ্গে মিল করে কেশবিন্যাস নিয়েও পরীক্ষা-নীরিক্ষা হয়েছে। অনেকে পছন্দ করেছেন রেখার মতো চুলকে লম্বা করে ছেড়ে দিতে আবার কেউ বা অভিনেত্রী ফারাহ ফসেটের মতো ঢেউ খেলানো চুলই পছন্দ করেছেন।
২০০০ সালে এসে নতুন সহস্রাব্দ আমাদেরকে ব্যাপকভাবে পরিচয় করিয়ে দেয় বিশ্বায়নের সঙ্গে। এই বিশ্বায়নই আমাদেরকে একে অপরের কাছে নিয়ে আসে। কাছে নিয়ে আসে সংস্কৃতি এবং মানুষদেরকে,  যা কিনা এর আগে কল্পনাও করা যায়নি। উন্নত প্রযুক্তির কল্যাণে আজকের ফ্যাশন হচ্ছে বিভিন্ন উপাদানে প্রস্তুত এক নতুন ধারা। এমন কি হলিউড এবং বলিউড একে অপরের স্টাইল গ্রহণ করছে।


সময়ের সঙ্গে সঙ্গে শাড়ি আর ব্লাউজ নিয়ে নানারকম পরীক্ষা-নিরীক্ষা হয়েছে; এবং ভবিষ্যতেও হতে থাকবে।
বলা যেতে পারে-শাড়ির কয়েক হাজার বছরের ইতিহাসে ব্যাপক পরিবর্তন হয়েছে, তা বলা যাবে না। ভারতবর্ষের নারী তথা বাংলার নারীরা যুগ যুগ ধরে শাড়িকে তার প্রধান পরিধেয় বস্ত্র হিসেবে আঁকড়ে রেখেছে।
প্রচ্ছদের ছবি সৌজন্যে: কে ক্র্যাফট। অন্যান্য ছবি: লেখকের ব্যক্তিগত সংগ্রহ থেকে।
লেখকের প্রকাশিত বই: ঢাকার ঐতিহাসিক নিদর্শন, ঢাকার কোচোয়ানরা কোথায়, ড’য়লির ঢাকা, ঘটনা সত্যি, ঢাকাই খাবার, ঢাকা কোষ (যৌথ) বাংলাদেশ এশিয়াটিক সোসাইটি প্রকাশিত।
ঈদ যুগে যুগে
