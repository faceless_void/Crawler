দাওয়াতে প্রায় সময়ই বেশি খাওয়া হয়ে যায়। খালি পেটে হঠাৎ অনেক খাবার যাওয়ার ফলে শরীর ক্লান্ত লাগে। অনেক সময় অসুস্থবোধও হয়। আর ওজন বাড়ার কথা না হয় বাদই গেল।
ইফতারের দাওয়াতে কম খেয়েও শরীরে সঠিক পুষ্টি যোগানের কৌশল জানাচ্ছেন জনস্বাস্থ্য পুষ্টিবিদ আসফিয়া আজিম।
এখন বেশিরভাগ দাওয়াতেই ব্যুফে পদ্ধতিতে খাবার পরিবেশন করা হয়। আবার ‘ওয়ান ডিশ পার্টি’র আয়োজনও হয়ে থাকে। চোখের সামনে খাবারের বিশাল আয়োজন দেখে প্লেট ভরে খাবার তুলে নেই আমরা।
স্বাস্থ্য বা পুষ্টির কথা ভুলে এমন সব খাবারে ভরিয়ে ফেলি নিজেদের প্লেট, যা কেবলই সুস্বাস্থ্যে অন্তরায়। তাই ছোট দুটি কৌশল মনে রাখুন ইফতারের দাওয়াতের জন্য। খাবার প্লেটে নেওয়ার আগে নিজেকে প্রশ্ন করুন-
১. আমি আসলে কী খেতে চাই?
২. আজকের মেন্যুতে বিশেষ খাবার কোনটি?
দাওয়াতে থাকে হরেক রকম ইফতারির আয়োজন। এরমধ্যে অধিকাংশই তেলে ভাজা ও মিষ্টিজাতীয় খাবার। সারা দিনের অনাহারের পর উদোরপূর্তি বা ক্ষুধা নিবৃত্তির দিকেই নজর থাকে বেশি। তাই দাওয়াতে পুষ্টির কথা ভুলে মুখরোচক খাবারের প্রতিই আগ্রহী হয়ে উঠি আমরা।
তবে একটু সচেতন হয়ে প্লেটে খাবার তুলে নিলেই মজাদার অথচ পুষ্টিকর খাবার গ্রহণের পাশাপাশি দাওয়াত রক্ষা করা যায়।
যেমন: ইফতারের দাওয়াতে তেলে ভাজা খাবারের পরিমাণ কমিয়ে গ্রিলড বা বেইকড খাবার পাতে তুলে নিতে পারেন। কারণ খালি পেটে তেলে ভাজা খাবার পাকস্থলির ক্ষতি করে।
যদি দাওয়াতে জিলাপি ও ফ্রুট সালাদ থাকে, তবে সালাদ খাওয়াই উত্তম। জিলাপিও ভাজা হয় তেলে।
সারাদিনের অনাহারের পর বারবার পানীয় ধরনের কিছু খেতে ইচ্ছে করে। আবার বেশি পানি খেয়ে পেট ভরালেও বিপদ। তাই ইফতারের দাওয়াতে টমেটো, শসা বা ফলের সালাদ খান। এগুলো শরীরে তাৎক্ষণিক এনার্জি দেওয়ার পাশাপাশি পুষ্টিরও জোগান দেয়। আর এসব সবজি ও ফলে পানির পরিমাণ বেশি থাকায় শরীরে পানির অভাব পূরণেও ভীষণভাবে কার্যকর।
দাওয়াতে ইফতার শেষে বন্ধু বা আত্মীয়ের সঙ্গে গল্প করার ফাঁকে চায়ে চুমুক দেওয়া আমাদের অভ্যস্ত জীবনের অংশ।
তবে মনে রাখা দরকার, কোলা বা কফি, চা এবং অতিরিক্ত চিনি বা মিষ্টিজাতীয় পানীয় শরীরকে আরও বেশি পানিশূন্য করে তোলে। তাই দাওয়াতে চা পান করুন অল্প চিনি দিয়ে। আর দোকানের কোমল পানীয়র পাশাপাশি যদি ফল বা লেবুর শরবত থাকে তবে সেগুলোই পান করার চেষ্টা করুন।
তাই ইফতারের দাওয়াতে অন্যকোনো পানীয় রাখার চাইতে পুদিনা-লেবুর শরবত, বেলের শরবত, তোকমা বা কাঁচাআমের শরবত, তেঁতুলের শরবত ইত্যাদি রাথলে রোজাদারদের জন্যই ভালো।
