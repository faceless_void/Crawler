স্বাস্থবিষয়ক একটি ওয়েবসাইটের প্রতিবেদনে জানানো হয়, পেটের ভেতরে জমে যাওয়া বের করে দেওয়ার একটি পদ্ধতি হল ঢেকুর ওঠা। যার দুটি প্রধান কারণ হল খাওয়ার সময় বেশি বাতাস গিলে ফেলা কিংবা হজমের সমস্যা। কারণ যাই হোক, জমে যাওয়া বাড়তি গ্যাস থেকে পেট ব্যথা হতে পারে। মাঝে মধ্যে ঢেকুর ওঠা ভালো তবে নিয়মিত হলে হজমের সমস্যা থাকার আশঙ্কা আছে এবং এ বিষয়ে চিকিৎসকের পরামর্শ নিতে হবে।
তবে বার বার ঢেকুর তোলা ঠেকাতে রয়েছে প্রাকৃতিক উপায়।
পুদিনা-পাতা: এতে থাকা ‘অ্যান্টিস্পাসমোডিক’ উপাদান হজমনালীকে শিথিল করে, যা পেটে গ্যাস সৃষ্টির আশঙ্কা কমায়। পাশাপাশি যকৃতের পাচক রস উন্নত করতে এবং হজমশক্তি বাড়াতেও এটি সহায়ক।
খাওয়ার পদ্ধতি: এক চা-চামচ শুকনা পুদিনা-পাতা এক কাপ গরম পানিতে ১০ মিনিট রেখে দিন। প্রতিদিন তিন থেকে চারবার এই পানি পান করুন।
দই: একটি ‘প্রোবায়োটিক’ যা অন্ত্রে ব্যাক্টিরিয়ার পরিমাণের ভারসাম্য বজায় রাখে। ব্যাক্টেরিয়ার পরিমাণের ভারসাম্য নষ্টই পেটে গ্যাস হওয়ার এবং ঢেকুর ওঠার একটি কারণ। ‘প্রোবায়োটিক’ খাবারগুলো হজমের বিভিন্ন সমস্যা যেমন, কোষ্ঠকাঠিন্য, ডায়রিয়া, পেট ফোলা ইত্যাদি দূর করতেও সহায়ক।
প্রতিদিনে কমপক্ষে একবার খাওয়ার পর দই খাওয়ার অভ্যাস করতে পারেন।
ক্যামোমাইল চা: পেটে জমা হওয়া গ্যাস ঢেকুরের পরিবর্তে শারীরিক নিয়মে বের হতে সাহায্য করে ক্যামোমাইল চা। পেট ব্যথা কমাতেই সহায়ক এটি। উপকার পেতে প্রতিদিন দুতিন কাপ ক্যামোমাইল চা খেতে পারেন। 
