খাগড়াছড়ি: মাধ্যমিক স্কুল সার্টিফিকেট (এসএসসি) পরীক্ষায় খাগড়াছড়ির পানছড়ির মডেল সরকারি মাধ্যমিক বিদ্যালয় কেন্দ্রে ভুল প্রশ্নে পরীক্ষা দিয়েছে ৪৭জন শিক্ষার্থী।
সোমবার (০৫ ফেব্রুয়ারি) ইংরেজি প্রথমপত্রের পরীক্ষায় এ অপ্রত্যাশিত ঘটনা ঘটে।
জানা যায়, এ দিনে পানছড়ি মডেল সরকারি মাধ্যমিক বিদ্যালয় কেন্দ্রে ৪৭ জন অনিয়নিত পরীক্ষার্থী ছিলো। তাদের জন্য আলাদা প্রশ্ন থাকলেও ভুলে নিয়মিত পরীক্ষার্থীদের প্রশ্নেই তাদের পরীক্ষা নেওয়া হয়। পরীক্ষা শেষে বিষয়টি পারে শিক্ষার্থীরা। এ বিষয়ে স্ব-স্ব বিদ্যালয়ের শিক্ষক ও অভিভাবকদের মধ্যে হতাশা বিরাজ করছে। এ ঘটনায় অভিভাবকদের মধ্যে ক্ষোভ বিরাজ করছে।
পানছড়ির নালকাটা উচ্চ বিদ্যালয়ের ৬জন, পুজগাংমুখ উচ্চ বিদ্যালয়ের ২১জন ও পানছড়ি বাজার উচ্চ বিদ্যালয়ের ২০জন অনিয়মিত শিক্ষার্থী এ ভুল প্রশ্নে পরীক্ষা দিয়েছে।
পানছড়ি-১ এর কেন্দ্র সচিব বেলী চাকমা বাংলানিউজকে জানান, ‘প্রশ্নপত্রের প্যাকেটে পুরাতন পরীক্ষার্থী লেখাটা বুঝতে না পারায় ভুলটা হয়েছে। এ ব্যাপারে বোর্ডে আবেদন করা হবে।
পরীক্ষা পরিচালনা কমিটির সভাপতি ও উপজেলা নির্বাহী কর্মকর্তা (ইউএনও) মুহাম্মদ আবুল হাশেম জানান, বিষয়টি শোনার পর আমি ঊর্ধ্বতন কর্মকর্তাদের অবগত করেছি। এ বিষয়ে আবেদন পাঠানোর প্রস্তুতি চলছে।
