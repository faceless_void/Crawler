চর্বিতে (সঙ্গে প্রোটিন এবং কার্বোহাইড্রেইটস) সঞ্চিত থাকে শক্তি। ক্যালরি হচ্ছে চর্বি, প্রোটিন এবং কার্বোহাইড্রেইটস থেকে তৈরি হওয়া শক্তি পরিমাপের একক। বিভিন্ন ধাপে রাসায়নিক বিক্রিয়ায় শরীরে জমে থাকা ফ্যাট বা চর্বি থেকে শক্তি তৈরি হয়। আর খরচ না হওয়া শক্তি জমে থাকে শরীরে। ওজন কমাতে তাই যত গ্রহণ করছেন তার চেয়ে বেশি ক্যালরি বা শক্তি খরচ করতে হবে।
যখনই খাবার খাওয়ার চাইতেও বেশি শক্তি খরচ হবে তখন শরীরের ভেতরে চর্বির কোষ কমতে থাকবে। এটা অনেকটা পানি বাষ্প হয়ে কমে যাওয়ার মতোই ব্যাপার। আর এই প্রক্রিয়াতেই ওজন কমানো সম্ভব।
উপরের প্রক্রিয়া ওজন কমানোর সার্বজনীন পন্থা হিসেবে স্বীকৃত। তবে এই প্রক্রিয়া আরও দ্রুত করতে রয়েছে প্রাকৃতিক উপায়।
স্বাস্থবিষয়ক একটি ওয়েবসাইটে এই বিষয়ে প্রকাশিত প্রতিবেদন অবলম্বনে প্রাকৃতিকভাবে চর্বি ঝরানোর কিছু পন্থা এখানে দেওয়া হল।


এই চা তৈরি করতে প্রয়োজন আধা ইঞ্চি আদা পরিষ্কার করে কুঁচি করে কাটা, ১ চা-চামচ গ্রিন টি, পরিমাণ মতো পানি এবং ভেষজ মধু স্বাদ মতো।
পানি ফুটিয়ে এর মধ্যে গ্রিন টি’র পাতা ও আদা দিয়ে কিছুক্ষণ অপেক্ষা করে ছেঁকে নিন। সঙ্গে মধু মিশিয়ে পান করুন। দিনে দুতিন কাপ এই চা পান করা উপকারী।


রক্তে শর্করার এই মাত্রা বজায় রাখতে দারুচিনি দারুণ কার্যকর। এই মিশ্রণ তৈরি করতে লাগবে এক টেবিল-চামচ দারুচিনি গুঁড়া, একটি আস্ত দারুচিনি এবং আট আউন্স বিশুদ্ধ পানি।
পানি গরম করে তাতে দারুচিনি মিশিয়ে কিছুক্ষণ রেখে দিন। কুসুম গরম হলে পান করুন। দিনে দুতিনবার এই পানীয় পান করুন।
গোলাপের পাপড়ির পানি: গোলাপের পাপড়ির পানি বা গোলাপ জল ত্বকের যত্নে অতুলনীয়, এটা কমবেশি সবারই জানা। তবে এই পানীয় যে শরীরের জন্যও উপকারী সে সম্পর্কে অনেকেরই ধারণা নেই।


পান করার জন্য এই পানীয় তৈরি করতে প্রয়োজন হবে ভালোভাবে পরিষ্কার করে নেওয়া একমুঠ তাজা বা শুকনা গোলাপের পাপড়ি, পরিমাণ মতো খাবার পানি এবং মুখ বন্ধ পাত্র।
পাত্রে পর্যাপ্ত পরিমাণ পানি দিয়ে তাতে গোলাপের পাপড়ি ছড়িয়ে পাত্রের মুখ আটকে ২০ মিনিট ফুটিয়ে নিন। এই পানীয় ঠাণ্ডা করে ছয় দিন পর্যন্ত ফ্রিজে রেখে সংরক্ষণ করা যায়। প্রতিদিন খালি পেটে এক কাপ করে ওই পানি পান করুন।


পানি পান: ওজন কমানোর অন্যতম একটি উপায় হল পানি পান। পর্যাপ্ত পানি শরীরের দূষিত পদার্থ ধুয়ে ফেলার পাশাপাশি হজম ক্ষমতা বাড়াতেও সাহায্য করে। প্রতিদিন অন্তত আট গ্লাস পরিমাণ পানি পান করা উচিত।
পর্যাপ্ত ঘুম: শরীরের স্বাভাবিক কার্যপ্রক্রিয়া ধরে রাখতে এবং বিশ্রামের জন্য পর্যাপ্ত ঘুম দরকার। ঘুম শরীরের প্রতিটি অংশের মধ্যে কাজের সামঞ্জস্যতা বজায় রাখতে সাহায্য করে। তাই সুস্থ থাকতে সঠিক মাত্রায় ঘুম অত্যন্ত গুরুত্বপূর্ণ। দীর্ঘদিন ঘুমে অনিয়ম হলে শরীরে ইনসুলিনের মাত্রা বেড়ে যায় যা থেকে ওজন বৃদ্ধির ঝুঁকি দেখা দিতে পারে। তাই সুস্থ থাকতে এবং ওজন নিয়ন্ত্রণে রাখতে নিয়ম করে ঘুমাতে হবে।


দুই কাপ দইয়ের সঙ্গে এক টেবিল-চামচ মধু মিশিয়ে সকালের নাস্তার সঙ্গে খেতে পারেন। স্বাদের ভিন্নতা আনতে পছন্দের ফল বা ওটসের সঙ্গে মিশিয়েও খাওয়া যেতে পারে।
গোলমরিচ ও লেবুর রস: লেবুর রসের সঙ্গে গোলমরিচের গুঁড়ার মিশ্রণ ওজন কমানোর প্রক্রিয়ায় সহায়তা করে। গোলমরিচে রয়েছে পিপারিন নামক উপাদান যা থেকে এর ঝাঁঝালো স্বাদ হয়ে থাকে। এই উপাদান চর্বি জমার প্রক্রিয়ায় ব্যাঘাত ঘটায়। তাছাড়া রক্তের চর্বির মাত্রা নিয়ন্ত্রণে রাখতেও গোলমরিচ বেশ উপকারী। অন্যদিকে লেবু হজমে সহায়তা করে। ফলে ওজন কমতে সাহায্য করে।
এই শরবত তৈরি করতে প্রয়োজন হবে অর্ধেক লেবুর রস, অল্প পরিমাণ গোলমরিচের গুঁড়া এবং পরিমাণ মতো খাবার পানি।
এই মিশ্রণ প্রতিদিন একবার খাবার পরে পান করতে হবে।


যেকোনো খাদ্যাভ্যাসের পাশাপাশি পর্যাপ্ত ব্যায়াম ওজন কমাতে সহায়ক। খাবার কমানোর পরও অনেক ক্ষেত্রে ব্যায়াম না করার কারণে ওজন কমানো সম্ভব হয় না।
ছবি: রয়টার্স ও নিজস্ব।
আরও পড়ুন
দারুচিনির যত গুণ
ওজন কমানোর ৩ পন্থা  
