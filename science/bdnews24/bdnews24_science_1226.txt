বিবিসি’র প্রতিবেদনে বলা হয়, কিছু গ্রাহক অভিযোগ করেছেন যে যখনই তারা আই লেখার চেষ্টা করছেন তখন তা স্বয়ংক্রিয়-সংশোধন হয়ে ক্যাপিটাল লেটার ‘এ’ এবং ‘প্রশ্নবোধক চিহ্ন’তে পরিবর্তিত হচ্ছে।
আইফোন ও আইপ্যাডের আইওএস ১১.১-এ এই ত্রুটি দেখা গেছে বলে জানানো হয়।
মাইক্রোব্লগিং সাইট টুইটারে প্রযুক্তি প্রতিবেদক মাইক মারফি বলেন, “আমার একটি ১১৫০ মার্কিন ডলারের ফোন রয়েছে যা ‘আই’ অক্ষর পড়তে পারে না।”
প্রতিষ্ঠানের ওয়েবসাইটে সাময়িকভাবে এটি সাড়ানোর জন্য উপায় জানিয়েছে অ্যাপল।
এতে আইওএস ডিভাইসের কিবোর্ড সেটিংস পরিবর্তন করতে বলেছে মার্কিন প্রযুক্তি জায়ান্ট প্রতিষ্ঠানটি, যাতে ক্যাপিটাল বা স্মল লেটারের সঠিক ‘আই অক্ষর’ ব্যবহার করা যায়।
প্রতিষ্ঠানের পক্ষ থেকে বলা হয় পরবর্তী সফটওয়্যার আপডেটে ত্রুটি সাড়ানো হবে।
সব আইওএস ১১.১ গ্রাহক এই ত্রুটির শিকার হননি। তবে, অনলাইনে এমন অনেক মন্তব্যই পাওয়া গেছে যাতে বলা হচ্ছে তারা এই সমস্যার মুখে পড়েছেন।
অ্যাপলের অনলাইন আলোচনা ফোরামে এক গ্রাহক বলেন, “এটি খুবই বিরক্তিকর।”
