তবে ঘরে ব্যবহৃত কিছু সহজলভ্য উপাদান দিয়ে চুল পড়া প্রতিরোধ উপযোগী কিছু মাস্ক তৈরি করা সম্ভব। রূপচর্চা বিষয়ক একটি ওয়েবসাইটে চুলের জন্য আদর্শ কিছু মাস্কের বিষয়ে উল্লেখ করা হয়।
১. মধু  ও ক্যাস্টর অয়েলের প্যাক
মধু হচ্ছে প্রাকৃতিক হাইড্রেটিং এজেন্ট যা চুলের আদ্রতা ধরে রাখতে সাহায্য করে। পাশাপাশি চুল পড়া কমাতেও মধুর জুড়ি নেই।
তৈরি করতে লাগবে: মধু ১ টেবিল-চামচ। ডিমের কুসুম। ক্যাস্টর অয়েল। ভিটামিন এ এবং ই ক্যাপসুল।
পদ্ধতি: এই মিশ্রণ তৈরি করতে প্রথমে ১ টেবিল-চামচ মধুর সঙ্গে ক্যাস্টর অয়েল ও ডিমের কুসুম মিশিয়ে নিতে হবে। সঙ্গে ভিটামিন এ এবং ভিটামিন ই ক্যাপসুল মিশিয়ে নিতে হবে। মিশ্রণটি মাথার ত্বক ও চুলে লাগিয়ে ১টি গরম তোয়ালে দিয়ে পুরো মাথা পেঁচিয়ে এক ঘণ্টা অপেক্ষা করে ধুয়ে ফেলতে হবে। চুল পরিষ্কার করতে বেবি শ্যাম্পু বা মাইল্ড শ্যাপু ব্যবহার করা ভালো।
২. মধু, অ্যালোভেরা ও রসুনের প্যাক
তৈরি করতে লাগবে: রসুনের রস ১ চা-চামচ। অ্যালোভেরার রস ২ টেবিল-চামচ। মধু ১ টেবিল-চামচ। ডিমের কুসুম ১ টেবিল-চামচ।
পদ্ধতি: মধুর সঙ্গে ১টি ডিমের কুসুম, অ্যালোভেরা বা ঘৃতকুমারীর রস ও ১ টেবিল-চামচ রসুনের রস মিশিয়ে নিতে হবে। এই মিশ্রণ মাথার ত্বক ও চুলে লাগিয়ে ২০ মিনিট অপেক্ষা করে ধুয়ে ফেললে চুল হবে মসৃণ ও উজ্জ্বল।
চাইলে মধু, অ্যালোভেরা ও রসুনের রস একসঙ্গে মিশিয়ে ফ্রিজে সংরক্ষণ করা যাবে এবং মিশ্রণটি ব্যবহারের আগে ডিমের কুসুম মিশিয়ে নিয়ে ব্যবহার করা যাবে।
৩. ডিমের কুসুম ও পেঁয়াজের মাস্ক
তৈরি করতে লাগবে: ডিমের কুসুম ১টি। অলিভ অয়েল ১ টেবিল-চামচ। পেঁয়াজের রস ২ টেবিল-চামচ। মধু ১ চা-চামচ।
পদ্ধতি: সবগুলো মিশ্রণ মিশিয়ে চুলের গোড়ায় ভালোভাবে মাখাতে হবে। এরপর পুরো চুলে মাখিয়ে নিতে হবে। ঘণ্টাখানেক পর শ্যাম্পু করে ধুয়ে কন্ডিশনার ব্যবহার করতে হবে।
৪. ডিমের কুসুম ও দইয়ের মাস্ক
তৈরি করতে লাগবে: দই ১ কাপের চার ভাগের ১ ভাগ। ডিম ১টি। মেয়োনেইজ ১ কাপের চার ভাগের ১ ভাগ।
পদ্ধতি: ডিম ফেটিয়ে ফোম তৈরি করে নিতে হবে। এর সঙ্গে দই ও মেয়োনেইজ মিশিয়ে নিন। মাথায় ভালোভাবে এই মিশ্রণ লাগিয়ে একটি প্লাস্টিকের ক্যাপ দিয়ে চুল ঢেকে রাখতে হবে। একঘণ্টা অপেক্ষা করতে হবে। এরপর মাইল্ড শ্যাম্পু দিয়ে চুল ধুয়ে ফেলতে হবে।
 
