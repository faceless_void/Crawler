রেসিপি দিয়েছেন ইশরাত সুলতানা।
উপকরণ: ডিম ৩টি। ময়দা ১/৩ ভাগ। চিনি ১/৩ ভাগ। তেল ২ টেবিল-চামচ। গুঁড়াদুধ ২ টেবিল-চামচ। লবণ এক চিমটি। যে কোনো ফ্লেইভারের জ্যাম অথবা জেলি ৪ টেবিল-চামচ (চামচ দিয়ে ফেটে নিতে হবে)। ভ্যানিলা এসেন্স ১/৪ চামচ।
পদ্ধতি: ডিমের সাদা অংশ ও কুসুম আলাদা করে রাখুন। ময়দা, লবণ আর গুঁড়াদুধ মিশিয়ে রাখুন।
এবার ডিমের সাদা অংশ ফেটিয়ে অল্প অল্প চিনি দিয়ে ফোম করুন। তারপর কুসুমগুলো দিয়ে বিট করুন ভালো ভাবে।
তেল আর এসেন্স দিয়ে ধীরে ধীরে বিট করুন। তারপর ময়দার মিশ্রণ অল্প অল্প করে ডিমের মিশ্রণে মেশান। তবে খেয়াল রাখবেন যেন ফোম নষ্ট না হয়। স্পেচুলা দিয়ে মেশালে ভালো হয়।
এবার বেইকিং ট্রেতে তেল ব্রাশ করে বেইকিং পেপার বিছিয়ে কেকের মিশ্রণ ঢেলে প্রি হিটেড ওভেনে ১৮০ ডিগ্রি সেলসিয়াসে ২০ মিনিট বেইক করতে হবে। হয়ে গেলে ওভেন থেকে বের করে দুতিন মিনিট পর কেকটা একটা ছোট ভেজা তোয়ালে অথবা ক্লিং ফিল্মের উপর রেখে সাবধানে রোল করে নিন।
