বিশ্বের বৃহত্তম এই সামাজিক যোগাযোগ মাধ্যম জানিয়েছে, তারা আত্মহত্যা প্রতিরোধী টুলের সঙ্গে লাইভ স্ট্রিমিং সেবা, ফেইসবুক লাইভ ও মেসেঞ্জার সেবা একীভূত করার পরিকল্পনা করছে।
কৃত্রিম বুদ্ধিমত্তা আত্মহত্যার প্রবণতা রয়েছে এমন ব্যবহারকারীদের খুঁজে বের করতে সাহায্য করবে বলে বুধবার এক ব্লগপোস্টে জানিয়েছে প্রতিষ্ঠানটি। ইতোমধ্যেই লাইভ ভিডিও স্ট্রিমিং এর মধ্যে আক্রমণাত্মক আর আপত্তিকর উপাদান পর্যবেক্ষণ করার জন্য কৃত্রিম বুদ্ধিমত্তা ব্যবহার করছে ফেইসবুক।
মার্কিন দৈনিক নিউ ইয়র্ক পোস্টের প্রতিবেদন অনুযায়ী, আগের জানুয়ারিতে ফ্লোরিডার এক ১৪ বছর বয়সী বালিকা ফেইসবুক লাইভে তার আত্মহত্যার ভিডিও সরাসরি সম্প্রচার করেন।
ফেইসবুকের ভাষ্য অনুযায়ী, আপডেটেড টুলস লাইভ ভিডিও দেখছেন এমন ব্যবহারকারীদের সরাসরি যিনি সম্প্রচার করছে তার নিকট পৌঁছাবার এবং ফেইসবুকের কাছে ভিডিওটি সম্পর্কে রিপোর্ট করার সুযোগ করে দেবে।
