রাবি: রাজশাহী বিশ্ববিদ্যালয়ের (রাবি) প্রক্টর প্রফেসর মজিবুল হক আজাদ খান পদত্যাগ করেছেন। 
রোববার (৬ আগস্ট) সকাল সাড়ে ১০টায় তিনি বিশ্ববিদ্যালয়ের উপাচার্য প্রফেসর এম আব্দুস সোবহানের কাছে পদত্যাগপত্র জমা দেন।
বিশ্ববিদ্যালয়ের উপাচার্য প্রফেসর এম আব্দুস সোবহান বাংলানিউজকে বলেন, ব্যক্তিগত কারণ দেখিয়ে তিনি পদত্যাগপত্র দিয়েছেন। তার পদত্যাগপত্র গ্রহণ করা হয়েছে। এখন পরবর্তী প্রক্টর হিসেবে অন্য কাউকে দায়িত্ব দেয়া হবে।
প্রফেসর মজিবুল হক আজাদ খান বাংলানিউজকে বলেন, আজ সকালে পদত্যাগপত্র দিয়েছি। আমাকে প্রক্টরের দায়িত্ব থেকে অব্যাহতি দেয়ার অনুরোধ জানিয়েছি। এর চেয়ে বেশি কিছু বলবো না। 
২০১৬ সালের ২৯ মার্চ প্রফেসর মজিবুল হক আজাদ খানকে বিশ্ববিদ্যালয়ের প্রক্টরের দায়িত্ব দেন তৎকালীন উপাচার্য প্রফেসর মুহম্মদ মিজানউদ্দিন।
