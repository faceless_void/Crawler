বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজের ‘খাদ্য ও পুষ্টি বিজ্ঞান’ বিভাগের প্রধান ফারাহ মাসুদা সুস্বাস্থ্যের জন্য প্রয়োজনীয় খাদ্যাভ্যাস সম্পর্কে পরামর্শ দেন।
সুস্থ থাকার জন্য সুষম খাবার খাওয়ার কোনো বিকল্প নেই। প্রতিদিনের খাবার তালিকায় খাদ্যের ছয়টি উপাদান- শর্করা, স্নেহ, প্রোটিন, ভিটামিন, খনিজ ও পানি- যেন সঠিক পরিমাণে থাকে সেদিকে খেয়াল রাখতে হবে। সুষম খাবার, পর্যাপ্ত পানি এবং হালকা শরীরচর্চা শরীর সুস্থ রাখার জন্য যথেষ্ট।
বিভিন্ন ধরনের খাবার সম্পর্কে এই পুষ্টিবিদের পরামর্শ হল-
কার্বোহাইড্রেইট: শরীরে শক্তি সরবারহের জন্য যথেষ্ট পরিমাণ ক্যালরি সমৃদ্ধ খাবার খেতে হবে। জটিল শর্করা অর্থাৎ শস্যজাতীয় খাবার, যব, ডাল, বাদাম, বীজ ইত্যাদি খাওয়া স্বাস্থ্যসম্মত।
উচ্চ গ্লাইসেমিক খাদ্য যেমন- ময়দা, আলু, চকলেট, জ্যাম, বিস্কুট ইত্যাদি না খাওয়াই ভালো। দেহের পুষ্টির জন্য প্রয়োজন প্রোটিন, বিশেষ করে বাড়ন্ত শিশু ও গর্ভবতী মায়ের। তবে প্রোটিন গ্রহণের বিষয়ে সচেতন হতে হবে।
প্রোটিনের জন্য সর তোলা দুধ ও টক দই উপকারী। সব ধরনের মাছ (চিংড়ি বাদ) বিশেষ করে সামদ্রিক মাছ খাওয়া ভালো। চর্বিহীন মাংস বা চামড়া ছাড়া মুরগির মাংস খাওয়া স্বাস্থ্যকর। তবে অবশ্যই ১৮০ গ্রামের বেশি খাওয়া যাবে না।
বাদ দিতে হবে- কলিজা, মগজ, খাসির মাংস ইত্যাদিতে কোলেস্টেরল ও সম্পৃক্ত চর্বি থাকে। তাই এসব খাবার বাদ দিতে হবে। বাদামে চর্বি বেশি থাকায় প্রতিদিন না খাওয়াই ভালো।
তেল- শস্যের তিন শতাংশ ফ্যাট। দৈনিক ৫০০ গ্রাম শস্য গ্রহণ করা হলে ১৫ গ্রাম চর্বি পাওয়া যায়। তবে ভালো স্বাস্থ্যের জন্য অ্যান্টিঅক্সিডেন্ট সমৃদ্ধ তেল ব্যবহার করা ভালো।


ভিটামিন- সুস্থ শরীরের জন্য প্রচুর পরিমাণে ভিটামিন প্রয়োজন। মৌসুমি ফল ও শাকসবজি থেকে ভিটামিন পাওয়া যায়। তাই প্রতিদিনের খাদ্য তালিকায় তাজা ফল রাখা উচিত।
পানি- প্রাপ্ত বয়স্ক মানুষের জন্য দৈনিক আট গ্লাস পানি পান করা উচিত। এতে শরীর আর্দ্র থাকে। পাশাপাশি ত্বক ও চুল হয় মসৃণ।
