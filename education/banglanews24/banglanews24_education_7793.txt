যশোর: প্রধানমন্ত্রীর অর্থনৈতিক বিষয়ক উপদেষ্টা ড. মসিউর রহমান বলেছেন, ভবিষ্যত বাংলাদেশের জন্য আমরা যে স্বপ্ন দেখছি তা বাস্তবায়নে বিজ্ঞান চর্চা জরুরি। বিজ্ঞান ও প্রযুক্তি ব্যতীত উন্নতি সম্ভব নয় বলেই বিদেশি বিশ্ববিদ্যালয়ে বিজ্ঞান ও প্রযুক্তির গুরুত্ব বেশি।
বুধবার (২৫ জানুয়ারি) দুপুরে যশোর বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয় (যবিপ্রবি) ক্যাম্পাসে ১০ম বিশ্ববিদ্যালয় দিবস উপলক্ষে আয়োজিত সভায় প্রধান অতিথির বক্তব্যে একথা বলেন তিনি।
যবিপ্রবি ভিসি প্রফেসর ড. আব্দুস সাত্তারের সভাপতিত্বে সভায় বিশেষ অতিথি ছিলেন যশোর-৩ (সদর) আসনের সংসদ সদস্য ও যবিপ্রবি রিজেন্ট বোর্ডের সদস্য কাজী নাবিল আহমেদ।
এর আগে বেলা সাড়ে ১১টার দিকে যবিপ্রবি ১০ম প্রতিষ্ঠাবার্ষিকীতে একটি বর্ণাঢ্য শোভাযাত্রা ক্যাম্পাস প্রদক্ষিণ করে।
শোভাযাত্রা শেষে বেলুন ও কবুতর উড়িয়ে বিশ্ববিদ্যালয় দিবসের বর্ণাঢ্য কর্মসূচির উদ্বোধন করেন ড. মসিউর রহমান।
পরে প্রধান অতিথি পিঠা উৎসবের উদ্বোধন করে স্টলগুলো ঘুরে দেখেন।
এসময় যশোর-৩ (সদর) আসনের সংসদ সদস্য কাজী নাবিল আহমেদ ও যবিপ্রবি উপাচার্য প্রফেসর ড. আবদুস সাত্তার, শিক্ষক, শিক্ষার্থী, অভিভাবকরা অংশ নেন।
২০০৭ সালের ২৫ জানুয়ারি যশোর বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ের যাত্রা শুরু হয়। বর্তমানে তিন সহস্রাধিক শিক্ষার্থী বিশ্ববিদ্যালয়ে অধ্যয়ন করছে।
