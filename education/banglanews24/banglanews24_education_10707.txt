ঢাকা: বাল্টিক সাগরের তীরে দক্ষিণ ইউরোপে অপরূপ সৌন্দর্যের এক দেশ লিথুয়ানিয়া। ইউরোপিয়ান ইউনিয়নভুক্ত এ দেশে সফলভাবে পড়াশোনা শেষে রয়েছে স্থায়ীভাবে বসবাসের সুযোগ। সেনজেনভুক্ত একটি রাষ্ট্রে উন্নত পড়াশোনার মাধ্যমে আপনার সামনে খুলে যাবে পুরো ইউরোপের দরজা। স্নাতক ও স্নাতকোত্তর পড়াশোনার সুযোগ রয়েছে লিথুয়ানিয়া। এর রাজধানী ভিলনিয়াস। এর পার্শ্ববর্তী দেশ লাটভিয়া, ডেনমার্ক, সুইডেন এবং পোল্যান্ড। লিথুয়ানিয়ার ক্লাইপেডিয়া ইউনিভার্সিটি দিচ্ছে স্বল্প খরচে মানসম্মত শিক্ষার সুযোগ। ভর্তি ফি, টিউশন ফি, বিমানভাড়াসহ সাড়ে ৪ থেকে সাড়ে ৬ লাখ টাকা খরচ করেই লিথুয়ানিয়ায় উচ্চ শিক্ষা নিতে পারবেন বাংলাদেশের শিক্ষার্থীরা। ঢাকার ফেইথ কনসালটেন্ট লিমিটেডের চেয়ারম্যান রাকিবুল ইসলাম বলেন, ঝামেলাবিহীন প্রক্রিয়ার মাধ্যমেই একজন শিক্ষার্থী ক্লাইপেডিয়া ইউনিভার্সিটিতে ভর্তি হতে পারেন। শিক্ষার্থীকে প্রথমে নিজের পছন্দের বিষয় বাছাই করতে হবে। এ বিশ্ববিদ্যালয়ে বিজ্ঞান, মানবিক ও বাণিজ্যের শিক্ষার্থীদের জন্যে রয়েছে আকর্ষণীয় সব বিষয়। বিজনেস ম্যানেজমেন্ট, ইলেকট্রনিক্স অ্যান্ড ইঞ্জিনিয়ারিং, মেরিন ইঞ্জিনিয়ারিং, নার্সিং, হোটেল ম্যানেজমেন্টসহ বিশ্ববিদ্যালয়টিতে রয়েছে শত‍াধিক বিষয়। বিষয় বাছাইয়ের পর শিক্ষা জীবনের সব পরীক্ষার তথ্য এবং পাসপোর্টের তথ্য পাঠাতে হবে ইউনিভার্সিটিতে। একই সঙ্গে ২০০ ইউরো অ্যাপ্লিকেশন ফি জমা দিতে হবে ব্যাংকের মাধ্যমে। লিথুয়ানিয়ায় শিক্ষার্থীর এপ্রোভাল লেটারকে বলা হয়-এসকেভিসি লেটার। এ প্রক্রিয়ার জন্যে ৪ থেকে ৮ সপ্তাহ সময় লাগবে। এসব প্রক্রিয়ার পরই ভারতের দিল্লির লিথুয়ানিয়া এম্বাসিতে ভিসার জন্যে আবেদন করবেন শিক্ষার্থী। এক সপ্তাহের মধ্যেই ভিসা হাতে চলে আসে। এরপরই লিথুয়ানিয়া যাওয়ার প্রস্তুতি সম্পন্ন হয়। রাকিবুল ইসলাম বলেন, লিথুয়ানিয়ায় চাইলে শিক্ষার্থীরা পার্টটাইম চাকুরি করে নিজের খরচ চালাতে পারেন। অবশ্য শিক্ষার্থীর টিউশন ফির সঙ্গেই এক মাসের থাকা খাওয়ার খরচও দিতে হবে বিশ্ববিদ্যালয়কে। তিনি জানান, টিউশন ফি ২১০০ ইউরো থেকে ৪ হাজার ইউরো পর্যন্ত হয়। এখানেই ১ মাসের থাকা খাওয়ার খরচ অর্ন্তভুক্ত থাকে। টিউশন ফি জমা দেওয়ার সময় শিক্ষার্থীকে ন্যূনতম ৮ লাখ টাকার ব্যাংক স্টেটমেন্ট দেখাতে হয়। আর প্রতিমাসে লিথুয়ানিয়া থাকতে একজন শিক্ষার্থীর জীবনযাপনের ধরনের ওপর নির্ভর করে ২০০ থেকে ৪০০ ইউরোর মতো খরচ হয়। লিথুয়ানিয়া পড়াশোনার বড় সুবিধা হলো-এখানে সফলভাবে কোর্স সম্পন্ন করতে পারলে সেনজেনভুক্ত ২৬টি দেশে স্থায়ীভাবে বসবাসের সুযোগ পাওয়া যায়। সেনজেনভুক্ত দেশগুলো হলো-অস্ট্রিয়া, বেলজিয়াম, চেক রিপাবলিক, ডেনমার্ক, এস্তোনিয়া, ফিনল্যান্ড, ফ্রান্স, জার্মানি, গ্রিস, হাঙ্গেরি, আইসল্যান্ড, ইতালি, লাটভিয়া, লিথিউয়নিয়া, লুক্সেমবার্গ, মাল্টা, নেদারল্যান্ডস, নরওয়ে, পোল্যান্ড, পর্তুগাল, স্লোভাকিয়া, স্লোভেনিয়া, স্পেন, সুইডেন, সুইজারল্যান্ড ও লিচেনস্টাইন। রাকিবুল ইসলাম বলেন, লিথুয়ানিয়ায় শিক্ষার্থীর ভর্তির জন্যে দূতাবাস, পররাষ্ট্র মন্ত্রণালয় এবং মেডিকেলের প্রয়োজনীয় কাগজপত্রের বিষয়েও সহায়তা করে তার প্রতিষ্ঠানটি। আইইএলটিএস ছাড়াই মালয়েশিয়ায় উচ্চ শিক্ষা মালয়েশিয়ায় রয়েছে বেশ কিছু ভাল শিক্ষা প্রতিষ্ঠান। এসব প্রতিষ্ঠানে আইইএলটিএস ছাড়াই রয়েছে ভর্তির সুযোগ। ভর্তির পর বিশ্ববিদ্যালয় কর্তৃপক্ষই ল্যাঙ্গুয়েজ কোর্সের মাধ্যমে দক্ষ করে তোলে শিক্ষার্থীদের। মালয়েশিয়ার সেগি ইউনিভার্সিটি, নিলাই ইউনিভার্সিটি, মাশাহ ইউনিভার্সিটি, ওয়েস্টমিনিস্টার ইউনিভার্সিটি এবং লিমককিনের মতো সেরা বিশ্ববিদ্যালয়গুলোতে বাংলাদেশের শিক্ষার্থীদের রয়েছে উচ্চ শিক্ষাগ্রহণের সুযোগ। ইউরোপের আবহে গড়ে ওঠা মালয়েশিয়া সারাবিশ্বে পরিচিত হয়ে উঠছে স্টুডেন্ট হাব নামে। শিক্ষার্থীরা এখানে পেয়ে থাকে বিশ্বমানের পড়াশোনার সুযোগ। লিথুয়ানিয়া ক্লাইপেডিয়া ইউনিভার্সিটিতে ভর্তি হতে অথবা মালয়েশিয়ায় উচ্চ শিক্ষা সর্ম্পকে আরও জানতে পারেন faithedubd@gmail.com এর মাধ্যমে। অথবা মগবাজারের গ্ল্যান্ড প্লাজার ফেইথ কনসালটেন্টসে যোগাযোগ করতে পারেন সরাসরি। ০১৯৭৩৩৮৮৬৬৬ ন‍াম্বারে ফোন দিয়েও জানা যাবে এ বিষয়ে বিস্তারিত তথ্য। বাংলাদেশ সময়: ০৯২৫ ঘণ্টা, জুন ১৩, ২০১৫ টিআই
