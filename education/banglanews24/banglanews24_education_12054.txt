ঢাকা: এসএসসি ও সমমানের পরীক্ষার শুরুর দিন আটটি সাধারণ শিক্ষা বোর্ড এবং মাদ্রাসা ও কারিগরি বোর্ডে আট হাজার ৫২০ জন শিক্ষার্থী অনুপস্থিত ছিল। এদিন ১৬ জন পরীক্ষার্থী বহিষ্কার হয়েছে।
বৃহস্পতিবার (২ ফেব্রুয়ারি) সকাল ১০টা থেকে সাধারণ বোর্ডে বাংলা (আবশ্যিক) প্রথম পত্র, সহজ বাংলা প্রথম পত্র এবং বাংলা ভাষা ও বাংলাদেশের সংষ্কৃতি; মাদ্রাসা বোর্ডে কুরআন মাজিদ ও তাজবিদ বিষয়ের পরীক্ষা অনুষ্ঠিত হয়।
