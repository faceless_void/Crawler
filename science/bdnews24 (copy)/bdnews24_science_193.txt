প্রথম প্রজন্মের আইপডে ছিল পাঁচ জিবি হার্ড ড্রাইভ, সিংকিংয়ের জন্য ফায়ারওয়্যার পোর্ট আর স্ক্রল করার মত চাকা। এই আইপডে প্রায় এক হাজার গান রাখা যেত।
দ্বিতীয় প্রজন্মের আইপডে স্পর্শের মাধ্যমে নিয়ন্ত্রণ করার সিস্টেম যোগ হয়। সেই সঙ্গে উইন্ডোজ সমর্থনও যুক্ত করা হয়।
বর্তমান বাজারে তিন সংস্করণের আইপ্যাড পাওয়া যায়- আলট্রা-কমপ্যাক্ট আইপড শাফল, আইপড ন্যানো আর টাচস্ক্রিন আইপড টাচ।
আইএএনএস-এর তথ্যানুসারে, ২০০৩ সালের জুন মাসের মধ্যেই অ্যাপল প্রায় ১০ লাখ আইপড বিক্রি করে। আর ২০০৪ সালে ১০ লাখ আইপড বিক্রি হয় বছরের শেষ নাগাদ। ২০১৫ সালের সেপ্টেম্বর নাগাদ বিক্রির পরিমাণ ৪২ মিলিয়নে দাঁড়ায়।
২০১৫ সালের জানুয়ারি মাসে অ্যাপলের পক্ষ থেকে জানানো হয়, তারা আইপড বিক্রির প্রতিবেদন প্রকাশ বন্ধ করে দেবে, কারণ আইফোন বিক্রির জন্য আইপডের বিক্রি কমে গেছে। এর মূল কারণ হচ্ছে আইপডের আদলেই তৈরি হয়েছে আইফোন। 
অ্যাপলের মিউজিক সফটওয়্যার আইটিউনস এখনও বেশ দাপটের সঙ্গেই রাজত্ব করে যাচ্ছে। যে কোনো বহনযোগ্য ডিভাইসে গান ডাউনলোডের পরিবর্তে কেবল স্ট্রিমিংয়ের মাধ্যমে গান শোনার জন্য ব্যবহৃত এই সফটওয়্যারটি এখন প্রতিষ্ঠানটির বর্তমান লক্ষ্যে পরিণত হচ্ছে।
