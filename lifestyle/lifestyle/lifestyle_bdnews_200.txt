সাধারণত অপরিকল্পিতভাবে বাড়ি বানালে ও নিম্ন মানের উপকরণ ব্যবহার করলে এই ধরনের সমস্যা দেখা দিতে পারে।
বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজের গৃহব্যবস্থাপনা ও গৃহায়ন বিভাগের প্রভাষক শারমিন সুলতানা বলেন, “স্যাঁতস্যাঁতেভাব দূর করার সবচেয়ে ভালো ও সহজ উপায় হল ঘরে আলো-বাতাস চলাচলের সুব্যবস্থা নিশ্চিত করা। এর পাশাপাশি নজর দিতে হবে সানশেইডের দিকে।”
তিনি পরামর্শ দেন, সানশেইড খুব বড় বা ছোট হওয়া ঠিক না। এটি এমন হওয়া প্রয়োজন যেন ঘরে সঠিক ভাবে আলো বাতাস প্রবেশ করবে আবার বৃষ্টির পানি প্রবেশে বাঁধা দেবে। অধিকাংশ ক্ষেত্রে বৃষ্টির পানি দেওয়াল চুইয়ে ঘরে আসে আর ঘরে স্যাঁতস্যাঁতে পরিবেশ সৃষ্টি হয়। তাই বাড়ি নির্মাণ করার সময়ই এই বিষয়ের প্রতি নজর দেওয়া উচিত।
অনেকসময় রান্নাঘর, শোবার ঘর ও বসার ঘরের একপাশের দেওয়ালে স্যাঁতস্যাঁতেভাব দেখা যায়। এর ফলে দেওয়ালের চুনকাম ফুলে ওঠে এবং দেওয়াল নষ্ট হয়ে যায়। এর মূল কারণ হল এটাচড বাথরুম ও পানির লাইন।
রান্না ঘরে ধোওয়া মোছার কাজ করা হয় এবং অ্যাপার্টমেন্টে রান্নাঘরে আলো বাতাস প্রবেশের সুব্যবস্থা না থাকায় খুব সহজে রান্নাঘর স্যাঁতস্যাঁতে হয়ে যায়। এর থেকে মুক্তি পেতে চাইলে রান্নাঘরকে সবসময় পরিষ্কার পরিচ্ছন্ন ও শুষ্ক রাখার চেষ্টা করতে হবে। রান্নাঘরের পানির কলে কোনো রকম সমস্যা হলে যত তাড়াতাড়ি সম্ভব তা ঠিক করাতে হবে।
বাথরুমে পানির কাজ বেশি তাই এর সঙ্গে সংযুক্ত ঘরটি স্যাঁতস্যাঁতে হয়ে যায়। এই ক্ষেত্রেও খেয়াল রাখতে হবে বাথরুমের পানির লাইনে ও কলে যেন কোনো সমস্যা না থাকে। আর যদি থাকেও তবে তা তাড়াতাড়ি ঠিক করানো উচিত। তা না হলে পাশের ঘরের দেওয়াল ক্ষতিগ্রস্ত হবে।
এরমধ্যে যদি কোনো দেওয়াল নষ্ট হয়ে গিয়ে থাকে তবে তা সংস্কার করা প্রয়োজন। প্রথমে এর কারণ খুঁজে বের করে তা ঠিক করতে হবে। এরপর সেসব দেওয়াল পুনরায় প্লাস্টার করে রং করতে হবে। বর্তমানে বাজারে নানা রকমের প্লাস্টিক পেইন্ট পাওয়া যায়। এসব রং ব্যবহার করে ঘরের স্যাঁতস্যাঁতেভাব কমানো সম্ভব।
নিচতলায় বসবাসকারীরা সবচেয়ে বেশি এই সমস্যার সম্মুখীন হন। তাদের উচিত চারপাশ সবসময় পরিষ্কার রাখা যেন বৃষ্টি হলে আশপাশে পানি জমে না থাকে। আর যতটা সম্ভব ঘরে আলো বাতাস প্রবেশের ব্যবস্থা করা।
আসবাবপত্র নষ্ট হওয়ার হাত থেকে বাঁচাতে চাইলে এর নিচে ছোট ইট বা কাগজের টুকরা ভাঁজ করে দিতে পারেন। সবচেয়ে ভালো হয় যদি ঘরে টাইলস করে দেওয়া যায়। আর তা সম্ভব না হলে বাজারে যে টাইলস পেপার পাওয়া যায় তা ব্যবহার করেও সমস্যার সমাধান করা যেতে পারে বলে জানান এই প্রভাষক।
এইসব পদক্ষেপ গ্রহণের পরেও যদি দেখা যায় যে স্যাঁতস্যাঁতেভাব কমছে না, তাহলে বুঝতে হবে বাড়ি নির্মাণে ত্রুটি এর জন্য দায়ী। সে ক্ষেত্রে ভবিষ্যতের বড় কোনো দূর্ঘটনা এড়ানোর জন্য সংশ্লিষ্ট প্রকৌশলীর সঙ্গে কথা বলার পরামর্শ দেন শারমিন সুলতানা।
আরেকটা বিষয় মনে রাখা প্রয়োজন তা হল এই স্যাঁতস্যাঁতে পরিবেশ শিশু ও বৃদ্ধদের জন্য ক্ষতিকারক। শীত, গ্রীষ্ম ও বর্ষা সবসময়ই এই পরিবেশ স্বাস্থের জন্য ক্ষতি করে তাই অবহেলা না করে দ্রুত এর সমাধান করা উচিত বলে মনে করেন তিনি।
বিল্ডিং কোডের টুকিটাকি
ঘর সাজাতে কার্পেট
ঘরের সৌন্দর্যে পর্দা
