রাজশাহী বিশ্ববিদ্যালয়ের ইংরেজি বিভাগের অধ্যাপক ড. রেজাউল করিম সিদ্দিকী হত্যা মামলায় মুনতাসিরুল আলম অনিন্দ্য (২৬) নামে এক শিক্ষার্থীকে গ্রেফতার করেছে পুলিশ।
রাজশাহী: রাজশাহী বিশ্ববিদ্যালয়ের ইংরেজি বিভাগের অধ্যাপক ড. রেজাউল করিম সিদ্দিকী হত্যা মামলায় মুনতাসিরুল আলম অনিন্দ্য (২৬) নামে এক শিক্ষার্থীকে গ্রেফতার করেছে পুলিশ।
রোববার (০৩ জুলাই) সন্ধ্যায় রাজশাহী মহানগর পুলিশের মুখপাত্র ও রাজপাড়া জোনের সিনিয়র সহকারী পুলিশ কমিশনার ইফতে খায়ের আলম বাংলানিউজকে বিষয়টি জানান।
তিনি বলেন, শনিবার (০২ জুলাই) দিনগত রাতে রাবি শিক্ষক রেজাউল হত্যা মামলায় রাবি শিক্ষার্থী মুনতাসিরুলকে রাজশাহী মহানগর গোয়েন্দা পুলিশ (ডিবি) গ্রেফতার করে। রোববার দুপুরে তাকে আদালতে পাঠানো হয়।
মুনতাসিরুল আলম রাজশাহী বিশ্ববিদ্যালয়ের ইংরেজি বিভাগের ছাত্র ও মহানগরীর কাদিরগঞ্জ এলাকার শফিউল আলমের ছেলে। তার বিরুদ্ধে ১০ দিনের রিমান্ডের আবেদন করে মামলার তদন্তকারী কর্মকর্তা রেজাউস সাদিক। তবে শুনানি অনুষ্ঠিত হয়নি।
গত ২৩ এপ্রিল সকালে মহনগরীর শালবাগান এলাকায় নিজ বাসার সামনে সন্ত্রাসীরা কুপিয়ে হত্যা করে অধ্যাপক ড. রেজাউল করিম সিদ্দিকীকে। এ ঘটনায় নিহত শিক্ষকের ছেলে রিয়াসাত ইমতিয়াজ সৌরভ বাদী হয়ে থানায় হত্যা মামলা দায়ের করেন। পুলিশ এক জেএমবি সদস্যসহ মোট ৯ জনকে গ্রেফতার করে।
