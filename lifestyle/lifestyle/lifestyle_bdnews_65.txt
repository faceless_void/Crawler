প্রচলিত ও প্রতিষ্ঠিত পন্থাগুলো নিয়ে গৃহসজ্জাবিষয়ক একটি ওয়েবসাইটে প্রকাশিত প্রতিবেদন অবলম্বনে ব্যবহৃত টি ব্যাগ দিয়ে বিকল্প কয়েকটি কাজের পন্থা এখানে দেওয়া হল।
পাস্তা রান্নায় স্বাদ বাড়াতে: জেসমিন রাইস পাস্তা কিংবা লেমন গ্রাস পাস্তা রান্নায় কাজে লাগাতে পারেন ব্যবহৃত টি ব্যাগ। ফুটন্ত পানিতে টি ব্যাগ ঝুলিয়ে রাখুন। এই ফুটন্ত পানিতে পাস্তার ঢালার আগে টি ব্যাগ তুলে নিতে হবে। ফলে পাস্তায় আসবে ভিন্ন এক স্বাদ।
গাছের পরিচর্যায়: ব্যবহৃত টি ব্যাগ পানিতে ফুটিয়ে সেই পানি গাছের গোড়ায় ঢালতে পারেন। এটি গাছকে সব ধরনের ছত্রাক সংক্রমণ থেকে রক্ষা করবে। টি ব্যাগ সার হিসেবেও ব্যবহার করতে পারেন। এজন্য ব্যবহৃত টি ব্যাগ ছিঁড়ে ভেতরের চা গাছের গোড়ার মাটির উপরের অংশে মিশিয়ে দিতে হবে।
জুতার গন্ধ তাড়াতে: জুতা ভেতর দুর্গন্ধ আপনাকে বিব্রতকর পরিস্থিতিতে ফেলে দিতে পারে। এই দুর্গন্ধ দূর করতে ব্যবহৃত টি ব্যাগ শুকিয়ে নিয়ে তা জুতার ভেতরে রেখে দিতে পারেন, টি ব্যাগ দুর্গন্ধ শুষে নেবে।
মাদুরের গন্ধ দূর করতে: এক্ষেত্রে বাসায় ভ্যাকুয়াম ক্লিনার থাকা চাই। প্রথমেই ব্যবহৃত টি ব্যাগগুলোকে কুসুম গরম পানি দিয়ে আধ ভেজা করে নিতে হবে। এবার টি ব্যাগগুলো খুলে ভেতরের আধভেজা চা মেঝের মাদুরে ছড়িয়ে দিন। শুকিয়ে যাওয়া পর্যন্ত ছিটিয়ে রেখে ভ্যাকুয়াম ক্লিনারের মাধ্যমে মাদুর পরিষ্কার করে ফেলতে হবে। মাদুরে সুগন্ধ আনতে চাইলে লেমন টি, মিন্ট টি ইত্যাদি সুগন্ধি চা ব্যবহার করতে পারেন।
কাঁচ পরিষ্কার করতে: টেবিলের কাঁচ, আয়না, কাঁচের দরজা ইত্যাদি ভালোভাবে পরিষ্কার করতে এক বিশেষ দ্রবণ ব্যবহার করতে হয়, যা বাজারে ‘গ্লাস ক্লিনার’ নামে পরিচিত। এই বিশেষ দ্রবণের বদলে ব্যবহার করতে পারেন টি ব্যাগ। পদ্ধতিও সহজ, ব্যবহৃত টি ব্যাগগুলো আবার গরম পানিতে ডুবিয়ে রেখে ওই পানি দিয়ে কাঁচ মুছতে হবে।
পেডিকিউর করতে: চায়ে আছে ত্বকের মৃত কোষ দূর করার সহায়ক উপাদান। আর এর অ্যান্টি-অক্সিডেন্ট ত্বককে পুনরুজ্জীবিত করে তোলে। সারাদিন জুতা স্যান্ডেল পরে ধুলাবালি হাঁটাহাঁটির পর পায়ের ত্বকের যত্নে তাই ব্যবহৃত টি ব্যাগ অতুলনীয়। এটি পায়ের পেশিকে শিথিল করবে, দূর করবে মৃদু মাত্রার ব্যথা ও ফোলাভাব।
চুলের কন্ডিশনার: টি ব্যাগ গরম পানিতে অল্প সেদ্ধ করে চুলের কন্ডিশনার হিসেবে ব্যবহার করতে পারেন।


 ‘অ্যান্টি-অক্সিডেন্ট’ সমৃদ্ধ গোসল: চা পাতা যেহেতু অ্যান্টি-অক্সিডেন্টের একটি উৎকৃষ্ট উৎস তাই একে গোসলের পানিতে ব্যবহার করলে পুরো শরীরের ত্বকই প্রাণ ফিরে পাবে। গোসলের পানিতে কিছুক্ষণ আগে থেকে ব্যবহৃত টি ব্যাগ ডুবিয়ে রাখতে হবে। এতে শরীরের দুর্গন্ধ ও মরা চামড়া দূর হবে, কমবে ত্বকের প্রদাহের আশঙ্কাও।
এয়ার ফ্রেশনার: ব্যবহৃত টি ব্যাগে আপনার পছন্দের এসেন্সিয়াল অয়েল কয়েক ফোঁটা দিয়ে ঘর, অফিস কিংবা গাড়ির এক কোণে ঝুলিয়ে দিতে পারেন। দুর্গন্ধ তাড়াতে এটি বেশ কার্যকর।
আরও পড়ুন
ব্যবহৃত টি-ব্যাগের অন্য ব্যবহার  
