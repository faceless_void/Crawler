জরুরি অবস্থা মোকাবিলায় প্রাথমিক চিকিৎসায় জ্ঞান থাকা উচিত। তবে প্রচলিত অনেক পদ্ধতি আমাদের হয়ত ভুল জানা।
যেমন ধরা যাক পান করতে গিয়ে পানি শ্বাসনালীতে আটকে যাওয়ার বিষয়টা।
চিকিৎসা-বিজ্ঞানে এই পিঠ-চাপড়ে দেওয়ার পন্থাকে একদমই ভুল কাজ হিসেবে আখ্যা দিচ্ছে। কারণ, গলায় কিছু আটকে গেলে, দম বন্ধ হওয়ার যোগাড় হলে, ওই ব্যক্তির পিঠে চাপড় মারলে শ্বাসনালী ক্ষতিগ্রস্ত হয়। এসময় আক্রান্ত ব্যক্তিকে হেলান দিয়ে বসিয়ে ধীরে ধীরে শ্বাস টানার পরামর্শ দিতে হবে।
প্রচলিত প্রাথমিক চিকিৎসায় এরকম ভুল ধারণা আরও রয়েছে। যেমন-
ঠাণ্ডায় জমে যাওয়া অঙ্গ ঘষা ঠিক না
শীতকালে ঠাণ্ডা বাতাসের প্রকোপে হাত-পায়ের আঙুল জমে যায়। আমাদের দেশের আবহাওয়ায় জমে যাওয়া তীব্রতা খুব একটা গুরুতর আকার ধারণ করে না, তবে এই অবস্থায় ঘষে গরম করার চেষ্টা করলে ওই অঙ্গের রক্তনালী এবং সঙ্গে যুক্ত শাখা রক্তনালীগুলোর ক্ষতি করতে পারে পারে।
তাই এই অবস্থায় কক্ষ বা সাধারণ তাপমাত্রার পানিতে জমে যাওয়া অঙ্গ ডুবিয়ে রেখে ধীরে ধীরে পানির তাপমাত্রা বাড়াতে হবে।
দুর্ঘটনায় কবলিত ব্যক্তিকে টানাহেঁচড়া ঠিক না
দুর্ঘটনা পরবর্তী উত্তেজনার কারণে কী করা উচিত তা ঝটপট মাথায় আসেনা। আবার আশপাশের মানুষগুলো দুর্ঘটনা কবলিত ব্যক্তিকে নিরাপদ দূরত্বে নেওয়া জন্য ব্যস্ত হয়ে পড়বেন সেটাই স্বাভাবিক। তবে দুর্ঘটনায় কবলিত ব্যক্তিকে টেনেহেঁচড়ে দুর্ঘটনাস্থল থেকে সরানো উচিত নয়। কারণ, আপনি দুর্ঘটনা কবলিত ব্যক্তির শরীরের যে অংশ ধরে টানছেন, সেই অংশেই আঘাত লেগে থাকতে পারে। ভাঙা হাড় বা গভীর ক্ষত থাকতে পারে। যা আপনার আপনার টানাটানিতে আরও মারাত্বক আকার ধারণ করতে পারে।
তাই প্রথম কাজ হওয়া উচিত অ্যাম্বুলেন্স যোগাড় করার চেষ্টা। সড়ক দুর্ঘটনার ক্ষেত্রে আক্রান্ত ব্যক্তির যানবাহনের ইঞ্জিন বন্ধ করার চেষ্টা করতে হবে।
আগুনে পোড়া অংঙ্গে সঙ্গে সঙ্গে ক্রিম বা মলম লাগানো ঠিক না।
কারণ পোড়া অংশের তাপমাত্রা থাকে বেশি। আর তখনই মলম বা ক্রিম প্রয়োগ করলে তাপ ওই পোড়া অংশে আটকা যায়।
তাই পোড়া অংশে প্রথমেই কমপক্ষে ১৫ মিনিট ঠাণ্ডা পানি প্রয়োগ করতে হবে। পরে আক্রান্ত অংশ ভালোভাবে পরিষ্কার করে তারপর ক্রিম বা মলম দিতে হবে।
খিঁচুনিতে অজ্ঞান হওয়া মৃগীরোগীর মুখে শক্ত জিনিস ঢুকানো থেকে বিরত থাকুন
কারণ এতে আক্রান্ত ব্যক্তির দাঁত ভাঙতে পারে, মাঢ়ী ও তালু ক্ষতিগ্রস্ত হতে পারে, শক্ত বস্তুটি গিলে ফেলতে পারে। তাই খিঁচুনি শুরু হলে তার মাথার বালিশ কিংবা নরম কিছু দিতে হবে যাতে মস্তিষ্কের কোনো ক্ষতি না হয়।
খিঁচুনি শেষে একপাশে কাত হয়ে শুয়ে থাকতে পরামর্শ দিতে হবে।
অজ্ঞান ব্যক্তির জিহ্বা বের করার চেষ্টা ভুল
জ্ঞান হারিয়ে ফেলা ব্যক্তিকে পিঠের ভরে বা চিত করে শোয়ানো উচিত নয়, কারণ এতে তার জিহ্বা ভেতরে চলে গিয়ে শ্বাস-প্রশ্বাস চলাচলের পথ বন্ধ করে দিতে পারে। তাই একপাশে কাত করে শোয়াতে হবে।
আর ভুলেও জিহ্বা টেনে বের করার চেষ্টা করা যাবে না।
তথ্যসূত্র: বিভিন্ন স্বাস্থ্যবিষয়ক প্রতিবেদন থেকে।
আরও পড়ুন
কাটাছেঁড়ায় ঘরোয়া প্রতিকার  
কোলেস্টেরল কমানোর উপায় ২  
