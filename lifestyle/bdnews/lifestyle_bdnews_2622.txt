বিশেষজ্ঞরা নিয়মিতই ফল ও সবজি খাওয়ার পরামর্শ দেন। এর মূল কারণ আসলে শারীরিক সুস্থতা। ফল ও সবজি খেয়ে মানুষ অনেক বয়স পর্যন্ত সজীব থাকে। তবে গবেষকরা জানাচ্ছেন, এই দীর্ঘকালীন উপকার ছাড়াও ফল-সবজির তাৎক্ষণিক কিছু উপকার রয়েছে।
তা হল, ফল ও সবজি খেলে মানুষ উৎফুল্ল থাকতে পারে।
অস্ট্রেলিয়ার জাতীয় জরিপে দেখা গিয়েছে, কোনো মানুষের খাবারের তালিকায় যদি ফল ও সবজির পরিমাণ বাড়ানো হয় তবে দুই বছরের মধ্যে মানুষটির সুখী হওয়ার হারও বেড়ে যায়।                                                                                                                                                                                                                                                                                                                                আমেরিকান জার্নাল অফ পাবলিক হেলথে প্রকাশিত এই গবেষণায় গবেষকরা জানান, প্রতিদিন যদি আট ভাগ ফল ও সবজি খাদ্য তালিকায় রাখা যায় তবে মানুষটির সুখ এমনই বেড়ে যায় যেন, সে বেকার অবস্থা থেকে চাকরি পেয়েছে।
যুক্তরাজ্যের কভেনট্রি শহরে অবস্থিত ইউনিভারসিটি অফ ওয়ারওয়েকের ইকোনমিক্স অ্যান্ড বিহেইভিয়রল সায়েন্সের শিক্ষার্থী এবং এই গবেষণার সহকারী লেখক অ্যান্ড্রু ওসওয়ল্ড বলেন, “আমার গবেষণা থেকে ফল ও সবজির যে শক্তিটা দৃশ্যমান হয়েছে তা খুবই চমকপ্রদ।”
এ গবেষণা চলাকালে ওসওয়ল্ড নিজের খাদ্য তালিকায় অতিরিক্ত তিন ভাগ ফল ও সবজি যোগ করেন।
রয়টার্সের হেলথকে করা একটি ইমেইলে তিনি জানান, এখন আমি সাত ভাগ পর্যন্ত সবজি ও ফল খেতে পারি।
ওসওয়ল্ড এবং তার সহযোগী গবেষকরা মনে করেন, এই তাৎক্ষণিক লাভের কথা জানার পরে মানুষ আরও বেশি বেশি ফল ও সবজি খেতে উৎসাহী হবে।
ফল ও সবজি খাওয়ার সঙ্গে সুখে থাকার সম্পর্ক নিরূপণ করতে গবেষকরা ১২ হাজার অস্ট্রেলীয় নাগরিকদের মধ্যে জরিপ চালান।
অংশগ্রহণকারীরা ২০০৭ সাল থেকে ২০১৩ সাল পর্যন্ত তাদের খাবারের তালিকা লিপিবদ্ধ করে রাখে এবং তাদের শারীরিক এবং মানসিক স্বাস্থ্য বিষয়ক নানান প্রশ্নের জবাব দেন।
দুই বছরের মধ্যে, গবেষক দল দেখতে পান, যারা মোটেই ফল সবজি খায়নি তাদের থেকে যারা মোট খাদ্যের আট ভাগ ফল ও সবজি খেয়ে কাটিয়েছেন তাদের সুখী হওয়ার প্রবণতা বেশি এবং তারা নিজেদের জীবন সম্পর্কেও বেশ সন্তুষ্ট।
যারা একদম ফল বা সবজি খেতেন না তারা যখন দিনে আটটি ফল সবজি খাচ্ছেন তখন তাদের জীবন সন্তুষ্টির পথে যায় বলে ফলাফল জানায়। এটা ঠিক একজন মানুষ বেকার অবস্থা থেকে চাকরিতে যোগ দানের সন্তুষ্টির সমান। এদিকে যারা খাদ্য তালিকায় ফল ও সবজি বাড়াননি তাদের সুখে থাকার মাত্রা দিন দিন কমতে থাকে। এর মান চাকরীচ্যুত হওয়ার সমতুল্য।
ওসওয়ল্ড বলেন, “তবে শূণ্য থেকে ৮টি ফল বা সবজি খাওয়ার পরিমাণ বাড়িয়েছে এমন অংশগ্রহণকারীদের সংখ্যা খুবই কম।”
তিনি আরও বলেন, “যদি দুইভাগ ফল বা সবজিও প্রতিদিনের খাদ্য তালিকায় যোগ করা যায় তাও বেশ উপকার পাওয়া যায়।”
তবে এ গবেষণায় গবেষক শুধু চাকরি গ্রহণের সঙ্গেই ফল ও সবজির খাওয়ার আনন্দ বৃদ্ধিকে তুলনা করেছেন। অন্য সব বিষয় এখানে ধরা হয়নি। তাই এ গবেষণার আলোকে খুব জোর দিয়ে বলা যায় না যে ফল বা সবজি মনের সুখ আনবেই।
ম্যারি জো ক্রাইৎজার নামে একজন বিশেষজ্ঞ যিনি এই গবেষণায় অংশ নেননি, এ ধরণের ব্যাখ্যায় আসার বিষয়ে সতর্ক থাকতে পরামর্শ দেন।
মিনিয়াপলিসে অবস্থিত ইউনিভার্সিটি অফ মিনেসোটা’র সেন্টার ফর স্পিরিচুয়াল অ্যান্ড হিলিং বিভাগের   পরিচালক ক্রাইৎজার বলেন, “শুধু ফল ও সবজি ক্ষণিকের সুখ দিতে পারে না।”
তিনি আরও জানান, খাদ্য তালিকায় চিনি ও লবণ গ্রহণের তারতম্যের উপরও ভালো থাকায় প্রভাব পড়ে। এটা শুধু মানসিক স্বাস্থ্য না শারীরিক স্বাস্থ্যেও প্রভাব ফেলে।
ক্রাইৎজারের মতে, ফল ও সবজির সঙ্গে সুখের সম্পর্ক নিরূপণ করার গবেষণাটা দারুণ। তবে এটাই যথেষ্ট নয়।
“মানুষকে স্বাস্থ্যকর খাবারের প্রতি আগ্রহী করে তাদের বেশি করে স্বাস্থ্যকর খাবার সম্পর্কে জানাতে হবে এবং সঠিক পদ্ধতিতে রান্নার করার বিষয়েও সচেতন করতে হবে”, যোগ করেন তিনি।
তিনি আরও বলেন, “তারপরেও এ ধরণের গবেষণা জরুরি। কারণ কে কী খাচ্ছে এই বিষয়ে তার সচেতন থাকা দরকার।”
