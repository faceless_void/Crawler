বিবিসি-এর প্রতিবেদনে বলা হয়, চীনে অনুষ্ঠিত প্রদর্শনী ম্যাচে ছয়জন মানব পোকার খেলোয়াড়কে হারিয়ে ২৯০০০০ মার্কিন ডলার জিতে নিয়েছে এআই। নতুন এআইটির নাম বলা হচ্ছে ‘লেংপুদাশি’।
চলতি বছরের শুরুতে পোকার গেইমে আরেকবার মানব খেলায়োড়কে হারিয়েছে এআই। ওই সংস্করণটির নাম ছিল লিব্রাটাস। সে সময় ২০ দিন ব্যাপী গেইমে চারজন বিশ্বসেরা খেলোয়াড়কে হারায় লিব্রাটাস।
কৃত্রিম বুদ্ধিমত্তার ওপর নির্ভর করে তৈরি খেলাগুলোর চেয়ে পোকার কৌশলগতভাবে আলাদা। এআইভিত্তিক তৈরি অন্য খেলাগুলোয় প্রতিপক্ষের সক্ষমতা সম্পর্কে জানা সম্ভব হয়। যেমন দাবা বা গো খেলা। কিন্তু পোকার খেলায় প্রতিপক্ষের কার্ড সম্পর্কে ধারণা পাওয়া সম্ভব নয়। এর ফলে ভালো কোনো কার্ড হাতে না থাকলেও প্রতিপক্ষকে ভিন্ন ধারণা দেওয়া যায়। নতুন এই এআই এই কৌশলটিও শিখে গেছে।
গেইমে খেলোয়াড়দের ফাঁকি (ব্লাফ) দিতে পারে লেংপুদাশি।
এবারের গেইমটির স্থায়িত্ব ছিল পাঁচ দিন।  লেংপুদাশি নামের এআইটি তৈরি করেছেন যুক্তরাষ্ট্রের কার্নেগি মেলন ইউনিভার্সিটির অধ্যাপক টুয়োমাস স্যান্ডম এবং পিএইচডি শিক্ষার্থী নোয়াম ব্রাউন।
এআইয়ের বিরুদ্ধে যে ছয়জন খেলোয়াড় ছিলেন তার নেতৃত্ব দেন ইউয়ে ডু। আগের বছর টেক্সাস হোল্ড’এম ক্যাটেগরিতে ‘ওয়ার্ল্ড সিরিজ অফ পোকার ৫০০০ ডলার’ জিতেছেন তিনি।
ব্লুমবার্গকে দেওয়া এক সাক্ষাৎকারে ব্রাউন বলেন, “মানুষ মনে করেন ব্লাফিং আসলে মানুষের জন্য। এখন দেখা যাচ্ছে এটি সত্য নয়। একটি কম্পিউটার অভিজ্ঞতা থেকে শিখতে পারে। তার হাতে যদি ভালো কার্ড না থাকে তবে এটি ব্লাফ করে এবং আরও বেশি অর্থ পেতে পারে।”
