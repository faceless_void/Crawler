খুলনা: খুলনার বিভিন্ন শিক্ষা প্রতিষ্ঠানে নিয়মিত জাতীয় সঙ্গীত ও শপথ বাক্য পাঠ না করানোর অভিযোগে সদর থানা  মাধ্যমিক শিক্ষা কর্মকর্তা মো. আব্দুল মমিনকে কারণ দর্শানোর নোটিশ দেওয়া হয়েছে। 
মঙ্গলবার (২২ আগস্ট) মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষা অধিদপ্তর, খুলনার আঞ্চলিক পরিচালক অধ্যাপক টিএম জাকির হোসেন এই কারণ দর্শানোর নোটিশ পাঠান। 
নোটিশে বলা হয়, ২১ আগষ্ট খুলনার সুলতানা হামিদ আলী মাধ্যমিক বালিকা বিদ্যালয়ে আকম্মিক পরির্দশনে দেখা যায়, প্রতিষ্ঠানটিতে প্রায় দুই সপ্তাহ ধরে শিক্ষার্থীদের জাতীয় সঙ্গীত ও শপথ বাক্য পাঠ করানো হয় না। 
এছাড়া বিদ্যালয়ের প্রধান শিক্ষকসহ অনেকে নির্ধারিত সময়ের পর স্কুলে আসেন। এ ঘটনায় দায়িত্ব অবহেলায় সদর থানা মাধ্যমিক শিক্ষা কর্মকর্তা মো. আব্দুল মমিনকে কারণ দর্শানোর নোটিশ দেওয়া হয়। আগামী ৭ দিনের মধ্যে কারণ দর্শাও নোটিশের জবাব দিতে বলা হয়েছে।   
** খুলনায় মাউশির অভিযান, স্কুলে হয় না জাতীয় সঙ্গীত
