ঢাকা: দেশের বিতর্ক আন্দোলনের কেন্দ্রীয় সংগঠন ন্যাশনাল ডিবেট ফেডারেশন বাংলাদেশ (এনডিএফ বিডি) আগামী ২৫ ও ২৬ মার্চ দুই দিনব্যাপী ‘এনডিএফ বিডি লিডারশিপ ট্রেনিং প্রোগ্রাম’র আয়োজন করেছে।
দেশের বিখ্যাত পর্যটন কেন্দ্র কুয়াকাটায় এ ট্রেনিং অনুষ্ঠিত হবে। সারাদেশের বিভিন্ন শিক্ষা প্রতিষ্ঠানের এনডিএফ বিডি’র মেধাবী বিতার্কিক ও বিভিন্ন প্রতিষ্ঠানের উচ্চপদস্থ কর্মকর্তার এতে অংশ নেবেন।
এ আয়োজনের স্লোগান হচ্ছে-‘সাগর বাঁচাও, সাগরে বাঁচি’। ট্রেনিংয়ে প্রতিযোগিতাপূর্ণ রাষ্ট্র ব্যবস্থায় নেতৃত্ব বিকাশের বিভিন্ন কর্মশালার সঙ্গে সাগর তীর পরিচ্ছন্ন রাখা ও সাগরকে প্রাণিকুলের বসবাস উপযোগী রাখতে সচেতনতামূলক বিভিন্ন কার্যক্রম পরিচালনা করা হবে।
শুক্রবার (২৪ মার্চ) প্রোগ্রামে অংশগ্রহণকারী দল ঢাকা থেকে যাত্রা শুরু করবে। দুই দিনব্যাপী এ অয়োজনের  নেতৃত্ব দেবেন ন্যাশনাল ডিবেট ফেডারেশন’র চেয়ারম্যান এ কে এম শোয়েব।
