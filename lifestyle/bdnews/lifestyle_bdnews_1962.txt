প্রেমের সম্পর্ককে আরও ‘রোমান্টিক’ করে তুলতে বেশি বেশি বই পড়ায় জুরি নেই। এক গবেষণায় দেখা গেছে, যাদের বই পড়ার অভ্যাস আছে তারা সাধারণত অন্যদের তুলনায় বেশি রোমান্টিক হয়ে থাকেন।
যুক্তরাষ্ট্রের বার্কলির ক্যালিফোর্নিয়া বিশ্ববিদ্যালয়ের অধ্যাপক অ্যান ই কানিংহামের মতে, “তরুণ বই পড়ুয়ারা বুদ্ধিদীপ্ত আবেগের অধিকারী হন। তারা ভাষা নির্বাচনেও পটু হয়ে থাকেন। ফলে সঙ্গীর সঙ্গে কথা বলার সময় শব্দ নির্বাচনেও দক্ষ হন তারা। যা সুন্দর প্রেমের সম্পর্কের জন্য খুবই দরকারী।”
যারা বই পড়েন তারা যে শুধুই আবেগপ্রবণ হন তা নয়, তারা প্রেমেও পড়েন বেশি। আর প্রেমের সম্পর্ককে আরও প্রেমময় করতে তাদের জুড়ি নেই।
গবেষকরা বলেন, “বইয়ের কাহিনীগুলো পাঠককে অন্য একজনের জীবনের মধ্যে টেনে নিয়ে যায়। ফলে পাঠক অন্য এক জীবনের বিভিন্ন মোড় অনুভব করতে পারে।”
