বাজারে এখন আনারস চলে এসেছে। যাদের ডিম খেতে মানা তারা ডিম ব্যবহার না করেই মজাদার এই কেক বানিয়ে খেতে পারেন। রেসিপি দিয়েছেন তাসমিন সুলতানা দিনা।
উপকরণ
ময়দা ২০০ গ্রাম। মাখন ১০০ গ্রাম এবং আধা টেবিল-চামচ। কনডেন্সড মিল্ক ১ কৌটা। বেকিং সোডা ১ চা-চামচ। বেকিং পাউডার ১ চা-চামচ। আনারস এসেন্স আধা চা-চামচ। সোডা (পান করার সোডা) ৩-৪ কাপ।  চেরি ফল ১০-১২টি। চিনি ৫-৬ টেবিল-চামচ (ক্যরামেলের জন্য)। আনারস ৬-৭ টুকরা।
পদ্ধতি
যে প্যানে (৯ ইঞ্চি প্যান ) কেক বানাবেন তাতে মাখন গ্রিস করুন কিংবা  লাগিয়ে নিন।
চিনি আর মাখন অন্য প্যানে নিয়ে ক্যারামেল তৈরি করে নিন। এবার ৯ ইঞ্চির প্যানে আনারসের টুকরাগুলো ছড়িয়ে দিন আর মাঝের ফাঁকা অংশে চেরি কেটে বসিয়ে দিন।


ওভেন প্রি হিট করুন ১৮০ ডিগ্রিতে। একটা বাটিতে মাখন আর কনডেন্সড মিল্ক ভালো করে মিশিয়ে এসেন্স দিন। এবার ময়দা, বেকিং সোডা, বেকিং পাউডার একে একে ওই মিশ্রণে ভাগ ভাগ করে মেশাতে থাকুন। অর্থাৎ একভাগ ময়দা, একভাগ সোডা নিন এবং মেশান।
আবার একভাগ সোডা, একভাগ ময়দা— এভাবে মেশান। ভালো করে মিশিয়ে এই মিশ্রণ আগে করে রাখা আনারস চেরির ক্যারামেলের উপর ঢেলে দিন।
৪৫ মিনিট বেইক করুন। তারপর টুথপিক ঢুকিয়ে দেখুন।
টুথপিক পরিষ্কার হয়ে উঠে আসলে বুঝতে হবে হয়ে গেছে। কেক বের করুন ওভেন থেকে এবং ১০ মিনিট ঠাণ্ডা হতে দিন। পরিবেশন করুন।
হার্ট চকলেট কেক
অরেঞ্জ কেক
