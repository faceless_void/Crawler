২০১৬ সালে বিদায় জানানো প্রযুক্তিগুলো নিয়ে প্রতিবেদন প্রকাশ করেছে সিএনএন।
আইফোনের হেডফোন জ্যাক-
চলতি বছর সেপ্টেম্বরে আইফোন ৭ আনার সঙ্গে বাদ দেওয়া হয় হেডফোন জ্যাক, পরিচয় করানো হয় তারবিহীন হেডফোন ‘এয়ারপডস’-এর।
এ বছর স্মার্টফোন বাজারে স্যামসাংয়ের সবচেয়ে বড় বাজির ঘোড়া ছিল গ্যালাক্সি নোট ৭। কিন্তু বিস্ফোরণ আর অগ্নিঝুঁকির কারণে বাজার থেকে চিরতরে তুলে নেওয়া হয় এই স্মার্টফোনটি। বিস্ফোরণের ঘটনায় একটি মডেলের ওয়াশিং মেশিনও তুলে নিতে হয় প্রতিষ্ঠানটিকে।
ভিসিআর
১৯৬০ সালে যাত্রা শুরু করেছিল ভিসিআর। বর্তমান যুগের উন্নত প্রযুক্তির বদৌলতে শেষমেশ হারিয়েই যেতে হল এক সময় বাড়িতে সিনেমা দেখার জনপ্রিয় এই মাধ্যমটিকে। চলতি বছর সর্বশেষ ভিসিআর নির্মাতা প্রতিষ্ঠান জাপানের ফুনাই ইলেকট্রনিক্সও এই বিভাগ গুটিয়ে নেয়। শেষ বছরেও প্রতিষ্ঠানটি ৭৫ হাজারের মতো ভিসিআর বিক্রি করতে সক্ষম হয়।
হোভারবোর্ড
২০১৫ সালে উপহারের জন্য বিভিন্ন দেশে আকর্ষণীয় পণ হয়ে উঠেছিল হোভারবোর্ড। কিন্তু এতে আগুন লাগা আর দুর্ঘটনার কয়েকটি নজির সৃষ্টি হওয়ার পর যুক্তরাষ্ট্রের ভোক্তা নিরাপত্তা বিভাগ এটি নিষিদ্ধ করে দেয়, ফেরত নেওয়া হয় প্রায় পাঁচ লাখ হোভারবোর্ড।
গুগল পিকাসা
২০১৬ সালের মার্চে নিজেদের ফটো-শেয়ারিং ও সংরক্ষণ সেবাদাতা অ্যাপ পিকাসা-কে বন্ধ করে দেয় ওয়েব জায়ান্ট গুগল।
মটোরোলা
এ বছর এক সময় মোবাইল ফোন বাজারে দাপিয়ে বেড়ানো ব্র্যান্ড মটোরোলার ইতি টানে এর মূল প্রতিষ্ঠান লেনোভো। দুই ব্র্যান্ডনামকে এক করে দেওয়া হয় নতুন নাম- ‘মটো বাই লেনোভো’।
গুগল’র প্রজেক্ট আরা
যেমন খুশি তেমন সাজ- এর মতো যেমন খুশি তেমন ফোন, এমন ধারণা নিয়েই যাত্রা শুরু করেছিল ওয়েব জায়ান্ট গুগলের প্রকল্প প্রজেক্ট আরা। কিন্তু কারিগরী সমস্যা আর খরচ অনেক বেশি হওয়ায় তা আর বাজারে ছাড়া হয়নি, এ বছর এই প্রকল্প বন্ধ করে দেয় প্রতিষ্ঠানটি। 
ব্ল্যাকবেরি
প্রযুক্তি দুনিয়ায় ‘স্মার্টফোন’ শব্দটিতে যে প্রতিষ্ঠানের অবদান সম্ভবত সবচেয়ে বেশি, সেই ব্ল্যাকবেরি আর ফোন তৈরি না করার সিদ্ধান্ত নেয়। অর্থ হারাতে থাকা ডিভাইস ব্যবসায়ের জন্য চলতি বছর সেপ্টেম্বর মাসকে ডেডলাইন হিসেবে বেছে নিয়েছিলেন প্রতিষ্ঠানের প্রধান নির্বাহী জন চেন।
ভাইন
