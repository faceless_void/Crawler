মহাকাশযান নির্মাতা মার্কিন প্রতিষ্ঠান স্পেসএক্স, বৈদ্যুতিক গাড়ি নির্মাতা প্রতিষ্ঠান টেসলাসহ বেশ কয়েকটি ভেঞ্চারের-এর ‘মালিক’ ইলন মাস্ক। তার সঙ্গে কথা বলার সুযোগ খুঁজছেন এমন ব্যক্তির সংখ্যা কম নয়। মাস্কের ব্যক্তিগত ফোন নাম্বার তাই অনেকের জন্যই বড় কিছু।
নিজের ব্যক্তিগত নাম্বার প্রকাশ করে তার শুভাকাংক্ষীদের সে সুযোগই দিয়েছেন মাস্ক। কিন্তু পুরো ব্যাপারটি ঘটেছে অনিচ্ছাকৃতভাবে।
অকুলাস ভার্চুয়াল রিয়ালিটি সহ-প্রতিষ্ঠাতা জন কারম্যাক-কে ব্যক্তিগত বার্তায় নিজের নাম্বার দিতে চেয়েছিলেন মাস্ক। বার্তায় কারম্যাককে ফোন দিতে বলেন মাস্ক। কিন্তু এটি ব্যক্তিগত বার্তায় না দিয়ে তা টুইট করে বসেন মাস্ক, বলা হয়েছে টেলিগ্রাফ-এর প্রতিবেদনে।
টুইটারে ১৬০৭০০০০ অনুসারী রয়েছে মাস্ক-এর। মাস্ক-এর নাম্বার পেয়ে তাকে কল করতে শুরু করেন অনেক অনুসারী।
টুইটার থেকে পরবর্তীতে তার টুইটটি মুছে ফেলা হয়। আর তার নাম্বারটিও গড অফ ওয়ার গেইমের একটি রেকর্ড করা বার্তায় রিডেরেক্ট করে দেওয়া হয়।
রেকর্ড বার্তায় বলা হয়, “সৃষ্টিকর্তার মাধ্যমে আপনি এটি করতে পেরেছেন। কোনোভাবে আপনি আমার এখানে আসার পথ পেয়েছেন। আমি আপনাদেরকে অভিনন্দন ও সম্মান জানাচ্ছি।”
চলতি বছরের শুরুতে মাস্ক ড্রোনের নীতিমালা জোরদার করার বিষয়ে নজর দেওয়ার আহ্বান জানান। আর মেশিন যে মানব সভ্যতার জন্য ‘অস্তিত্বগত ঝুঁকি’ হতে পারে সে বিষয়টিতেও জোর দিয়েছেন। মাস্ক এর আগে অনেকবারই উল্লেখ করেছেন যে রোবট মানুষের চাকুরি কমাবে।
