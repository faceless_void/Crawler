ঢাকা: বাংলাদেশ ইউনিভার্সিটি অব বিজনেস অ্যান্ড টেকনোলজিতে (বিইউবিটিতে) অনুষ্ঠিত হলো ব্যবসায় শিক্ষা বিষয়ক দিনব্যাপী কর্মশালা। 
শনিবার (১৯ আগস্ট) ‘কোয়ালিটি অ্যাস্যুরেন্স ইন টারশিয়ারি লেভেল বিজনেস এডুকেশন অ্যান্ড টিম বিল্ডিং ফর সেল্ফ অ্যাসেসমেন্ট’ শীর্ষক এ কর্মশালার আয়োজন করে বিইউবিটি’র ব্যবসায় প্রশাসন বিভাগ। এতে অংশ নেন ব্যবসায় প্রশাসন বিভাগের শিক্ষকরা। 
কর্মশালা পরিচালনা ও মূল প্রবন্ধ উপস্থাপন করেন বিশ্ববিদ্যালয় মঞ্জুরি কমিশনের (ইউজিসি) প্রাক্তন সদস্য অধ্যাপক ড. মুহিবুর রহমান। 
কর্মশালার উদ্বোধনী পর্বে সভাপতিত্ব করেন বিইউবিটি’র উপাচার্য অধ্যাপক আবু সালেহ। বিশেষ অতিথি ছিলেন-- বিইউবিটি’র ট্রেজারার, অধ্যাপক মো. এনায়েত হোসেন মিয়া ও প্রক্টর প্রফেসর মিঞা লুৎফার রহমান। আরও উপস্থিত ছিলেন বিইউবিটি’র ব্যবসায় অনুষদের ডিন অধ্যাপক ড. সৈয়দ মাসুদ হুসেন। 
