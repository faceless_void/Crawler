ত্বক কালো হয়ে যাওয়া, কালো ছোপ পড়া, ত্বকের রংয়ের অসমতা ইত্যাদি সমস্যাগুলোতে কমবেশি সবাই ভুগছেন। বিশেষজ্ঞদের মতে, সমস্যা সমাধানে আলু ও পেঁপে অনেক উপকারী। কারণ এতে থাকে ত্বকের কোষ পুণর্গঠনকারী এবং স্কিন এক্সফোলিয়েশন উপাদান।
ত্বকের এসব সমস্যা সমাধানের উপায় জানিয়েছেন ভারতের সোহাম ওয়েলনেস ক্লিনিকের কর্ণধার ও রূপবিশেষজ্ঞ দিভিয়া অরি।
আলু


ব্যবহার পদ্ধতি: আলু পাতলা করে কেটে টুকরোগুলো নিয়ে ১০ মিনিট ধরে ত্বকে আলতোভাবে ঘষতে হবে। কাটা টুকরোগুলো শুকিরে গেলে পরিবর্তন করে নিতে হবে।
লেবু


ব্যবহার পদ্ধতি: এক টেবিল-চামচ বাদামি চিনির সঙ্গে ডিমের সাদা অংশ মিশিয়ে নিন। এত যোগ করুন এক টেবিল-চামচ লেবুর রস। এই মিশ্রণ ত্বকে আলতো মাখিয়ে নিয়ে ১০ থেকে ১৫ মিনিট রেখে ধুয়ে ফেলুন।
পেঁপে


