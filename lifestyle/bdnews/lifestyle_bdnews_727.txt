নিউ দিল্লির ‘সাউথএক্স ডেন্টাল’য়ের দন্ত্য চিকিৎসক সৌরভ গুপ্তা দাঁত ভালো রাখার আরও কিছু উপায় জানিয়েছেন।
ডেন্টাল ফ্লস বা ইন্টারডেন্টাল ব্রাশ: দিনে একবার বিশেষ করে রাতে ধীরে ধীরে ‘ফ্লস’ করুন বা দাঁতের ফাঁকে জমে থাকা খাদ্যকণা পরিষ্কার করুন।
ফ্লোরাইড-যুক্ত টুথপেস্ট: গর্ত তৈরি হওয়া রোধ করতে এবং দাঁতের এনামেল সবল রাখতে ফ্লোরাইডযুক্ত টুথপেস্ট বেশি কার্যকর।
অ্যাসিডিক পানীয়: বিভিন্ন ধরনের কোমল পানীয়, প্যাকেটজাত ফলের রস, মিষ্টিজাতীয় খাবার যত কম খাওয়া যায় ততই দাঁতের জন্য মঙ্গল।
স্কেলিং: ডাক্তারের পরামর্শ নিয়ে ছয় মাস অন্তর দাঁত পরিষ্কার বা স্কেলিং করানো উচিত। এতে মাঢ়ী শক্ত ও মজবুত হবে। পাশাপাশি দাঁতে অন্য কোনো সমস্যা থাকলে, আগেভাগেই জানা যাবে।
