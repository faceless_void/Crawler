জমকালো সাজের মাঝেও নিজের স্বাভাবিক লাবণ্য বজায় রাখার কৌশল জানাচ্ছেন নভিন’স অ্যারোমা থেরাপি সেন্টারের রূপবিশেষজ্ঞ আমিনা হক।
বেইজ মেইকআপের ক্ষেত্রে ভারী ফাউন্ডেশন যতটা সম্ভব এড়িয়ে চলুন। দিনের বেলা ম্যাট ফাউন্ডেশন দিয়ে বেইজ করা ভালো।
ত্বক তৈলাক্ত হলে প্রথমে সারা মুখে লুজ পাউডার লাগিয়ে তারপর ফাউন্ডেশন লাগাতে হবে। এতে মেইকআপ দীর্ঘস্থায়ী হবে। ত্বক যা-ই হোক, কমপ্যাক্ট পাউডার দিয়ে বেইজ মেইকআপ শেষ করা ভালো।
রাতের সাজে তৈলাক্ত ত্বকে দিন ‘লিকুইড ফাউন্ডেশন’। আর শুষ্ক ত্বকে ‘ক্রিম ফাউন্ডেশন’। তারপর প্যানকেক।
জমকালো আর ভারী সাজেই কেবল প্যানকেক মানানসই। সাধারণত হলুদ আর গোলাপি এই দু’টি রংয়ের প্যানকেক ব্যবহৃত হয়। প্রথমে হলুদ প্যানকেক দিয়ে তারপর ত্বকের রংয়ের সঙ্গে মিলিয়ে গোলাপি শেইডের প্যানকেক দিন।
সব ক্ষেত্রেই মেইকআপ ব্লেন্ডিং খুব জরুরি। ত্বকের সঙ্গে বেইজ যত ভালোভাবে মিশে যাবে ততই ‘ন্যাচারাল লুক’ আসবে। তারপর কমপ্যাক্ট পাউডার দিন। রাতের জমকালো অনুষ্ঠানে চাইলে ‘শিমার পাউডার’ ব্যবহার করতে পারেন।
নাক একটু টিকালো দেখানোর জন্য নাকের দু’পাশে গাঢ় শেইডের কনসিলার দিয়ে উপরে লম্বা করে হালকা শেইডের কনসিলার দিন। একইভাবে চোয়ালের শেইপ ঠিক করে নিন। কনসিলার ছাড়াও ব্রোঞ্জিং পাউডার দিয়েও কনট্যুর ও হাইলাইট করা যায়।


চাইলে চোখও হাইলাইট করতে পারেন। সে ক্ষেত্রে ঠোঁটের জন্য ‘ন্যাচারাল স্কিন কালার’ মানানসই।
মোদ্দা কথা হল হালকা বা ভারী— সাজ যা-ই হোক, চোখ আর ঠোঁটের সাজ হবে বিপরীত।
চোখের সাজে লাইনার, মাসকারা আর শ্যাডো ব্যবহার করা হয়। দিনের সাজে তিনটি একসঙ্গে ব্যবহার না করে যে কোনো দু’টি বা একটিতে সীমাবদ্ধ রাখা যেতে পারে। কাজল ও আইলাইনারের চেয়ে মাসকারা প্রাধান্য দিন।
চোখের পাতা ঘন দেখালে আরও আকর্ষণীয় লাগবে চোখ। সেজন্য কাজল বা আইলাইনার টেনে দিতে পারেন। তিনটি একসঙ্গে কেবল রাতের সাজেই চলতে পারে।
ঠোঁটে ম্যাট লিপস্টিক ব্যবহার করুন।
ব্লাশন সব সময়ই হালকা হলে ভালো লাগে। এমনকি রাতের পার্টিতেও এখন গাঢ় ব্লাশনের চল নেই।
মনে রাখুন
* মেইকআপ তোলা বা চটজলদি রিটাচ করার জন্য ক্লিনজিং মিল্ক রাখুন।
* ত্বকের ধরন বুঝে নির্বাচন করুন কসমেটিকস।
* আবহাওয়ার সঙ্গে বদলে ফেলুন মেইকআপ কিটের সরঞ্জাম।
* কেনার সময় অবশ্যই ভালো ব্র্যান্ড দেখে কিনুন।
* একই সঙ্গে পণ্যের গুণগত মান ও মেয়াদ দেখে নিন।
* মেয়াদ শেষ হওয়ার পর নামি কসমেটিকসও ত্বকের জন্য ক্ষতিকর।
 
