ইবি: ইসলামী বিশ্ববিদ্যালয়ের ২০১৭-১৮ শিক্ষাবর্ষের সম্মান প্রথমবর্ষের ভর্তি পরীক্ষার আইন ও শরীয়াহ অনুষদভুক্ত ‘এইচ’ ইউনিটের ফল প্রকাশ করা হয়েছে।
সোমবার (০৪ ডিসেম্বর) রাত সাড়ে আটটায় উপাচার্য অধ্যাপক ড. রাশিদ আসকারী বরাবর  'এইচ' ইউনিট সমন্বয়কারী অধ্যাপক ড. নুরুন্নাহার ফলাফল হস্তান্তর করেন।
প্রকাশিত ফলাফলে উক্ত ইউনিটের পাশের হার ৪২ শতাংশ বলে জানা গেছে। সে হিসেবে ভর্তি পরীক্ষায় অংশগ্রহণকারী শিক্ষার্থীদের মধ্যে মোট ৩ হাজার ৮৯৬ জন পরীক্ষার্থী পাশ করেছে। আগামী ১২ ডিসেম্বর মেধা তালিকায় স্থানপ্রাপ্ত শিক্ষার্থীদের সাক্ষা‍ৎকার অনুষ্ঠিত হবে।
এসময় সেখানে উপস্থিত ছিলেন উপ-উপাচার্য অধ্যাপক ড. মো. শাহিনুর রহমান, কোষাধ্যক্ষ অধ্যাপক ড. সেলিম তোহা, এবং ‘এইচ’ ইউনিটের সদস্য অধ্যাপক ড. জহুরুল ইসলাম, অধ্যাপক ড. নুরুল ইসলাম, সহযোগী অধ্যাপক আরমিন খাতুন প্রমুখ।
রোববার (৩ ডিসেম্বর) দুপুর দেড়টা থেকে আড়াইটা এবং বিকেল সাড়ে তিনটা থেকে সাড়ে চারটা পর্যন্ত দুই শিফটে 'এইচ' ইউনিটের ভর্তি পরীক্ষা অনুষ্ঠিত হয়। এ বছর ‘এইচ’ ইউনিটে ২৪০ আসনের বিপরীতে ৯ হাজার ৮৯৫ জন শিক্ষার্থী ভর্তি পরীক্ষায় অংশগ্রহণ করেন।
ভর্তি পরীক্ষার ফল ও সাক্ষাৎকারসহ বিস্তারিত বিশ্ববিদ্যালয়ের নিজস্ব ওয়েবসাইট www.iu.ac.bd থেকে পাওয়া যাবে।
