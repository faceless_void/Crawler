এ ঈদে হয়তো আপনার ফ্রিজ কেনা হল না। তবে মাংস তো সংরক্ষণ করতে হবে। সংরক্ষণের সঠিক নিয়ম বাতলিয়েছেন গৃহিনী তানজিন চৌধুরী।
মাংস সংরক্ষণের পদ্ধতি
১. মাংস কেটে ভালোভাবে পরিষ্কার করে নিন। এরপর আদা বাটা, রসুন বাটা, পেঁয়াজ বাটা বেশি করে দিয়ে ডুবানো তেলে মাংস ছেড়ে দিন। ভালোভাবে সেদ্ধ করুন, যাতে মসলা মাংসের ভেতরে ঢোকে। তবে খেয়াল রাখবেন, মাংস যাতে নরম হয়ে না যায়। একদিন অন্তর মাংসগুলো গরম করুন। এতে ১৫-২০ দিন পর্যন্ত ভালো থাকবে।
২. মাংস লম্বা লম্বা করে টুকরো করুন। এরপর লবণ, হলুদ মেখে রোদে শুকিয়ে নিন। এটি তেলে ভেজেও খেতে পারেন। তবে রান্নার আগে মাংসগুলো ভিজিয়ে নিন।
৩. এ পদ্ধতিতে মাংসের টুকরোগুলো কাঁটাচামচ বা ছুরি দিয়ে কেঁচে নিন। এরপর লবণ ও লেবুর রসে ডুবিয়ে নিন, যাতে ভালোভাবে এ মিশ্রণ মাংসের ভেতরে ঢোকে। এভাবে রাখলেও মাংস অনেক দিন ভালো থাকে।
এ তো গেল ফ্রিজ না থাকলে কী করবেন তার সমাধান। ফ্রিজ থাকলেও অনেকে সঠিকভাবে মাংস রাখতে পারেন না। ফলে জায়গার সংকুলান হয় না, সে ক্ষেত্রে আলাদা করে ডিপ ফ্রিজ কেনার কথা ভাবতে হয়। সাধ থাকলেও সামর্থ্য থাকে না। তাই তো এই ফ্রিজের যত্নটাই প্রথমে নেওয়া উচিত বলে মনে করেন গার্হস্থ অর্থনীতি কলেজের সাবেক অধ্যক্ষ ফিরোজা সুলতানা।
ফ্রিজের যত্ন
ঈদের আগে সময় করে ফ্রিজ পরিষ্কার করে ফেলা উচিত। ফ্রিজ বন্ধ করে তরল সাবান দিয়ে ভালোভাবে পরিষ্কার করে শুকিয়ে নিন। এরপর দরজা খুলে রাখুন যাতে ভ্যাপসা গন্ধ দূর হয়ে যায়। আবার ফ্রিজ চালু করে দিন, যাতে সংরক্ষণের উপযোগী শীতল হয়। পরিষ্কার পাত্রে খাবার রাখুন।
ফ্রিজে মাংস সংরক্ষণ
যদি মাংস পরিমাণে বেশি হওয়ার সম্ভাবনা থাকে, তাহলে মাছ-মুরগিসহ অন্যান্য খাবার আগেই সরিয়ে ফেলুন। সবাইকে মাংস বিলিয়ে দেওয়ার পর বাকিগুলো আলাদা করুন। ঠিক করুন কতটুকু করে রান্না করবেন। সেভাবেই ছোট ছোট মোড়কে মুড়িয়ে ফ্রিজে রাখুন। এ ছাড়া বড় আকারের মাংস না কেটে ভালোভাবে পলিথিনে মুড়িয় ফ্রিজে রাখুন। তবে খেয়াল রাখবেন, ব্যবহৃত পলিথিনটি যেন যেখানে-সেখানে না খোলা হয়। মাংসগুলো বেটে কিমা করেও রাখতে পারেন। এতে জায়গা কম লাগবে। আত্মীয়স্বজন ও পাড়া-প্রতিবেশীর কাছ থেকে পাওয়া মাংসগুলো একসঙ্গে রান্না করুন। স্বাদটাও ভিন্ন হবে।
রান্নাঘরের যত্নে
এ ঈদে রান্নাঘরের প্রতি বাড়তি যত্নশীল হতে হয়। খেয়াল রাখুন, মাংসের রক্ত ও চর্বি যেন মেঝে বা তাকে লেগে না থাকে। এসব থেকেই দুর্গন্ধ ছড়ায়। মাংস কাটা ও রাখার স্থান তরল সাবান ও জীবাণুনাশক দিয়ে পরিষ্কার করে ফেলুন। এরপর এয়ার ফ্রেশনার ব্যবহার করুন, যাতে ভ্যাপসা গন্ধ দূর হয়।
গোছানো রসুই ঘর
সাজানো-গোছানো হেঁশেলে কাজ করতে সবারই ভালো লাগে। নাগালের মধ্যে জিনিস পেলে অল্প সময়ে কাজ করা যায়। কোরবানির ঈদে রান্নাঘরের কাজই বেশি। সে জন্য ঈদের আগেই এটি গুছিয়ে ফেলুন। যেসব জিনিসের ব্যবহার বেশি হবে, তা হাতের কাছেই রাখুন। মসলার বাক্সগুলো পরিষ্কার করে ফেলুন। বড় হাঁড়ি-পাতিল, সসপ্যানগুলো ওপরের তাক থেকে নামিয়ে পরিষ্কার করে রাখুন। প্রেসার কুকারটি ঠিক আছে কি না পরীক্ষা করে নিন। চাপাতি, বঁটি, ছুরি, শিলপাটায় আগেই ধার দিয়ে নিন। আগত অতিথির সম্ভাব্য সংখ্যা ভেবে রাখুন। সে অনুযায়ী তৈজসপত্র পরিষ্কার করে রাখুন। পরে তাড়াহুড়োয় ভেঙে যাওয়ার আশঙ্কা থাকে। রন্ধনপ্রণালি এখনই ঠিক করে ফেলুন। এরপর তালিকা অনুযায়ী জিনিসপত্র কিনে রান্নাঘরে গুছিয়ে রাখুন। দেখবেন, সব কাজ কত সহজেই হয়ে যাবে। ঝক্কি-ঝামেলা ছাড়াই কেটে যাবে ঈদের দিনটি। আপনার মুখের হাসি অন্যদের মনেও প্রশান্তি বয়ে আনবে।
লক্ষ রাখবেন
গ্যাসের চুলা ও ফ্রিজের কোনো সমস্যা আছে কি না তা এখনই পরীক্ষা করে নিন।
