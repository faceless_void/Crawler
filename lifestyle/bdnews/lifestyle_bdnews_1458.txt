এ বিষয়ে কথা বলেন, ফাস্ট কেয়ার হসপিটালের মেডিসিন বিশেষজ্ঞ ডা. মোহাম্মদ কামরুল হাসান।
গরমে অতিরিক্ত তৈলাক্ত ও ভারি খাবার শরীরের জন্য ক্ষতিকর।
ডা. হাসান বলেন, “রমজানের একমাস ভোর রাতে সেহেরি খাওয়ার পর দীর্ঘ সময় রোজা রাখা হয়। একবারে খাওয়া হয় সন্ধ্যায়, ইফতারে। এই একমাসে শরীর এই নিয়মের সঙ্গে অভ্যস্থ হয়ে যায়। তাই ঈদের সকালে হুট করে এই নিয়ম ভাঙলে কিছুটা অস্বস্তি অনুভূত হতেই পারে।”
তাই ঈদের সকালে নাস্তায় প্রথমেই ভারি খাবার এড়িয়ে চলার পরামর্শ দেন তিনি। তাছাড়া এদিন প্রথমেই একসঙ্গে বেশি খাবার খাওয়াও উচিত নয়, এমনটাই জানান তিনি।
তিনি বলেন, “এক মাসের নিয়ম ভেঙে আবার আগের নিয়মের সঙ্গে অভ্যস্থ হতে শরীরের কিছু সময় লাগে। তাই ঈদের দিন শুরু করা উচিত হালকা নাস্তা খেয়ে। তাছাড়া গরমের কারণে প্রচুর পানি পান করতে হবে। আর একসঙ্গে বেশি খাবার না খেয়ে কিছুক্ষণ পর পর খেলে ভালো।”
তাছাড়া শারীরিক অবস্থা বুঝেও খাবার খাওয়ার পরামর্শ দেন ডা. কামরুল। যাদের ডায়বেটিস, প্রেশার বা হার্টের সমস্যা আছে তাদের খাবার খাওয়ার ক্ষেত্রে বিশেষভাবে সচেতন হতে হবে।
ডা. হাসান বলেন, “ঈদে হঠাৎ করে এক সঙ্গে বেশি ভারি খাবার খেয়ে ফেলার কারণে বমি ভাব, হজমে সমস্যা, গ্যাস্ট্রিক ইত্যাদি সমস্যা দেখা দেয়। তাছাড়া এবার ঈদ গরমে, তাই পেটের সমস্যা হওয়ার সম্ভাবনাও বেশি। এ কারণে সচেতনভাবে খাওয়া উচিত।”
ঈদে একই সঙ্গে বেশি না খেয়ে, কিছুক্ষণ পরপর খাওয়া যেতে পারে। এতে শারীরিক সমস্যা হওয়ার সম্ভাবনা কম থাকে।
ঈদের দিন গরম এবং রোদ বেশি থাকলে রোদে বেশি ঘোরাঘুরি না করার পরামর্শ দেন ডা. কামরুল। আর বাইরে বের হলেও হালকা ধরনের পোশাক, যেমন সুতি, লিলেন ইত্যাদি ধরনের পাতলা কাপড়ের তৈরি পোশাক পরা উচিত বলে জানান তিনি।
অতিরিক্ত ঘাম হলে এবং গরমে অস্বস্তি লাগলে প্রচুর পানিজাতীয় খাবার খাওয়া উচিত বলে জানান তিনি।
